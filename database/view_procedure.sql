/*
Navicat MySQL Data Transfer

Source Server         : local
Source Server Version : 50624
Source Host           : ::1:3306
Source Database       : sikopat

Target Server Type    : MYSQL
Target Server Version : 50624
File Encoding         : 65001

Date: 2017-02-09 03:00:14
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- View structure for count_all_tunggakan
-- ----------------------------
DROP VIEW IF EXISTS `count_all_tunggakan`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER  VIEW `count_all_tunggakan` AS SELECT
(SELECT
Count(A.id)
from 
simpanan_wajib as A
INNER JOIN anggota as B on A.id_anggota = B.id
WHERE 
-- A.periode <= MONTH(now()) AND A.tahun <= YEAR(now()) AND 
A.status_pembayaran = 0) as simpanan_wajib
,
(SElect count(A.id) from pembayaran as A
WHERE A.tanggal_batas <= now() and A.`status` = 0) as peminjaman ; ;

-- ----------------------------
-- View structure for get_count_anggota_tunggakan
-- ----------------------------
DROP VIEW IF EXISTS `get_count_anggota_tunggakan`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER  VIEW `get_count_anggota_tunggakan` AS (SELECT
Count(A.id) simpanan_wajib, 0 as peminjaman, B.id as id_anggota
from 
simpanan_wajib as A
INNER JOIN anggota as B on A.id_anggota = B.id
WHERE 
-- A.periode <= MONTH(now()) AND A.tahun <= YEAR(now()) AND 
A.status_pembayaran = 0 group by B.id)
union
(SElect 0 as simpanan_wajib, count(A.id) as peminjaman, BB.id_anggota from pembayaran as A
INNER JOIN peminjaman as BB on A.id_peminjaman = BB.id
WHERE 
-- A.tanggal_batas <= now() and 
A.`status` = 0 GROUP BY BB.id_anggota) ; ;

-- ----------------------------
-- View structure for v_daftar_akses_anggota
-- ----------------------------
DROP VIEW IF EXISTS `v_daftar_akses_anggota`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost`  VIEW `v_daftar_akses_anggota` AS SELECT * FROM anggota
INNER JOIN hak_akses_anggota on anggota.id = hak_akses_anggota.id_anggota
GROUP BY anggota.id ; ;

-- ----------------------------
-- View structure for v_get_access
-- ----------------------------
DROP VIEW IF EXISTS `v_get_access`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER  VIEW `v_get_access` AS SELECT
B.id_anggota,
C.nama,
B.id_hak_akses,
A.hak_akses
FROM
hak_akses AS A
INNER JOIN hak_akses_anggota AS B ON B.id_hak_akses = A.id
INNER JOIN anggota AS C ON B.id_anggota = C.id ; ;

-- ----------------------------
-- View structure for v_get_daftar_shu_anggota
-- ----------------------------
DROP VIEW IF EXISTS `v_get_daftar_shu_anggota`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost`  VIEW `v_get_daftar_shu_anggota` AS SELECT A.*, B.periode, B.tahun, C.nama
FROM
detail_shu AS A
INNER JOIN shu AS B ON A.id_shu = B.id
INNER JOIN anggota AS C ON A.id_anggota = C.id ; ;

-- ----------------------------
-- View structure for v_get_detail_shu
-- ----------------------------
DROP VIEW IF EXISTS `v_get_detail_shu`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER  VIEW `v_get_detail_shu` AS SELECT
detail_shu.id_shu,
detail_shu.id,
shu.periode,
shu.tahun,
detail_shu.id_anggota,
anggota.nama,
detail_shu.anggota,
detail_shu.peminjam,
detail_shu.pengurus,
detail_shu.pengawas,
detail_shu.jumlah
FROM
detail_shu
INNER JOIN shu ON detail_shu.id_shu = shu.id
INNER JOIN anggota ON detail_shu.id_anggota = anggota.id ; ;

-- ----------------------------
-- View structure for v_get_tunggakan
-- ----------------------------
DROP VIEW IF EXISTS `v_get_tunggakan`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER  VIEW `v_get_tunggakan` AS SELECT * from pembayaran WHERE tanggal_batas <= now() and `status` = 0 ; ;

-- ----------------------------
-- View structure for v_get_tunggakan_pembayaran
-- ----------------------------
DROP VIEW IF EXISTS `v_get_tunggakan_pembayaran`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER  VIEW `v_get_tunggakan_pembayaran` AS SELECT
A.id,
A.id_peminjaman,
B.no_pinjaman,
B.id_anggota,
C.nama as nama_anggota,
A.tanggal_bayar,
A.tanggal_batas,
A.jumlah_bayar,
A.bunga,
A.pembayaran_ke,
A.keterangan,
A.`status`
from 
pembayaran as A
INNER JOIN peminjaman as B on A.id_peminjaman = B.id
INNER JOIN anggota as C on B.id_anggota = C.id
WHERE A.tanggal_batas <= now() and A.`status` = 0 ; ;

-- ----------------------------
-- View structure for v_grafik_simpanan_wajib
-- ----------------------------
DROP VIEW IF EXISTS `v_grafik_simpanan_wajib`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost`  VIEW `v_grafik_simpanan_wajib` AS SELECT
  tahun as Tahun,
  Count(case when periode=1 then id end) As Januari,
  Count(case when periode=1 and tgl_bayar is not null then id end) As JanuariBayar,
  Count(case when periode=2 then id end) As Februari,
  Count(case when periode=2 and tgl_bayar is not null then id end) As FebruariBayar,
  Count(case when periode=3 then id end) As Maret,
  Count(case when periode=3 and tgl_bayar is not null then id end) As MaretBayar,
  Count(case when periode=4 then id end) As April,
  Count(case when periode=4 and tgl_bayar is not null then id end) As AprilBayar,
  Count(case when periode=5 then id end) As Mei,
  Count(case when periode=5 and tgl_bayar is not null then id end) As MeiBayar,
  Count(case when periode=6 then id end) As Juni,
  Count(case when periode=6 and tgl_bayar is not null then id end) As JuniBayar,
  Count(case when periode=7 then id end) As Juli,
  Count(case when periode=7 and tgl_bayar is not null then id end) As Bayar,
  Count(case when periode=8 then id end) As Agustus,
  Count(case when periode=8 and tgl_bayar is not null then id end) As AgustusBayar,
  Count(case when periode=9 then id end) As September,
  Count(case when periode=9 and tgl_bayar is not null then id end) As SeptemberBayar,
  Count(case when periode=10 then id end) As Oktober,
  Count(case when periode=10 and tgl_bayar is not null then id end) As OktoberBayar,
  Count(case when periode=11 then id end) As Nopember,
  Count(case when periode=11 and tgl_bayar is not null then id end) As NopemberBayar,
  Count(case when periode=12 then id end) As Desember,
  Count(case when periode=12 and tgl_bayar is not null then id end) As DesemberBayar
FROM simpanan_wajib
GROUP BY tahun ;

-- ----------------------------
-- View structure for v_grafik_tunggakan_bulanan
-- ----------------------------
DROP VIEW IF EXISTS `v_grafik_tunggakan_bulanan`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER  VIEW `v_grafik_tunggakan_bulanan` AS SELECT  
	MONTH(tanggal_batas) as Bulan, 
	YEAR(tanggal_batas) as Tahun,	
	Count(id) as Total,
  Count(case when `status`=1 then id end) As Membayar,
  Count(case when `status`=0 then id end) As Menunggak
FROM pembayaran
GROUP BY YEAR(tanggal_batas), MONTH(tanggal_batas) ; ;

-- ----------------------------
-- View structure for v_grafikpeminjaman
-- ----------------------------
DROP VIEW IF EXISTS `v_grafikpeminjaman`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost`  VIEW `v_grafikpeminjaman` AS SELECT
  Year(tgl_pinjaman) as Tahun,
  Count(case when month(`tgl_pinjaman`)=1 then id end) As Januari,
  Count(case when month(`tgl_pinjaman`)=2 then id end) As Februari,
  Count(case when month(`tgl_pinjaman`)=3 then id end) As Maret,
  Count(case when month(`tgl_pinjaman`)=4 then id end) As April,
  Count(case when month(`tgl_pinjaman`)=5 then id end) As Mei,
  Count(case when month(`tgl_pinjaman`)=6 then id end) As Juni,
  Count(case when month(`tgl_pinjaman`)=7 then id end) As Juli,
  Count(case when month(`tgl_pinjaman`)=8 then id end) As Agustus,
  Count(case when month(`tgl_pinjaman`)=9 then id end) As September,
  Count(case when month(`tgl_pinjaman`)=10 then id end) As Oktober,
  Count(case when month(`tgl_pinjaman`)=11 then id end) As Nopember,
  Count(case when month(`tgl_pinjaman`)=12 then id end) As Desember
FROM peminjaman
GROUP BY Year(`tgl_pinjaman`) ; ;

-- ----------------------------
-- View structure for v_list_total_tunggakan
-- ----------------------------
DROP VIEW IF EXISTS `v_list_total_tunggakan`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost`  VIEW `v_list_total_tunggakan` AS SELECT 
	id_anggota,
	no_pinjaman,
	count(`status`) kali_tunggakan, 
	sum(jumlah_bayar) total_jml_bayar, 
	sum(bunga) total_jml_bunga, 
	(sum(jumlah_bayar)+sum(bunga)) total
	FROM v_get_tunggakan_pembayaran 
	GROUP BY id_anggota ; ;

-- ----------------------------
-- View structure for v_pinjaman_aktif
-- ----------------------------
DROP VIEW IF EXISTS `v_pinjaman_aktif`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost`  VIEW `v_pinjaman_aktif` AS SELECT * from peminjaman WHERE `status` = 0 ; ;

-- ----------------------------
-- View structure for v_status_pinjaman
-- ----------------------------
DROP VIEW IF EXISTS `v_status_pinjaman`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost`  VIEW `v_status_pinjaman` AS SELECT
A.id_peminjaman,
B.no_pinjaman,
B.tgl_pinjaman,
B.id_anggota,
C.nama,
B.total_pinjaman,
sum(CASE WHEN A.`status` = 0 THEN 1 ELSE 0 end) as jml_belum_bayar,
sum(CASE WHEN A.`status` = 1 THEN 1 ELSE 0 end) as jml_sudah_bayar,
sum(CASE WHEN A.`status` = 0 THEN A.jumlah_bayar + A.bunga ELSE 0 end) as total_belum_bayar,
sum(CASE WHEN A.`status` = 1 THEN A.jumlah_bayar + A.bunga ELSE 0 end) as total_sudah_bayar,
(CASE WHEN B.`status` = 1 THEN 'Lunas' ELSE 'Belum Lunas' end) as `status`
from pembayaran AS A
INNER JOIN peminjaman AS B on A.id_peminjaman = B.id
INNER JOIN anggota AS C on B.id_anggota = C.id
GROUP BY A.id_peminjaman ; ;

-- ----------------------------
-- Procedure structure for generate_pembayaran
-- ----------------------------
DROP PROCEDURE IF EXISTS `generate_pembayaran`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `generate_pembayaran`(`id_pinjaman` int,`jumlah_pinjaman` decimal,`bunga` decimal,`kali_bayar` int, tgl_pinjaman date)
BEGIN
	declare xx int;	
	declare split_jml_pinjaman decimal(15,2);
	declare split_bunga decimal(15,2);
	
	set xx = 1;
	set split_jml_pinjaman = jumlah_pinjaman / kali_bayar;
	set split_bunga = bunga;
	
	while xx <=kali_bayar DO
		-- set tgl_batas = SELECT now() + INTERVAL xx MONTH;
		INSERT INTO pembayaran(id_peminjaman,jumlah_bayar,bunga,pembayaran_ke, tanggal_batas)VALUES(id_pinjaman,split_jml_pinjaman,split_bunga,xx,(SELECT tgl_pinjaman + INTERVAL xx MONTH));
		SET xx= xx+1;
	end while;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for p_generateSimpananW
-- ----------------------------
DROP PROCEDURE IF EXISTS `p_generateSimpananW`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `p_generateSimpananW`()
BEGIN
	DECLARE jumlahGen INT;
	declare jumlah_simpanan decimal(15,2);
	
	SELECT count(*) INTO jumlahGen from simpanan_wajib where periode = MONTH(NOW()) and tahun = YEAR(now());
	
	if(jumlahGen < 1) then
		select jumlah into jumlah_simpanan from jenis_simpanan where kode_simpanan = "wajib" limit 1;
		INSERT INTO simpanan_wajib(id_anggota,jumlah_bayar,status_pembayaran,periode,tahun)
		SELECT id,jumlah_simpanan,0,MONTH(NOW()),YEAR(now()) FROM anggota;
	end if;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for p_get_total_tunggakan_per_pelanggan
-- ----------------------------
DROP PROCEDURE IF EXISTS `p_get_total_tunggakan_per_pelanggan`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `p_get_total_tunggakan_per_pelanggan`(`anggota_id` int)
BEGIN
	SELECT 
	count(`status`) kali_tunggakan, 
	sum(jumlah_bayar) total_jml_bayar, 
	sum(bunga) total_jml_bunga, 
	(sum(jumlah_bayar)+sum(bunga)) total
 
	FROM v_get_tunggakan_pembayaran 
	WHERE id_anggota = anggota_id
	GROUP BY id_peminjaman;

END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for p_trans_rutin_del
-- ----------------------------
DROP PROCEDURE IF EXISTS `p_trans_rutin_del`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `p_trans_rutin_del`(`id_trans` int(11),`jns` varchar(255))
BEGIN
	DELETE FROM transaksi_rutin WHERE id = id_trans AND transaksi = jns;

END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for p_trans_rutin_in
-- ----------------------------
DROP PROCEDURE IF EXISTS `p_trans_rutin_in`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `p_trans_rutin_in`(`id_trans` int(11),`jenis_trans` varchar(255),`tgl_trans` date,`v_masuk` decimal(15,2),`v_keluar` decimal(15,2),`ket` varchar(255))
BEGIN
	INSERT INTO transaksi_rutin(transaksi,id_transaksi,tanggal_transaksi,masuk,keluar,keterangan) 
	VALUES (jenis_trans,id_trans,tgl_trans,v_masuk, v_keluar, ket);

END
;;
DELIMITER ;

-- ----------------------------
-- Event structure for generateSimpananWajib
-- ----------------------------
DROP EVENT IF EXISTS `generateSimpananWajib`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` EVENT `generateSimpananWajib` ON SCHEDULE EVERY 1 MONTH STARTS '2017-03-02 01:06:46' ON COMPLETION NOT PRESERVE ENABLE DO call p_generateSimpananW()
;;
DELIMITER ;
