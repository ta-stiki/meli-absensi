/*
Navicat MySQL Data Transfer

Source Server         : local
Source Server Version : 50505
Source Host           : localhost:3306
Source Database       : sikopat

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2017-02-02 01:02:59
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for auto_number
-- ----------------------------
DROP TABLE IF EXISTS `auto_number`;
CREATE TABLE `auto_number` (
`group`  varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL ,
`number`  int(11) NULL DEFAULT NULL ,
`lock_p`  int(255) NULL DEFAULT NULL ,
`update`  int(11) NULL DEFAULT NULL 
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=latin1 COLLATE=latin1_swedish_ci

;

-- ----------------------------
-- Table structure for detail_shu
-- ----------------------------
DROP TABLE IF EXISTS `detail_shu`;
CREATE TABLE `detail_shu` (
`id`  int(11) NOT NULL AUTO_INCREMENT ,
`id_shu`  int(11) NULL DEFAULT NULL ,
`id_anggota`  int(11) NULL DEFAULT NULL ,
`anggota`  decimal(15,2) NULL DEFAULT NULL ,
`peminjam`  decimal(15,2) NULL DEFAULT NULL ,
`pengurus`  decimal(15,2) NULL DEFAULT NULL ,
`jumlah`  decimal(15,2) NULL DEFAULT NULL ,
PRIMARY KEY (`id`),
FOREIGN KEY (`id_anggota`) REFERENCES `anggota` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
FOREIGN KEY (`id_shu`) REFERENCES `shu` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
INDEX `fk_detail_shu_shu_1` (`id_shu`) USING BTREE ,
INDEX `fk_detail_shu_anggota_1` (`id_anggota`) USING BTREE 
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=latin1 COLLATE=latin1_swedish_ci
AUTO_INCREMENT=169

;

-- ----------------------------
-- Table structure for hak_akses
-- ----------------------------
DROP TABLE IF EXISTS `hak_akses`;
CREATE TABLE `hak_akses` (
`id`  int(11) NOT NULL AUTO_INCREMENT ,
`hak_akses`  varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL ,
PRIMARY KEY (`id`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=latin1 COLLATE=latin1_swedish_ci
AUTO_INCREMENT=6

;

-- ----------------------------
-- Table structure for hak_akses_anggota
-- ----------------------------
DROP TABLE IF EXISTS `hak_akses_anggota`;
CREATE TABLE `hak_akses_anggota` (
`id_anggota`  int(11) NULL DEFAULT NULL ,
`id_hak_akses`  int(11) NULL DEFAULT NULL ,
FOREIGN KEY (`id_anggota`) REFERENCES `anggota` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
FOREIGN KEY (`id_hak_akses`) REFERENCES `hak_akses` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
INDEX `fk_hak_akses_anggota_hak_akses_1` (`id_hak_akses`) USING BTREE ,
INDEX `fk_hak_akses_anggota_anggota_1` (`id_anggota`) USING BTREE 
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=latin1 COLLATE=latin1_swedish_ci

;

-- ----------------------------
-- Table structure for jabatan
-- ----------------------------
DROP TABLE IF EXISTS `jabatan`;
CREATE TABLE `jabatan` (
`id`  int(11) NOT NULL AUTO_INCREMENT ,
`jenis_jabatan`  varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL ,
`nama_jabatan`  varchar(15) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL ,
PRIMARY KEY (`id`),
FOREIGN KEY (`jenis_jabatan`) REFERENCES `persentase_shu` (`jenis_jabatan`) ON DELETE RESTRICT ON UPDATE RESTRICT,
INDEX `fk_jabatan_persentase_shu_1` (`jenis_jabatan`) USING BTREE 
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=latin1 COLLATE=latin1_swedish_ci
AUTO_INCREMENT=6

;

-- ----------------------------
-- Table structure for jenis_simpanan
-- ----------------------------
DROP TABLE IF EXISTS `jenis_simpanan`;
CREATE TABLE `jenis_simpanan` (
`id`  int(11) NOT NULL AUTO_INCREMENT ,
`kode_simpanan`  varchar(5) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL ,
`jenis_simpanan`  varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL ,
`jumlah`  decimal(15,2) NULL DEFAULT NULL ,
PRIMARY KEY (`id`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=latin1 COLLATE=latin1_swedish_ci
AUTO_INCREMENT=2

;

-- ----------------------------
-- Table structure for menu
-- ----------------------------
DROP TABLE IF EXISTS `menu`;
CREATE TABLE `menu` (
`id`  int(11) NOT NULL AUTO_INCREMENT ,
`name`  varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL ,
`link`  varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL ,
`icon`  varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL ,
`is_active`  int(1) NULL DEFAULT NULL ,
`is_parent`  int(1) NULL DEFAULT NULL ,
`akses`  varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL ,
PRIMARY KEY (`id`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=latin1 COLLATE=latin1_swedish_ci
AUTO_INCREMENT=15

;

-- ----------------------------
-- Table structure for pembayaran
-- ----------------------------
DROP TABLE IF EXISTS `pembayaran`;
CREATE TABLE `pembayaran` (
`id`  int(11) NOT NULL AUTO_INCREMENT ,
`id_peminjaman`  int(11) NULL DEFAULT NULL ,
`tanggal_bayar`  date NULL DEFAULT NULL ,
`tanggal_batas`  date NULL DEFAULT NULL ,
`jumlah_bayar`  decimal(15,2) NULL DEFAULT NULL ,
`bunga`  decimal(15,2) NULL DEFAULT NULL ,
`pembayaran_ke`  int(11) NULL DEFAULT NULL ,
`keterangan`  text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL ,
`status`  int(1) NULL DEFAULT 0 ,
`is_bayar_bunga`  int(1) NULL DEFAULT 0 ,
PRIMARY KEY (`id`),
FOREIGN KEY (`id_peminjaman`) REFERENCES `peminjaman` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
INDEX `fk_pembayaran_peminjaman_1` (`id_peminjaman`) USING BTREE 
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=latin1 COLLATE=latin1_swedish_ci
AUTO_INCREMENT=79

;

-- ----------------------------
-- Table structure for peminjaman
-- ----------------------------
DROP TABLE IF EXISTS `peminjaman`;
CREATE TABLE `peminjaman` (
`id`  int(11) NOT NULL AUTO_INCREMENT ,
`id_anggota`  int(11) NULL DEFAULT NULL ,
`no_pinjaman`  varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL ,
`tgl_pinjaman`  date NULL DEFAULT NULL ,
`jumlah_pinjaman`  decimal(15,2) NULL DEFAULT NULL ,
`bunga`  decimal(15,2) NULL DEFAULT NULL ,
`total_pinjaman`  decimal(15,2) NULL DEFAULT NULL COMMENT 'lunas\r\nbelum lunas' ,
`status`  int(1) NULL DEFAULT 0 ,
`input_oleh`  int(11) NULL DEFAULT NULL ,
`tgl_input`  datetime NULL DEFAULT NULL ,
`update_oleh`  int(11) NULL DEFAULT NULL ,
`tgl_update`  datetime NULL DEFAULT NULL ,
`sisa_pembayaran`  decimal(15,2) NULL DEFAULT NULL ,
`kali_bayar`  int(11) NULL DEFAULT NULL ,
`kali_bayar_bunga`  int(11) NULL DEFAULT NULL ,
`kali_tunggakan`  int(11) NULL DEFAULT NULL ,
PRIMARY KEY (`id`),
FOREIGN KEY (`id_anggota`) REFERENCES `anggota` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
INDEX `fk_peminjaman_anggota_1` (`id_anggota`) USING BTREE 
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=latin1 COLLATE=latin1_swedish_ci
AUTO_INCREMENT=6

;

-- ----------------------------
-- Table structure for pengaturan
-- ----------------------------
DROP TABLE IF EXISTS `pengaturan`;
CREATE TABLE `pengaturan` (
`key`  varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL ,
`value`  varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL ,
PRIMARY KEY (`key`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=latin1 COLLATE=latin1_swedish_ci

;

-- ----------------------------
-- Table structure for persentase_shu
-- ----------------------------
DROP TABLE IF EXISTS `persentase_shu`;
CREATE TABLE `persentase_shu` (
`jenis_jabatan`  varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL ,
`persentase_shu`  decimal(15,2) NULL DEFAULT NULL ,
PRIMARY KEY (`jenis_jabatan`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=latin1 COLLATE=latin1_swedish_ci

;

-- ----------------------------
-- Table structure for shu
-- ----------------------------
DROP TABLE IF EXISTS `shu`;
CREATE TABLE `shu` (
`id`  int(11) NOT NULL AUTO_INCREMENT ,
`periode`  int(4) NULL DEFAULT NULL ,
`tahun`  int(5) NULL DEFAULT NULL ,
`laba_tahunan`  double(15,2) NULL DEFAULT NULL ,
`tanggal_generate`  datetime NULL DEFAULT NULL ,
`generate_oleh`  int(11) NULL DEFAULT NULL ,
PRIMARY KEY (`id`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=latin1 COLLATE=latin1_swedish_ci
AUTO_INCREMENT=7

;

-- ----------------------------
-- Table structure for simpanan_wajib
-- ----------------------------
DROP TABLE IF EXISTS `simpanan_wajib`;
CREATE TABLE `simpanan_wajib` (
`id`  int(11) NOT NULL AUTO_INCREMENT ,
`id_anggota`  int(11) NULL DEFAULT NULL ,
`tgl_bayar`  date NULL DEFAULT NULL ,
`jumlah_bayar`  double(15,2) NULL DEFAULT NULL ,
`status_pembayaran`  int(1) NULL DEFAULT NULL ,
`periode`  int(11) NULL DEFAULT NULL ,
`tahun`  int(11) NULL DEFAULT NULL ,
PRIMARY KEY (`id`),
FOREIGN KEY (`id_anggota`) REFERENCES `anggota` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
INDEX `fk_simpanan_wajib_anggota_1` (`id_anggota`) USING BTREE 
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=latin1 COLLATE=latin1_swedish_ci
AUTO_INCREMENT=840

;

-- ----------------------------
-- Table structure for transaksi_rutin
-- ----------------------------
DROP TABLE IF EXISTS `transaksi_rutin`;
CREATE TABLE `transaksi_rutin` (
`id`  int(11) NOT NULL AUTO_INCREMENT ,
`transaksi`  varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL ,
`id_transaksi`  int(11) NULL DEFAULT NULL ,
`tanggal_transaksi`  date NULL DEFAULT NULL ,
`masuk`  decimal(15,2) NULL DEFAULT 0.00 ,
`keluar`  decimal(15,2) NULL DEFAULT 0.00 ,
`keterangan`  varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL ,
PRIMARY KEY (`id`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=latin1 COLLATE=latin1_swedish_ci
AUTO_INCREMENT=187

;

-- ----------------------------
-- Table structure for tunggakan
-- ----------------------------
DROP TABLE IF EXISTS `tunggakan`;
CREATE TABLE `tunggakan` (
`id_pinjaman`  int(11) NULL DEFAULT NULL ,
`jumlah_tunggakan`  decimal(15,2) NULL DEFAULT NULL ,
`tanggal`  date NULL DEFAULT NULL ,
`status`  int(11) NULL DEFAULT 0 ,
`tanggal_bayar`  date NULL DEFAULT NULL 
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=latin1 COLLATE=latin1_swedish_ci

;

-- ----------------------------
-- View structure for count_all_tunggakan
-- ----------------------------
DROP VIEW IF EXISTS `count_all_tunggakan`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER  VIEW `count_all_tunggakan` AS SELECT
(SELECT
Count(A.id)
from 
simpanan_wajib as A
INNER JOIN anggota as B on A.id_anggota = B.id
WHERE 
-- A.periode <= MONTH(now()) AND A.tahun <= YEAR(now()) AND 
A.status_pembayaran = 0) as simpanan_wajib
,
(SElect count(A.id) from pembayaran as A
WHERE A.tanggal_batas <= now() and A.`status` = 0) as peminjaman ;

-- ----------------------------
-- View structure for get_count_anggota_tunggakan
-- ----------------------------
DROP VIEW IF EXISTS `get_count_anggota_tunggakan`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER  VIEW `get_count_anggota_tunggakan` AS (SELECT
Count(A.id) simpanan_wajib, 0 as peminjaman, B.id as id_anggota
from 
simpanan_wajib as A
INNER JOIN anggota as B on A.id_anggota = B.id
WHERE 
-- A.periode <= MONTH(now()) AND A.tahun <= YEAR(now()) AND 
A.status_pembayaran = 0 group by B.id)
union
(SElect 0 as simpanan_wajib, count(A.id) as peminjaman, BB.id_anggota from pembayaran as A
INNER JOIN peminjaman as BB on A.id_peminjaman = BB.id
WHERE 
-- A.tanggal_batas <= now() and 
A.`status` = 0 GROUP BY BB.id_anggota) ;

-- ----------------------------
-- View structure for v_daftar_akses_anggota
-- ----------------------------
DROP VIEW IF EXISTS `v_daftar_akses_anggota`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost`  VIEW `v_daftar_akses_anggota` AS SELECT * FROM anggota
INNER JOIN hak_akses_anggota on anggota.id = hak_akses_anggota.id_anggota
GROUP BY anggota.id ;

-- ----------------------------
-- View structure for v_get_access
-- ----------------------------
DROP VIEW IF EXISTS `v_get_access`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER  VIEW `v_get_access` AS SELECT
B.id_anggota,
C.nama,
B.id_hak_akses,
A.hak_akses
FROM
hak_akses AS A
INNER JOIN hak_akses_anggota AS B ON B.id_hak_akses = A.id
INNER JOIN anggota AS C ON B.id_anggota = C.id ;

-- ----------------------------
-- View structure for v_get_daftar_shu_anggota
-- ----------------------------
DROP VIEW IF EXISTS `v_get_daftar_shu_anggota`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost`  VIEW `v_get_daftar_shu_anggota` AS SELECT A.*, B.periode, B.tahun, C.nama
FROM
detail_shu AS A
INNER JOIN shu AS B ON A.id_shu = B.id
INNER JOIN anggota AS C ON A.id_anggota = C.id ;

-- ----------------------------
-- View structure for v_get_detail_shu
-- ----------------------------
DROP VIEW IF EXISTS `v_get_detail_shu`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost`  VIEW `v_get_detail_shu` AS SELECT
detail_shu.id_shu,
detail_shu.id,
shu.periode,
shu.tahun,
detail_shu.id_anggota,
anggota.nama,
detail_shu.anggota,
detail_shu.peminjam,
detail_shu.pengurus,
detail_shu.jumlah
FROM
detail_shu
INNER JOIN shu ON detail_shu.id_shu = shu.id
INNER JOIN anggota ON detail_shu.id_anggota = anggota.id ;

-- ----------------------------
-- View structure for v_get_tunggakan
-- ----------------------------
DROP VIEW IF EXISTS `v_get_tunggakan`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER  VIEW `v_get_tunggakan` AS SELECT * from pembayaran WHERE tanggal_batas <= now() and `status` = 0 ;

-- ----------------------------
-- View structure for v_get_tunggakan_pembayaran
-- ----------------------------
DROP VIEW IF EXISTS `v_get_tunggakan_pembayaran`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER  VIEW `v_get_tunggakan_pembayaran` AS SELECT
A.id,
A.id_peminjaman,
B.no_pinjaman,
B.id_anggota,
C.nama as nama_anggota,
A.tanggal_bayar,
A.tanggal_batas,
A.jumlah_bayar,
A.bunga,
A.pembayaran_ke,
A.keterangan,
A.`status`
from 
pembayaran as A
INNER JOIN peminjaman as B on A.id_peminjaman = B.id
INNER JOIN anggota as C on B.id_anggota = C.id
WHERE A.tanggal_batas <= now() and A.`status` = 0 ;

-- ----------------------------
-- View structure for v_grafik_tunggakan_bulanan
-- ----------------------------
DROP VIEW IF EXISTS `v_grafik_tunggakan_bulanan`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER  VIEW `v_grafik_tunggakan_bulanan` AS SELECT  
	MONTH(tanggal_batas) as Bulan, 
	YEAR(tanggal_batas) as Tahun,
	Count(id) as Total,
  Count(case when `status`=1 then id end) As Membayar,
  Count(case when `status`=0 then id end) As Menunggak
FROM pembayaran
GROUP BY YEAR(tanggal_batas), MONTH(tanggal_batas) ;

-- ----------------------------
-- View structure for v_grafikpeminjaman
-- ----------------------------
DROP VIEW IF EXISTS `v_grafikpeminjaman`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost`  VIEW `v_grafikpeminjaman` AS SELECT
  Year(tgl_pinjaman) as Tahun,
  Count(case when month(`tgl_pinjaman`)=1 then id end) As Januari,
  Count(case when month(`tgl_pinjaman`)=2 then id end) As Februari,
  Count(case when month(`tgl_pinjaman`)=3 then id end) As Maret,
  Count(case when month(`tgl_pinjaman`)=4 then id end) As April,
  Count(case when month(`tgl_pinjaman`)=5 then id end) As Mei,
  Count(case when month(`tgl_pinjaman`)=6 then id end) As Juni,
  Count(case when month(`tgl_pinjaman`)=7 then id end) As Juli,
  Count(case when month(`tgl_pinjaman`)=8 then id end) As Agustus,
  Count(case when month(`tgl_pinjaman`)=9 then id end) As September,
  Count(case when month(`tgl_pinjaman`)=10 then id end) As Oktober,
  Count(case when month(`tgl_pinjaman`)=11 then id end) As Nopember,
  Count(case when month(`tgl_pinjaman`)=12 then id end) As Desember
FROM peminjaman
GROUP BY Year(`tgl_pinjaman`) ;

-- ----------------------------
-- View structure for v_list_total_tunggakan
-- ----------------------------
DROP VIEW IF EXISTS `v_list_total_tunggakan`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost`  VIEW `v_list_total_tunggakan` AS SELECT 
	id_anggota,
	no_pinjaman,
	count(`status`) kali_tunggakan, 
	sum(jumlah_bayar) total_jml_bayar, 
	sum(bunga) total_jml_bunga, 
	(sum(jumlah_bayar)+sum(bunga)) total
	FROM v_get_tunggakan_pembayaran 
	GROUP BY id_anggota ;

-- ----------------------------
-- View structure for v_pinjaman_aktif
-- ----------------------------
DROP VIEW IF EXISTS `v_pinjaman_aktif`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost`  VIEW `v_pinjaman_aktif` AS SELECT * from peminjaman WHERE `status` = 0 ;

-- ----------------------------
-- View structure for v_status_pinjaman
-- ----------------------------
DROP VIEW IF EXISTS `v_status_pinjaman`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost`  VIEW `v_status_pinjaman` AS SELECT
A.id_peminjaman,
B.no_pinjaman,
B.tgl_pinjaman,
B.id_anggota,
C.nama,
B.total_pinjaman,
sum(CASE WHEN A.`status` = 0 THEN 1 ELSE 0 end) as jml_belum_bayar,
sum(CASE WHEN A.`status` = 1 THEN 1 ELSE 0 end) as jml_sudah_bayar,
sum(CASE WHEN A.`status` = 0 THEN A.jumlah_bayar + A.bunga ELSE 0 end) as total_belum_bayar,
sum(CASE WHEN A.`status` = 1 THEN A.jumlah_bayar + A.bunga ELSE 0 end) as total_sudah_bayar,
(CASE WHEN B.`status` = 1 THEN 'Lunas' ELSE 'Belum Lunas' end) as `status`
from pembayaran AS A
INNER JOIN peminjaman AS B on A.id_peminjaman = B.id
INNER JOIN anggota AS C on B.id_anggota = C.id
GROUP BY A.id_peminjaman ;

-- ----------------------------
-- Procedure structure for generate_pembayaran
-- ----------------------------
DROP PROCEDURE IF EXISTS `generate_pembayaran`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `generate_pembayaran`(`id_pinjaman` int,`jumlah_pinjaman` decimal,`bunga` decimal,`kali_bayar` int, tgl_pinjaman date)
BEGIN
	declare xx int;	
	declare split_jml_pinjaman decimal(15,2);
	declare split_bunga decimal(15,2);
	
	set xx = 1;
	set split_jml_pinjaman = jumlah_pinjaman / kali_bayar;
	set split_bunga = bunga;
	
	while xx <=kali_bayar DO
		-- set tgl_batas = SELECT now() + INTERVAL xx MONTH;
		INSERT INTO pembayaran(id_peminjaman,jumlah_bayar,bunga,pembayaran_ke, tanggal_batas)VALUES(id_pinjaman,split_jml_pinjaman,split_bunga,xx,(SELECT tgl_pinjaman + INTERVAL xx MONTH));
		SET xx= xx+1;
	end while;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for p_generateSimpananW
-- ----------------------------
DROP PROCEDURE IF EXISTS `p_generateSimpananW`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `p_generateSimpananW`()
BEGIN
	DECLARE jumlahGen INT;
	declare jumlah_simpanan decimal(15,2);
	
	SELECT count(*) INTO jumlahGen from simpanan_wajib where periode = MONTH(NOW()) and tahun = YEAR(now());
	
	if(jumlahGen < 1) then
		select jumlah into jumlah_simpanan from jenis_simpanan where kode_simpanan = "wajib" limit 1;
		INSERT INTO simpanan_wajib(id_anggota,jumlah_bayar,status_pembayaran,periode,tahun)
		SELECT id,jumlah_simpanan,0,MONTH(NOW()),YEAR(now()) FROM anggota;
	end if;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for p_get_total_tunggakan_per_pelanggan
-- ----------------------------
DROP PROCEDURE IF EXISTS `p_get_total_tunggakan_per_pelanggan`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `p_get_total_tunggakan_per_pelanggan`(`anggota_id` int)
BEGIN
	SELECT 
	count(`status`) kali_tunggakan, 
	sum(jumlah_bayar) total_jml_bayar, 
	sum(bunga) total_jml_bunga, 
	(sum(jumlah_bayar)+sum(bunga)) total
 
	FROM v_get_tunggakan_pembayaran 
	WHERE id_anggota = anggota_id
	GROUP BY id_peminjaman;

END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for p_trans_rutin_del
-- ----------------------------
DROP PROCEDURE IF EXISTS `p_trans_rutin_del`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `p_trans_rutin_del`(`id_trans` int(11),`jns` varchar(255))
BEGIN
	DELETE FROM transaksi_rutin WHERE id = id_trans AND transaksi = jns;

END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for p_trans_rutin_in
-- ----------------------------
DROP PROCEDURE IF EXISTS `p_trans_rutin_in`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `p_trans_rutin_in`(`id_trans` int(11),`jenis_trans` varchar(255),`tgl_trans` date,`v_masuk` decimal(15,2),`v_keluar` decimal(15,2),`ket` varchar(255))
BEGIN
	INSERT INTO transaksi_rutin(transaksi,id_transaksi,tanggal_transaksi,masuk,keluar,keterangan) 
	VALUES (jenis_trans,id_trans,tgl_trans,v_masuk, v_keluar, ket);

END
;;
DELIMITER ;

-- ----------------------------
-- Auto increment value for detail_shu
-- ----------------------------
ALTER TABLE `detail_shu` AUTO_INCREMENT=169;

-- ----------------------------
-- Auto increment value for hak_akses
-- ----------------------------
ALTER TABLE `hak_akses` AUTO_INCREMENT=6;

-- ----------------------------
-- Auto increment value for jabatan
-- ----------------------------
ALTER TABLE `jabatan` AUTO_INCREMENT=6;

-- ----------------------------
-- Auto increment value for jenis_simpanan
-- ----------------------------
ALTER TABLE `jenis_simpanan` AUTO_INCREMENT=2;

-- ----------------------------
-- Auto increment value for menu
-- ----------------------------
ALTER TABLE `menu` AUTO_INCREMENT=15;

-- ----------------------------
-- Auto increment value for pembayaran
-- ----------------------------
ALTER TABLE `pembayaran` AUTO_INCREMENT=79;
DROP TRIGGER IF EXISTS `generate_pembayaran`;
DELIMITER ;;
CREATE TRIGGER `generate_pembayaran` AFTER INSERT ON `peminjaman` FOR EACH ROW call generate_pembayaran(NEW.id,NEW.jumlah_pinjaman, NEW.bunga, NEW.kali_bayar, NEW.tgl_pinjaman)
;;
DELIMITER ;

-- ----------------------------
-- Auto increment value for peminjaman
-- ----------------------------
ALTER TABLE `peminjaman` AUTO_INCREMENT=6;

-- ----------------------------
-- Auto increment value for shu
-- ----------------------------
ALTER TABLE `shu` AUTO_INCREMENT=7;

-- ----------------------------
-- Auto increment value for simpanan_wajib
-- ----------------------------
ALTER TABLE `simpanan_wajib` AUTO_INCREMENT=840;

-- ----------------------------
-- Auto increment value for transaksi_rutin
-- ----------------------------
ALTER TABLE `transaksi_rutin` AUTO_INCREMENT=187;
