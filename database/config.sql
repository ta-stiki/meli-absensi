/*
Navicat MySQL Data Transfer

Source Server         : local
Source Server Version : 50505
Source Host           : localhost:3306
Source Database       : sikopat

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2017-02-02 12:58:24
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for auto_number
-- ----------------------------
DROP TABLE IF EXISTS `auto_number`;
CREATE TABLE `auto_number` (
  `group` varchar(10) DEFAULT NULL,
  `number` int(11) DEFAULT NULL,
  `lock_p` int(255) DEFAULT NULL,
  `update` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of auto_number
-- ----------------------------
INSERT INTO `auto_number` VALUES ('pinjaman', '1', null, null);

-- ----------------------------
-- Table structure for hak_akses
-- ----------------------------
DROP TABLE IF EXISTS `hak_akses`;
CREATE TABLE `hak_akses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `hak_akses` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of hak_akses
-- ----------------------------
INSERT INTO `hak_akses` VALUES ('1', 'Anggota');
INSERT INTO `hak_akses` VALUES ('2', 'Pengawas');
INSERT INTO `hak_akses` VALUES ('3', 'Ketua');
INSERT INTO `hak_akses` VALUES ('4', 'Bendahara');
INSERT INTO `hak_akses` VALUES ('5', 'Wakil');

-- ----------------------------
-- Table structure for hak_akses_anggota
-- ----------------------------
DROP TABLE IF EXISTS `hak_akses_anggota`;
CREATE TABLE `hak_akses_anggota` (
  `id_anggota` int(11) DEFAULT NULL,
  `id_hak_akses` int(11) DEFAULT NULL,
  KEY `fk_hak_akses_anggota_hak_akses_1` (`id_hak_akses`),
  KEY `fk_hak_akses_anggota_anggota_1` (`id_anggota`),
  CONSTRAINT `fk_hak_akses_anggota_anggota_1` FOREIGN KEY (`id_anggota`) REFERENCES `anggota` (`id`),
  CONSTRAINT `fk_hak_akses_anggota_hak_akses_1` FOREIGN KEY (`id_hak_akses`) REFERENCES `hak_akses` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of hak_akses_anggota
-- ----------------------------
INSERT INTO `hak_akses_anggota` VALUES ('109', '2');
INSERT INTO `hak_akses_anggota` VALUES ('110', '2');
INSERT INTO `hak_akses_anggota` VALUES ('111', '2');
INSERT INTO `hak_akses_anggota` VALUES ('112', '2');
INSERT INTO `hak_akses_anggota` VALUES ('113', '2');
INSERT INTO `hak_akses_anggota` VALUES ('114', '2');
INSERT INTO `hak_akses_anggota` VALUES ('115', '2');
INSERT INTO `hak_akses_anggota` VALUES ('116', '2');
INSERT INTO `hak_akses_anggota` VALUES ('117', '2');
INSERT INTO `hak_akses_anggota` VALUES ('118', '2');
INSERT INTO `hak_akses_anggota` VALUES ('119', '2');
INSERT INTO `hak_akses_anggota` VALUES ('120', '2');
INSERT INTO `hak_akses_anggota` VALUES ('121', '2');
INSERT INTO `hak_akses_anggota` VALUES ('122', '2');
INSERT INTO `hak_akses_anggota` VALUES ('123', '2');
INSERT INTO `hak_akses_anggota` VALUES ('124', '2');
INSERT INTO `hak_akses_anggota` VALUES ('125', '2');
INSERT INTO `hak_akses_anggota` VALUES ('126', '2');
INSERT INTO `hak_akses_anggota` VALUES ('127', '2');
INSERT INTO `hak_akses_anggota` VALUES ('128', '2');
INSERT INTO `hak_akses_anggota` VALUES ('129', '2');
INSERT INTO `hak_akses_anggota` VALUES ('130', '2');
INSERT INTO `hak_akses_anggota` VALUES ('131', '2');
INSERT INTO `hak_akses_anggota` VALUES ('132', '2');
INSERT INTO `hak_akses_anggota` VALUES ('133', '2');
INSERT INTO `hak_akses_anggota` VALUES ('134', '2');
INSERT INTO `hak_akses_anggota` VALUES ('135', '2');
INSERT INTO `hak_akses_anggota` VALUES ('136', '2');
INSERT INTO `hak_akses_anggota` VALUES ('137', '2');
INSERT INTO `hak_akses_anggota` VALUES ('138', '2');
INSERT INTO `hak_akses_anggota` VALUES ('139', '2');
INSERT INTO `hak_akses_anggota` VALUES ('141', '2');
INSERT INTO `hak_akses_anggota` VALUES ('142', '2');
INSERT INTO `hak_akses_anggota` VALUES ('143', '2');
INSERT INTO `hak_akses_anggota` VALUES ('144', '2');
INSERT INTO `hak_akses_anggota` VALUES ('145', '2');
INSERT INTO `hak_akses_anggota` VALUES ('146', '2');
INSERT INTO `hak_akses_anggota` VALUES ('147', '2');
INSERT INTO `hak_akses_anggota` VALUES ('148', '2');
INSERT INTO `hak_akses_anggota` VALUES ('149', '2');
INSERT INTO `hak_akses_anggota` VALUES ('150', '2');
INSERT INTO `hak_akses_anggota` VALUES ('151', '2');
INSERT INTO `hak_akses_anggota` VALUES ('152', '2');
INSERT INTO `hak_akses_anggota` VALUES ('153', '2');
INSERT INTO `hak_akses_anggota` VALUES ('154', '2');
INSERT INTO `hak_akses_anggota` VALUES ('155', '2');
INSERT INTO `hak_akses_anggota` VALUES ('156', '2');
INSERT INTO `hak_akses_anggota` VALUES ('157', '2');
INSERT INTO `hak_akses_anggota` VALUES ('158', '2');
INSERT INTO `hak_akses_anggota` VALUES ('159', '2');
INSERT INTO `hak_akses_anggota` VALUES ('160', '2');
INSERT INTO `hak_akses_anggota` VALUES ('161', '2');
INSERT INTO `hak_akses_anggota` VALUES ('162', '2');
INSERT INTO `hak_akses_anggota` VALUES ('163', '2');
INSERT INTO `hak_akses_anggota` VALUES ('164', '2');
INSERT INTO `hak_akses_anggota` VALUES ('165', '2');
INSERT INTO `hak_akses_anggota` VALUES ('166', '2');
INSERT INTO `hak_akses_anggota` VALUES ('167', '2');
INSERT INTO `hak_akses_anggota` VALUES ('168', '2');
INSERT INTO `hak_akses_anggota` VALUES ('169', '2');
INSERT INTO `hak_akses_anggota` VALUES ('170', '2');
INSERT INTO `hak_akses_anggota` VALUES ('171', '2');
INSERT INTO `hak_akses_anggota` VALUES ('172', '2');
INSERT INTO `hak_akses_anggota` VALUES ('173', '2');
INSERT INTO `hak_akses_anggota` VALUES ('174', '2');
INSERT INTO `hak_akses_anggota` VALUES ('175', '2');
INSERT INTO `hak_akses_anggota` VALUES ('176', '2');
INSERT INTO `hak_akses_anggota` VALUES ('177', '2');
INSERT INTO `hak_akses_anggota` VALUES ('178', '2');
INSERT INTO `hak_akses_anggota` VALUES ('179', '2');
INSERT INTO `hak_akses_anggota` VALUES ('180', '2');
INSERT INTO `hak_akses_anggota` VALUES ('181', '2');
INSERT INTO `hak_akses_anggota` VALUES ('182', '2');
INSERT INTO `hak_akses_anggota` VALUES ('183', '2');
INSERT INTO `hak_akses_anggota` VALUES ('184', '2');
INSERT INTO `hak_akses_anggota` VALUES ('185', '2');
INSERT INTO `hak_akses_anggota` VALUES ('186', '2');
INSERT INTO `hak_akses_anggota` VALUES ('187', '2');
INSERT INTO `hak_akses_anggota` VALUES ('188', '2');
INSERT INTO `hak_akses_anggota` VALUES ('189', '2');
INSERT INTO `hak_akses_anggota` VALUES ('174', '1');
INSERT INTO `hak_akses_anggota` VALUES ('174', '3');
INSERT INTO `hak_akses_anggota` VALUES ('174', '4');
INSERT INTO `hak_akses_anggota` VALUES ('108', '2');
INSERT INTO `hak_akses_anggota` VALUES ('108', '3');
INSERT INTO `hak_akses_anggota` VALUES ('140', '2');
INSERT INTO `hak_akses_anggota` VALUES ('140', '3');

-- ----------------------------
-- Table structure for jabatan
-- ----------------------------
DROP TABLE IF EXISTS `jabatan`;
CREATE TABLE `jabatan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `jenis_jabatan` varchar(10) DEFAULT NULL,
  `nama_jabatan` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_jabatan_persentase_shu_1` (`jenis_jabatan`),
  CONSTRAINT `fk_jabatan_persentase_shu_1` FOREIGN KEY (`jenis_jabatan`) REFERENCES `persentase_shu` (`jenis_jabatan`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of jabatan
-- ----------------------------
INSERT INTO `jabatan` VALUES ('1', 'anggota', 'Anggota');
INSERT INTO `jabatan` VALUES ('2', 'pengurus', 'Ketua');
INSERT INTO `jabatan` VALUES ('3', 'pengurus', 'Bendahara');
INSERT INTO `jabatan` VALUES ('4', 'pengurus', 'Pengawas');
INSERT INTO `jabatan` VALUES ('5', 'pengurus', 'Wakil');

-- ----------------------------
-- Table structure for menu
-- ----------------------------
DROP TABLE IF EXISTS `menu`;
CREATE TABLE `menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `link` varchar(255) DEFAULT NULL,
  `icon` varchar(50) DEFAULT NULL,
  `is_active` int(1) DEFAULT NULL,
  `is_parent` int(1) DEFAULT NULL,
  `akses` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of menu
-- ----------------------------
INSERT INTO `menu` VALUES ('1', 'Anggota', 'admin/anggota', 'fa fa-users', '1', '0', null);
INSERT INTO `menu` VALUES ('2', 'Peminjaman', 'admin/peminjaman', 'fa fa-download', '1', '0', null);
INSERT INTO `menu` VALUES ('3', 'Pembayaran', 'admin/pembayaran', 'fa fa-upload', '1', '0', null);
INSERT INTO `menu` VALUES ('4', 'Simpanan Wajib', 'admin/simpanan_wajib', 'fa fa-cloud-download', '1', '0', null);
INSERT INTO `menu` VALUES ('12', 'SHU', 'admin/shu', 'fa  fa-balance-scale', '1', '0', null);

-- ----------------------------
-- Table structure for pengaturan
-- ----------------------------
DROP TABLE IF EXISTS `pengaturan`;
CREATE TABLE `pengaturan` (
  `key` varchar(50) NOT NULL,
  `value` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`key`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of pengaturan
-- ----------------------------
INSERT INTO `pengaturan` VALUES ('bunga_pinjaman', '1');
INSERT INTO `pengaturan` VALUES ('max_kali_bunga', '3');
INSERT INTO `pengaturan` VALUES ('max_kali_tunggakan', '3');
INSERT INTO `pengaturan` VALUES ('saldo_awal', '25000');

-- ----------------------------
-- Table structure for persentase_shu
-- ----------------------------
DROP TABLE IF EXISTS `persentase_shu`;
CREATE TABLE `persentase_shu` (
  `jenis_jabatan` varchar(10) NOT NULL,
  `persentase_shu` decimal(15,2) DEFAULT NULL,
  PRIMARY KEY (`jenis_jabatan`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of persentase_shu
-- ----------------------------
INSERT INTO `persentase_shu` VALUES ('anggota', '50.00');
INSERT INTO `persentase_shu` VALUES ('modal', '20.00');
INSERT INTO `persentase_shu` VALUES ('peminjam', '20.00');
INSERT INTO `persentase_shu` VALUES ('pengurus', '10.00');
