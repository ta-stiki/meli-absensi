/*
Navicat MySQL Data Transfer

Source Server         : local
Source Server Version : 50505
Source Host           : localhost:3306
Source Database       : sikopat

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2017-01-19 13:19:56
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for anggota
-- ----------------------------
DROP TABLE IF EXISTS `anggota`;
CREATE TABLE `anggota` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(255) DEFAULT NULL,
  `tgl_lahir` date DEFAULT NULL,
  `jenis_kelamin` varchar(20) DEFAULT NULL,
  `alamat` varchar(100) DEFAULT NULL,
  `no_telp` varchar(20) DEFAULT NULL,
  `id_jabatan` int(11) DEFAULT NULL,
  `tgl_masuk` date DEFAULT NULL,
  `tgl_berhenti` date DEFAULT NULL,
  `tipe_anggota` varchar(10) DEFAULT NULL,
  `no_identitas` varchar(20) DEFAULT NULL,
  `saldo_simpanan` double(15,2) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `username` varchar(20) DEFAULT NULL,
  `password` varchar(50) DEFAULT NULL,
  `foto` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_anggota_jabatan_1` (`id_jabatan`),
  CONSTRAINT `fk_anggota_jabatan_1` FOREIGN KEY (`id_jabatan`) REFERENCES `jabatan` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=108 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of anggota
-- ----------------------------
INSERT INTO `anggota` VALUES ('2', 'Stewart A. Justice', '2003-05-07', 'perempuan', '5517 Nec Street', '036-056-0665', '1', '2016-03-28', null, null, '1684072037199', '229765.00', '1', 'admin', 'admin', null);
INSERT INTO `anggota` VALUES ('3', 'Joelle O. Meadows', '1948-05-24', 'laki-laki', 'P.O. Box 944, 4901 Diam Street', '017-013-8804', '1', '2017-12-18', null, null, '1630122671699', '159112.00', '1', null, null, null);
INSERT INTO `anggota` VALUES ('4', 'Dacey Mcguire', '1946-12-25', 'perempuan', '458 Pede St.', '055-676-1162', '1', '2016-08-06', null, null, '1606120153599', '107419.00', '1', null, null, null);
INSERT INTO `anggota` VALUES ('5', 'Ebony P. Church', '2013-12-01', 'perempuan', '2301 Mollis Rd.', '008-988-0303', '1', '2016-02-29', null, null, '1683112181299', '116882.00', '1', null, null, null);
INSERT INTO `anggota` VALUES ('6', 'Darius Rasmussen', '1931-06-20', 'laki-laki', 'Ap #746-7269 Tellus St.', '087-134-1562', '1', '2017-02-19', null, null, '1645011617299', '116163.00', '1', null, null, null);
INSERT INTO `anggota` VALUES ('7', 'Charde Q. Justice', '1965-10-13', 'perempuan', 'P.O. Box 167, 513 Parturient Avenue', '025-791-4840', '1', '2016-05-05', null, null, '1680092368499', '200735.00', '1', null, null, null);
INSERT INTO `anggota` VALUES ('8', 'Darius Burton', '2003-11-09', 'perempuan', '8149 Varius St.', '043-868-8510', '1', '2016-03-05', null, null, '1696070882899', '134390.00', '1', null, null, null);
INSERT INTO `anggota` VALUES ('9', 'Samson Randall', '1984-10-15', 'laki-laki', '5034 Commodo Av.', '023-665-1364', '1', '2016-05-08', null, null, '1679060243099', '111184.00', '1', null, null, null);
INSERT INTO `anggota` VALUES ('10', 'Peter Wilkerson', '1950-02-13', 'laki-laki', '280-7312 Ipsum. Rd.', '054-504-5191', '1', '2018-01-05', null, null, '1677121054599', '130306.00', '1', null, null, null);
INSERT INTO `anggota` VALUES ('11', 'Serena Price', '2000-08-01', 'perempuan', 'Ap #642-8767 Nunc St.', '004-335-7527', '1', '2017-05-14', null, null, '1679040537899', '122802.00', '1', null, null, null);
INSERT INTO `anggota` VALUES ('12', 'Lucy Beasley', '1985-09-26', 'laki-laki', '381-1499 Tortor Av.', '085-629-0872', '1', '2016-11-24', '0000-00-00', null, '1686092409399', '213343.00', '1', 'dxx', 'dxx', null);
INSERT INTO `anggota` VALUES ('13', 'Xenos Weber', '1953-06-29', 'laki-laki', '3672 Cubilia Rd.', '000-428-6692', '1', '2016-10-10', null, null, '1686091137699', '166048.00', '1', null, null, null);
INSERT INTO `anggota` VALUES ('14', 'Paul S. Brady', '1943-04-19', 'perempuan', 'Ap #281-9910 Neque. St.', '020-936-5282', '1', '2016-05-06', null, null, '1626111030999', '215544.00', '1', null, null, null);
INSERT INTO `anggota` VALUES ('15', 'Drew Sykes', '1924-05-01', 'laki-laki', '1065 Nisi. Av.', '042-851-5306', '1', '2017-04-24', null, null, '1602090411399', '129705.00', '1', null, null, null);
INSERT INTO `anggota` VALUES ('16', 'Amity Lang', '1999-08-12', 'perempuan', '430-5997 Gravida St.', '032-106-8087', '1', '2017-01-07', null, null, '1670092035599', '205077.00', '1', null, null, null);
INSERT INTO `anggota` VALUES ('17', 'Tatyana Z. Sosa', '1940-07-27', 'perempuan', '8610 Diam Av.', '093-917-1611', '1', '2016-01-11', null, null, '1614042623599', '167971.00', '1', null, null, null);
INSERT INTO `anggota` VALUES ('18', 'Maisie X. Wong', '1997-10-23', 'laki-laki', '8681 Nunc Rd.', '054-977-3322', '1', '2016-05-10', null, null, '1666101361099', '160193.00', '1', null, null, null);
INSERT INTO `anggota` VALUES ('19', 'Rose G. Livingston', '2005-10-08', 'laki-laki', 'P.O. Box 378, 303 Sem St.', '092-755-2002', '1', '2017-01-13', null, null, '1642043065699', '116008.00', '1', null, null, null);
INSERT INTO `anggota` VALUES ('20', 'Jackson T. Good', '1998-01-07', 'perempuan', 'P.O. Box 671, 6359 Nullam Ave', '062-739-7419', '1', '2016-01-08', null, null, '1657100484399', '187630.00', '1', null, null, null);
INSERT INTO `anggota` VALUES ('21', 'Simon J. Case', '1923-12-14', 'laki-laki', 'P.O. Box 666, 8867 Nunc. Street', '035-168-8237', '1', '2016-06-06', null, null, '1696061194199', '160367.00', '1', null, null, null);
INSERT INTO `anggota` VALUES ('22', 'Neve C. Valdez', '1962-07-29', 'laki-laki', 'P.O. Box 179, 7331 Turpis Avenue', '030-440-0402', '1', '2017-04-15', null, null, '1666111861399', '126240.00', '1', null, null, null);
INSERT INTO `anggota` VALUES ('23', 'Evelyn Caldwell', '1917-10-01', 'laki-laki', 'Ap #396-9556 Ornare, Avenue', '019-708-7544', '1', '2017-09-30', null, null, '1629053086999', '236700.00', '1', null, null, null);
INSERT INTO `anggota` VALUES ('24', 'Dylan V. Palmer', '1926-04-16', 'laki-laki', '5872 Ornare Rd.', '038-658-5355', '1', '2017-06-01', null, null, '1622052240099', '196546.00', '1', null, null, null);
INSERT INTO `anggota` VALUES ('25', 'Laith Hodge', '2017-02-18', 'perempuan', '8071 Id, Rd.', '037-751-1584', '1', '2017-08-28', null, null, '1695042777999', '158646.00', '1', null, null, null);
INSERT INTO `anggota` VALUES ('26', 'Lenore Q. Mcneil', '1987-03-06', 'laki-laki', 'P.O. Box 670, 3117 Eu St.', '019-365-3581', '1', '2017-11-19', null, null, '1605092106199', '147785.00', '1', null, null, null);
INSERT INTO `anggota` VALUES ('27', 'Tyler W. Cox', '1939-11-30', 'perempuan', '987-9510 Donec Road', '060-250-7308', '1', '2016-04-24', null, null, '1647101031499', '144640.00', '1', null, null, null);
INSERT INTO `anggota` VALUES ('28', 'Morgan Holland', '1928-01-25', 'laki-laki', '293-1709 Ornare, Avenue', '091-006-0798', '1', '2016-10-19', null, null, '1667101529899', '140021.00', '1', null, null, null);
INSERT INTO `anggota` VALUES ('29', 'Lunea Dunn', '1963-10-17', 'perempuan', '4597 Elit Street', '031-811-3289', '1', '2017-10-23', null, null, '1610012711199', '242627.00', '1', null, null, null);
INSERT INTO `anggota` VALUES ('30', 'Paki Rios', '1981-02-11', 'laki-laki', '4251 Vulputate St.', '035-389-2661', '1', '2016-10-26', null, null, '1679052925099', '193919.00', '1', null, null, null);
INSERT INTO `anggota` VALUES ('31', 'Amela Collins', '1917-04-10', 'perempuan', '3384 Leo. Street', '048-764-7015', '1', '2017-10-06', null, null, '1669090941799', '210305.00', '1', null, null, null);
INSERT INTO `anggota` VALUES ('32', 'Craig Rodriquez', '1966-10-22', 'perempuan', 'Ap #594-9919 Justo St.', '021-686-9412', '1', '2017-07-24', null, null, '1665102371599', '222235.00', '1', null, null, null);
INSERT INTO `anggota` VALUES ('33', 'Cynthia S. Woodward', '1990-06-13', 'perempuan', '224-2770 Tempus, Road', '097-212-9790', '1', '2017-03-10', null, null, '1637062693199', '170940.00', '1', null, null, null);
INSERT INTO `anggota` VALUES ('34', 'Cally P. Blackburn', '1917-08-16', 'perempuan', 'P.O. Box 991, 4663 A, St.', '051-167-8567', '1', '2017-02-11', null, null, '1666030528299', '221168.00', '1', null, null, null);
INSERT INTO `anggota` VALUES ('35', 'Xaviera E. Swanson', '2007-11-03', 'laki-laki', '272-9222 Mollis Ave', '067-643-8362', '1', '2016-05-30', null, null, '1607080640299', '214394.00', '1', null, null, null);
INSERT INTO `anggota` VALUES ('36', 'Clarke Y. Albert', '1979-04-07', 'perempuan', 'Ap #311-3692 Ut Rd.', '095-108-4622', '1', '2016-12-01', null, null, '1653080938599', '208768.00', '1', null, null, null);
INSERT INTO `anggota` VALUES ('37', 'Ora S. Downs', '1929-04-19', 'laki-laki', 'P.O. Box 900, 9809 Non, Rd.', '022-830-9545', '1', '2017-02-09', null, null, '1604011613399', '230784.00', '1', null, null, null);
INSERT INTO `anggota` VALUES ('38', 'Kirestin C. Patterson', '1957-03-03', 'perempuan', 'Ap #908-4650 Maecenas St.', '022-818-9159', '1', '2017-05-13', null, null, '1639032498499', '115015.00', '1', null, null, null);
INSERT INTO `anggota` VALUES ('39', 'Ocean R. Hicks', '2014-08-15', 'perempuan', 'P.O. Box 639, 8904 Nec, St.', '091-178-7921', '1', '2017-12-15', null, null, '1620092966199', '169533.00', '1', null, null, null);
INSERT INTO `anggota` VALUES ('40', 'Hilel U. Shannon', '2017-04-16', 'perempuan', 'P.O. Box 530, 9667 Egestas. St.', '034-701-9549', '1', '2017-07-17', null, null, '1653021673599', '140190.00', '1', null, null, null);
INSERT INTO `anggota` VALUES ('41', 'Destiny B. Swanson', '1968-06-29', 'perempuan', '624-3215 Mollis. St.', '018-508-7740', '1', '2017-02-19', null, null, '1671030132799', '208250.00', '1', null, null, null);
INSERT INTO `anggota` VALUES ('42', 'Chantale C. Carney', '1930-04-08', 'perempuan', '5031 Convallis Av.', '020-721-1055', '1', '2016-09-04', null, null, '1694070327499', '226145.00', '1', null, null, null);
INSERT INTO `anggota` VALUES ('43', 'Yuli Estrada', '1990-05-26', 'laki-laki', 'Ap #663-980 Nam Rd.', '084-263-7063', '1', '2017-02-17', null, null, '1629071481099', '241550.00', '1', null, null, null);
INSERT INTO `anggota` VALUES ('44', 'Kenyon Lindsay', '1932-12-15', 'perempuan', '605-895 Mus. St.', '085-727-2301', '1', '2017-11-21', null, null, '1656062982299', '210995.00', '1', null, null, null);
INSERT INTO `anggota` VALUES ('45', 'Carla W. Harris', '1956-04-26', 'perempuan', '759 Nascetur Avenue', '011-118-8926', '1', '2017-10-03', null, null, '1640040435699', '162954.00', '1', null, null, null);
INSERT INTO `anggota` VALUES ('46', 'Levi E. Becker', '1970-05-31', 'perempuan', 'Ap #518-2754 Dui, Street', '087-210-3426', '1', '2017-08-08', null, null, '1656020521799', '166665.00', '1', null, null, null);
INSERT INTO `anggota` VALUES ('47', 'Aristotle G. Hester', '1964-02-27', 'perempuan', 'P.O. Box 126, 4187 At Road', '071-091-7131', '1', '2017-03-03', null, null, '1633061423399', '120251.00', '1', null, null, null);
INSERT INTO `anggota` VALUES ('48', 'Kevin Webster', '2002-01-30', 'perempuan', '803-635 A Avenue', '052-815-3990', '1', '2016-01-08', null, null, '1663040166199', '242847.00', '1', null, null, null);
INSERT INTO `anggota` VALUES ('49', 'Bruno Moran', '1990-02-25', 'perempuan', '959-980 Adipiscing Street', '091-692-0895', '1', '2016-04-27', null, null, '1608041447399', '201303.00', '1', null, null, null);
INSERT INTO `anggota` VALUES ('50', 'May Morrow', '1944-03-09', 'laki-laki', 'P.O. Box 772, 513 Vel Street', '026-709-7892', '1', '2017-02-17', null, null, '1663011862599', '155760.00', '1', null, null, null);
INSERT INTO `anggota` VALUES ('51', 'Hu Berg', '1948-06-26', 'laki-laki', 'P.O. Box 581, 6168 Nisl Avenue', '031-933-6214', '1', '2016-07-16', null, null, '1634051429899', '242331.00', '1', null, null, null);
INSERT INTO `anggota` VALUES ('52', 'Uriah T. Bell', '1937-07-06', 'perempuan', '5810 At, Ave', '098-089-3278', '1', '2017-06-22', null, null, '1623040422299', '147904.00', '1', null, null, null);
INSERT INTO `anggota` VALUES ('53', 'Gisela Kim', '1919-01-15', 'laki-laki', 'Ap #674-7663 Ac Av.', '072-562-9919', '1', '2016-06-28', null, null, '1651060767599', '118471.00', '1', null, null, null);
INSERT INTO `anggota` VALUES ('54', 'Jerome A. White', '1926-09-19', 'laki-laki', 'Ap #689-537 Lobortis. St.', '057-869-6326', '1', '2017-05-02', null, null, '1604102937499', '166474.00', '1', null, null, null);
INSERT INTO `anggota` VALUES ('55', 'Gretchen Stout', '2013-08-09', 'perempuan', 'P.O. Box 926, 9083 Aliquam Rd.', '070-855-4263', '1', '2017-05-25', null, null, '1605022687699', '130286.00', '1', null, null, null);
INSERT INTO `anggota` VALUES ('56', 'Ariana V. Russell', '2017-09-17', 'perempuan', '735 Auctor Rd.', '099-615-6414', '1', '2017-05-01', null, null, '1647071197899', '249731.00', '1', null, null, null);
INSERT INTO `anggota` VALUES ('57', 'Karyn J. Finley', '1927-01-16', 'perempuan', '457-6417 Orci, Road', '042-445-7709', '1', '2016-03-29', null, null, '1632062154099', '158357.00', '1', null, null, null);
INSERT INTO `anggota` VALUES ('58', 'Marsden Mcguire', '1952-11-25', 'perempuan', '413-272 Et St.', '044-672-3789', '1', '2016-03-12', null, null, '1653122099299', '166130.00', '1', null, null, null);
INSERT INTO `anggota` VALUES ('59', 'Angela Ingram', '1934-03-24', 'laki-laki', 'P.O. Box 456, 7504 Vitae Road', '080-799-9979', '1', '2016-10-25', null, null, '1618111598799', '248503.00', '1', null, null, null);
INSERT INTO `anggota` VALUES ('60', 'Uriah Gordon', '1919-08-01', 'perempuan', '111-7155 Sociosqu Rd.', '053-636-7670', '1', '2016-10-10', null, null, '1657060875099', '216218.00', '1', null, null, null);
INSERT INTO `anggota` VALUES ('61', 'Sage J. Bradshaw', '2012-01-14', 'perempuan', '303-3612 Nibh. Road', '080-434-6386', '1', '2017-07-28', null, null, '1667042860099', '239898.00', '1', null, null, null);
INSERT INTO `anggota` VALUES ('62', 'Mia Burch', '1927-01-31', 'laki-laki', '2680 Nulla St.', '059-913-9130', '1', '2016-03-21', null, null, '1628102356299', '190911.00', '1', null, null, null);
INSERT INTO `anggota` VALUES ('63', 'Raphael H. Watson', '2007-09-28', 'laki-laki', 'P.O. Box 710, 9365 Mi St.', '004-412-6935', '1', '2017-05-21', null, null, '1642090175799', '143925.00', '1', null, null, null);
INSERT INTO `anggota` VALUES ('64', 'Baxter R. Lang', '1937-01-25', 'perempuan', '6324 Tellus, Rd.', '006-343-8683', '1', '2017-01-06', null, null, '1634081759399', '141266.00', '1', null, null, null);
INSERT INTO `anggota` VALUES ('65', 'Reese English', '2003-10-10', 'laki-laki', '994-2120 Pharetra Rd.', '080-370-8606', '1', '2017-09-21', null, null, '1682122771499', '165620.00', '1', null, null, null);
INSERT INTO `anggota` VALUES ('66', 'MacKenzie Rush', '1982-04-27', 'perempuan', 'P.O. Box 423, 9683 Nec Ave', '083-568-8785', '1', '2017-05-18', null, null, '1663043023999', '114628.00', '1', null, null, null);
INSERT INTO `anggota` VALUES ('67', 'Rae V. Beard', '1922-11-20', 'perempuan', 'P.O. Box 793, 2180 At Street', '059-645-3138', '1', '2016-11-02', null, null, '1688042792699', '210879.00', '1', null, null, null);
INSERT INTO `anggota` VALUES ('68', 'Vera Conway', '1996-05-05', 'perempuan', '925-1458 Auctor. Street', '000-681-7824', '1', '2016-10-07', null, null, '1632102308499', '235477.00', '1', null, null, null);
INSERT INTO `anggota` VALUES ('69', 'Cain G. Juarez', '1994-05-19', 'laki-laki', '8706 Ultrices Rd.', '027-346-4546', '1', '2016-02-29', null, null, '1643061198799', '231042.00', '1', null, null, null);
INSERT INTO `anggota` VALUES ('70', 'Lael Hebert', '2001-07-14', 'laki-laki', 'Ap #804-3869 Posuere, Ave', '082-156-1044', '1', '2017-06-10', null, null, '1685063092599', '167989.00', '1', null, null, null);
INSERT INTO `anggota` VALUES ('71', 'Chandler Velasquez', '1947-04-05', 'perempuan', '969-8893 Mauris Road', '048-427-2605', '1', '2017-12-04', null, null, '1645032165799', '163010.00', '1', null, null, null);
INSERT INTO `anggota` VALUES ('72', 'Dominic Potts', '2016-09-26', 'perempuan', '4196 Ante, Ave', '052-735-1555', '1', '2016-04-08', null, null, '1641121369499', '182915.00', '1', null, null, null);
INSERT INTO `anggota` VALUES ('73', 'Elliott I. Neal', '1952-11-02', 'laki-laki', '4261 Lobortis Street', '046-261-2945', '1', '2017-12-27', null, null, '1616021104899', '205061.00', '1', null, null, null);
INSERT INTO `anggota` VALUES ('74', 'Gannon Rivas', '1990-04-17', 'perempuan', 'Ap #705-4148 Phasellus Ave', '014-984-1212', '1', '2016-06-23', null, null, '1668081438099', '160136.00', '1', null, null, null);
INSERT INTO `anggota` VALUES ('75', 'Randall Howell', '1916-09-18', 'perempuan', 'P.O. Box 143, 6832 Fringilla Ave', '021-038-5642', '1', '2017-03-09', null, null, '1610012997799', '209262.00', '1', null, null, null);
INSERT INTO `anggota` VALUES ('76', 'Autumn S. Fulton', '1992-12-23', 'laki-laki', 'P.O. Box 638, 9925 Natoque Ave', '063-535-6535', '1', '2017-04-20', null, null, '1631122897999', '213013.00', '1', null, null, null);
INSERT INTO `anggota` VALUES ('77', 'Candace N. Colon', '1927-04-28', 'laki-laki', '227-3661 Imperdiet Rd.', '000-824-8549', '1', '2016-12-31', null, null, '1615122674599', '246703.00', '1', null, null, null);
INSERT INTO `anggota` VALUES ('78', 'Zephr E. Lowe', '1979-09-05', 'perempuan', '661-5972 Commodo St.', '006-023-8851', '1', '2017-07-03', null, null, '1638100786699', '112131.00', '1', null, null, null);
INSERT INTO `anggota` VALUES ('79', 'Scarlett H. Boyle', '2014-10-18', 'laki-laki', '9881 Mattis Road', '049-946-2891', '1', '2017-03-21', null, null, '1671022022299', '245716.00', '1', null, null, null);
INSERT INTO `anggota` VALUES ('80', 'Zeus Marks', '1939-04-09', 'perempuan', 'P.O. Box 956, 1796 Dictum Ave', '042-645-9253', '1', '2017-10-26', null, null, '1624120282299', '211163.00', '1', null, null, null);
INSERT INTO `anggota` VALUES ('81', 'Howard Armstrong', '1967-02-19', 'perempuan', '9638 Nunc Avenue', '094-659-8949', '1', '2016-06-06', null, null, '1645012978599', '165406.00', '1', null, null, null);
INSERT INTO `anggota` VALUES ('82', 'Paloma Jacobs', '1972-11-15', 'perempuan', 'P.O. Box 296, 9937 Id, Rd.', '078-552-5764', '1', '2016-08-09', null, null, '1668072675399', '219709.00', '1', null, null, null);
INSERT INTO `anggota` VALUES ('83', 'Angelica Hendricks', '1998-07-10', 'perempuan', '349-4359 Sagittis Street', '049-057-2906', '1', '2017-03-03', null, null, '1652082751499', '128189.00', '1', null, null, null);
INSERT INTO `anggota` VALUES ('84', 'Gemma V. Mullins', '1983-10-05', 'laki-laki', 'Ap #883-9531 Metus Rd.', '065-991-7237', '1', '2016-05-01', null, null, '1670072512899', '231780.00', '1', null, null, null);
INSERT INTO `anggota` VALUES ('85', 'Marcia Fischer', '1923-08-31', 'laki-laki', '2937 Non Rd.', '046-440-3290', '1', '2017-04-19', null, null, '1641031104999', '107893.00', '1', null, null, null);
INSERT INTO `anggota` VALUES ('86', 'Brynn U. Preston', '1916-06-18', 'perempuan', 'Ap #999-7797 Lorem Rd.', '068-104-1497', '1', '2016-09-18', null, null, '1689081677699', '147192.00', '1', null, null, null);
INSERT INTO `anggota` VALUES ('87', 'Lisandra X. Schneider', '1922-01-07', 'laki-laki', '760-2562 In, St.', '080-069-8790', '1', '2017-09-13', null, null, '1684080337499', '172563.00', '1', null, null, null);
INSERT INTO `anggota` VALUES ('88', 'Ainsley Price', '1943-02-17', 'perempuan', 'P.O. Box 156, 3517 Nulla Avenue', '041-966-2084', '1', '2017-05-26', null, null, '1648092086499', '155270.00', '1', null, null, null);
INSERT INTO `anggota` VALUES ('89', 'Gavin Collier', '2002-11-11', 'perempuan', 'P.O. Box 607, 8005 Mauris St.', '038-955-2066', '1', '2016-04-28', null, null, '1629122588999', '113782.00', '1', null, null, null);
INSERT INTO `anggota` VALUES ('90', 'Randall Z. Norris', '1940-04-19', 'perempuan', '8054 Tincidunt Road', '002-325-2023', '1', '2017-01-31', null, null, '1654083030699', '205685.00', '1', null, null, null);
INSERT INTO `anggota` VALUES ('91', 'Amaya Torres', '1953-09-02', 'perempuan', 'P.O. Box 335, 1349 Lobortis St.', '024-355-8753', '1', '2016-11-01', null, null, '1628060592499', '125351.00', '1', null, null, null);
INSERT INTO `anggota` VALUES ('92', 'Jelani Mann', '1966-11-30', 'perempuan', 'Ap #187-1854 Fringilla Rd.', '000-534-8943', '1', '2017-08-17', null, null, '1683120492299', '108171.00', '1', null, null, null);
INSERT INTO `anggota` VALUES ('93', 'Hiram Ratliff', '1991-08-30', 'laki-laki', '7340 Dictum St.', '062-146-6535', '1', '2016-01-24', null, null, '1675050928199', '189342.00', '1', null, null, null);
INSERT INTO `anggota` VALUES ('94', 'Clayton Maddox', '1929-04-26', 'perempuan', '731-7135 At, St.', '009-503-2945', '1', '2017-10-22', null, null, '1633080175199', '145996.00', '1', null, null, null);
INSERT INTO `anggota` VALUES ('95', 'Abraham Stark', '1948-03-08', 'laki-laki', 'Ap #211-4981 Consectetuer Ave', '038-284-1240', '1', '2016-11-25', null, null, '1654111783199', '239054.00', '1', null, null, null);
INSERT INTO `anggota` VALUES ('96', 'Damon Nieves', '1993-10-24', 'laki-laki', '9482 Sem, Rd.', '014-976-3210', '1', '2017-08-28', null, null, '1699052717499', '144754.00', '1', null, null, null);
INSERT INTO `anggota` VALUES ('97', 'Iona V. Bridges', '1997-10-30', 'perempuan', '295-2744 Facilisi. St.', '022-269-0715', '1', '2016-11-02', null, null, '1676031504599', '215701.00', '1', null, null, null);
INSERT INTO `anggota` VALUES ('98', 'Bethany M. Patrick', '1967-11-09', 'perempuan', 'P.O. Box 824, 9663 Tincidunt Ave', '002-042-2436', '1', '2017-03-01', null, null, '1636081117199', '217127.00', '1', null, null, null);
INSERT INTO `anggota` VALUES ('99', 'Tad K. Stark', '1961-11-30', 'perempuan', '933-7818 Sed St.', '019-978-6876', '1', '2016-05-13', null, null, '1686061000399', '152577.00', '1', null, null, null);
INSERT INTO `anggota` VALUES ('100', 'Casey J. Romero', '1985-04-03', 'perempuan', '950-9526 Mauris Avenue', '096-281-4015', '1', '2016-08-06', '2017-01-18', null, '1668061102399', '203971.00', '0', null, null, null);
INSERT INTO `anggota` VALUES ('101', 'Cain Murray', '1932-09-05', 'laki-laki', 'Ap #163-870 Augue Street', '006-318-2517', '1', '2017-08-22', '2017-01-09', null, '1690091699099', '222362.00', '1', 'dx', 'dx', null);
INSERT INTO `anggota` VALUES ('107', 'Rusli', '2017-01-01', 'Laki-Laki', 'Jl.asas sa', '576879809-09', '1', '2017-01-16', '0000-00-00', null, '34567890', '0.00', '1', 'dexter', 'dexter', null);

-- ----------------------------
-- Table structure for arus_kas
-- ----------------------------
DROP TABLE IF EXISTS `arus_kas`;
CREATE TABLE `arus_kas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `transaksi` varchar(255) DEFAULT NULL,
  `id_transaksi` int(11) DEFAULT NULL,
  `id_user` int(11) DEFAULT NULL,
  `waktu` datetime DEFAULT NULL,
  `masuk` decimal(15,2) DEFAULT NULL,
  `keluar` decimal(15,2) DEFAULT NULL,
  `keterangan` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of arus_kas
-- ----------------------------

-- ----------------------------
-- Table structure for auto_number
-- ----------------------------
DROP TABLE IF EXISTS `auto_number`;
CREATE TABLE `auto_number` (
  `group` varchar(10) DEFAULT NULL,
  `number` int(11) DEFAULT NULL,
  `lock_p` int(255) DEFAULT NULL,
  `update` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of auto_number
-- ----------------------------
INSERT INTO `auto_number` VALUES ('pinjaman', '1', null, null);

-- ----------------------------
-- Table structure for detail_shu
-- ----------------------------
DROP TABLE IF EXISTS `detail_shu`;
CREATE TABLE `detail_shu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_shu` int(11) DEFAULT NULL,
  `id_anggota` int(11) DEFAULT NULL,
  `anggota` decimal(15,2) DEFAULT NULL,
  `peminjam` decimal(15,2) DEFAULT NULL,
  `pengurus` decimal(15,2) DEFAULT NULL,
  `jumlah` decimal(15,2) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_detail_shu_shu_1` (`id_shu`),
  KEY `fk_detail_shu_anggota_1` (`id_anggota`),
  CONSTRAINT `fk_detail_shu_anggota_1` FOREIGN KEY (`id_anggota`) REFERENCES `anggota` (`id`),
  CONSTRAINT `fk_detail_shu_shu_1` FOREIGN KEY (`id_shu`) REFERENCES `shu` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of detail_shu
-- ----------------------------

-- ----------------------------
-- Table structure for hak_akses
-- ----------------------------
DROP TABLE IF EXISTS `hak_akses`;
CREATE TABLE `hak_akses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `hak_akses` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of hak_akses
-- ----------------------------
INSERT INTO `hak_akses` VALUES ('1', 'Administrator');
INSERT INTO `hak_akses` VALUES ('2', 'Anggota');
INSERT INTO `hak_akses` VALUES ('3', 'Ketua');
INSERT INTO `hak_akses` VALUES ('4', 'Bendahara');

-- ----------------------------
-- Table structure for hak_akses_anggota
-- ----------------------------
DROP TABLE IF EXISTS `hak_akses_anggota`;
CREATE TABLE `hak_akses_anggota` (
  `id_anggota` int(11) DEFAULT NULL,
  `id_hak_akses` int(11) DEFAULT NULL,
  KEY `fk_hak_akses_anggota_hak_akses_1` (`id_hak_akses`),
  KEY `fk_hak_akses_anggota_anggota_1` (`id_anggota`),
  CONSTRAINT `fk_hak_akses_anggota_anggota_1` FOREIGN KEY (`id_anggota`) REFERENCES `anggota` (`id`),
  CONSTRAINT `fk_hak_akses_anggota_hak_akses_1` FOREIGN KEY (`id_hak_akses`) REFERENCES `hak_akses` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of hak_akses_anggota
-- ----------------------------
INSERT INTO `hak_akses_anggota` VALUES ('16', '2');
INSERT INTO `hak_akses_anggota` VALUES ('2', '1');
INSERT INTO `hak_akses_anggota` VALUES ('2', '2');
INSERT INTO `hak_akses_anggota` VALUES ('2', '3');
INSERT INTO `hak_akses_anggota` VALUES ('12', '2');
INSERT INTO `hak_akses_anggota` VALUES ('12', '3');

-- ----------------------------
-- Table structure for jabatan
-- ----------------------------
DROP TABLE IF EXISTS `jabatan`;
CREATE TABLE `jabatan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `jenis_jabatan` varchar(10) DEFAULT NULL,
  `nama_jabatan` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_jabatan_persentase_shu_1` (`jenis_jabatan`),
  CONSTRAINT `fk_jabatan_persentase_shu_1` FOREIGN KEY (`jenis_jabatan`) REFERENCES `persentase_shu` (`jenis_jabatan`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of jabatan
-- ----------------------------
INSERT INTO `jabatan` VALUES ('1', 'anggota', 'Anggota');
INSERT INTO `jabatan` VALUES ('2', 'pengurus', 'Ketua');
INSERT INTO `jabatan` VALUES ('3', 'pengurus', 'Bendahara');

-- ----------------------------
-- Table structure for jenis_simpanan
-- ----------------------------
DROP TABLE IF EXISTS `jenis_simpanan`;
CREATE TABLE `jenis_simpanan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kode_simpanan` varchar(5) DEFAULT NULL,
  `jenis_simpanan` varchar(50) DEFAULT NULL,
  `jumlah` decimal(15,2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of jenis_simpanan
-- ----------------------------
INSERT INTO `jenis_simpanan` VALUES ('1', 'wajib', 'Simpanan Wajib', '25000.00');

-- ----------------------------
-- Table structure for menu
-- ----------------------------
DROP TABLE IF EXISTS `menu`;
CREATE TABLE `menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `link` varchar(255) DEFAULT NULL,
  `icon` varchar(50) DEFAULT NULL,
  `is_active` int(1) DEFAULT NULL,
  `is_parent` int(1) DEFAULT NULL,
  `akses` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of menu
-- ----------------------------
INSERT INTO `menu` VALUES ('1', 'Anggota', 'admin/anggota', 'fa fa-users', '1', '0', null);
INSERT INTO `menu` VALUES ('2', 'Peminjaman', 'admin/peminjaman', 'fa fa-download', '1', '0', null);
INSERT INTO `menu` VALUES ('3', 'Pembayaran', 'admin/pembayaran', 'fa fa-upload', '1', '0', null);
INSERT INTO `menu` VALUES ('4', 'Simpanan Wajib', 'admin/simpanan_wajib', 'fa fa-cloud-download', '1', '0', null);
INSERT INTO `menu` VALUES ('12', 'SHU', 'admin/shu', 'fa  fa-balance-scale', '1', '0', null);
INSERT INTO `menu` VALUES ('13', 'Pengaturan', null, 'fa  fa-gears', '1', '0', null);
INSERT INTO `menu` VALUES ('14', 'Hak Akses', 'admin/hak_akses_anggota', 'fa  fa-mortar-board', '1', '13', null);

-- ----------------------------
-- Table structure for pembayaran
-- ----------------------------
DROP TABLE IF EXISTS `pembayaran`;
CREATE TABLE `pembayaran` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_peminjaman` int(11) DEFAULT NULL,
  `tanggal_bayar` date DEFAULT NULL,
  `tanggal_batas` date DEFAULT NULL,
  `jumlah_bayar` decimal(15,2) DEFAULT NULL,
  `bunga` decimal(15,2) DEFAULT NULL,
  `pembayaran_ke` int(11) DEFAULT NULL,
  `keterangan` text,
  `status` int(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `fk_pembayaran_peminjaman_1` (`id_peminjaman`),
  CONSTRAINT `fk_pembayaran_peminjaman_1` FOREIGN KEY (`id_peminjaman`) REFERENCES `peminjaman` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=222 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of pembayaran
-- ----------------------------
INSERT INTO `pembayaran` VALUES ('172', '9', '2017-01-18', '2016-02-01', '250000.00', '2500.00', '1', null, '1');
INSERT INTO `pembayaran` VALUES ('173', '9', '2017-01-16', '2016-03-01', '250000.00', '2500.00', '2', null, '1');
INSERT INTO `pembayaran` VALUES ('174', '9', '2017-01-16', '2016-04-01', '250000.00', '2500.00', '3', null, '1');
INSERT INTO `pembayaran` VALUES ('175', '9', '2017-01-16', '2016-05-01', '250000.00', '2500.00', '4', null, '1');
INSERT INTO `pembayaran` VALUES ('176', '9', '2017-01-16', '2016-06-01', '250000.00', '2500.00', '5', null, '1');
INSERT INTO `pembayaran` VALUES ('177', '9', '2017-01-16', '2016-07-01', '250000.00', '2500.00', '6', null, '1');
INSERT INTO `pembayaran` VALUES ('178', '9', '2017-01-16', '2016-08-01', '250000.00', '2500.00', '7', null, '1');
INSERT INTO `pembayaran` VALUES ('179', '9', null, '2016-09-01', '250000.00', '2500.00', '8', null, '0');
INSERT INTO `pembayaran` VALUES ('180', '9', null, '2016-10-01', '250000.00', '2500.00', '9', null, '0');
INSERT INTO `pembayaran` VALUES ('181', '9', null, '2016-11-01', '250000.00', '2500.00', '10', null, '0');
INSERT INTO `pembayaran` VALUES ('182', '9', null, '2016-12-01', '250000.00', '2500.00', '11', null, '0');
INSERT INTO `pembayaran` VALUES ('183', '9', null, '2017-01-01', '250000.00', '2500.00', '12', null, '0');
INSERT INTO `pembayaran` VALUES ('184', '9', null, '2017-02-01', '250000.00', '2500.00', '13', null, '0');
INSERT INTO `pembayaran` VALUES ('185', '9', null, '2017-03-01', '250000.00', '2500.00', '14', null, '0');
INSERT INTO `pembayaran` VALUES ('186', '9', null, '2017-04-01', '250000.00', '2500.00', '15', null, '0');
INSERT INTO `pembayaran` VALUES ('187', '9', null, '2017-05-01', '250000.00', '2500.00', '16', null, '0');
INSERT INTO `pembayaran` VALUES ('188', '9', null, '2017-06-01', '250000.00', '2500.00', '17', null, '0');
INSERT INTO `pembayaran` VALUES ('189', '9', null, '2017-07-01', '250000.00', '2500.00', '18', null, '0');
INSERT INTO `pembayaran` VALUES ('190', '9', null, '2017-08-01', '250000.00', '2500.00', '19', null, '0');
INSERT INTO `pembayaran` VALUES ('191', '9', null, '2017-09-01', '250000.00', '2500.00', '20', null, '0');
INSERT INTO `pembayaran` VALUES ('192', '9', null, '2017-10-01', '250000.00', '2500.00', '21', null, '0');
INSERT INTO `pembayaran` VALUES ('193', '9', null, '2017-11-01', '250000.00', '2500.00', '22', null, '0');
INSERT INTO `pembayaran` VALUES ('194', '9', null, '2017-12-01', '250000.00', '2500.00', '23', null, '0');
INSERT INTO `pembayaran` VALUES ('195', '9', null, '2018-01-01', '250000.00', '2500.00', '24', null, '0');
INSERT INTO `pembayaran` VALUES ('217', '13', null, '2017-02-10', '100000.00', '1000.00', '1', null, '0');
INSERT INTO `pembayaran` VALUES ('218', '13', null, '2017-03-10', '100000.00', '1000.00', '2', null, '0');
INSERT INTO `pembayaran` VALUES ('219', '13', null, '2017-04-10', '100000.00', '1000.00', '3', null, '0');
INSERT INTO `pembayaran` VALUES ('220', '13', null, '2017-05-10', '100000.00', '1000.00', '4', null, '0');
INSERT INTO `pembayaran` VALUES ('221', '13', null, '2017-06-10', '100000.00', '1000.00', '5', null, '0');

-- ----------------------------
-- Table structure for peminjaman
-- ----------------------------
DROP TABLE IF EXISTS `peminjaman`;
CREATE TABLE `peminjaman` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_anggota` int(11) DEFAULT NULL,
  `no_pinjaman` varchar(20) DEFAULT NULL,
  `tgl_pinjaman` date DEFAULT NULL,
  `jumlah_pinjaman` decimal(15,2) DEFAULT NULL,
  `bunga` decimal(15,2) DEFAULT NULL,
  `total_pinjaman` decimal(15,2) DEFAULT NULL COMMENT 'lunas\r\nbelum lunas',
  `status` int(1) DEFAULT '0',
  `input_oleh` int(11) DEFAULT NULL,
  `tgl_input` datetime DEFAULT NULL,
  `update_oleh` int(11) DEFAULT NULL,
  `tgl_update` datetime DEFAULT NULL,
  `sisa_pembayaran` decimal(15,2) DEFAULT NULL,
  `kali_bayar` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_peminjaman_anggota_1` (`id_anggota`),
  CONSTRAINT `fk_peminjaman_anggota_1` FOREIGN KEY (`id_anggota`) REFERENCES `anggota` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of peminjaman
-- ----------------------------
INSERT INTO `peminjaman` VALUES ('9', '2', 'PINJ0120170000000001', '2016-01-01', '6000000.00', '60000.00', '6060000.00', '0', null, '2017-01-16 13:35:41', null, '2017-01-16 13:35:41', '6060000.00', '24');
INSERT INTO `peminjaman` VALUES ('13', '6', 'PINJ0120170000000002', '2017-01-10', '500000.00', '5000.00', '505000.00', '0', '2', '2017-01-19 05:40:19', '2', '2017-01-19 05:40:19', '505000.00', '5');

-- ----------------------------
-- Table structure for persentase_shu
-- ----------------------------
DROP TABLE IF EXISTS `persentase_shu`;
CREATE TABLE `persentase_shu` (
  `jenis_jabatan` varchar(10) NOT NULL,
  `persentase_shu` decimal(15,2) DEFAULT NULL,
  PRIMARY KEY (`jenis_jabatan`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of persentase_shu
-- ----------------------------
INSERT INTO `persentase_shu` VALUES ('anggota', '50.00');
INSERT INTO `persentase_shu` VALUES ('modal', '20.00');
INSERT INTO `persentase_shu` VALUES ('peminjam', '20.00');
INSERT INTO `persentase_shu` VALUES ('pengurus', '10.00');

-- ----------------------------
-- Table structure for shu
-- ----------------------------
DROP TABLE IF EXISTS `shu`;
CREATE TABLE `shu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `periode` int(4) DEFAULT NULL,
  `tahun` int(5) DEFAULT NULL,
  `laba_tahunan` double(15,2) DEFAULT NULL,
  `tanggal_generate` datetime DEFAULT NULL,
  `generate_oleh` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of shu
-- ----------------------------
INSERT INTO `shu` VALUES ('1', '2017', null, '0.00', '2017-01-18 01:01:06', '2');

-- ----------------------------
-- Table structure for simpanan_wajib
-- ----------------------------
DROP TABLE IF EXISTS `simpanan_wajib`;
CREATE TABLE `simpanan_wajib` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_anggota` int(11) DEFAULT NULL,
  `tgl_bayar` date DEFAULT NULL,
  `jumlah_bayar` double(15,2) DEFAULT NULL,
  `status_pembayaran` int(1) DEFAULT NULL,
  `periode` int(11) DEFAULT NULL,
  `tahun` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_simpanan_wajib_anggota_1` (`id_anggota`),
  CONSTRAINT `fk_simpanan_wajib_anggota_1` FOREIGN KEY (`id_anggota`) REFERENCES `anggota` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=101 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of simpanan_wajib
-- ----------------------------
INSERT INTO `simpanan_wajib` VALUES ('1', '2', null, '25000.00', '0', '2', '2017');
INSERT INTO `simpanan_wajib` VALUES ('2', '3', null, '25000.00', '0', '2', '2017');
INSERT INTO `simpanan_wajib` VALUES ('3', '4', null, '25000.00', '0', '2', '2017');
INSERT INTO `simpanan_wajib` VALUES ('4', '5', null, '25000.00', '0', '2', '2017');
INSERT INTO `simpanan_wajib` VALUES ('5', '6', null, '25000.00', '0', '2', '2017');
INSERT INTO `simpanan_wajib` VALUES ('6', '7', null, '25000.00', '0', '2', '2017');
INSERT INTO `simpanan_wajib` VALUES ('7', '8', null, '25000.00', '0', '2', '2017');
INSERT INTO `simpanan_wajib` VALUES ('8', '9', null, '25000.00', '0', '2', '2017');
INSERT INTO `simpanan_wajib` VALUES ('9', '10', null, '25000.00', '0', '2', '2017');
INSERT INTO `simpanan_wajib` VALUES ('10', '11', null, '25000.00', '0', '2', '2017');
INSERT INTO `simpanan_wajib` VALUES ('11', '12', null, '25000.00', '0', '2', '2017');
INSERT INTO `simpanan_wajib` VALUES ('12', '13', null, '25000.00', '0', '2', '2017');
INSERT INTO `simpanan_wajib` VALUES ('13', '14', null, '25000.00', '0', '2', '2017');
INSERT INTO `simpanan_wajib` VALUES ('14', '15', null, '25000.00', '0', '2', '2017');
INSERT INTO `simpanan_wajib` VALUES ('15', '16', null, '25000.00', '0', '2', '2017');
INSERT INTO `simpanan_wajib` VALUES ('16', '17', null, '25000.00', '0', '2', '2017');
INSERT INTO `simpanan_wajib` VALUES ('17', '18', null, '25000.00', '0', '2', '2017');
INSERT INTO `simpanan_wajib` VALUES ('18', '19', null, '25000.00', '0', '2', '2017');
INSERT INTO `simpanan_wajib` VALUES ('19', '20', null, '25000.00', '0', '2', '2017');
INSERT INTO `simpanan_wajib` VALUES ('20', '21', null, '25000.00', '0', '2', '2017');
INSERT INTO `simpanan_wajib` VALUES ('21', '22', null, '25000.00', '0', '2', '2017');
INSERT INTO `simpanan_wajib` VALUES ('22', '23', null, '25000.00', '0', '2', '2017');
INSERT INTO `simpanan_wajib` VALUES ('23', '24', null, '25000.00', '0', '2', '2017');
INSERT INTO `simpanan_wajib` VALUES ('24', '25', null, '25000.00', '0', '2', '2017');
INSERT INTO `simpanan_wajib` VALUES ('25', '26', null, '25000.00', '0', '2', '2017');
INSERT INTO `simpanan_wajib` VALUES ('26', '27', null, '25000.00', '0', '2', '2017');
INSERT INTO `simpanan_wajib` VALUES ('27', '28', null, '25000.00', '0', '2', '2017');
INSERT INTO `simpanan_wajib` VALUES ('28', '29', null, '25000.00', '0', '2', '2017');
INSERT INTO `simpanan_wajib` VALUES ('29', '30', null, '25000.00', '0', '2', '2017');
INSERT INTO `simpanan_wajib` VALUES ('30', '31', null, '25000.00', '0', '2', '2017');
INSERT INTO `simpanan_wajib` VALUES ('31', '32', null, '25000.00', '0', '2', '2017');
INSERT INTO `simpanan_wajib` VALUES ('32', '33', null, '25000.00', '0', '2', '2017');
INSERT INTO `simpanan_wajib` VALUES ('33', '34', null, '25000.00', '0', '2', '2017');
INSERT INTO `simpanan_wajib` VALUES ('34', '35', null, '25000.00', '0', '2', '2017');
INSERT INTO `simpanan_wajib` VALUES ('35', '36', null, '25000.00', '0', '2', '2017');
INSERT INTO `simpanan_wajib` VALUES ('36', '37', null, '25000.00', '0', '2', '2017');
INSERT INTO `simpanan_wajib` VALUES ('37', '38', null, '25000.00', '0', '2', '2017');
INSERT INTO `simpanan_wajib` VALUES ('38', '39', null, '25000.00', '0', '2', '2017');
INSERT INTO `simpanan_wajib` VALUES ('39', '40', null, '25000.00', '0', '2', '2017');
INSERT INTO `simpanan_wajib` VALUES ('40', '41', null, '25000.00', '0', '2', '2017');
INSERT INTO `simpanan_wajib` VALUES ('41', '42', null, '25000.00', '0', '2', '2017');
INSERT INTO `simpanan_wajib` VALUES ('42', '43', null, '25000.00', '0', '2', '2017');
INSERT INTO `simpanan_wajib` VALUES ('43', '44', null, '25000.00', '0', '2', '2017');
INSERT INTO `simpanan_wajib` VALUES ('44', '45', null, '25000.00', '0', '2', '2017');
INSERT INTO `simpanan_wajib` VALUES ('45', '46', null, '25000.00', '0', '2', '2017');
INSERT INTO `simpanan_wajib` VALUES ('46', '47', null, '25000.00', '0', '2', '2017');
INSERT INTO `simpanan_wajib` VALUES ('47', '48', null, '25000.00', '0', '2', '2017');
INSERT INTO `simpanan_wajib` VALUES ('48', '49', null, '25000.00', '0', '2', '2017');
INSERT INTO `simpanan_wajib` VALUES ('49', '50', null, '25000.00', '0', '2', '2017');
INSERT INTO `simpanan_wajib` VALUES ('50', '51', null, '25000.00', '0', '2', '2017');
INSERT INTO `simpanan_wajib` VALUES ('51', '52', null, '25000.00', '0', '2', '2017');
INSERT INTO `simpanan_wajib` VALUES ('52', '53', null, '25000.00', '0', '2', '2017');
INSERT INTO `simpanan_wajib` VALUES ('53', '54', null, '25000.00', '0', '2', '2017');
INSERT INTO `simpanan_wajib` VALUES ('54', '55', null, '25000.00', '0', '2', '2017');
INSERT INTO `simpanan_wajib` VALUES ('55', '56', null, '25000.00', '0', '2', '2017');
INSERT INTO `simpanan_wajib` VALUES ('56', '57', null, '25000.00', '0', '2', '2017');
INSERT INTO `simpanan_wajib` VALUES ('57', '58', null, '25000.00', '0', '2', '2017');
INSERT INTO `simpanan_wajib` VALUES ('58', '59', null, '25000.00', '0', '2', '2017');
INSERT INTO `simpanan_wajib` VALUES ('59', '60', null, '25000.00', '0', '2', '2017');
INSERT INTO `simpanan_wajib` VALUES ('60', '61', null, '25000.00', '0', '2', '2017');
INSERT INTO `simpanan_wajib` VALUES ('61', '62', null, '25000.00', '0', '2', '2017');
INSERT INTO `simpanan_wajib` VALUES ('62', '63', null, '25000.00', '0', '2', '2017');
INSERT INTO `simpanan_wajib` VALUES ('63', '64', null, '25000.00', '0', '2', '2017');
INSERT INTO `simpanan_wajib` VALUES ('64', '65', null, '25000.00', '0', '2', '2017');
INSERT INTO `simpanan_wajib` VALUES ('65', '66', null, '25000.00', '0', '2', '2017');
INSERT INTO `simpanan_wajib` VALUES ('66', '67', null, '25000.00', '0', '2', '2017');
INSERT INTO `simpanan_wajib` VALUES ('67', '68', null, '25000.00', '0', '2', '2017');
INSERT INTO `simpanan_wajib` VALUES ('68', '69', null, '25000.00', '0', '2', '2017');
INSERT INTO `simpanan_wajib` VALUES ('69', '70', null, '25000.00', '0', '2', '2017');
INSERT INTO `simpanan_wajib` VALUES ('70', '71', null, '25000.00', '0', '2', '2017');
INSERT INTO `simpanan_wajib` VALUES ('71', '72', '2017-01-19', '25000.00', '1', '2', '2017');
INSERT INTO `simpanan_wajib` VALUES ('72', '73', '2017-01-19', '25000.00', '1', '2', '2017');
INSERT INTO `simpanan_wajib` VALUES ('73', '74', '2017-01-19', '25000.00', '1', '2', '2017');
INSERT INTO `simpanan_wajib` VALUES ('74', '75', '2017-01-19', '25000.00', '1', '2', '2017');
INSERT INTO `simpanan_wajib` VALUES ('75', '76', '2017-01-19', '25000.00', '1', '2', '2017');
INSERT INTO `simpanan_wajib` VALUES ('76', '77', '2017-01-19', '25000.00', '1', '2', '2017');
INSERT INTO `simpanan_wajib` VALUES ('77', '78', '2017-01-19', '25000.00', '1', '2', '2017');
INSERT INTO `simpanan_wajib` VALUES ('78', '79', '2017-01-19', '25000.00', '1', '2', '2017');
INSERT INTO `simpanan_wajib` VALUES ('79', '80', '2017-01-19', '25000.00', '1', '2', '2017');
INSERT INTO `simpanan_wajib` VALUES ('80', '81', '2017-01-19', '25000.00', '1', '2', '2017');
INSERT INTO `simpanan_wajib` VALUES ('81', '82', '2017-01-19', '25000.00', '1', '2', '2017');
INSERT INTO `simpanan_wajib` VALUES ('82', '83', '2017-01-19', '25000.00', '1', '2', '2017');
INSERT INTO `simpanan_wajib` VALUES ('83', '84', '2017-01-19', '25000.00', '1', '2', '2017');
INSERT INTO `simpanan_wajib` VALUES ('84', '85', '2017-01-19', '25000.00', '1', '2', '2017');
INSERT INTO `simpanan_wajib` VALUES ('85', '86', '2017-01-19', '25000.00', '1', '2', '2017');
INSERT INTO `simpanan_wajib` VALUES ('86', '87', '2017-01-19', '25000.00', '1', '2', '2017');
INSERT INTO `simpanan_wajib` VALUES ('87', '88', '2017-01-19', '25000.00', '1', '2', '2017');
INSERT INTO `simpanan_wajib` VALUES ('88', '89', '2017-01-19', '25000.00', '1', '2', '2017');
INSERT INTO `simpanan_wajib` VALUES ('89', '90', '2017-01-19', '25000.00', '1', '2', '2017');
INSERT INTO `simpanan_wajib` VALUES ('90', '91', '2017-01-19', '25000.00', '1', '2', '2017');
INSERT INTO `simpanan_wajib` VALUES ('91', '92', '2017-01-19', '25000.00', '1', '2', '2017');
INSERT INTO `simpanan_wajib` VALUES ('92', '93', '2017-01-19', '25000.00', '1', '2', '2017');
INSERT INTO `simpanan_wajib` VALUES ('93', '94', '2017-01-19', '25000.00', '1', '2', '2017');
INSERT INTO `simpanan_wajib` VALUES ('94', '95', '2017-01-19', '25000.00', '1', '2', '2017');
INSERT INTO `simpanan_wajib` VALUES ('95', '96', '2017-01-19', '25000.00', '1', '2', '2017');
INSERT INTO `simpanan_wajib` VALUES ('96', '97', '2017-01-19', '25000.00', '1', '2', '2017');
INSERT INTO `simpanan_wajib` VALUES ('97', '98', '2017-01-19', '25000.00', '1', '2', '2017');
INSERT INTO `simpanan_wajib` VALUES ('98', '99', '2017-01-19', '25000.00', '1', '2', '2017');
INSERT INTO `simpanan_wajib` VALUES ('99', '101', '2017-01-19', '25000.00', '1', '2', '2017');
INSERT INTO `simpanan_wajib` VALUES ('100', '107', '2017-01-19', '25000.00', '1', '2', '2017');

-- ----------------------------
-- Table structure for transaksi_rutin
-- ----------------------------
DROP TABLE IF EXISTS `transaksi_rutin`;
CREATE TABLE `transaksi_rutin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `transaksi` varchar(100) DEFAULT NULL,
  `id_transaksi` int(11) DEFAULT NULL,
  `tanggal_transaksi` date DEFAULT NULL,
  `masuk` decimal(15,2) DEFAULT '0.00',
  `keluar` decimal(15,2) DEFAULT '0.00',
  `keterangan` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of transaksi_rutin
-- ----------------------------
INSERT INTO `transaksi_rutin` VALUES ('1', 'Peminjaman Anggota', '10', '2017-01-19', '0.00', '1010000.00', null);
INSERT INTO `transaksi_rutin` VALUES ('7', 'Pembayaran Simpanan Wajib', '100', '2017-01-19', '25000.00', '0.00', null);
INSERT INTO `transaksi_rutin` VALUES ('8', 'Pembayaran Simpanan Wajib', '99', '2017-01-19', '25000.00', '0.00', null);
INSERT INTO `transaksi_rutin` VALUES ('9', 'Pembayaran Simpanan Wajib', '98', '2017-01-19', '25000.00', '0.00', null);
INSERT INTO `transaksi_rutin` VALUES ('10', 'Pembayaran Simpanan Wajib', '97', '2017-01-19', '25000.00', '0.00', null);
INSERT INTO `transaksi_rutin` VALUES ('11', 'Peminjaman Anggota', '13', '2017-01-19', '0.00', '505000.00', null);
INSERT INTO `transaksi_rutin` VALUES ('12', 'Pembayaran Simpanan Wajib', '96', '2017-01-19', '25000.00', '0.00', null);
INSERT INTO `transaksi_rutin` VALUES ('13', 'Pembayaran Simpanan Wajib', '95', '2017-01-19', '25000.00', '0.00', null);
INSERT INTO `transaksi_rutin` VALUES ('14', 'Pembayaran Simpanan Wajib', '94', '2017-01-19', '25000.00', '0.00', null);
INSERT INTO `transaksi_rutin` VALUES ('15', 'Pembayaran Simpanan Wajib', '93', '2017-01-19', '25000.00', '0.00', null);
INSERT INTO `transaksi_rutin` VALUES ('16', 'Pembayaran Simpanan Wajib', '92', '2017-01-19', '25000.00', '0.00', null);
INSERT INTO `transaksi_rutin` VALUES ('17', 'Pembayaran Simpanan Wajib', '91', '2017-01-19', '25000.00', '0.00', null);
INSERT INTO `transaksi_rutin` VALUES ('18', 'Pembayaran Simpanan Wajib', '90', '2017-01-19', '25000.00', '0.00', null);
INSERT INTO `transaksi_rutin` VALUES ('19', 'Pembayaran Simpanan Wajib', '89', '2017-01-19', '25000.00', '0.00', null);
INSERT INTO `transaksi_rutin` VALUES ('20', 'Pembayaran Simpanan Wajib', '88', '2017-01-19', '25000.00', '0.00', null);
INSERT INTO `transaksi_rutin` VALUES ('21', 'Pembayaran Simpanan Wajib', '87', '2017-01-19', '25000.00', '0.00', null);
INSERT INTO `transaksi_rutin` VALUES ('22', 'Pembayaran Simpanan Wajib', '86', '2017-01-19', '25000.00', '0.00', null);
INSERT INTO `transaksi_rutin` VALUES ('23', 'Pembayaran Simpanan Wajib', '85', '2017-01-19', '25000.00', '0.00', null);
INSERT INTO `transaksi_rutin` VALUES ('24', 'Pembayaran Simpanan Wajib', '84', '2017-01-19', '25000.00', '0.00', null);
INSERT INTO `transaksi_rutin` VALUES ('25', 'Pembayaran Simpanan Wajib', '83', '2017-01-19', '25000.00', '0.00', null);
INSERT INTO `transaksi_rutin` VALUES ('26', 'Pembayaran Simpanan Wajib', '82', '2017-01-19', '25000.00', '0.00', null);
INSERT INTO `transaksi_rutin` VALUES ('27', 'Pembayaran Simpanan Wajib', '81', '2017-01-19', '25000.00', '0.00', null);
INSERT INTO `transaksi_rutin` VALUES ('28', 'Pembayaran Simpanan Wajib', '80', '2017-01-19', '25000.00', '0.00', null);
INSERT INTO `transaksi_rutin` VALUES ('29', 'Pembayaran Simpanan Wajib', '79', '2017-01-19', '25000.00', '0.00', null);
INSERT INTO `transaksi_rutin` VALUES ('30', 'Pembayaran Simpanan Wajib', '78', '2017-01-19', '25000.00', '0.00', null);
INSERT INTO `transaksi_rutin` VALUES ('31', 'Pembayaran Simpanan Wajib', '77', '2017-01-19', '25000.00', '0.00', null);
INSERT INTO `transaksi_rutin` VALUES ('32', 'Pembayaran Simpanan Wajib', '76', '2017-01-19', '25000.00', '0.00', null);
INSERT INTO `transaksi_rutin` VALUES ('33', 'Pembayaran Simpanan Wajib', '75', '2017-01-19', '25000.00', '0.00', null);
INSERT INTO `transaksi_rutin` VALUES ('34', 'Pembayaran Simpanan Wajib', '74', '2017-01-19', '25000.00', '0.00', null);
INSERT INTO `transaksi_rutin` VALUES ('35', 'Pembayaran Simpanan Wajib', '73', '2017-01-19', '25000.00', '0.00', null);
INSERT INTO `transaksi_rutin` VALUES ('36', 'Pembayaran Simpanan Wajib', '72', '2017-01-19', '25000.00', '0.00', null);
INSERT INTO `transaksi_rutin` VALUES ('37', 'Pembayaran Simpanan Wajib', '71', '2017-01-19', '25000.00', '0.00', null);

-- ----------------------------
-- Table structure for tunggakan
-- ----------------------------
DROP TABLE IF EXISTS `tunggakan`;
CREATE TABLE `tunggakan` (
  `id_pinjaman` int(11) DEFAULT NULL,
  `jumlah_tunggakan` decimal(15,2) DEFAULT NULL,
  `tanggal` date DEFAULT NULL,
  `status` int(11) DEFAULT '0',
  `tanggal_bayar` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tunggakan
-- ----------------------------

-- ----------------------------
-- View structure for count_all_tunggakan
-- ----------------------------
DROP VIEW IF EXISTS `count_all_tunggakan`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER  VIEW `count_all_tunggakan` AS SELECT
(SELECT
Count(A.id)
from 
simpanan_wajib as A
INNER JOIN anggota as B on A.id_anggota = B.id
WHERE 
-- A.periode <= MONTH(now()) AND A.tahun <= YEAR(now()) AND 
A.status_pembayaran = 0) as simpanan_wajib
,
(SElect count(A.id) from pembayaran as A
WHERE A.tanggal_batas <= now() and A.`status` = 0) as peminjaman ;

-- ----------------------------
-- View structure for v_daftar_akses_anggota
-- ----------------------------
DROP VIEW IF EXISTS `v_daftar_akses_anggota`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost`  VIEW `v_daftar_akses_anggota` AS SELECT * FROM anggota
INNER JOIN hak_akses_anggota on anggota.id = hak_akses_anggota.id_anggota
GROUP BY anggota.id ;

-- ----------------------------
-- View structure for v_get_access
-- ----------------------------
DROP VIEW IF EXISTS `v_get_access`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER  VIEW `v_get_access` AS SELECT
B.id_anggota,
C.nama,
B.id_hak_akses,
A.hak_akses
FROM
hak_akses AS A
INNER JOIN hak_akses_anggota AS B ON B.id_hak_akses = A.id
INNER JOIN anggota AS C ON B.id_anggota = C.id ;

-- ----------------------------
-- View structure for v_get_daftar_shu_anggota
-- ----------------------------
DROP VIEW IF EXISTS `v_get_daftar_shu_anggota`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost`  VIEW `v_get_daftar_shu_anggota` AS SELECT A.*, B.periode, B.tahun, C.nama
FROM
detail_shu AS A
INNER JOIN shu AS B ON A.id_shu = B.id
INNER JOIN anggota AS C ON A.id_anggota = C.id ;

-- ----------------------------
-- View structure for v_get_tunggakan
-- ----------------------------
DROP VIEW IF EXISTS `v_get_tunggakan`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER  VIEW `v_get_tunggakan` AS SELECT * from pembayaran WHERE tanggal_batas <= now() and `status` = 0 ;

-- ----------------------------
-- View structure for v_get_tunggakan_pembayaran
-- ----------------------------
DROP VIEW IF EXISTS `v_get_tunggakan_pembayaran`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER  VIEW `v_get_tunggakan_pembayaran` AS SELECT
A.id,
A.id_peminjaman,
B.no_pinjaman,
B.id_anggota,
C.nama as nama_anggota,
A.tanggal_bayar,
A.tanggal_batas,
A.jumlah_bayar,
A.bunga,
A.pembayaran_ke,
A.keterangan,
A.`status`
from 
pembayaran as A
INNER JOIN peminjaman as B on A.id_peminjaman = B.id
INNER JOIN anggota as C on B.id_anggota = C.id
WHERE A.tanggal_batas <= now() and A.`status` = 0 ;

-- ----------------------------
-- View structure for v_list_total_tunggakan
-- ----------------------------
DROP VIEW IF EXISTS `v_list_total_tunggakan`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost`  VIEW `v_list_total_tunggakan` AS SELECT 
	id_anggota,
	no_pinjaman,
	count(`status`) kali_tunggakan, 
	sum(jumlah_bayar) total_jml_bayar, 
	sum(bunga) total_jml_bunga, 
	(sum(jumlah_bayar)+sum(bunga)) total
	FROM v_get_tunggakan_pembayaran 
	GROUP BY id_anggota ;

-- ----------------------------
-- View structure for v_pinjaman_aktif
-- ----------------------------
DROP VIEW IF EXISTS `v_pinjaman_aktif`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost`  VIEW `v_pinjaman_aktif` AS SELECT * from peminjaman WHERE `status` = 0 ;

-- ----------------------------
-- View structure for v_status_pinjaman
-- ----------------------------
DROP VIEW IF EXISTS `v_status_pinjaman`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost`  VIEW `v_status_pinjaman` AS SELECT
A.id_peminjaman,
B.no_pinjaman,
B.tgl_pinjaman,
B.id_anggota,
C.nama,
B.total_pinjaman,
sum(CASE WHEN A.`status` = 0 THEN 1 ELSE 0 end) as jml_belum_bayar,
sum(CASE WHEN A.`status` = 1 THEN 1 ELSE 0 end) as jml_sudah_bayar,
sum(CASE WHEN A.`status` = 0 THEN A.jumlah_bayar + A.bunga ELSE 0 end) as total_belum_bayar,
sum(CASE WHEN A.`status` = 1 THEN A.jumlah_bayar + A.bunga ELSE 0 end) as total_sudah_bayar,
(CASE WHEN B.`status` = 1 THEN 'Lunas' ELSE 'Belum Lunas' end) as `status`
from pembayaran AS A
INNER JOIN peminjaman AS B on A.id_peminjaman = B.id
INNER JOIN anggota AS C on B.id_anggota = C.id
GROUP BY A.id_peminjaman ;

-- ----------------------------
-- Procedure structure for generate_pembayaran
-- ----------------------------
DROP PROCEDURE IF EXISTS `generate_pembayaran`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `generate_pembayaran`(`id_pinjaman` int,`jumlah_pinjaman` decimal,`bunga` decimal,`kali_bayar` int, tgl_pinjaman date)
BEGIN
	declare xx int;	
	declare split_jml_pinjaman decimal(15,2);
	declare split_bunga decimal(15,2);
	
	set xx = 1;
	set split_jml_pinjaman = jumlah_pinjaman / kali_bayar;
	set split_bunga = bunga / kali_bayar;
	
	while xx <=kali_bayar DO
		-- set tgl_batas = SELECT now() + INTERVAL xx MONTH;
		INSERT INTO pembayaran(id_peminjaman,jumlah_bayar,bunga,pembayaran_ke, tanggal_batas)VALUES(id_pinjaman,split_jml_pinjaman,split_bunga,xx,(SELECT tgl_pinjaman + INTERVAL xx MONTH));
		SET xx= xx+1;
	end while;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for p_generateSimpananW
-- ----------------------------
DROP PROCEDURE IF EXISTS `p_generateSimpananW`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `p_generateSimpananW`()
BEGIN
	DECLARE jumlahGen INT;
	declare jumlah_simpanan decimal(15,2);
	
	SELECT count(*) INTO jumlahGen from simpanan_wajib where periode = MONTH(NOW()) and tahun = YEAR(now());
	
	if(jumlahGen < 1) then
		select jumlah into jumlah_simpanan from jenis_simpanan where kode_simpanan = "wajib" limit 1;
		INSERT INTO simpanan_wajib(id_anggota,jumlah_bayar,status_pembayaran,periode,tahun)
		SELECT id,jumlah_simpanan,0,MONTH(NOW()),YEAR(now()) FROM anggota;
	end if;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for p_get_total_tunggakan_per_pelanggan
-- ----------------------------
DROP PROCEDURE IF EXISTS `p_get_total_tunggakan_per_pelanggan`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `p_get_total_tunggakan_per_pelanggan`(`anggota_id` int)
BEGIN
	SELECT 
	count(`status`) kali_tunggakan, 
	sum(jumlah_bayar) total_jml_bayar, 
	sum(bunga) total_jml_bunga, 
	(sum(jumlah_bayar)+sum(bunga)) total
 
	FROM v_get_tunggakan_pembayaran 
	WHERE id_anggota = anggota_id
	GROUP BY id_peminjaman;

END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for p_trans_rutin_del
-- ----------------------------
DROP PROCEDURE IF EXISTS `p_trans_rutin_del`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `p_trans_rutin_del`(`id_trans` int(11),`jns` varchar(255))
BEGIN
	DELETE FROM transaksi_rutin WHERE id = id_trans AND transaksi = jns;

END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for p_trans_rutin_in
-- ----------------------------
DROP PROCEDURE IF EXISTS `p_trans_rutin_in`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `p_trans_rutin_in`(`id_trans` int(11),`jenis_trans` varchar(255),`tgl_trans` date,`v_masuk` decimal(15,2),`v_keluar` decimal(15,2),`ket` varchar(255))
BEGIN
	INSERT INTO transaksi_rutin(transaksi,id_transaksi,tanggal_transaksi,masuk,keluar,keterangan) 
	VALUES (jenis_trans,id_trans,tgl_trans,v_masuk, v_keluar, ket);

END
;;
DELIMITER ;
DROP TRIGGER IF EXISTS `generate_pembayaran`;
DELIMITER ;;
CREATE TRIGGER `generate_pembayaran` AFTER INSERT ON `peminjaman` FOR EACH ROW call generate_pembayaran(NEW.id,NEW.jumlah_pinjaman, NEW.bunga, NEW.kali_bayar, NEW.tgl_pinjaman)
;;
DELIMITER ;
