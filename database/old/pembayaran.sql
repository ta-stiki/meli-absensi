/*
Navicat MySQL Data Transfer

Source Server         : local
Source Server Version : 50505
Source Host           : localhost:3306
Source Database       : sikopat

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2017-02-01 00:17:04
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for pembayaran
-- ----------------------------
DROP TABLE IF EXISTS `pembayaran`;
CREATE TABLE `pembayaran` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_peminjaman` int(11) DEFAULT NULL,
  `tanggal_bayar` date DEFAULT NULL,
  `tanggal_batas` date DEFAULT NULL,
  `jumlah_bayar` decimal(15,2) DEFAULT NULL,
  `bunga` decimal(15,2) DEFAULT NULL,
  `pembayaran_ke` int(11) DEFAULT NULL,
  `keterangan` text,
  `status` int(1) DEFAULT '0',
  `is_bayar_bunga` int(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `fk_pembayaran_peminjaman_1` (`id_peminjaman`),
  CONSTRAINT `fk_pembayaran_peminjaman_1` FOREIGN KEY (`id_peminjaman`) REFERENCES `peminjaman` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=69 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for peminjaman
-- ----------------------------
DROP TABLE IF EXISTS `peminjaman`;
CREATE TABLE `peminjaman` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_anggota` int(11) DEFAULT NULL,
  `no_pinjaman` varchar(20) DEFAULT NULL,
  `tgl_pinjaman` date DEFAULT NULL,
  `jumlah_pinjaman` decimal(15,2) DEFAULT NULL,
  `bunga` decimal(15,2) DEFAULT NULL,
  `total_pinjaman` decimal(15,2) DEFAULT NULL COMMENT 'lunas\r\nbelum lunas',
  `status` int(1) DEFAULT '0',
  `input_oleh` int(11) DEFAULT NULL,
  `tgl_input` datetime DEFAULT NULL,
  `update_oleh` int(11) DEFAULT NULL,
  `tgl_update` datetime DEFAULT NULL,
  `sisa_pembayaran` decimal(15,2) DEFAULT NULL,
  `kali_bayar` int(11) DEFAULT NULL,
  `kali_bayar_bunga` int(11) DEFAULT NULL,
  `kali_tunggakan` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_peminjaman_anggota_1` (`id_anggota`),
  CONSTRAINT `fk_peminjaman_anggota_1` FOREIGN KEY (`id_anggota`) REFERENCES `anggota` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for pengaturan
-- ----------------------------
DROP TABLE IF EXISTS `pengaturan`;
CREATE TABLE `pengaturan` (
  `key` varchar(50) NOT NULL,
  `value` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`key`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
DROP TRIGGER IF EXISTS `generate_pembayaran`;
DELIMITER ;;
CREATE TRIGGER `generate_pembayaran` AFTER INSERT ON `peminjaman` FOR EACH ROW call generate_pembayaran(NEW.id,NEW.jumlah_pinjaman, NEW.bunga, NEW.kali_bayar, NEW.tgl_pinjaman)
;;
DELIMITER ;
