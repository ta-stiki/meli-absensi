/*
Navicat MySQL Data Transfer

Source Server         : local
Source Server Version : 50505
Source Host           : localhost:3306
Source Database       : sikopat

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2017-01-27 21:19:27
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for anggota
-- ----------------------------
DROP TABLE IF EXISTS `anggota`;
CREATE TABLE `anggota` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(255) DEFAULT NULL,
  `tgl_lahir` date DEFAULT NULL,
  `jenis_kelamin` varchar(20) DEFAULT NULL,
  `alamat` varchar(100) DEFAULT NULL,
  `no_telp` varchar(20) DEFAULT NULL,
  `id_jabatan` int(11) DEFAULT NULL,
  `tgl_masuk` date DEFAULT NULL,
  `tgl_berhenti` date DEFAULT NULL,
  `tipe_anggota` varchar(10) DEFAULT NULL,
  `no_identitas` varchar(20) DEFAULT NULL,
  `saldo_simpanan` double(15,2) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `username` varchar(20) DEFAULT NULL,
  `password` varchar(50) DEFAULT NULL,
  `foto` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_anggota_jabatan_1` (`id_jabatan`),
  CONSTRAINT `fk_anggota_jabatan_1` FOREIGN KEY (`id_jabatan`) REFERENCES `jabatan` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=191 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of anggota
-- ----------------------------
INSERT INTO `anggota` VALUES ('108', 'I Dewa Made Khrisna Muku, M.T.', '2017-01-01', 'laki-laki', null, null, null, null, null, null, null, '100000.00', '1', 'dewa', 'muku', null);
INSERT INTO `anggota` VALUES ('109', 'A.A Gde Bagus Ariana, S.T.', '2017-01-01', 'laki-laki', null, null, null, null, null, null, null, null, '1', null, null, null);
INSERT INTO `anggota` VALUES ('110', 'Aniek Suryanti Kusuma, M.Kom.', '2017-01-01', 'laki-laki', null, null, null, null, null, null, null, null, '1', null, null, null);
INSERT INTO `anggota` VALUES ('111', 'Brigida Arie Minartiningtyas, M.Kom.', '2017-01-01', 'laki-laki', null, null, null, null, null, null, null, null, '1', null, null, null);
INSERT INTO `anggota` VALUES ('112', 'Dewa Putu Yudhi Ardiana, S.KOM', '2017-01-01', 'laki-laki', null, null, null, null, null, null, null, null, '1', null, null, null);
INSERT INTO `anggota` VALUES ('113', 'Dwi Putra Githa, S.T., M.T.', '2017-01-01', 'laki-laki', null, null, null, null, null, null, null, null, '1', null, null, null);
INSERT INTO `anggota` VALUES ('114', 'I Wayan Sudiarsa, S. T., M. Kom', '2017-01-01', 'laki-laki', null, null, null, null, null, null, null, null, '1', null, null, null);
INSERT INTO `anggota` VALUES ('115', 'I Dewa Made Adi Baskara Joni, M.Kom.', '2017-01-01', 'laki-laki', null, null, null, null, null, null, null, null, '1', null, null, null);
INSERT INTO `anggota` VALUES ('116', 'I Kadek Dwi Gandika Supartha, S.T.', '2017-01-01', 'laki-laki', null, null, null, null, null, null, null, null, '1', null, null, null);
INSERT INTO `anggota` VALUES ('117', 'I Made Ardwi Pradnyana, M.T.', '2017-01-01', 'laki-laki', null, null, null, null, null, null, null, null, '1', null, null, null);
INSERT INTO `anggota` VALUES ('118', 'I Made Marthana Yusa, M.Ds.', '2017-01-01', 'laki-laki', null, null, null, null, null, null, null, null, '1', null, null, null);
INSERT INTO `anggota` VALUES ('119', 'I Putu Gd Budayasa. SST.Par.,M.T.I', '2017-01-01', 'laki-laki', null, null, null, null, null, null, null, null, '1', null, null, null);
INSERT INTO `anggota` VALUES ('120', 'Desak Made Dwi Utami Putra, S.Si, M. Cs', '2017-01-01', 'laki-laki', null, null, null, null, null, null, null, null, '1', null, null, null);
INSERT INTO `anggota` VALUES ('121', 'I Nyoman Jayanegara, S.Sn', '2017-01-01', 'laki-laki', null, null, null, null, null, null, null, null, '1', null, null, null);
INSERT INTO `anggota` VALUES ('122', 'I Nyoman Anom Fajaraditya, S. Sn', '2017-01-01', 'laki-laki', null, null, null, null, null, null, null, null, '1', null, null, null);
INSERT INTO `anggota` VALUES ('123', 'I Nyoman Agus Suarya, M. Sn', '2017-01-01', 'laki-laki', null, null, null, null, null, null, null, null, '1', null, null, null);
INSERT INTO `anggota` VALUES ('124', 'Ida Bagus Ary Indra Iswara,S.Kom.,M.Kom', '2017-01-01', 'laki-laki', null, null, null, null, null, null, null, null, '1', null, null, null);
INSERT INTO `anggota` VALUES ('125', 'Gede Aditra Pradnyana,S.Kom.,M.Kom', '2017-01-01', 'laki-laki', null, null, null, null, null, null, null, null, '1', null, null, null);
INSERT INTO `anggota` VALUES ('126', 'I Nyoman Buda Hartawan,S.Kom.,M.Kom', '2017-01-01', 'laki-laki', null, null, null, null, null, null, null, null, '1', null, null, null);
INSERT INTO `anggota` VALUES ('127', 'I Nyoman Widhi Adnyana,S.Kom', '2017-01-01', 'laki-laki', null, null, null, null, null, null, null, null, '1', null, null, null);
INSERT INTO `anggota` VALUES ('128', 'Komang Kurniawan Widiartha,S.Kom.,M.Cs', '2017-01-01', 'laki-laki', null, null, null, null, null, null, null, null, '1', null, null, null);
INSERT INTO `anggota` VALUES ('129', 'I Putu Adi Pratama,S.Kom.,M.Cs', '2017-01-01', 'laki-laki', null, null, null, null, null, null, null, null, '1', null, null, null);
INSERT INTO `anggota` VALUES ('130', 'Anak Agung Gde Ekayana,M.Pd', '2017-01-01', 'laki-laki', null, null, null, null, null, null, null, null, '1', null, null, null);
INSERT INTO `anggota` VALUES ('131', 'Welda,S.Kom.,M.T.I', '2017-01-01', 'laki-laki', null, null, null, null, null, null, null, null, '1', null, null, null);
INSERT INTO `anggota` VALUES ('132', 'Putu Sugiartawan, S.Kom.,M.Cs.,M.Agb', '2017-01-01', 'laki-laki', null, null, null, null, null, null, null, null, '1', null, null, null);
INSERT INTO `anggota` VALUES ('133', 'Made Hanindia Prami Swari,S.Kom.,M.Cs', '2017-01-01', 'laki-laki', null, null, null, null, null, null, null, null, '1', null, null, null);
INSERT INTO `anggota` VALUES ('134', 'Ni Wayan Sumartini Saraswati,MT', '2017-01-01', 'laki-laki', null, null, null, null, null, null, null, null, '1', null, null, null);
INSERT INTO `anggota` VALUES ('135', 'I Gd Putu Eka Suryana,S.Pd.,M.Cs', '2017-01-01', 'laki-laki', null, null, null, null, null, null, null, null, '1', null, null, null);
INSERT INTO `anggota` VALUES ('136', 'Sri Widiastutik, S.S., M.Hum', '2017-01-01', 'laki-laki', null, null, null, null, null, null, null, null, '1', null, null, null);
INSERT INTO `anggota` VALUES ('137', 'Ida Bagus Gede Anandita, S. Kom', '2017-01-01', 'laki-laki', null, null, null, null, null, null, null, null, '1', null, null, null);
INSERT INTO `anggota` VALUES ('138', 'Ketut Jaya Atmaja, S.Kom', '2017-01-01', 'laki-laki', null, null, null, null, null, null, null, null, '1', null, null, null);
INSERT INTO `anggota` VALUES ('139', 'Ayu Gede Wildahlia, S.E, M.M.', '2017-01-01', 'laki-laki', null, null, null, null, null, null, null, null, '1', null, null, null);
INSERT INTO `anggota` VALUES ('140', 'I Nym Saputra Wahyu Wijaya', '2017-01-01', 'laki-laki', null, null, null, null, null, null, null, null, '1', 'wahyu', 'wahyu', null);
INSERT INTO `anggota` VALUES ('141', 'Wayan Gede Suka Parwita', '2017-01-01', 'laki-laki', null, null, null, null, null, null, null, null, '1', null, null, null);
INSERT INTO `anggota` VALUES ('142', 'Putu Praba Santika', '2017-01-01', 'laki-laki', null, null, null, null, null, null, null, null, '1', null, null, null);
INSERT INTO `anggota` VALUES ('143', 'I Gede Totok Suryawan', '2017-01-01', 'laki-laki', null, null, null, null, null, null, null, null, '1', null, null, null);
INSERT INTO `anggota` VALUES ('144', 'I Gusti Md Ngurah Desnanjaya', '2017-01-01', 'laki-laki', null, null, null, null, null, null, null, null, '1', null, null, null);
INSERT INTO `anggota` VALUES ('145', 'I Kadek Susila Satwika, S.T.', '2017-01-01', 'laki-laki', null, null, null, null, null, null, null, null, '1', null, null, null);
INSERT INTO `anggota` VALUES ('146', 'Putu Satriya Udnyana Putra', '2017-01-01', 'laki-laki', null, null, null, null, null, null, null, null, '1', null, null, null);
INSERT INTO `anggota` VALUES ('147', 'A. A. Ketut Putra', '2017-01-01', 'laki-laki', null, null, null, null, null, null, null, null, '1', null, null, null);
INSERT INTO `anggota` VALUES ('148', 'Ketut Suriana', '2017-01-01', 'laki-laki', null, null, null, null, null, null, null, null, '1', null, null, null);
INSERT INTO `anggota` VALUES ('149', 'I Ketut Merta', '2017-01-01', 'laki-laki', null, null, null, null, null, null, null, null, '1', null, null, null);
INSERT INTO `anggota` VALUES ('150', 'I Nyoman Erik Sindunata', '2017-01-01', 'laki-laki', null, null, null, null, null, null, null, null, '1', null, null, null);
INSERT INTO `anggota` VALUES ('151', 'I Made Suantara', '2017-01-01', 'laki-laki', null, null, null, null, null, null, null, null, '1', null, null, null);
INSERT INTO `anggota` VALUES ('152', 'M. Rofii (Pak Robi)', '2017-01-01', 'laki-laki', null, null, null, null, null, null, null, null, '1', null, null, null);
INSERT INTO `anggota` VALUES ('153', 'I Dewa Nyoman Mudita', '2017-01-01', 'laki-laki', null, null, null, null, null, null, null, null, '1', null, null, null);
INSERT INTO `anggota` VALUES ('154', 'I Wayan Sulendra', '2017-01-01', 'laki-laki', null, null, null, null, null, null, null, null, '1', null, null, null);
INSERT INTO `anggota` VALUES ('155', 'Ni Nengah Rinca Wati, S. E', '2017-01-01', 'laki-laki', null, null, null, null, null, null, null, null, '1', null, null, null);
INSERT INTO `anggota` VALUES ('156', 'Putu Anik Suryantini', '2017-01-01', 'laki-laki', null, null, null, null, null, null, null, null, '1', null, null, null);
INSERT INTO `anggota` VALUES ('157', 'Kadek Ayu Ariningsih', '2017-01-01', 'laki-laki', null, null, null, null, null, null, null, null, '1', null, null, null);
INSERT INTO `anggota` VALUES ('158', 'Ni Putu Meilita Yani', '2017-01-01', 'laki-laki', null, null, null, null, null, null, null, null, '1', null, null, null);
INSERT INTO `anggota` VALUES ('159', 'Ni Kadek Nita Noviani Pande, S.Pd', '2017-01-01', 'laki-laki', null, null, null, null, null, null, null, null, '1', null, null, null);
INSERT INTO `anggota` VALUES ('160', 'Robert Wijaya', '2017-01-01', 'laki-laki', null, null, null, null, null, null, null, null, '1', null, null, null);
INSERT INTO `anggota` VALUES ('161', 'M. Hanafi', '2017-01-01', 'laki-laki', null, null, null, null, null, null, null, null, '1', null, null, null);
INSERT INTO `anggota` VALUES ('162', 'Khairrudin, S.E', '2017-01-01', 'laki-laki', null, null, null, null, null, null, null, null, '1', null, null, null);
INSERT INTO `anggota` VALUES ('163', 'I Nyoman Sedana', '2017-01-01', 'laki-laki', null, null, null, null, null, null, null, null, '1', null, null, null);
INSERT INTO `anggota` VALUES ('164', 'Komang Widiantara', '2017-01-01', 'laki-laki', null, null, null, null, null, null, null, null, '1', null, null, null);
INSERT INTO `anggota` VALUES ('165', 'Dewa Gede Surya Damanik', '2017-01-01', 'laki-laki', null, null, null, null, null, null, null, null, '1', null, null, null);
INSERT INTO `anggota` VALUES ('166', 'Emi Widya Sari', '2017-01-01', 'laki-laki', null, null, null, null, null, null, null, null, '1', null, null, null);
INSERT INTO `anggota` VALUES ('167', 'Rury Ayuning Lati, S.Pd', '2017-01-01', 'laki-laki', null, null, null, null, null, null, null, '100000.00', '1', null, null, null);
INSERT INTO `anggota` VALUES ('168', 'Ni Kade Ayu Nirwana, S.Pd', '2017-01-01', 'laki-laki', null, null, null, null, null, null, null, null, '1', null, null, null);
INSERT INTO `anggota` VALUES ('169', 'Ni Putu Erra Budiantari, S. Pd', '2017-01-01', 'laki-laki', null, null, null, null, null, null, null, null, '1', null, null, null);
INSERT INTO `anggota` VALUES ('170', 'Luh Putu Mega Pratami', '2017-01-01', 'laki-laki', null, null, null, null, null, null, null, '25000.00', '1', null, null, null);
INSERT INTO `anggota` VALUES ('171', 'Ni Made Eny Indrawati,S.Pd', '2017-01-01', 'laki-laki', null, null, null, null, null, null, null, '25000.00', '1', null, null, null);
INSERT INTO `anggota` VALUES ('172', 'Gabriella Christine Lahal', '2017-01-01', 'laki-laki', null, null, null, null, null, null, null, '25000.00', '1', null, null, null);
INSERT INTO `anggota` VALUES ('173', 'Ni Komang Novita Dewi', '2017-01-01', 'laki-laki', null, null, null, null, null, null, null, '25000.00', '1', null, null, null);
INSERT INTO `anggota` VALUES ('174', 'Ida Ayu Gede Anindyatari', '2017-01-01', 'laki-laki', null, null, null, null, null, null, null, '25000.00', '1', 'dayu', 'anin', null);
INSERT INTO `anggota` VALUES ('175', 'Agus Swartawan', '2017-01-01', 'laki-laki', null, null, null, null, null, null, null, '25000.00', '1', null, null, null);
INSERT INTO `anggota` VALUES ('176', 'I Dewa Ayu Tantri Pramawati, S.E', '2017-01-01', 'laki-laki', null, null, null, null, null, null, null, '25000.00', '1', null, null, null);
INSERT INTO `anggota` VALUES ('177', 'Ni Putu Sri Pancadewi, S. Pd', '2017-01-01', 'laki-laki', null, null, null, null, null, null, null, '25000.00', '1', null, null, null);
INSERT INTO `anggota` VALUES ('178', 'Ni Kadek Ceryna Dewi', '2017-01-01', 'laki-laki', null, null, null, null, null, null, null, '25000.00', '1', null, null, null);
INSERT INTO `anggota` VALUES ('179', 'Alamsyah', '2017-01-01', 'laki-laki', null, null, null, null, null, null, null, '25000.00', '1', null, null, null);
INSERT INTO `anggota` VALUES ('180', 'Andrianus T. Balibo', '2017-01-01', 'laki-laki', null, null, null, null, null, null, null, '25000.00', '1', null, null, null);
INSERT INTO `anggota` VALUES ('181', 'Agus Harianto', '2017-01-01', 'laki-laki', null, null, null, null, null, null, null, '25000.00', '1', null, null, null);
INSERT INTO `anggota` VALUES ('182', 'Ketut Suartana', '2017-01-01', 'laki-laki', null, null, null, null, null, null, null, '25000.00', '1', null, null, null);
INSERT INTO `anggota` VALUES ('183', 'I Komang Catra', '2017-01-01', 'laki-laki', null, null, null, null, null, null, null, '25000.00', '1', null, null, null);
INSERT INTO `anggota` VALUES ('184', 'Ni Luh Wiwik Sri Rahayu G. S.Kom., M.Kom.', '2017-01-01', 'laki-laki', null, null, null, null, null, null, null, '25000.00', '1', null, null, null);
INSERT INTO `anggota` VALUES ('185', 'Made Suci Ariantini, S.Pd., M.Kom.', '2017-01-01', 'laki-laki', null, null, null, null, null, null, null, '25000.00', '1', null, null, null);
INSERT INTO `anggota` VALUES ('186', 'I G. A.A. Diatri Indradewi, S.Kom., M.T.', '2017-01-01', 'laki-laki', null, null, null, null, null, null, null, '25000.00', '1', null, null, null);
INSERT INTO `anggota` VALUES ('187', 'Wayan Eny Mariani, S.M.B., M.Si.', '2017-01-01', 'laki-laki', null, null, null, null, null, null, null, '25000.00', '1', null, null, null);
INSERT INTO `anggota` VALUES ('188', 'I Wayan Agus Surya Darma, S.Kom., M.T.', '2017-01-01', 'laki-laki', null, null, null, null, null, null, null, '25000.00', '1', null, null, null);
INSERT INTO `anggota` VALUES ('189', 'Luh Putu Ariestari Pradnyadewi, S. Ab.', '2017-01-01', 'laki-laki', null, null, null, null, null, null, null, '25000.00', '1', null, null, null);
INSERT INTO `anggota` VALUES ('190', 'Jojo', '2017-02-06', 'laki-laki', 'fgjhk;l\'lkjhgfd', '3546789087654', '2', '2017-01-01', '0000-00-00', null, '23456789', '50000.00', '1', '1234', '1234', null);

-- ----------------------------
-- Table structure for arus_kas
-- ----------------------------
DROP TABLE IF EXISTS `arus_kas`;
CREATE TABLE `arus_kas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `transaksi` varchar(255) DEFAULT NULL,
  `id_transaksi` int(11) DEFAULT NULL,
  `id_user` int(11) DEFAULT NULL,
  `waktu` datetime DEFAULT NULL,
  `masuk` decimal(15,2) DEFAULT NULL,
  `keluar` decimal(15,2) DEFAULT NULL,
  `keterangan` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of arus_kas
-- ----------------------------

-- ----------------------------
-- Table structure for auto_number
-- ----------------------------
DROP TABLE IF EXISTS `auto_number`;
CREATE TABLE `auto_number` (
  `group` varchar(10) DEFAULT NULL,
  `number` int(11) DEFAULT NULL,
  `lock_p` int(255) DEFAULT NULL,
  `update` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of auto_number
-- ----------------------------
INSERT INTO `auto_number` VALUES ('pinjaman', '1', null, null);

-- ----------------------------
-- Table structure for detail_shu
-- ----------------------------
DROP TABLE IF EXISTS `detail_shu`;
CREATE TABLE `detail_shu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_shu` int(11) DEFAULT NULL,
  `id_anggota` int(11) DEFAULT NULL,
  `anggota` decimal(15,2) DEFAULT NULL,
  `peminjam` decimal(15,2) DEFAULT NULL,
  `pengurus` decimal(15,2) DEFAULT NULL,
  `jumlah` decimal(15,2) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_detail_shu_shu_1` (`id_shu`),
  KEY `fk_detail_shu_anggota_1` (`id_anggota`),
  CONSTRAINT `fk_detail_shu_anggota_1` FOREIGN KEY (`id_anggota`) REFERENCES `anggota` (`id`),
  CONSTRAINT `fk_detail_shu_shu_1` FOREIGN KEY (`id_shu`) REFERENCES `shu` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=86 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of detail_shu
-- ----------------------------
INSERT INTO `detail_shu` VALUES ('3', '5', '108', '3096.39', '102800.00', null, null);
INSERT INTO `detail_shu` VALUES ('4', '5', '109', '3096.39', null, null, null);
INSERT INTO `detail_shu` VALUES ('5', '5', '110', '3096.39', null, null, null);
INSERT INTO `detail_shu` VALUES ('6', '5', '111', '3096.39', null, null, null);
INSERT INTO `detail_shu` VALUES ('7', '5', '112', '3096.39', null, null, null);
INSERT INTO `detail_shu` VALUES ('8', '5', '113', '3096.39', null, null, null);
INSERT INTO `detail_shu` VALUES ('9', '5', '114', '3096.39', null, null, null);
INSERT INTO `detail_shu` VALUES ('10', '5', '115', '3096.39', null, null, null);
INSERT INTO `detail_shu` VALUES ('11', '5', '116', '3096.39', null, null, null);
INSERT INTO `detail_shu` VALUES ('12', '5', '117', '3096.39', null, null, null);
INSERT INTO `detail_shu` VALUES ('13', '5', '118', '3096.39', null, null, null);
INSERT INTO `detail_shu` VALUES ('14', '5', '119', '3096.39', null, null, null);
INSERT INTO `detail_shu` VALUES ('15', '5', '120', '3096.39', null, null, null);
INSERT INTO `detail_shu` VALUES ('16', '5', '121', '3096.39', null, null, null);
INSERT INTO `detail_shu` VALUES ('17', '5', '122', '3096.39', null, null, null);
INSERT INTO `detail_shu` VALUES ('18', '5', '123', '3096.39', null, null, null);
INSERT INTO `detail_shu` VALUES ('19', '5', '124', '3096.39', null, null, null);
INSERT INTO `detail_shu` VALUES ('20', '5', '125', '3096.39', null, null, null);
INSERT INTO `detail_shu` VALUES ('21', '5', '126', '3096.39', null, null, null);
INSERT INTO `detail_shu` VALUES ('22', '5', '127', '3096.39', null, null, null);
INSERT INTO `detail_shu` VALUES ('23', '5', '128', '3096.39', null, null, null);
INSERT INTO `detail_shu` VALUES ('24', '5', '129', '3096.39', null, null, null);
INSERT INTO `detail_shu` VALUES ('25', '5', '130', '3096.39', null, null, null);
INSERT INTO `detail_shu` VALUES ('26', '5', '131', '3096.39', null, null, null);
INSERT INTO `detail_shu` VALUES ('27', '5', '132', '3096.39', null, null, null);
INSERT INTO `detail_shu` VALUES ('28', '5', '133', '3096.39', null, null, null);
INSERT INTO `detail_shu` VALUES ('29', '5', '134', '3096.39', null, null, null);
INSERT INTO `detail_shu` VALUES ('30', '5', '135', '3096.39', null, null, null);
INSERT INTO `detail_shu` VALUES ('31', '5', '136', '3096.39', null, null, null);
INSERT INTO `detail_shu` VALUES ('32', '5', '137', '3096.39', null, null, null);
INSERT INTO `detail_shu` VALUES ('33', '5', '138', '3096.39', null, null, null);
INSERT INTO `detail_shu` VALUES ('34', '5', '139', '3096.39', null, null, null);
INSERT INTO `detail_shu` VALUES ('35', '5', '140', '3096.39', null, null, null);
INSERT INTO `detail_shu` VALUES ('36', '5', '141', '3096.39', null, null, null);
INSERT INTO `detail_shu` VALUES ('37', '5', '142', '3096.39', null, null, null);
INSERT INTO `detail_shu` VALUES ('38', '5', '143', '3096.39', null, null, null);
INSERT INTO `detail_shu` VALUES ('39', '5', '144', '3096.39', null, null, null);
INSERT INTO `detail_shu` VALUES ('40', '5', '145', '3096.39', null, null, null);
INSERT INTO `detail_shu` VALUES ('41', '5', '146', '3096.39', null, null, null);
INSERT INTO `detail_shu` VALUES ('42', '5', '147', '3096.39', null, null, null);
INSERT INTO `detail_shu` VALUES ('43', '5', '148', '3096.39', null, null, null);
INSERT INTO `detail_shu` VALUES ('44', '5', '149', '3096.39', null, null, null);
INSERT INTO `detail_shu` VALUES ('45', '5', '150', '3096.39', null, null, null);
INSERT INTO `detail_shu` VALUES ('46', '5', '151', '3096.39', null, null, null);
INSERT INTO `detail_shu` VALUES ('47', '5', '152', '3096.39', null, null, null);
INSERT INTO `detail_shu` VALUES ('48', '5', '153', '3096.39', null, null, null);
INSERT INTO `detail_shu` VALUES ('49', '5', '154', '3096.39', null, null, null);
INSERT INTO `detail_shu` VALUES ('50', '5', '155', '3096.39', null, null, null);
INSERT INTO `detail_shu` VALUES ('51', '5', '156', '3096.39', null, null, null);
INSERT INTO `detail_shu` VALUES ('52', '5', '157', '3096.39', null, null, null);
INSERT INTO `detail_shu` VALUES ('53', '5', '158', '3096.39', null, null, null);
INSERT INTO `detail_shu` VALUES ('54', '5', '159', '3096.39', null, null, null);
INSERT INTO `detail_shu` VALUES ('55', '5', '160', '3096.39', null, null, null);
INSERT INTO `detail_shu` VALUES ('56', '5', '161', '3096.39', null, null, null);
INSERT INTO `detail_shu` VALUES ('57', '5', '162', '3096.39', null, null, null);
INSERT INTO `detail_shu` VALUES ('58', '5', '163', '3096.39', null, null, null);
INSERT INTO `detail_shu` VALUES ('59', '5', '164', '3096.39', null, null, null);
INSERT INTO `detail_shu` VALUES ('60', '5', '165', '3096.39', null, null, null);
INSERT INTO `detail_shu` VALUES ('61', '5', '166', '3096.39', null, null, null);
INSERT INTO `detail_shu` VALUES ('62', '5', '167', '3096.39', null, null, null);
INSERT INTO `detail_shu` VALUES ('63', '5', '168', '3096.39', null, null, null);
INSERT INTO `detail_shu` VALUES ('64', '5', '169', '3096.39', null, null, null);
INSERT INTO `detail_shu` VALUES ('65', '5', '170', '3096.39', null, null, null);
INSERT INTO `detail_shu` VALUES ('66', '5', '171', '3096.39', null, null, null);
INSERT INTO `detail_shu` VALUES ('67', '5', '172', '3096.39', null, null, null);
INSERT INTO `detail_shu` VALUES ('68', '5', '173', '3096.39', null, null, null);
INSERT INTO `detail_shu` VALUES ('69', '5', '174', '3096.39', null, null, null);
INSERT INTO `detail_shu` VALUES ('70', '5', '175', '3096.39', null, null, null);
INSERT INTO `detail_shu` VALUES ('71', '5', '176', '3096.39', null, null, null);
INSERT INTO `detail_shu` VALUES ('72', '5', '177', '3096.39', null, null, null);
INSERT INTO `detail_shu` VALUES ('73', '5', '178', '3096.39', null, null, null);
INSERT INTO `detail_shu` VALUES ('74', '5', '179', '3096.39', null, null, null);
INSERT INTO `detail_shu` VALUES ('75', '5', '180', '3096.39', null, null, null);
INSERT INTO `detail_shu` VALUES ('76', '5', '181', '3096.39', null, null, null);
INSERT INTO `detail_shu` VALUES ('77', '5', '182', '3096.39', null, null, null);
INSERT INTO `detail_shu` VALUES ('78', '5', '183', '3096.39', null, null, null);
INSERT INTO `detail_shu` VALUES ('79', '5', '184', '3096.39', null, null, null);
INSERT INTO `detail_shu` VALUES ('80', '5', '185', '3096.39', null, null, null);
INSERT INTO `detail_shu` VALUES ('81', '5', '186', '3096.39', null, null, null);
INSERT INTO `detail_shu` VALUES ('82', '5', '187', '3096.39', null, null, null);
INSERT INTO `detail_shu` VALUES ('83', '5', '188', '3096.39', null, null, null);
INSERT INTO `detail_shu` VALUES ('84', '5', '189', '3096.39', null, null, null);
INSERT INTO `detail_shu` VALUES ('85', '5', '190', '3096.39', null, '51400.00', null);

-- ----------------------------
-- Table structure for hak_akses
-- ----------------------------
DROP TABLE IF EXISTS `hak_akses`;
CREATE TABLE `hak_akses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `hak_akses` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of hak_akses
-- ----------------------------
INSERT INTO `hak_akses` VALUES ('1', 'Administrator');
INSERT INTO `hak_akses` VALUES ('2', 'Anggota');
INSERT INTO `hak_akses` VALUES ('3', 'Ketua');
INSERT INTO `hak_akses` VALUES ('4', 'Bendahara');

-- ----------------------------
-- Table structure for hak_akses_anggota
-- ----------------------------
DROP TABLE IF EXISTS `hak_akses_anggota`;
CREATE TABLE `hak_akses_anggota` (
  `id_anggota` int(11) DEFAULT NULL,
  `id_hak_akses` int(11) DEFAULT NULL,
  KEY `fk_hak_akses_anggota_hak_akses_1` (`id_hak_akses`),
  KEY `fk_hak_akses_anggota_anggota_1` (`id_anggota`),
  CONSTRAINT `fk_hak_akses_anggota_anggota_1` FOREIGN KEY (`id_anggota`) REFERENCES `anggota` (`id`),
  CONSTRAINT `fk_hak_akses_anggota_hak_akses_1` FOREIGN KEY (`id_hak_akses`) REFERENCES `hak_akses` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of hak_akses_anggota
-- ----------------------------
INSERT INTO `hak_akses_anggota` VALUES ('109', '2');
INSERT INTO `hak_akses_anggota` VALUES ('110', '2');
INSERT INTO `hak_akses_anggota` VALUES ('111', '2');
INSERT INTO `hak_akses_anggota` VALUES ('112', '2');
INSERT INTO `hak_akses_anggota` VALUES ('113', '2');
INSERT INTO `hak_akses_anggota` VALUES ('114', '2');
INSERT INTO `hak_akses_anggota` VALUES ('115', '2');
INSERT INTO `hak_akses_anggota` VALUES ('116', '2');
INSERT INTO `hak_akses_anggota` VALUES ('117', '2');
INSERT INTO `hak_akses_anggota` VALUES ('118', '2');
INSERT INTO `hak_akses_anggota` VALUES ('119', '2');
INSERT INTO `hak_akses_anggota` VALUES ('120', '2');
INSERT INTO `hak_akses_anggota` VALUES ('121', '2');
INSERT INTO `hak_akses_anggota` VALUES ('122', '2');
INSERT INTO `hak_akses_anggota` VALUES ('123', '2');
INSERT INTO `hak_akses_anggota` VALUES ('124', '2');
INSERT INTO `hak_akses_anggota` VALUES ('125', '2');
INSERT INTO `hak_akses_anggota` VALUES ('126', '2');
INSERT INTO `hak_akses_anggota` VALUES ('127', '2');
INSERT INTO `hak_akses_anggota` VALUES ('128', '2');
INSERT INTO `hak_akses_anggota` VALUES ('129', '2');
INSERT INTO `hak_akses_anggota` VALUES ('130', '2');
INSERT INTO `hak_akses_anggota` VALUES ('131', '2');
INSERT INTO `hak_akses_anggota` VALUES ('132', '2');
INSERT INTO `hak_akses_anggota` VALUES ('133', '2');
INSERT INTO `hak_akses_anggota` VALUES ('134', '2');
INSERT INTO `hak_akses_anggota` VALUES ('135', '2');
INSERT INTO `hak_akses_anggota` VALUES ('136', '2');
INSERT INTO `hak_akses_anggota` VALUES ('137', '2');
INSERT INTO `hak_akses_anggota` VALUES ('138', '2');
INSERT INTO `hak_akses_anggota` VALUES ('139', '2');
INSERT INTO `hak_akses_anggota` VALUES ('141', '2');
INSERT INTO `hak_akses_anggota` VALUES ('142', '2');
INSERT INTO `hak_akses_anggota` VALUES ('143', '2');
INSERT INTO `hak_akses_anggota` VALUES ('144', '2');
INSERT INTO `hak_akses_anggota` VALUES ('145', '2');
INSERT INTO `hak_akses_anggota` VALUES ('146', '2');
INSERT INTO `hak_akses_anggota` VALUES ('147', '2');
INSERT INTO `hak_akses_anggota` VALUES ('148', '2');
INSERT INTO `hak_akses_anggota` VALUES ('149', '2');
INSERT INTO `hak_akses_anggota` VALUES ('150', '2');
INSERT INTO `hak_akses_anggota` VALUES ('151', '2');
INSERT INTO `hak_akses_anggota` VALUES ('152', '2');
INSERT INTO `hak_akses_anggota` VALUES ('153', '2');
INSERT INTO `hak_akses_anggota` VALUES ('154', '2');
INSERT INTO `hak_akses_anggota` VALUES ('155', '2');
INSERT INTO `hak_akses_anggota` VALUES ('156', '2');
INSERT INTO `hak_akses_anggota` VALUES ('157', '2');
INSERT INTO `hak_akses_anggota` VALUES ('158', '2');
INSERT INTO `hak_akses_anggota` VALUES ('159', '2');
INSERT INTO `hak_akses_anggota` VALUES ('160', '2');
INSERT INTO `hak_akses_anggota` VALUES ('161', '2');
INSERT INTO `hak_akses_anggota` VALUES ('162', '2');
INSERT INTO `hak_akses_anggota` VALUES ('163', '2');
INSERT INTO `hak_akses_anggota` VALUES ('164', '2');
INSERT INTO `hak_akses_anggota` VALUES ('165', '2');
INSERT INTO `hak_akses_anggota` VALUES ('166', '2');
INSERT INTO `hak_akses_anggota` VALUES ('167', '2');
INSERT INTO `hak_akses_anggota` VALUES ('168', '2');
INSERT INTO `hak_akses_anggota` VALUES ('169', '2');
INSERT INTO `hak_akses_anggota` VALUES ('170', '2');
INSERT INTO `hak_akses_anggota` VALUES ('171', '2');
INSERT INTO `hak_akses_anggota` VALUES ('172', '2');
INSERT INTO `hak_akses_anggota` VALUES ('173', '2');
INSERT INTO `hak_akses_anggota` VALUES ('174', '2');
INSERT INTO `hak_akses_anggota` VALUES ('175', '2');
INSERT INTO `hak_akses_anggota` VALUES ('176', '2');
INSERT INTO `hak_akses_anggota` VALUES ('177', '2');
INSERT INTO `hak_akses_anggota` VALUES ('178', '2');
INSERT INTO `hak_akses_anggota` VALUES ('179', '2');
INSERT INTO `hak_akses_anggota` VALUES ('180', '2');
INSERT INTO `hak_akses_anggota` VALUES ('181', '2');
INSERT INTO `hak_akses_anggota` VALUES ('182', '2');
INSERT INTO `hak_akses_anggota` VALUES ('183', '2');
INSERT INTO `hak_akses_anggota` VALUES ('184', '2');
INSERT INTO `hak_akses_anggota` VALUES ('185', '2');
INSERT INTO `hak_akses_anggota` VALUES ('186', '2');
INSERT INTO `hak_akses_anggota` VALUES ('187', '2');
INSERT INTO `hak_akses_anggota` VALUES ('188', '2');
INSERT INTO `hak_akses_anggota` VALUES ('189', '2');
INSERT INTO `hak_akses_anggota` VALUES ('174', '1');
INSERT INTO `hak_akses_anggota` VALUES ('174', '3');
INSERT INTO `hak_akses_anggota` VALUES ('174', '4');
INSERT INTO `hak_akses_anggota` VALUES ('108', '2');
INSERT INTO `hak_akses_anggota` VALUES ('108', '3');
INSERT INTO `hak_akses_anggota` VALUES ('140', '2');
INSERT INTO `hak_akses_anggota` VALUES ('140', '3');

-- ----------------------------
-- Table structure for jabatan
-- ----------------------------
DROP TABLE IF EXISTS `jabatan`;
CREATE TABLE `jabatan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `jenis_jabatan` varchar(10) DEFAULT NULL,
  `nama_jabatan` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_jabatan_persentase_shu_1` (`jenis_jabatan`),
  CONSTRAINT `fk_jabatan_persentase_shu_1` FOREIGN KEY (`jenis_jabatan`) REFERENCES `persentase_shu` (`jenis_jabatan`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of jabatan
-- ----------------------------
INSERT INTO `jabatan` VALUES ('1', 'anggota', 'Anggota');
INSERT INTO `jabatan` VALUES ('2', 'pengurus', 'Ketua');
INSERT INTO `jabatan` VALUES ('3', 'pengurus', 'Bendahara');

-- ----------------------------
-- Table structure for jenis_simpanan
-- ----------------------------
DROP TABLE IF EXISTS `jenis_simpanan`;
CREATE TABLE `jenis_simpanan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kode_simpanan` varchar(5) DEFAULT NULL,
  `jenis_simpanan` varchar(50) DEFAULT NULL,
  `jumlah` decimal(15,2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of jenis_simpanan
-- ----------------------------
INSERT INTO `jenis_simpanan` VALUES ('1', 'wajib', 'Simpanan Wajib', '25000.00');

-- ----------------------------
-- Table structure for menu
-- ----------------------------
DROP TABLE IF EXISTS `menu`;
CREATE TABLE `menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `link` varchar(255) DEFAULT NULL,
  `icon` varchar(50) DEFAULT NULL,
  `is_active` int(1) DEFAULT NULL,
  `is_parent` int(1) DEFAULT NULL,
  `akses` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of menu
-- ----------------------------
INSERT INTO `menu` VALUES ('1', 'Anggota', 'admin/anggota', 'fa fa-users', '1', '0', null);
INSERT INTO `menu` VALUES ('2', 'Peminjaman', 'admin/peminjaman', 'fa fa-download', '1', '0', null);
INSERT INTO `menu` VALUES ('3', 'Pembayaran', 'admin/pembayaran', 'fa fa-upload', '1', '0', null);
INSERT INTO `menu` VALUES ('4', 'Simpanan Wajib', 'admin/simpanan_wajib', 'fa fa-cloud-download', '1', '0', null);
INSERT INTO `menu` VALUES ('12', 'SHU', 'admin/shu', 'fa  fa-balance-scale', '1', '0', null);
INSERT INTO `menu` VALUES ('13', 'Pengaturan', null, 'fa  fa-gears', '1', '0', null);
INSERT INTO `menu` VALUES ('14', 'Hak Akses', 'admin/hak_akses_anggota', 'fa  fa-mortar-board', '1', '13', null);

-- ----------------------------
-- Table structure for pembayaran
-- ----------------------------
DROP TABLE IF EXISTS `pembayaran`;
CREATE TABLE `pembayaran` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_peminjaman` int(11) DEFAULT NULL,
  `tanggal_bayar` date DEFAULT NULL,
  `tanggal_batas` date DEFAULT NULL,
  `jumlah_bayar` decimal(15,2) DEFAULT NULL,
  `bunga` decimal(15,2) DEFAULT NULL,
  `pembayaran_ke` int(11) DEFAULT NULL,
  `keterangan` text,
  `status` int(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `fk_pembayaran_peminjaman_1` (`id_peminjaman`),
  CONSTRAINT `fk_pembayaran_peminjaman_1` FOREIGN KEY (`id_peminjaman`) REFERENCES `peminjaman` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of pembayaran
-- ----------------------------
INSERT INTO `pembayaran` VALUES ('11', '2', null, '2017-01-01', '10000.00', '1000.00', '1', null, '0');
INSERT INTO `pembayaran` VALUES ('12', '2', null, '2017-02-01', '10000.00', '1000.00', '2', null, '0');
INSERT INTO `pembayaran` VALUES ('13', '2', null, '2017-03-01', '10000.00', '1000.00', '3', null, '0');
INSERT INTO `pembayaran` VALUES ('14', '2', null, '2017-04-01', '10000.00', '1000.00', '4', null, '0');
INSERT INTO `pembayaran` VALUES ('15', '2', null, '2017-05-01', '10000.00', '1000.00', '5', null, '0');
INSERT INTO `pembayaran` VALUES ('16', '2', null, '2017-06-01', '10000.00', '1000.00', '6', null, '0');
INSERT INTO `pembayaran` VALUES ('17', '2', '2017-01-27', '2017-07-01', '10000.00', '1000.00', '7', null, '1');
INSERT INTO `pembayaran` VALUES ('18', '2', '2017-01-27', '2017-08-01', '10000.00', '1000.00', '8', null, '1');
INSERT INTO `pembayaran` VALUES ('19', '2', '2017-01-27', '2017-09-01', '10000.00', '1000.00', '9', null, '1');
INSERT INTO `pembayaran` VALUES ('20', '2', '2017-01-27', '2017-10-01', '10000.00', '1000.00', '10', null, '1');

-- ----------------------------
-- Table structure for peminjaman
-- ----------------------------
DROP TABLE IF EXISTS `peminjaman`;
CREATE TABLE `peminjaman` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_anggota` int(11) DEFAULT NULL,
  `no_pinjaman` varchar(20) DEFAULT NULL,
  `tgl_pinjaman` date DEFAULT NULL,
  `jumlah_pinjaman` decimal(15,2) DEFAULT NULL,
  `bunga` decimal(15,2) DEFAULT NULL,
  `total_pinjaman` decimal(15,2) DEFAULT NULL COMMENT 'lunas\r\nbelum lunas',
  `status` int(1) DEFAULT '0',
  `input_oleh` int(11) DEFAULT NULL,
  `tgl_input` datetime DEFAULT NULL,
  `update_oleh` int(11) DEFAULT NULL,
  `tgl_update` datetime DEFAULT NULL,
  `sisa_pembayaran` decimal(15,2) DEFAULT NULL,
  `kali_bayar` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_peminjaman_anggota_1` (`id_anggota`),
  CONSTRAINT `fk_peminjaman_anggota_1` FOREIGN KEY (`id_anggota`) REFERENCES `anggota` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of peminjaman
-- ----------------------------
INSERT INTO `peminjaman` VALUES ('2', '108', 'PINJ0120170000000001', '2016-12-01', '100000.00', '1000.00', '110000.00', '0', '174', '2017-01-27 10:25:56', '174', '2017-01-27 10:25:56', '66000.00', '10');

-- ----------------------------
-- Table structure for persentase_shu
-- ----------------------------
DROP TABLE IF EXISTS `persentase_shu`;
CREATE TABLE `persentase_shu` (
  `jenis_jabatan` varchar(10) NOT NULL,
  `persentase_shu` decimal(15,2) DEFAULT NULL,
  PRIMARY KEY (`jenis_jabatan`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of persentase_shu
-- ----------------------------
INSERT INTO `persentase_shu` VALUES ('anggota', '50.00');
INSERT INTO `persentase_shu` VALUES ('modal', '20.00');
INSERT INTO `persentase_shu` VALUES ('peminjam', '20.00');
INSERT INTO `persentase_shu` VALUES ('pengurus', '10.00');

-- ----------------------------
-- Table structure for shu
-- ----------------------------
DROP TABLE IF EXISTS `shu`;
CREATE TABLE `shu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `periode` int(4) DEFAULT NULL,
  `tahun` int(5) DEFAULT NULL,
  `laba_tahunan` double(15,2) DEFAULT NULL,
  `tanggal_generate` datetime DEFAULT NULL,
  `generate_oleh` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of shu
-- ----------------------------
INSERT INTO `shu` VALUES ('5', '2017', null, '514000.00', '2017-01-27 10:01:08', '174');

-- ----------------------------
-- Table structure for simpanan_wajib
-- ----------------------------
DROP TABLE IF EXISTS `simpanan_wajib`;
CREATE TABLE `simpanan_wajib` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_anggota` int(11) DEFAULT NULL,
  `tgl_bayar` date DEFAULT NULL,
  `jumlah_bayar` double(15,2) DEFAULT NULL,
  `status_pembayaran` int(1) DEFAULT NULL,
  `periode` int(11) DEFAULT NULL,
  `tahun` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_simpanan_wajib_anggota_1` (`id_anggota`),
  CONSTRAINT `fk_simpanan_wajib_anggota_1` FOREIGN KEY (`id_anggota`) REFERENCES `anggota` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=840 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of simpanan_wajib
-- ----------------------------
INSERT INTO `simpanan_wajib` VALUES ('757', '108', null, '25000.00', '0', '1', '2017');
INSERT INTO `simpanan_wajib` VALUES ('758', '109', null, '25000.00', '0', '1', '2017');
INSERT INTO `simpanan_wajib` VALUES ('759', '110', null, '25000.00', '0', '1', '2017');
INSERT INTO `simpanan_wajib` VALUES ('760', '111', null, '25000.00', '0', '1', '2017');
INSERT INTO `simpanan_wajib` VALUES ('761', '112', null, '25000.00', '0', '1', '2017');
INSERT INTO `simpanan_wajib` VALUES ('762', '113', null, '25000.00', '0', '1', '2017');
INSERT INTO `simpanan_wajib` VALUES ('763', '114', null, '25000.00', '0', '1', '2017');
INSERT INTO `simpanan_wajib` VALUES ('764', '115', null, '25000.00', '0', '1', '2017');
INSERT INTO `simpanan_wajib` VALUES ('765', '116', null, '25000.00', '0', '1', '2017');
INSERT INTO `simpanan_wajib` VALUES ('766', '117', null, '25000.00', '0', '1', '2017');
INSERT INTO `simpanan_wajib` VALUES ('767', '118', null, '25000.00', '0', '1', '2017');
INSERT INTO `simpanan_wajib` VALUES ('768', '119', null, '25000.00', '0', '1', '2017');
INSERT INTO `simpanan_wajib` VALUES ('769', '120', null, '25000.00', '0', '1', '2017');
INSERT INTO `simpanan_wajib` VALUES ('770', '121', null, '25000.00', '0', '1', '2017');
INSERT INTO `simpanan_wajib` VALUES ('771', '122', null, '25000.00', '0', '1', '2017');
INSERT INTO `simpanan_wajib` VALUES ('772', '123', null, '25000.00', '0', '1', '2017');
INSERT INTO `simpanan_wajib` VALUES ('773', '124', null, '25000.00', '0', '1', '2017');
INSERT INTO `simpanan_wajib` VALUES ('774', '125', null, '25000.00', '0', '1', '2017');
INSERT INTO `simpanan_wajib` VALUES ('775', '126', null, '25000.00', '0', '1', '2017');
INSERT INTO `simpanan_wajib` VALUES ('776', '127', null, '25000.00', '0', '1', '2017');
INSERT INTO `simpanan_wajib` VALUES ('777', '128', null, '25000.00', '0', '1', '2017');
INSERT INTO `simpanan_wajib` VALUES ('778', '129', null, '25000.00', '0', '1', '2017');
INSERT INTO `simpanan_wajib` VALUES ('779', '130', null, '25000.00', '0', '1', '2017');
INSERT INTO `simpanan_wajib` VALUES ('780', '131', null, '25000.00', '0', '1', '2017');
INSERT INTO `simpanan_wajib` VALUES ('781', '132', null, '25000.00', '0', '1', '2017');
INSERT INTO `simpanan_wajib` VALUES ('782', '133', null, '25000.00', '0', '1', '2017');
INSERT INTO `simpanan_wajib` VALUES ('783', '134', null, '25000.00', '0', '1', '2017');
INSERT INTO `simpanan_wajib` VALUES ('784', '135', null, '25000.00', '0', '1', '2017');
INSERT INTO `simpanan_wajib` VALUES ('785', '136', null, '25000.00', '0', '1', '2017');
INSERT INTO `simpanan_wajib` VALUES ('786', '137', null, '25000.00', '0', '1', '2017');
INSERT INTO `simpanan_wajib` VALUES ('787', '138', null, '25000.00', '0', '1', '2017');
INSERT INTO `simpanan_wajib` VALUES ('788', '139', null, '25000.00', '0', '1', '2017');
INSERT INTO `simpanan_wajib` VALUES ('789', '140', null, '25000.00', '0', '1', '2017');
INSERT INTO `simpanan_wajib` VALUES ('790', '141', null, '25000.00', '0', '1', '2017');
INSERT INTO `simpanan_wajib` VALUES ('791', '142', null, '25000.00', '0', '1', '2017');
INSERT INTO `simpanan_wajib` VALUES ('792', '143', null, '25000.00', '0', '1', '2017');
INSERT INTO `simpanan_wajib` VALUES ('793', '144', null, '25000.00', '0', '1', '2017');
INSERT INTO `simpanan_wajib` VALUES ('794', '145', null, '25000.00', '0', '1', '2017');
INSERT INTO `simpanan_wajib` VALUES ('795', '146', null, '25000.00', '0', '1', '2017');
INSERT INTO `simpanan_wajib` VALUES ('796', '147', null, '25000.00', '0', '1', '2017');
INSERT INTO `simpanan_wajib` VALUES ('797', '148', null, '25000.00', '0', '1', '2017');
INSERT INTO `simpanan_wajib` VALUES ('798', '149', null, '25000.00', '0', '1', '2017');
INSERT INTO `simpanan_wajib` VALUES ('799', '150', null, '25000.00', '0', '1', '2017');
INSERT INTO `simpanan_wajib` VALUES ('800', '151', null, '25000.00', '0', '1', '2017');
INSERT INTO `simpanan_wajib` VALUES ('801', '152', null, '25000.00', '0', '1', '2017');
INSERT INTO `simpanan_wajib` VALUES ('802', '153', null, '25000.00', '0', '1', '2017');
INSERT INTO `simpanan_wajib` VALUES ('803', '154', null, '25000.00', '0', '1', '2017');
INSERT INTO `simpanan_wajib` VALUES ('804', '155', null, '25000.00', '0', '1', '2017');
INSERT INTO `simpanan_wajib` VALUES ('805', '156', null, '25000.00', '0', '1', '2017');
INSERT INTO `simpanan_wajib` VALUES ('806', '157', null, '25000.00', '0', '1', '2017');
INSERT INTO `simpanan_wajib` VALUES ('807', '158', null, '25000.00', '0', '1', '2017');
INSERT INTO `simpanan_wajib` VALUES ('808', '159', null, '25000.00', '0', '1', '2017');
INSERT INTO `simpanan_wajib` VALUES ('809', '160', null, '25000.00', '0', '1', '2017');
INSERT INTO `simpanan_wajib` VALUES ('810', '161', null, '25000.00', '0', '1', '2017');
INSERT INTO `simpanan_wajib` VALUES ('811', '162', null, '25000.00', '0', '1', '2017');
INSERT INTO `simpanan_wajib` VALUES ('812', '163', null, '25000.00', '0', '1', '2017');
INSERT INTO `simpanan_wajib` VALUES ('813', '164', null, '25000.00', '0', '1', '2017');
INSERT INTO `simpanan_wajib` VALUES ('814', '165', null, '25000.00', '0', '1', '2017');
INSERT INTO `simpanan_wajib` VALUES ('815', '166', null, '25000.00', '0', '1', '2017');
INSERT INTO `simpanan_wajib` VALUES ('816', '167', null, '25000.00', '0', '1', '2017');
INSERT INTO `simpanan_wajib` VALUES ('817', '168', null, '25000.00', '0', '1', '2017');
INSERT INTO `simpanan_wajib` VALUES ('818', '169', null, '25000.00', '0', '1', '2017');
INSERT INTO `simpanan_wajib` VALUES ('819', '170', '2017-01-27', '25000.00', '1', '1', '2017');
INSERT INTO `simpanan_wajib` VALUES ('820', '171', '2017-01-27', '25000.00', '1', '1', '2017');
INSERT INTO `simpanan_wajib` VALUES ('821', '172', '2017-01-27', '25000.00', '1', '1', '2017');
INSERT INTO `simpanan_wajib` VALUES ('822', '173', '2017-01-27', '25000.00', '1', '1', '2017');
INSERT INTO `simpanan_wajib` VALUES ('823', '174', '2017-01-27', '25000.00', '1', '1', '2017');
INSERT INTO `simpanan_wajib` VALUES ('824', '175', '2017-01-27', '25000.00', '1', '1', '2017');
INSERT INTO `simpanan_wajib` VALUES ('825', '176', '2017-01-27', '25000.00', '1', '1', '2017');
INSERT INTO `simpanan_wajib` VALUES ('826', '177', '2017-01-27', '25000.00', '1', '1', '2017');
INSERT INTO `simpanan_wajib` VALUES ('827', '178', '2017-01-27', '25000.00', '1', '1', '2017');
INSERT INTO `simpanan_wajib` VALUES ('828', '179', '2017-01-27', '25000.00', '1', '1', '2017');
INSERT INTO `simpanan_wajib` VALUES ('829', '180', '2017-01-27', '25000.00', '1', '1', '2017');
INSERT INTO `simpanan_wajib` VALUES ('830', '181', '2017-01-27', '25000.00', '1', '1', '2017');
INSERT INTO `simpanan_wajib` VALUES ('831', '182', '2017-01-27', '25000.00', '1', '1', '2017');
INSERT INTO `simpanan_wajib` VALUES ('832', '183', '2017-01-27', '25000.00', '1', '1', '2017');
INSERT INTO `simpanan_wajib` VALUES ('833', '184', '2017-01-27', '25000.00', '1', '1', '2017');
INSERT INTO `simpanan_wajib` VALUES ('834', '185', '2017-01-27', '25000.00', '1', '1', '2017');
INSERT INTO `simpanan_wajib` VALUES ('835', '186', '2017-01-27', '25000.00', '1', '1', '2017');
INSERT INTO `simpanan_wajib` VALUES ('836', '187', '2017-01-27', '25000.00', '1', '1', '2017');
INSERT INTO `simpanan_wajib` VALUES ('837', '188', '2017-01-27', '25000.00', '1', '1', '2017');
INSERT INTO `simpanan_wajib` VALUES ('838', '189', '2017-01-27', '25000.00', '1', '1', '2017');
INSERT INTO `simpanan_wajib` VALUES ('839', '190', '2017-01-27', '25000.00', '1', '1', '2017');

-- ----------------------------
-- Table structure for transaksi_rutin
-- ----------------------------
DROP TABLE IF EXISTS `transaksi_rutin`;
CREATE TABLE `transaksi_rutin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `transaksi` varchar(100) DEFAULT NULL,
  `id_transaksi` int(11) DEFAULT NULL,
  `tanggal_transaksi` date DEFAULT NULL,
  `masuk` decimal(15,2) DEFAULT '0.00',
  `keluar` decimal(15,2) DEFAULT '0.00',
  `keterangan` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=103 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of transaksi_rutin
-- ----------------------------
INSERT INTO `transaksi_rutin` VALUES ('61', 'Pembayaran Simpanan Wajib', '838', '2017-01-27', '25000.00', '0.00', null);
INSERT INTO `transaksi_rutin` VALUES ('62', 'Pembayaran Simpanan Wajib', '837', '2017-01-27', '25000.00', '0.00', null);
INSERT INTO `transaksi_rutin` VALUES ('63', 'Pembayaran Simpanan Wajib', '836', '2017-01-27', '25000.00', '0.00', null);
INSERT INTO `transaksi_rutin` VALUES ('64', 'Pembayaran Simpanan Wajib', '835', '2017-01-27', '25000.00', '0.00', null);
INSERT INTO `transaksi_rutin` VALUES ('65', 'Pembayaran Simpanan Wajib', '834', '2017-01-27', '25000.00', '0.00', null);
INSERT INTO `transaksi_rutin` VALUES ('66', 'Pembayaran Simpanan Wajib', '833', '2017-01-27', '25000.00', '0.00', null);
INSERT INTO `transaksi_rutin` VALUES ('67', 'Pembayaran Simpanan Wajib', '832', '2017-01-27', '25000.00', '0.00', null);
INSERT INTO `transaksi_rutin` VALUES ('68', 'Pembayaran Simpanan Wajib', '831', '2017-01-27', '25000.00', '0.00', null);
INSERT INTO `transaksi_rutin` VALUES ('69', 'Pembayaran Simpanan Wajib', '830', '2017-01-27', '25000.00', '0.00', null);
INSERT INTO `transaksi_rutin` VALUES ('70', 'Pembayaran Simpanan Wajib', '829', '2017-01-27', '25000.00', '0.00', null);
INSERT INTO `transaksi_rutin` VALUES ('71', 'Pembayaran Simpanan Wajib', '828', '2017-01-27', '25000.00', '0.00', null);
INSERT INTO `transaksi_rutin` VALUES ('72', 'Pembayaran Simpanan Wajib', '827', '2017-01-27', '25000.00', '0.00', null);
INSERT INTO `transaksi_rutin` VALUES ('73', 'Pembayaran Simpanan Wajib', '826', '2017-01-27', '25000.00', '0.00', null);
INSERT INTO `transaksi_rutin` VALUES ('74', 'Pembayaran Simpanan Wajib', '825', '2017-01-27', '25000.00', '0.00', null);
INSERT INTO `transaksi_rutin` VALUES ('75', 'Pembayaran Simpanan Wajib', '824', '2017-01-27', '25000.00', '0.00', null);
INSERT INTO `transaksi_rutin` VALUES ('76', 'Pembayaran Simpanan Wajib', '823', '2017-01-27', '25000.00', '0.00', null);
INSERT INTO `transaksi_rutin` VALUES ('77', 'Pembayaran Simpanan Wajib', '822', '2017-01-27', '25000.00', '0.00', null);
INSERT INTO `transaksi_rutin` VALUES ('78', 'Pembayaran Simpanan Wajib', '821', '2017-01-27', '25000.00', '0.00', null);
INSERT INTO `transaksi_rutin` VALUES ('79', 'Pembayaran Simpanan Wajib', '820', '2017-01-27', '25000.00', '0.00', null);
INSERT INTO `transaksi_rutin` VALUES ('80', 'Pembayaran Simpanan Wajib', '819', '2017-01-27', '25000.00', '0.00', null);
INSERT INTO `transaksi_rutin` VALUES ('81', 'Peminjaman Anggota', '2', '2017-01-27', '0.00', '110000.00', null);
INSERT INTO `transaksi_rutin` VALUES ('94', 'Pembayaran Pinjaman Anggota', '17', '2017-01-27', '10000.00', '0.00', null);
INSERT INTO `transaksi_rutin` VALUES ('95', 'Bunga Pinjaman Anggota', '17', '2017-01-27', '1000.00', '0.00', null);
INSERT INTO `transaksi_rutin` VALUES ('96', 'Pembayaran Pinjaman Anggota', '18', '2017-01-27', '10000.00', '0.00', null);
INSERT INTO `transaksi_rutin` VALUES ('97', 'Bunga Pinjaman Anggota', '18', '2017-01-27', '1000.00', '0.00', null);
INSERT INTO `transaksi_rutin` VALUES ('98', 'Pembayaran Pinjaman Anggota', '19', '2017-01-27', '10000.00', '0.00', null);
INSERT INTO `transaksi_rutin` VALUES ('99', 'Bunga Pinjaman Anggota', '19', '2017-01-27', '1000.00', '0.00', null);
INSERT INTO `transaksi_rutin` VALUES ('100', 'Pembayaran Pinjaman Anggota', '20', '2017-01-27', '10000.00', '0.00', null);
INSERT INTO `transaksi_rutin` VALUES ('101', 'Bunga Pinjaman Anggota', '20', '2017-01-27', '1000.00', '0.00', null);
INSERT INTO `transaksi_rutin` VALUES ('102', 'Pembayaran Simpanan Wajib', '839', '2017-01-27', '25000.00', '0.00', null);

-- ----------------------------
-- Table structure for tunggakan
-- ----------------------------
DROP TABLE IF EXISTS `tunggakan`;
CREATE TABLE `tunggakan` (
  `id_pinjaman` int(11) DEFAULT NULL,
  `jumlah_tunggakan` decimal(15,2) DEFAULT NULL,
  `tanggal` date DEFAULT NULL,
  `status` int(11) DEFAULT '0',
  `tanggal_bayar` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tunggakan
-- ----------------------------

-- ----------------------------
-- View structure for count_all_tunggakan
-- ----------------------------
DROP VIEW IF EXISTS `count_all_tunggakan`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER  VIEW `count_all_tunggakan` AS SELECT
(SELECT
Count(A.id)
from 
simpanan_wajib as A
INNER JOIN anggota as B on A.id_anggota = B.id
WHERE 
-- A.periode <= MONTH(now()) AND A.tahun <= YEAR(now()) AND 
A.status_pembayaran = 0) as simpanan_wajib
,
(SElect count(A.id) from pembayaran as A
WHERE A.tanggal_batas <= now() and A.`status` = 0) as peminjaman ;

-- ----------------------------
-- View structure for get_count_anggota_tunggakan
-- ----------------------------
DROP VIEW IF EXISTS `get_count_anggota_tunggakan`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER  VIEW `get_count_anggota_tunggakan` AS (SELECT
Count(A.id) simpanan_wajib, 0 as peminjaman, B.id as id_anggota
from 
simpanan_wajib as A
INNER JOIN anggota as B on A.id_anggota = B.id
WHERE 
-- A.periode <= MONTH(now()) AND A.tahun <= YEAR(now()) AND 
A.status_pembayaran = 0 group by B.id)
union
(SElect 0 as simpanan_wajib, count(A.id) as peminjaman, BB.id_anggota from pembayaran as A
INNER JOIN peminjaman as BB on A.id_peminjaman = BB.id
WHERE 
-- A.tanggal_batas <= now() and 
A.`status` = 0 GROUP BY BB.id_anggota) ;

-- ----------------------------
-- View structure for v_daftar_akses_anggota
-- ----------------------------
DROP VIEW IF EXISTS `v_daftar_akses_anggota`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost`  VIEW `v_daftar_akses_anggota` AS SELECT * FROM anggota
INNER JOIN hak_akses_anggota on anggota.id = hak_akses_anggota.id_anggota
GROUP BY anggota.id ;

-- ----------------------------
-- View structure for v_get_access
-- ----------------------------
DROP VIEW IF EXISTS `v_get_access`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER  VIEW `v_get_access` AS SELECT
B.id_anggota,
C.nama,
B.id_hak_akses,
A.hak_akses
FROM
hak_akses AS A
INNER JOIN hak_akses_anggota AS B ON B.id_hak_akses = A.id
INNER JOIN anggota AS C ON B.id_anggota = C.id ;

-- ----------------------------
-- View structure for v_get_daftar_shu_anggota
-- ----------------------------
DROP VIEW IF EXISTS `v_get_daftar_shu_anggota`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost`  VIEW `v_get_daftar_shu_anggota` AS SELECT A.*, B.periode, B.tahun, C.nama
FROM
detail_shu AS A
INNER JOIN shu AS B ON A.id_shu = B.id
INNER JOIN anggota AS C ON A.id_anggota = C.id ;

-- ----------------------------
-- View structure for v_get_detail_shu
-- ----------------------------
DROP VIEW IF EXISTS `v_get_detail_shu`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost`  VIEW `v_get_detail_shu` AS SELECT
detail_shu.id_shu,
detail_shu.id,
shu.periode,
shu.tahun,
detail_shu.id_anggota,
anggota.nama,
detail_shu.anggota,
detail_shu.peminjam,
detail_shu.pengurus,
detail_shu.jumlah
FROM
detail_shu
INNER JOIN shu ON detail_shu.id_shu = shu.id
INNER JOIN anggota ON detail_shu.id_anggota = anggota.id ;

-- ----------------------------
-- View structure for v_get_tunggakan
-- ----------------------------
DROP VIEW IF EXISTS `v_get_tunggakan`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER  VIEW `v_get_tunggakan` AS SELECT * from pembayaran WHERE tanggal_batas <= now() and `status` = 0 ;

-- ----------------------------
-- View structure for v_get_tunggakan_pembayaran
-- ----------------------------
DROP VIEW IF EXISTS `v_get_tunggakan_pembayaran`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER  VIEW `v_get_tunggakan_pembayaran` AS SELECT
A.id,
A.id_peminjaman,
B.no_pinjaman,
B.id_anggota,
C.nama as nama_anggota,
A.tanggal_bayar,
A.tanggal_batas,
A.jumlah_bayar,
A.bunga,
A.pembayaran_ke,
A.keterangan,
A.`status`
from 
pembayaran as A
INNER JOIN peminjaman as B on A.id_peminjaman = B.id
INNER JOIN anggota as C on B.id_anggota = C.id
WHERE A.tanggal_batas <= now() and A.`status` = 0 ;

-- ----------------------------
-- View structure for v_list_total_tunggakan
-- ----------------------------
DROP VIEW IF EXISTS `v_list_total_tunggakan`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost`  VIEW `v_list_total_tunggakan` AS SELECT 
	id_anggota,
	no_pinjaman,
	count(`status`) kali_tunggakan, 
	sum(jumlah_bayar) total_jml_bayar, 
	sum(bunga) total_jml_bunga, 
	(sum(jumlah_bayar)+sum(bunga)) total
	FROM v_get_tunggakan_pembayaran 
	GROUP BY id_anggota ;

-- ----------------------------
-- View structure for v_pinjaman_aktif
-- ----------------------------
DROP VIEW IF EXISTS `v_pinjaman_aktif`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost`  VIEW `v_pinjaman_aktif` AS SELECT * from peminjaman WHERE `status` = 0 ;

-- ----------------------------
-- View structure for v_status_pinjaman
-- ----------------------------
DROP VIEW IF EXISTS `v_status_pinjaman`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost`  VIEW `v_status_pinjaman` AS SELECT
A.id_peminjaman,
B.no_pinjaman,
B.tgl_pinjaman,
B.id_anggota,
C.nama,
B.total_pinjaman,
sum(CASE WHEN A.`status` = 0 THEN 1 ELSE 0 end) as jml_belum_bayar,
sum(CASE WHEN A.`status` = 1 THEN 1 ELSE 0 end) as jml_sudah_bayar,
sum(CASE WHEN A.`status` = 0 THEN A.jumlah_bayar + A.bunga ELSE 0 end) as total_belum_bayar,
sum(CASE WHEN A.`status` = 1 THEN A.jumlah_bayar + A.bunga ELSE 0 end) as total_sudah_bayar,
(CASE WHEN B.`status` = 1 THEN 'Lunas' ELSE 'Belum Lunas' end) as `status`
from pembayaran AS A
INNER JOIN peminjaman AS B on A.id_peminjaman = B.id
INNER JOIN anggota AS C on B.id_anggota = C.id
GROUP BY A.id_peminjaman ;

-- ----------------------------
-- Procedure structure for generate_pembayaran
-- ----------------------------
DROP PROCEDURE IF EXISTS `generate_pembayaran`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `generate_pembayaran`(`id_pinjaman` int,`jumlah_pinjaman` decimal,`bunga` decimal,`kali_bayar` int, tgl_pinjaman date)
BEGIN
	declare xx int;	
	declare split_jml_pinjaman decimal(15,2);
	declare split_bunga decimal(15,2);
	
	set xx = 1;
	set split_jml_pinjaman = jumlah_pinjaman / kali_bayar;
	set split_bunga = bunga;
	
	while xx <=kali_bayar DO
		-- set tgl_batas = SELECT now() + INTERVAL xx MONTH;
		INSERT INTO pembayaran(id_peminjaman,jumlah_bayar,bunga,pembayaran_ke, tanggal_batas)VALUES(id_pinjaman,split_jml_pinjaman,split_bunga,xx,(SELECT tgl_pinjaman + INTERVAL xx MONTH));
		SET xx= xx+1;
	end while;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for p_generateSimpananW
-- ----------------------------
DROP PROCEDURE IF EXISTS `p_generateSimpananW`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `p_generateSimpananW`()
BEGIN
	DECLARE jumlahGen INT;
	declare jumlah_simpanan decimal(15,2);
	
	SELECT count(*) INTO jumlahGen from simpanan_wajib where periode = MONTH(NOW()) and tahun = YEAR(now());
	
	if(jumlahGen < 1) then
		select jumlah into jumlah_simpanan from jenis_simpanan where kode_simpanan = "wajib" limit 1;
		INSERT INTO simpanan_wajib(id_anggota,jumlah_bayar,status_pembayaran,periode,tahun)
		SELECT id,jumlah_simpanan,0,MONTH(NOW()),YEAR(now()) FROM anggota;
	end if;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for p_get_total_tunggakan_per_pelanggan
-- ----------------------------
DROP PROCEDURE IF EXISTS `p_get_total_tunggakan_per_pelanggan`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `p_get_total_tunggakan_per_pelanggan`(`anggota_id` int)
BEGIN
	SELECT 
	count(`status`) kali_tunggakan, 
	sum(jumlah_bayar) total_jml_bayar, 
	sum(bunga) total_jml_bunga, 
	(sum(jumlah_bayar)+sum(bunga)) total
 
	FROM v_get_tunggakan_pembayaran 
	WHERE id_anggota = anggota_id
	GROUP BY id_peminjaman;

END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for p_trans_rutin_del
-- ----------------------------
DROP PROCEDURE IF EXISTS `p_trans_rutin_del`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `p_trans_rutin_del`(`id_trans` int(11),`jns` varchar(255))
BEGIN
	DELETE FROM transaksi_rutin WHERE id = id_trans AND transaksi = jns;

END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for p_trans_rutin_in
-- ----------------------------
DROP PROCEDURE IF EXISTS `p_trans_rutin_in`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `p_trans_rutin_in`(`id_trans` int(11),`jenis_trans` varchar(255),`tgl_trans` date,`v_masuk` decimal(15,2),`v_keluar` decimal(15,2),`ket` varchar(255))
BEGIN
	INSERT INTO transaksi_rutin(transaksi,id_transaksi,tanggal_transaksi,masuk,keluar,keterangan) 
	VALUES (jenis_trans,id_trans,tgl_trans,v_masuk, v_keluar, ket);

END
;;
DELIMITER ;
DROP TRIGGER IF EXISTS `generate_pembayaran`;
DELIMITER ;;
CREATE TRIGGER `generate_pembayaran` AFTER INSERT ON `peminjaman` FOR EACH ROW call generate_pembayaran(NEW.id,NEW.jumlah_pinjaman, NEW.bunga, NEW.kali_bayar, NEW.tgl_pinjaman)
;;
DELIMITER ;
