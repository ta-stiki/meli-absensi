/*
Navicat MySQL Data Transfer

Source Server         : local
Source Server Version : 50505
Source Host           : localhost:3306
Source Database       : sikopat

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2017-02-08 01:36:03
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for anggota
-- ----------------------------
DROP TABLE IF EXISTS `anggota`;
CREATE TABLE `anggota` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nik` varchar(20) DEFAULT NULL,
  `nama` varchar(255) DEFAULT NULL,
  `tgl_lahir` date DEFAULT NULL,
  `jenis_kelamin` varchar(20) DEFAULT NULL,
  `alamat` varchar(100) DEFAULT NULL,
  `no_telp` varchar(20) DEFAULT NULL,
  `id_jabatan` int(11) DEFAULT NULL,
  `tgl_masuk` date DEFAULT NULL,
  `tgl_berhenti` date DEFAULT NULL,
  `tipe_anggota` varchar(10) DEFAULT NULL,
  `no_identitas` varchar(20) DEFAULT NULL,
  `saldo_simpanan` double(15,2) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `username` varchar(20) DEFAULT NULL,
  `password` varchar(50) DEFAULT NULL,
  `foto` varchar(255) DEFAULT NULL,
  `kali_bayar_bunga` int(11) DEFAULT NULL,
  `kali_tunggakan` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_anggota_jabatan_1` (`id_jabatan`),
  CONSTRAINT `fk_anggota_jabatan_1` FOREIGN KEY (`id_jabatan`) REFERENCES `jabatan` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=191 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of anggota
-- ----------------------------
INSERT INTO `anggota` VALUES ('108', null, 'I Dewa Made Khrisna Muku, M.T.', '2017-01-01', 'laki-laki', null, null, '4', null, null, null, null, '100000.00', '1', 'dewa', 'muku', null, null, null);
INSERT INTO `anggota` VALUES ('109', null, 'A.A Gde Bagus Ariana, S.T.', '2017-01-01', 'laki-laki', null, null, null, null, null, null, null, null, '1', null, null, null, null, null);
INSERT INTO `anggota` VALUES ('110', null, 'Aniek Suryanti Kusuma, M.Kom.', '2017-01-01', 'laki-laki', null, null, null, null, null, null, null, null, '1', null, null, null, null, null);
INSERT INTO `anggota` VALUES ('111', null, 'Brigida Arie Minartiningtyas, M.Kom.', '2017-01-01', 'laki-laki', null, null, null, null, null, null, null, null, '1', null, null, null, null, null);
INSERT INTO `anggota` VALUES ('112', null, 'Dewa Putu Yudhi Ardiana, S.KOM', '2017-01-01', 'laki-laki', null, null, null, null, null, null, null, null, '1', null, null, null, null, null);
INSERT INTO `anggota` VALUES ('113', null, 'Dwi Putra Githa, S.T., M.T.', '2017-01-01', 'laki-laki', null, null, null, null, null, null, null, null, '1', null, null, null, null, null);
INSERT INTO `anggota` VALUES ('114', null, 'I Wayan Sudiarsa, S. T., M. Kom', '2017-01-01', 'laki-laki', null, null, null, null, null, null, null, null, '1', null, null, null, null, null);
INSERT INTO `anggota` VALUES ('115', null, 'I Dewa Made Adi Baskara Joni, M.Kom.', '2017-01-01', 'laki-laki', null, null, '4', null, null, null, null, '100000.00', '1', null, null, null, null, null);
INSERT INTO `anggota` VALUES ('116', null, 'I Kadek Dwi Gandika Supartha, S.T.', '2017-01-01', 'laki-laki', null, null, null, null, null, null, null, null, '1', null, null, null, null, null);
INSERT INTO `anggota` VALUES ('117', null, 'I Made Ardwi Pradnyana, M.T.', '2017-01-01', 'laki-laki', null, null, null, null, null, null, null, null, '1', null, null, null, null, null);
INSERT INTO `anggota` VALUES ('118', null, 'I Made Marthana Yusa, M.Ds.', '2017-01-01', 'laki-laki', null, null, null, null, null, null, null, null, '1', null, null, null, null, null);
INSERT INTO `anggota` VALUES ('119', null, 'I Putu Gd Budayasa. SST.Par.,M.T.I', '2017-01-01', 'laki-laki', null, null, null, null, null, null, null, null, '1', null, null, null, null, null);
INSERT INTO `anggota` VALUES ('120', null, 'Desak Made Dwi Utami Putra, S.Si, M. Cs', '2017-01-01', 'laki-laki', null, null, null, null, null, null, null, null, '1', null, null, null, null, null);
INSERT INTO `anggota` VALUES ('121', null, 'I Nyoman Jayanegara, S.Sn', '2017-01-01', 'laki-laki', null, null, null, null, null, null, null, null, '1', null, null, null, null, null);
INSERT INTO `anggota` VALUES ('122', null, 'I Nyoman Anom Fajaraditya, S. Sn', '2017-01-01', 'laki-laki', null, null, null, null, null, null, null, null, '1', null, null, null, null, null);
INSERT INTO `anggota` VALUES ('123', null, 'I Nyoman Agus Suarya, M. Sn', '2017-01-01', 'laki-laki', null, null, null, null, null, null, null, null, '1', null, null, null, null, null);
INSERT INTO `anggota` VALUES ('124', null, 'Ida Bagus Ary Indra Iswara,S.Kom.,M.Kom', '2017-01-01', 'laki-laki', null, null, null, null, null, null, null, null, '1', null, null, null, null, null);
INSERT INTO `anggota` VALUES ('125', null, 'Gede Aditra Pradnyana,S.Kom.,M.Kom', '2017-01-01', 'laki-laki', null, null, null, null, null, null, null, null, '1', null, null, null, null, null);
INSERT INTO `anggota` VALUES ('126', null, 'I Nyoman Buda Hartawan,S.Kom.,M.Kom', '2017-01-01', 'laki-laki', null, null, null, null, null, null, null, null, '1', null, null, null, null, null);
INSERT INTO `anggota` VALUES ('127', null, 'I Nyoman Widhi Adnyana,S.Kom', '2017-01-01', 'laki-laki', null, null, null, null, null, null, null, null, '1', null, null, null, null, null);
INSERT INTO `anggota` VALUES ('128', null, 'Komang Kurniawan Widiartha,S.Kom.,M.Cs', '2017-01-01', 'laki-laki', null, null, null, null, null, null, null, null, '1', null, null, null, null, null);
INSERT INTO `anggota` VALUES ('129', null, 'I Putu Adi Pratama,S.Kom.,M.Cs', '2017-01-01', 'laki-laki', null, null, null, null, null, null, null, null, '1', null, null, null, null, null);
INSERT INTO `anggota` VALUES ('130', null, 'Anak Agung Gde Ekayana,M.Pd', '2017-01-01', 'laki-laki', null, null, null, null, null, null, null, null, '1', null, null, null, null, null);
INSERT INTO `anggota` VALUES ('131', null, 'Welda,S.Kom.,M.T.I', '2017-01-01', 'laki-laki', null, null, null, null, null, null, null, null, '1', null, null, null, null, null);
INSERT INTO `anggota` VALUES ('132', null, 'Putu Sugiartawan, S.Kom.,M.Cs.,M.Agb', '2017-01-01', 'laki-laki', null, null, null, null, null, null, null, null, '1', null, null, null, null, null);
INSERT INTO `anggota` VALUES ('133', null, 'Made Hanindia Prami Swari,S.Kom.,M.Cs', '2017-01-01', 'laki-laki', null, null, '1', null, null, null, null, null, '1', '1111', '1111', null, null, null);
INSERT INTO `anggota` VALUES ('134', null, 'Ni Wayan Sumartini Saraswati,MT', '2017-01-01', 'laki-laki', null, null, null, null, null, null, null, null, '1', null, null, null, null, null);
INSERT INTO `anggota` VALUES ('135', null, 'I Gd Putu Eka Suryana,S.Pd.,M.Cs', '2017-01-01', 'laki-laki', null, null, null, null, null, null, null, null, '1', null, null, null, null, null);
INSERT INTO `anggota` VALUES ('136', null, 'Sri Widiastutik, S.S., M.Hum', '2017-01-01', 'laki-laki', null, null, null, null, null, null, null, null, '1', null, null, null, null, null);
INSERT INTO `anggota` VALUES ('137', null, 'Ida Bagus Gede Anandita, S. Kom', '2017-01-01', 'laki-laki', null, null, null, null, null, null, null, null, '1', null, null, null, null, null);
INSERT INTO `anggota` VALUES ('138', null, 'Ketut Jaya Atmaja, S.Kom', '2017-01-01', 'laki-laki', null, null, null, null, null, null, null, null, '1', null, null, null, null, null);
INSERT INTO `anggota` VALUES ('139', null, 'Ayu Gede Wildahlia, S.E, M.M.', '2017-01-01', 'laki-laki', null, null, null, null, null, null, null, null, '1', null, null, null, null, null);
INSERT INTO `anggota` VALUES ('140', null, 'I Nym Saputra Wahyu Wijaya', '2017-01-01', 'laki-laki', null, null, '1', null, null, null, null, null, '1', 'wahyu', 'wahyu', null, null, null);
INSERT INTO `anggota` VALUES ('141', null, 'Wayan Gede Suka Parwita', '2017-01-01', 'laki-laki', null, null, null, null, null, null, null, '25000.00', '1', null, null, null, null, null);
INSERT INTO `anggota` VALUES ('142', null, 'Putu Praba Santika', '2017-01-01', 'laki-laki', null, null, null, null, null, null, null, '25000.00', '1', null, null, null, null, null);
INSERT INTO `anggota` VALUES ('143', null, 'I Gede Totok Suryawan', '2017-01-01', 'laki-laki', null, null, null, null, null, null, null, '25000.00', '1', null, null, null, null, null);
INSERT INTO `anggota` VALUES ('144', null, 'I Gusti Md Ngurah Desnanjaya', '2017-01-01', 'laki-laki', null, null, null, null, null, null, null, '25000.00', '1', null, null, null, null, null);
INSERT INTO `anggota` VALUES ('145', null, 'I Kadek Susila Satwika, S.T.', '2017-01-01', 'laki-laki', null, null, null, null, null, null, null, '25000.00', '1', null, null, null, null, null);
INSERT INTO `anggota` VALUES ('146', null, 'Putu Satriya Udnyana Putra', '2017-01-01', 'laki-laki', null, null, null, null, null, null, null, '25000.00', '1', null, null, null, null, null);
INSERT INTO `anggota` VALUES ('147', null, 'A. A. Ketut Putra', '2017-01-01', 'laki-laki', null, null, null, null, null, null, null, '25000.00', '1', null, null, null, null, null);
INSERT INTO `anggota` VALUES ('148', null, 'Ketut Suriana', '2017-01-01', 'laki-laki', null, null, null, null, null, null, null, '25000.00', '1', null, null, null, null, null);
INSERT INTO `anggota` VALUES ('149', null, 'I Ketut Merta', '2017-01-01', 'laki-laki', null, null, null, null, null, null, null, '25000.00', '1', null, null, null, null, null);
INSERT INTO `anggota` VALUES ('150', null, 'I Nyoman Erik Sindunata', '2017-01-01', 'laki-laki', null, null, null, null, null, null, null, '25000.00', '1', null, null, null, null, null);
INSERT INTO `anggota` VALUES ('151', null, 'I Made Suantara', '2017-01-01', 'laki-laki', null, null, null, null, null, null, null, '25000.00', '1', null, null, null, null, null);
INSERT INTO `anggota` VALUES ('152', null, 'M. Rofii (Pak Robi)', '2017-01-01', 'laki-laki', null, null, null, null, null, null, null, '25000.00', '1', null, null, null, null, null);
INSERT INTO `anggota` VALUES ('153', null, 'I Dewa Nyoman Mudita', '2017-01-01', 'laki-laki', null, null, null, null, null, null, null, '25000.00', '1', null, null, null, null, null);
INSERT INTO `anggota` VALUES ('154', null, 'I Wayan Sulendra', '2017-01-01', 'laki-laki', null, null, null, null, null, null, null, '25000.00', '1', null, null, null, null, null);
INSERT INTO `anggota` VALUES ('155', null, 'Ni Nengah Rinca Wati, S. E', '2017-01-01', 'laki-laki', null, null, null, null, null, null, null, '25000.00', '1', null, null, null, null, null);
INSERT INTO `anggota` VALUES ('156', null, 'Putu Anik Suryantini', '2017-01-01', 'laki-laki', null, null, null, null, null, null, null, '25000.00', '1', null, null, null, null, null);
INSERT INTO `anggota` VALUES ('157', null, 'Kadek Ayu Ariningsih', '2017-01-01', 'laki-laki', null, null, null, null, null, null, null, '25000.00', '1', null, null, null, null, null);
INSERT INTO `anggota` VALUES ('158', null, 'Ni Putu Meilita Yani', '2017-01-01', 'laki-laki', null, null, null, null, null, null, null, '25000.00', '1', null, null, null, null, null);
INSERT INTO `anggota` VALUES ('159', null, 'Ni Kadek Nita Noviani Pande, S.Pd', '2017-01-01', 'laki-laki', null, null, null, null, null, null, null, '25000.00', '1', null, null, null, null, null);
INSERT INTO `anggota` VALUES ('160', null, 'Robert Wijaya', '2017-01-01', 'laki-laki', null, null, null, null, null, null, null, '25000.00', '1', null, null, null, null, null);
INSERT INTO `anggota` VALUES ('161', null, 'M. Hanafi', '2017-01-01', 'laki-laki', null, null, null, null, null, null, null, '25000.00', '1', null, null, null, null, null);
INSERT INTO `anggota` VALUES ('162', null, 'Khairrudin, S.E', '2017-01-01', 'laki-laki', null, null, null, null, null, null, null, '25000.00', '1', null, null, null, null, null);
INSERT INTO `anggota` VALUES ('163', null, 'I Nyoman Sedana', '2017-01-01', 'laki-laki', null, null, null, null, null, null, null, '25000.00', '1', null, null, null, null, null);
INSERT INTO `anggota` VALUES ('164', null, 'Komang Widiantara', '2017-01-01', 'laki-laki', null, null, null, null, null, null, null, '25000.00', '1', null, null, null, null, null);
INSERT INTO `anggota` VALUES ('165', null, 'Dewa Gede Surya Damanik', '2017-01-01', 'laki-laki', null, null, null, null, null, null, null, '25000.00', '1', null, null, null, null, null);
INSERT INTO `anggota` VALUES ('166', null, 'Emi Widya Sari', '2017-01-01', 'laki-laki', null, null, null, null, null, null, null, '25000.00', '1', null, null, null, null, null);
INSERT INTO `anggota` VALUES ('167', null, 'Rury Ayuning Lati, S.Pd', '2017-01-01', 'laki-laki', null, null, null, null, null, null, null, '125000.00', '1', null, null, null, null, null);
INSERT INTO `anggota` VALUES ('168', null, 'Ni Kade Ayu Nirwana, S.Pd', '2017-01-01', 'laki-laki', null, null, null, null, null, null, null, '25000.00', '1', null, null, null, null, null);
INSERT INTO `anggota` VALUES ('169', null, 'Ni Putu Erra Budiantari, S. Pd', '2017-01-01', 'laki-laki', null, null, null, null, null, null, null, '25000.00', '1', null, null, null, null, null);
INSERT INTO `anggota` VALUES ('170', null, 'Luh Putu Mega Pratami', '2017-01-01', 'laki-laki', null, null, null, null, null, null, null, '50000.00', '1', null, null, null, null, null);
INSERT INTO `anggota` VALUES ('171', null, 'Ni Made Eny Indrawati,S.Pd', '2017-01-01', 'laki-laki', null, null, null, null, null, null, null, '50000.00', '1', null, null, null, null, null);
INSERT INTO `anggota` VALUES ('172', null, 'Gabriella Christine Lahal', '2017-01-01', 'laki-laki', null, null, null, null, null, null, null, '50000.00', '1', null, null, null, null, null);
INSERT INTO `anggota` VALUES ('173', null, 'Ni Komang Novita Dewi', '2017-01-01', 'laki-laki', null, null, null, null, null, null, null, '50000.00', '1', null, null, null, null, null);
INSERT INTO `anggota` VALUES ('174', null, 'Ida Ayu Gede Anindyatari', '2017-01-01', 'laki-laki', 'sdsdas', '355445', '2', '2017-02-05', '0000-00-00', null, '45345sd', '50000.00', '1', 'dayu', '12345', null, null, null);
INSERT INTO `anggota` VALUES ('175', null, 'Agus Swartawan', '2017-01-01', 'laki-laki', null, null, null, null, null, null, null, '50000.00', '1', null, null, null, null, null);
INSERT INTO `anggota` VALUES ('176', null, 'I Dewa Ayu Tantri Pramawati, S.E', '2017-01-01', 'laki-laki', null, null, null, null, null, null, null, '50000.00', '1', null, null, null, null, null);
INSERT INTO `anggota` VALUES ('177', null, 'Ni Putu Sri Pancadewi, S. Pd', '2017-01-01', 'laki-laki', null, null, null, null, null, null, null, '50000.00', '1', null, null, null, null, null);
INSERT INTO `anggota` VALUES ('178', null, 'Ni Kadek Ceryna Dewi', '2017-01-01', 'laki-laki', null, null, null, null, null, null, null, '50000.00', '1', null, null, null, null, null);
INSERT INTO `anggota` VALUES ('179', null, 'Alamsyah', '2017-01-01', 'laki-laki', null, null, null, null, null, null, null, '50000.00', '1', null, null, null, null, null);
INSERT INTO `anggota` VALUES ('180', null, 'Andrianus T. Balibo', '2017-01-01', 'laki-laki', null, null, null, null, null, null, null, '50000.00', '1', null, null, null, null, null);
INSERT INTO `anggota` VALUES ('181', null, 'Agus Harianto', '2017-01-01', 'laki-laki', null, null, '1', null, null, null, null, '50000.00', '1', null, null, null, null, null);
INSERT INTO `anggota` VALUES ('182', null, 'Ketut Suartana', '2017-01-01', 'laki-laki', null, null, null, null, null, null, null, '50000.00', '1', null, null, null, null, null);
INSERT INTO `anggota` VALUES ('183', null, 'I Komang Catra', '2017-01-01', 'laki-laki', null, null, null, null, null, null, null, '50000.00', '1', null, null, null, null, null);
INSERT INTO `anggota` VALUES ('184', null, 'Ni Luh Wiwik Sri Rahayu G. S.Kom., M.Kom.', '2017-01-01', 'laki-laki', null, null, null, null, null, null, null, '50000.00', '1', null, null, null, null, null);
INSERT INTO `anggota` VALUES ('185', null, 'Made Suci Ariantini, S.Pd., M.Kom.', '2017-01-01', 'laki-laki', null, null, '1', null, null, null, null, '50000.00', '1', null, null, null, null, null);
INSERT INTO `anggota` VALUES ('186', null, 'I G. A.A. Diatri Indradewi, S.Kom., M.T.', '2017-01-01', 'laki-laki', null, null, '1', null, null, null, null, '50000.00', '1', null, null, null, null, null);
INSERT INTO `anggota` VALUES ('187', null, 'Wayan Eny Mariani, S.M.B., M.Si.', '2017-01-01', 'laki-laki', null, null, '5', null, null, null, null, '50000.00', '1', null, null, null, null, null);
INSERT INTO `anggota` VALUES ('188', null, 'I Wayan Agus Surya Darma, S.Kom., M.T.', '2017-01-01', 'laki-laki', null, null, '2', null, null, null, null, '50000.00', '1', null, null, null, null, null);
INSERT INTO `anggota` VALUES ('189', '12314', 'Luh Putu Ariestari Pradnyadewi, S. Ab.', '2017-01-01', 'laki-laki', 'sdsdas', '355445', '3', '2017-02-07', null, null, '45345sd', '50000.00', '1', '1234', '1234', null, null, null);
INSERT INTO `anggota` VALUES ('190', '123', 'Jojo', '2017-02-06', 'laki-laki', 'fgjhk;l\'lkjhgfd', '3546789087654', '1', '2017-01-01', '0000-00-00', null, '23456789', '75000.00', '1', '12345', '12345', null, null, null);

-- ----------------------------
-- Table structure for arus_kas
-- ----------------------------
DROP TABLE IF EXISTS `arus_kas`;
CREATE TABLE `arus_kas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `transaksi` varchar(255) DEFAULT NULL,
  `id_transaksi` int(11) DEFAULT NULL,
  `id_user` int(11) DEFAULT NULL,
  `waktu` datetime DEFAULT NULL,
  `masuk` decimal(15,2) DEFAULT NULL,
  `keluar` decimal(15,2) DEFAULT NULL,
  `keterangan` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of arus_kas
-- ----------------------------

-- ----------------------------
-- Table structure for auto_number
-- ----------------------------
DROP TABLE IF EXISTS `auto_number`;
CREATE TABLE `auto_number` (
  `group` varchar(10) DEFAULT NULL,
  `number` int(11) DEFAULT NULL,
  `lock_p` int(255) DEFAULT NULL,
  `update` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of auto_number
-- ----------------------------
INSERT INTO `auto_number` VALUES ('pinjaman', '1', null, null);

-- ----------------------------
-- Table structure for detail_shu
-- ----------------------------
DROP TABLE IF EXISTS `detail_shu`;
CREATE TABLE `detail_shu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_shu` int(11) DEFAULT NULL,
  `id_anggota` int(11) DEFAULT NULL,
  `anggota` decimal(15,2) DEFAULT NULL,
  `peminjam` decimal(15,2) DEFAULT NULL,
  `pengurus` decimal(15,2) DEFAULT NULL,
  `pengawas` decimal(15,2) DEFAULT NULL,
  `jumlah` decimal(15,2) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_detail_shu_shu_1` (`id_shu`),
  KEY `fk_detail_shu_anggota_1` (`id_anggota`),
  CONSTRAINT `fk_detail_shu_anggota_1` FOREIGN KEY (`id_anggota`) REFERENCES `anggota` (`id`),
  CONSTRAINT `fk_detail_shu_shu_1` FOREIGN KEY (`id_shu`) REFERENCES `shu` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=833 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of detail_shu
-- ----------------------------
INSERT INTO `detail_shu` VALUES ('750', '14', '108', '1385.54', null, null, '5750.00', null);
INSERT INTO `detail_shu` VALUES ('751', '14', '109', '1385.54', null, null, null, null);
INSERT INTO `detail_shu` VALUES ('752', '14', '110', '1385.54', null, null, null, null);
INSERT INTO `detail_shu` VALUES ('753', '14', '111', '1385.54', null, null, null, null);
INSERT INTO `detail_shu` VALUES ('754', '14', '112', '1385.54', null, null, null, null);
INSERT INTO `detail_shu` VALUES ('755', '14', '113', '1385.54', null, null, null, null);
INSERT INTO `detail_shu` VALUES ('756', '14', '114', '1385.54', null, null, null, null);
INSERT INTO `detail_shu` VALUES ('757', '14', '115', '1385.54', '46000.00', null, '5750.00', null);
INSERT INTO `detail_shu` VALUES ('758', '14', '116', '1385.54', null, null, null, null);
INSERT INTO `detail_shu` VALUES ('759', '14', '117', '1385.54', null, null, null, null);
INSERT INTO `detail_shu` VALUES ('760', '14', '118', '1385.54', null, null, null, null);
INSERT INTO `detail_shu` VALUES ('761', '14', '119', '1385.54', null, null, null, null);
INSERT INTO `detail_shu` VALUES ('762', '14', '120', '1385.54', null, null, null, null);
INSERT INTO `detail_shu` VALUES ('763', '14', '121', '1385.54', null, null, null, null);
INSERT INTO `detail_shu` VALUES ('764', '14', '122', '1385.54', null, null, null, null);
INSERT INTO `detail_shu` VALUES ('765', '14', '123', '1385.54', null, null, null, null);
INSERT INTO `detail_shu` VALUES ('766', '14', '124', '1385.54', null, null, null, null);
INSERT INTO `detail_shu` VALUES ('767', '14', '125', '1385.54', null, null, null, null);
INSERT INTO `detail_shu` VALUES ('768', '14', '126', '1385.54', null, null, null, null);
INSERT INTO `detail_shu` VALUES ('769', '14', '127', '1385.54', null, null, null, null);
INSERT INTO `detail_shu` VALUES ('770', '14', '128', '1385.54', null, null, null, null);
INSERT INTO `detail_shu` VALUES ('771', '14', '129', '1385.54', null, null, null, null);
INSERT INTO `detail_shu` VALUES ('772', '14', '130', '1385.54', null, null, null, null);
INSERT INTO `detail_shu` VALUES ('773', '14', '131', '1385.54', null, null, null, null);
INSERT INTO `detail_shu` VALUES ('774', '14', '132', '1385.54', null, null, null, null);
INSERT INTO `detail_shu` VALUES ('775', '14', '133', '1385.54', null, null, null, null);
INSERT INTO `detail_shu` VALUES ('776', '14', '134', '1385.54', null, null, null, null);
INSERT INTO `detail_shu` VALUES ('777', '14', '135', '1385.54', null, null, null, null);
INSERT INTO `detail_shu` VALUES ('778', '14', '136', '1385.54', null, null, null, null);
INSERT INTO `detail_shu` VALUES ('779', '14', '137', '1385.54', null, null, null, null);
INSERT INTO `detail_shu` VALUES ('780', '14', '138', '1385.54', null, null, null, null);
INSERT INTO `detail_shu` VALUES ('781', '14', '139', '1385.54', null, null, null, null);
INSERT INTO `detail_shu` VALUES ('782', '14', '140', '1385.54', null, null, null, null);
INSERT INTO `detail_shu` VALUES ('783', '14', '141', '1385.54', null, null, null, null);
INSERT INTO `detail_shu` VALUES ('784', '14', '142', '1385.54', null, null, null, null);
INSERT INTO `detail_shu` VALUES ('785', '14', '143', '1385.54', null, null, null, null);
INSERT INTO `detail_shu` VALUES ('786', '14', '144', '1385.54', null, null, null, null);
INSERT INTO `detail_shu` VALUES ('787', '14', '145', '1385.54', null, null, null, null);
INSERT INTO `detail_shu` VALUES ('788', '14', '146', '1385.54', null, null, null, null);
INSERT INTO `detail_shu` VALUES ('789', '14', '147', '1385.54', null, null, null, null);
INSERT INTO `detail_shu` VALUES ('790', '14', '148', '1385.54', null, null, null, null);
INSERT INTO `detail_shu` VALUES ('791', '14', '149', '1385.54', null, null, null, null);
INSERT INTO `detail_shu` VALUES ('792', '14', '150', '1385.54', null, null, null, null);
INSERT INTO `detail_shu` VALUES ('793', '14', '151', '1385.54', null, null, null, null);
INSERT INTO `detail_shu` VALUES ('794', '14', '152', '1385.54', null, null, null, null);
INSERT INTO `detail_shu` VALUES ('795', '14', '153', '1385.54', null, null, null, null);
INSERT INTO `detail_shu` VALUES ('796', '14', '154', '1385.54', null, null, null, null);
INSERT INTO `detail_shu` VALUES ('797', '14', '155', '1385.54', null, null, null, null);
INSERT INTO `detail_shu` VALUES ('798', '14', '156', '1385.54', null, null, null, null);
INSERT INTO `detail_shu` VALUES ('799', '14', '157', '1385.54', null, null, null, null);
INSERT INTO `detail_shu` VALUES ('800', '14', '158', '1385.54', null, null, null, null);
INSERT INTO `detail_shu` VALUES ('801', '14', '159', '1385.54', null, null, null, null);
INSERT INTO `detail_shu` VALUES ('802', '14', '160', '1385.54', null, null, null, null);
INSERT INTO `detail_shu` VALUES ('803', '14', '161', '1385.54', null, null, null, null);
INSERT INTO `detail_shu` VALUES ('804', '14', '162', '1385.54', null, null, null, null);
INSERT INTO `detail_shu` VALUES ('805', '14', '163', '1385.54', null, null, null, null);
INSERT INTO `detail_shu` VALUES ('806', '14', '164', '1385.54', null, null, null, null);
INSERT INTO `detail_shu` VALUES ('807', '14', '165', '1385.54', null, null, null, null);
INSERT INTO `detail_shu` VALUES ('808', '14', '166', '1385.54', null, null, null, null);
INSERT INTO `detail_shu` VALUES ('809', '14', '167', '1385.54', null, null, null, null);
INSERT INTO `detail_shu` VALUES ('810', '14', '168', '1385.54', null, null, null, null);
INSERT INTO `detail_shu` VALUES ('811', '14', '169', '1385.54', null, null, null, null);
INSERT INTO `detail_shu` VALUES ('812', '14', '170', '1385.54', null, null, null, null);
INSERT INTO `detail_shu` VALUES ('813', '14', '171', '1385.54', null, null, null, null);
INSERT INTO `detail_shu` VALUES ('814', '14', '172', '1385.54', null, null, null, null);
INSERT INTO `detail_shu` VALUES ('815', '14', '173', '1385.54', null, null, null, null);
INSERT INTO `detail_shu` VALUES ('816', '14', '174', '1385.54', null, '11500.00', null, null);
INSERT INTO `detail_shu` VALUES ('817', '14', '175', '1385.54', null, null, null, null);
INSERT INTO `detail_shu` VALUES ('818', '14', '176', '1385.54', null, null, null, null);
INSERT INTO `detail_shu` VALUES ('819', '14', '177', '1385.54', null, null, null, null);
INSERT INTO `detail_shu` VALUES ('820', '14', '178', '1385.54', null, null, null, null);
INSERT INTO `detail_shu` VALUES ('821', '14', '179', '1385.54', null, null, null, null);
INSERT INTO `detail_shu` VALUES ('822', '14', '180', '1385.54', null, null, null, null);
INSERT INTO `detail_shu` VALUES ('823', '14', '181', '1385.54', null, null, null, null);
INSERT INTO `detail_shu` VALUES ('824', '14', '182', '1385.54', null, null, null, null);
INSERT INTO `detail_shu` VALUES ('825', '14', '183', '1385.54', null, null, null, null);
INSERT INTO `detail_shu` VALUES ('826', '14', '184', '1385.54', null, null, null, null);
INSERT INTO `detail_shu` VALUES ('827', '14', '185', '1385.54', null, null, null, null);
INSERT INTO `detail_shu` VALUES ('828', '14', '186', '1385.54', null, null, null, null);
INSERT INTO `detail_shu` VALUES ('829', '14', '187', '1385.54', null, null, null, null);
INSERT INTO `detail_shu` VALUES ('830', '14', '188', '1385.54', null, null, null, null);
INSERT INTO `detail_shu` VALUES ('831', '14', '189', '1385.54', null, null, null, null);
INSERT INTO `detail_shu` VALUES ('832', '14', '190', '1385.54', null, '11500.00', null, null);

-- ----------------------------
-- Table structure for hak_akses
-- ----------------------------
DROP TABLE IF EXISTS `hak_akses`;
CREATE TABLE `hak_akses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `hak_akses` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of hak_akses
-- ----------------------------
INSERT INTO `hak_akses` VALUES ('1', 'Anggota');
INSERT INTO `hak_akses` VALUES ('2', 'Pengawas');
INSERT INTO `hak_akses` VALUES ('3', 'Ketua');
INSERT INTO `hak_akses` VALUES ('4', 'Bendahara');
INSERT INTO `hak_akses` VALUES ('5', 'Wakil');

-- ----------------------------
-- Table structure for hak_akses_anggota
-- ----------------------------
DROP TABLE IF EXISTS `hak_akses_anggota`;
CREATE TABLE `hak_akses_anggota` (
  `id_anggota` int(11) DEFAULT NULL,
  `id_hak_akses` int(11) DEFAULT NULL,
  KEY `fk_hak_akses_anggota_hak_akses_1` (`id_hak_akses`) USING BTREE,
  KEY `fk_hak_akses_anggota_anggota_1` (`id_anggota`) USING BTREE,
  CONSTRAINT `hak_akses_anggota_ibfk_1` FOREIGN KEY (`id_anggota`) REFERENCES `anggota` (`id`),
  CONSTRAINT `hak_akses_anggota_ibfk_2` FOREIGN KEY (`id_hak_akses`) REFERENCES `hak_akses` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of hak_akses_anggota
-- ----------------------------
INSERT INTO `hak_akses_anggota` VALUES ('109', '1');
INSERT INTO `hak_akses_anggota` VALUES ('110', '1');
INSERT INTO `hak_akses_anggota` VALUES ('111', '1');
INSERT INTO `hak_akses_anggota` VALUES ('112', '1');
INSERT INTO `hak_akses_anggota` VALUES ('113', '1');
INSERT INTO `hak_akses_anggota` VALUES ('114', '1');
INSERT INTO `hak_akses_anggota` VALUES ('116', '1');
INSERT INTO `hak_akses_anggota` VALUES ('117', '1');
INSERT INTO `hak_akses_anggota` VALUES ('118', '1');
INSERT INTO `hak_akses_anggota` VALUES ('119', '1');
INSERT INTO `hak_akses_anggota` VALUES ('120', '1');
INSERT INTO `hak_akses_anggota` VALUES ('121', '1');
INSERT INTO `hak_akses_anggota` VALUES ('122', '1');
INSERT INTO `hak_akses_anggota` VALUES ('123', '1');
INSERT INTO `hak_akses_anggota` VALUES ('124', '1');
INSERT INTO `hak_akses_anggota` VALUES ('125', '1');
INSERT INTO `hak_akses_anggota` VALUES ('126', '1');
INSERT INTO `hak_akses_anggota` VALUES ('127', '1');
INSERT INTO `hak_akses_anggota` VALUES ('128', '1');
INSERT INTO `hak_akses_anggota` VALUES ('129', '1');
INSERT INTO `hak_akses_anggota` VALUES ('130', '1');
INSERT INTO `hak_akses_anggota` VALUES ('131', '1');
INSERT INTO `hak_akses_anggota` VALUES ('132', '1');
INSERT INTO `hak_akses_anggota` VALUES ('134', '1');
INSERT INTO `hak_akses_anggota` VALUES ('135', '1');
INSERT INTO `hak_akses_anggota` VALUES ('136', '1');
INSERT INTO `hak_akses_anggota` VALUES ('137', '1');
INSERT INTO `hak_akses_anggota` VALUES ('138', '1');
INSERT INTO `hak_akses_anggota` VALUES ('139', '1');
INSERT INTO `hak_akses_anggota` VALUES ('141', '1');
INSERT INTO `hak_akses_anggota` VALUES ('142', '1');
INSERT INTO `hak_akses_anggota` VALUES ('143', '1');
INSERT INTO `hak_akses_anggota` VALUES ('144', '1');
INSERT INTO `hak_akses_anggota` VALUES ('145', '1');
INSERT INTO `hak_akses_anggota` VALUES ('146', '1');
INSERT INTO `hak_akses_anggota` VALUES ('147', '1');
INSERT INTO `hak_akses_anggota` VALUES ('148', '1');
INSERT INTO `hak_akses_anggota` VALUES ('149', '1');
INSERT INTO `hak_akses_anggota` VALUES ('150', '1');
INSERT INTO `hak_akses_anggota` VALUES ('151', '1');
INSERT INTO `hak_akses_anggota` VALUES ('152', '1');
INSERT INTO `hak_akses_anggota` VALUES ('153', '1');
INSERT INTO `hak_akses_anggota` VALUES ('154', '1');
INSERT INTO `hak_akses_anggota` VALUES ('155', '1');
INSERT INTO `hak_akses_anggota` VALUES ('156', '1');
INSERT INTO `hak_akses_anggota` VALUES ('157', '1');
INSERT INTO `hak_akses_anggota` VALUES ('158', '1');
INSERT INTO `hak_akses_anggota` VALUES ('159', '1');
INSERT INTO `hak_akses_anggota` VALUES ('160', '1');
INSERT INTO `hak_akses_anggota` VALUES ('161', '1');
INSERT INTO `hak_akses_anggota` VALUES ('162', '1');
INSERT INTO `hak_akses_anggota` VALUES ('163', '1');
INSERT INTO `hak_akses_anggota` VALUES ('164', '1');
INSERT INTO `hak_akses_anggota` VALUES ('165', '1');
INSERT INTO `hak_akses_anggota` VALUES ('166', '1');
INSERT INTO `hak_akses_anggota` VALUES ('167', '1');
INSERT INTO `hak_akses_anggota` VALUES ('168', '1');
INSERT INTO `hak_akses_anggota` VALUES ('169', '1');
INSERT INTO `hak_akses_anggota` VALUES ('170', '1');
INSERT INTO `hak_akses_anggota` VALUES ('171', '1');
INSERT INTO `hak_akses_anggota` VALUES ('172', '1');
INSERT INTO `hak_akses_anggota` VALUES ('173', '1');
INSERT INTO `hak_akses_anggota` VALUES ('175', '1');
INSERT INTO `hak_akses_anggota` VALUES ('176', '1');
INSERT INTO `hak_akses_anggota` VALUES ('177', '1');
INSERT INTO `hak_akses_anggota` VALUES ('178', '1');
INSERT INTO `hak_akses_anggota` VALUES ('179', '1');
INSERT INTO `hak_akses_anggota` VALUES ('180', '1');
INSERT INTO `hak_akses_anggota` VALUES ('182', '1');
INSERT INTO `hak_akses_anggota` VALUES ('183', '1');
INSERT INTO `hak_akses_anggota` VALUES ('184', '1');
INSERT INTO `hak_akses_anggota` VALUES ('190', '1');
INSERT INTO `hak_akses_anggota` VALUES ('190', '5');
INSERT INTO `hak_akses_anggota` VALUES ('189', '1');
INSERT INTO `hak_akses_anggota` VALUES ('189', '4');
INSERT INTO `hak_akses_anggota` VALUES ('187', '1');
INSERT INTO `hak_akses_anggota` VALUES ('187', '5');
INSERT INTO `hak_akses_anggota` VALUES ('108', '1');
INSERT INTO `hak_akses_anggota` VALUES ('108', '2');
INSERT INTO `hak_akses_anggota` VALUES ('115', '1');
INSERT INTO `hak_akses_anggota` VALUES ('115', '2');
INSERT INTO `hak_akses_anggota` VALUES ('186', '1');
INSERT INTO `hak_akses_anggota` VALUES ('185', '1');
INSERT INTO `hak_akses_anggota` VALUES ('181', '1');
INSERT INTO `hak_akses_anggota` VALUES ('133', '1');
INSERT INTO `hak_akses_anggota` VALUES ('174', '3');
INSERT INTO `hak_akses_anggota` VALUES ('174', '1');
INSERT INTO `hak_akses_anggota` VALUES ('140', '1');

-- ----------------------------
-- Table structure for jabatan
-- ----------------------------
DROP TABLE IF EXISTS `jabatan`;
CREATE TABLE `jabatan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `jenis_jabatan` varchar(10) DEFAULT NULL,
  `nama_jabatan` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_jabatan_persentase_shu_1` (`jenis_jabatan`) USING BTREE,
  CONSTRAINT `jabatan_ibfk_1` FOREIGN KEY (`jenis_jabatan`) REFERENCES `persentase_shu` (`jenis_jabatan`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of jabatan
-- ----------------------------
INSERT INTO `jabatan` VALUES ('1', 'anggota', 'Anggota');
INSERT INTO `jabatan` VALUES ('2', 'pengurus', 'Ketua');
INSERT INTO `jabatan` VALUES ('3', 'pengurus', 'Bendahara');
INSERT INTO `jabatan` VALUES ('4', 'pengawas', 'Pengawas');
INSERT INTO `jabatan` VALUES ('5', 'pengurus', 'Wakil');

-- ----------------------------
-- Table structure for jenis_simpanan
-- ----------------------------
DROP TABLE IF EXISTS `jenis_simpanan`;
CREATE TABLE `jenis_simpanan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kode_simpanan` varchar(5) DEFAULT NULL,
  `jenis_simpanan` varchar(50) DEFAULT NULL,
  `jumlah` decimal(15,2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of jenis_simpanan
-- ----------------------------
INSERT INTO `jenis_simpanan` VALUES ('1', 'wajib', 'Simpanan Wajib', '25000.00');

-- ----------------------------
-- Table structure for menu
-- ----------------------------
DROP TABLE IF EXISTS `menu`;
CREATE TABLE `menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `link` varchar(255) DEFAULT NULL,
  `icon` varchar(50) DEFAULT NULL,
  `is_active` int(1) DEFAULT NULL,
  `is_parent` int(1) DEFAULT NULL,
  `akses` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of menu
-- ----------------------------
INSERT INTO `menu` VALUES ('1', 'Anggota', 'admin/anggota', 'fa fa-user', '1', '0', null);
INSERT INTO `menu` VALUES ('2', 'Peminjaman', 'admin/peminjaman', 'fa fa-download', '1', '0', null);
INSERT INTO `menu` VALUES ('3', 'Pembayaran', 'admin/pembayaran', 'fa fa-upload', '1', '0', null);
INSERT INTO `menu` VALUES ('4', 'Simpanan Wajib', 'admin/simpanan_wajib', 'fa fa-cloud-download', '1', '0', null);
INSERT INTO `menu` VALUES ('12', 'SHU', 'admin/shu', 'fa  fa-balance-scale', '1', '0', null);

-- ----------------------------
-- Table structure for pembayaran
-- ----------------------------
DROP TABLE IF EXISTS `pembayaran`;
CREATE TABLE `pembayaran` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_peminjaman` int(11) DEFAULT NULL,
  `tanggal_bayar` date DEFAULT NULL,
  `tanggal_batas` date DEFAULT NULL,
  `jumlah_bayar` decimal(15,2) DEFAULT NULL,
  `bunga` decimal(15,2) DEFAULT NULL,
  `pembayaran_ke` int(11) DEFAULT NULL,
  `keterangan` text,
  `status` int(1) DEFAULT '0',
  `is_bayar_bunga` int(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `fk_pembayaran_peminjaman_1` (`id_peminjaman`),
  CONSTRAINT `fk_pembayaran_peminjaman_1` FOREIGN KEY (`id_peminjaman`) REFERENCES `peminjaman` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=207 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of pembayaran
-- ----------------------------
INSERT INTO `pembayaran` VALUES ('169', '15', '2017-02-07', '2017-03-07', '100000.00', '10000.00', '1', null, '1', '0');
INSERT INTO `pembayaran` VALUES ('170', '15', '2017-02-07', '2017-04-07', '100000.00', '10000.00', '2', null, '1', '0');
INSERT INTO `pembayaran` VALUES ('171', '15', '2017-02-07', '2017-05-07', '100000.00', '10000.00', '3', null, '1', '0');
INSERT INTO `pembayaran` VALUES ('172', '15', '2017-02-07', '2017-06-07', '100000.00', '10000.00', '4', null, '1', '0');
INSERT INTO `pembayaran` VALUES ('173', '15', '2017-02-07', '2017-07-07', '100000.00', '10000.00', '5', null, '1', '0');
INSERT INTO `pembayaran` VALUES ('174', '15', '2017-02-07', '2017-08-07', '100000.00', '10000.00', '6', null, '1', '0');
INSERT INTO `pembayaran` VALUES ('175', '15', '2017-02-07', '2017-09-07', '100000.00', '10000.00', '7', null, '1', '0');
INSERT INTO `pembayaran` VALUES ('176', '15', '2017-02-07', '2017-10-07', '100000.00', '10000.00', '8', null, '1', '0');
INSERT INTO `pembayaran` VALUES ('177', '15', '2017-02-07', '2017-11-07', '100000.00', '10000.00', '9', null, '1', '0');
INSERT INTO `pembayaran` VALUES ('178', '15', '2017-02-07', '2017-12-07', '100000.00', '10000.00', '10', null, '1', '0');
INSERT INTO `pembayaran` VALUES ('179', '16', null, '2016-03-07', '100000.00', '10000.00', '1', null, '0', '0');
INSERT INTO `pembayaran` VALUES ('180', '16', null, '2016-04-07', '100000.00', '10000.00', '2', null, '0', '1');
INSERT INTO `pembayaran` VALUES ('181', '16', null, '2016-05-07', '100000.00', '10000.00', '3', null, '0', '1');
INSERT INTO `pembayaran` VALUES ('182', '16', null, '2016-06-07', '100000.00', '10000.00', '4', null, '0', '1');
INSERT INTO `pembayaran` VALUES ('183', '16', null, '2016-07-07', '100000.00', '10000.00', '5', null, '0', '1');
INSERT INTO `pembayaran` VALUES ('184', '16', null, '2016-08-07', '100000.00', '10000.00', '6', null, '0', '1');
INSERT INTO `pembayaran` VALUES ('185', '16', null, '2017-09-07', '100000.00', '10000.00', '7', null, '0', '0');
INSERT INTO `pembayaran` VALUES ('186', '16', null, '2017-10-07', '100000.00', '10000.00', '8', null, '0', '0');
INSERT INTO `pembayaran` VALUES ('187', '16', null, '2017-11-07', '100000.00', '10000.00', '9', null, '0', '0');
INSERT INTO `pembayaran` VALUES ('188', '16', null, '2017-12-07', '100000.00', '10000.00', '10', null, '0', '0');
INSERT INTO `pembayaran` VALUES ('189', '16', '2017-02-07', '2017-02-07', '0.00', '10000.00', '11', 'Bayar Bunga pinjaman pembayaran ke 1', '1', '1');
INSERT INTO `pembayaran` VALUES ('190', '16', '2017-02-07', '2017-02-07', '0.00', '10000.00', '12', 'Bayar Bunga pinjaman pembayaran ke 2', '1', '1');
INSERT INTO `pembayaran` VALUES ('191', '16', '2017-02-07', '2017-02-07', '0.00', '10000.00', '13', 'Bayar Bunga pinjaman pembayaran ke 3', '1', '1');
INSERT INTO `pembayaran` VALUES ('192', '16', '2017-02-07', '2017-02-07', '0.00', '10000.00', '14', 'Bayar Bunga pinjaman pembayaran ke 4', '1', '1');
INSERT INTO `pembayaran` VALUES ('193', '16', '2017-02-07', '2017-02-07', '0.00', '10000.00', '15', 'Bayar Bunga pinjaman pembayaran ke 5', '1', '1');
INSERT INTO `pembayaran` VALUES ('194', '16', '2017-02-07', '2017-02-07', '0.00', '10000.00', '16', 'Bayar Bunga pinjaman pembayaran ke 6', '1', '1');
INSERT INTO `pembayaran` VALUES ('195', '16', '2017-02-07', '2017-02-07', '0.00', '10000.00', '17', 'Bayar Bunga pinjaman pembayaran ke 3', '1', '1');
INSERT INTO `pembayaran` VALUES ('196', '16', '2017-02-07', '2017-02-07', '0.00', '10000.00', '18', 'Bayar Bunga pinjaman pembayaran ke 2', '1', '1');
INSERT INTO `pembayaran` VALUES ('197', '17', null, '2017-03-07', '100000.00', '20000.00', '1', null, '0', '0');
INSERT INTO `pembayaran` VALUES ('198', '17', null, '2017-04-07', '100000.00', '20000.00', '2', null, '0', '0');
INSERT INTO `pembayaran` VALUES ('199', '17', null, '2017-05-07', '100000.00', '20000.00', '3', null, '0', '0');
INSERT INTO `pembayaran` VALUES ('200', '17', null, '2017-06-07', '100000.00', '20000.00', '4', null, '0', '0');
INSERT INTO `pembayaran` VALUES ('201', '17', null, '2017-07-07', '100000.00', '20000.00', '5', null, '0', '0');
INSERT INTO `pembayaran` VALUES ('202', '17', null, '2017-08-07', '100000.00', '20000.00', '6', null, '0', '0');
INSERT INTO `pembayaran` VALUES ('203', '17', null, '2017-09-07', '100000.00', '20000.00', '7', null, '0', '0');
INSERT INTO `pembayaran` VALUES ('204', '17', null, '2017-10-07', '100000.00', '20000.00', '8', null, '0', '0');
INSERT INTO `pembayaran` VALUES ('205', '17', null, '2017-11-07', '100000.00', '20000.00', '9', null, '0', '0');
INSERT INTO `pembayaran` VALUES ('206', '17', null, '2017-12-07', '100000.00', '20000.00', '10', null, '0', '0');

-- ----------------------------
-- Table structure for peminjaman
-- ----------------------------
DROP TABLE IF EXISTS `peminjaman`;
CREATE TABLE `peminjaman` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_anggota` int(11) DEFAULT NULL,
  `no_pinjaman` varchar(20) DEFAULT NULL,
  `tgl_pinjaman` date DEFAULT NULL,
  `jumlah_pinjaman` decimal(15,2) DEFAULT NULL,
  `bunga` decimal(15,2) DEFAULT NULL,
  `total_pinjaman` decimal(15,2) DEFAULT NULL COMMENT 'lunas\r\nbelum lunas',
  `status` int(1) DEFAULT '0',
  `input_oleh` int(11) DEFAULT NULL,
  `tgl_input` datetime DEFAULT NULL,
  `update_oleh` int(11) DEFAULT NULL,
  `tgl_update` datetime DEFAULT NULL,
  `sisa_pembayaran` decimal(15,2) DEFAULT NULL,
  `kali_bayar` int(11) DEFAULT NULL,
  `kali_bayar_bunga` int(11) DEFAULT NULL,
  `kali_tunggakan` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_peminjaman_anggota_1` (`id_anggota`),
  CONSTRAINT `fk_peminjaman_anggota_1` FOREIGN KEY (`id_anggota`) REFERENCES `anggota` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of peminjaman
-- ----------------------------
INSERT INTO `peminjaman` VALUES ('15', '115', 'PINJ0220170000000001', '2017-02-07', '1000000.00', '10000.00', '1100000.00', '1', '174', '2017-02-07 09:54:41', '174', '2017-02-07 09:54:41', '0.00', '10', null, null);
INSERT INTO `peminjaman` VALUES ('16', '115', 'PINJ0220170000000002', '2017-02-07', '1000000.00', '10000.00', '1100000.00', '0', '174', '2017-02-07 09:56:40', '174', '2017-02-07 09:56:40', '1100000.00', '18', '4', '8');
INSERT INTO `peminjaman` VALUES ('17', '108', 'PINJ0220170000000003', '2017-02-07', '1000000.00', '20000.00', '1200000.00', '0', '174', '2017-02-07 15:18:36', '174', '2017-02-07 15:18:36', '1200000.00', '10', null, null);

-- ----------------------------
-- Table structure for pengaturan
-- ----------------------------
DROP TABLE IF EXISTS `pengaturan`;
CREATE TABLE `pengaturan` (
  `key` varchar(50) NOT NULL,
  `value` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`key`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of pengaturan
-- ----------------------------
INSERT INTO `pengaturan` VALUES ('bunga_pinjaman', '2');
INSERT INTO `pengaturan` VALUES ('iuran_wajib', '25000.00');
INSERT INTO `pengaturan` VALUES ('kali_bayar', '10');
INSERT INTO `pengaturan` VALUES ('max_kali_bunga', '4');
INSERT INTO `pengaturan` VALUES ('max_kali_tunggakan', '3');

-- ----------------------------
-- Table structure for persentase_shu
-- ----------------------------
DROP TABLE IF EXISTS `persentase_shu`;
CREATE TABLE `persentase_shu` (
  `jenis_jabatan` varchar(10) NOT NULL,
  `persentase_shu` decimal(15,2) DEFAULT NULL,
  PRIMARY KEY (`jenis_jabatan`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of persentase_shu
-- ----------------------------
INSERT INTO `persentase_shu` VALUES ('anggota', '50.00');
INSERT INTO `persentase_shu` VALUES ('modal', '15.00');
INSERT INTO `persentase_shu` VALUES ('peminjam', '20.00');
INSERT INTO `persentase_shu` VALUES ('pengawas', '5.00');
INSERT INTO `persentase_shu` VALUES ('pengurus', '10.00');

-- ----------------------------
-- Table structure for shu
-- ----------------------------
DROP TABLE IF EXISTS `shu`;
CREATE TABLE `shu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `periode` int(4) DEFAULT NULL,
  `tahun` int(5) DEFAULT NULL,
  `laba_tahunan` double(15,2) DEFAULT NULL,
  `tanggal_generate` datetime DEFAULT NULL,
  `generate_oleh` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of shu
-- ----------------------------
INSERT INTO `shu` VALUES ('14', '2017', null, '230000.00', '2017-02-07 02:02:42', '174');

-- ----------------------------
-- Table structure for simpanan_wajib
-- ----------------------------
DROP TABLE IF EXISTS `simpanan_wajib`;
CREATE TABLE `simpanan_wajib` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_anggota` int(11) DEFAULT NULL,
  `tgl_bayar` date DEFAULT NULL,
  `jumlah_bayar` double(15,2) DEFAULT NULL,
  `status_pembayaran` int(1) DEFAULT NULL,
  `periode` int(11) DEFAULT NULL,
  `tahun` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_simpanan_wajib_anggota_1` (`id_anggota`),
  CONSTRAINT `fk_simpanan_wajib_anggota_1` FOREIGN KEY (`id_anggota`) REFERENCES `anggota` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=840 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of simpanan_wajib
-- ----------------------------
INSERT INTO `simpanan_wajib` VALUES ('757', '108', null, '25000.00', '0', '1', '2017');
INSERT INTO `simpanan_wajib` VALUES ('758', '109', null, '25000.00', '0', '1', '2017');
INSERT INTO `simpanan_wajib` VALUES ('759', '110', null, '25000.00', '0', '1', '2017');
INSERT INTO `simpanan_wajib` VALUES ('760', '111', null, '25000.00', '0', '1', '2017');
INSERT INTO `simpanan_wajib` VALUES ('761', '112', null, '25000.00', '0', '1', '2017');
INSERT INTO `simpanan_wajib` VALUES ('762', '113', null, '25000.00', '0', '1', '2017');
INSERT INTO `simpanan_wajib` VALUES ('763', '114', null, '25000.00', '0', '1', '2017');
INSERT INTO `simpanan_wajib` VALUES ('764', '115', null, '25000.00', '0', '1', '2017');
INSERT INTO `simpanan_wajib` VALUES ('765', '116', null, '25000.00', '0', '1', '2017');
INSERT INTO `simpanan_wajib` VALUES ('766', '117', null, '25000.00', '0', '1', '2017');
INSERT INTO `simpanan_wajib` VALUES ('767', '118', null, '25000.00', '0', '1', '2017');
INSERT INTO `simpanan_wajib` VALUES ('768', '119', null, '25000.00', '0', '1', '2017');
INSERT INTO `simpanan_wajib` VALUES ('769', '120', null, '25000.00', '0', '1', '2017');
INSERT INTO `simpanan_wajib` VALUES ('770', '121', null, '25000.00', '0', '1', '2017');
INSERT INTO `simpanan_wajib` VALUES ('771', '122', null, '25000.00', '0', '1', '2017');
INSERT INTO `simpanan_wajib` VALUES ('772', '123', null, '25000.00', '0', '1', '2017');
INSERT INTO `simpanan_wajib` VALUES ('773', '124', null, '25000.00', '0', '1', '2017');
INSERT INTO `simpanan_wajib` VALUES ('774', '125', null, '25000.00', '0', '1', '2017');
INSERT INTO `simpanan_wajib` VALUES ('775', '126', null, '25000.00', '0', '1', '2017');
INSERT INTO `simpanan_wajib` VALUES ('776', '127', null, '25000.00', '0', '1', '2017');
INSERT INTO `simpanan_wajib` VALUES ('777', '128', null, '25000.00', '0', '1', '2017');
INSERT INTO `simpanan_wajib` VALUES ('778', '129', null, '25000.00', '0', '1', '2017');
INSERT INTO `simpanan_wajib` VALUES ('779', '130', null, '25000.00', '0', '1', '2017');
INSERT INTO `simpanan_wajib` VALUES ('780', '131', null, '25000.00', '0', '1', '2017');
INSERT INTO `simpanan_wajib` VALUES ('781', '132', null, '25000.00', '0', '1', '2017');
INSERT INTO `simpanan_wajib` VALUES ('782', '133', null, '25000.00', '0', '1', '2017');
INSERT INTO `simpanan_wajib` VALUES ('783', '134', null, '25000.00', '0', '1', '2017');
INSERT INTO `simpanan_wajib` VALUES ('784', '135', null, '25000.00', '0', '1', '2017');
INSERT INTO `simpanan_wajib` VALUES ('785', '136', null, '25000.00', '0', '1', '2017');
INSERT INTO `simpanan_wajib` VALUES ('786', '137', null, '25000.00', '0', '1', '2017');
INSERT INTO `simpanan_wajib` VALUES ('787', '138', null, '25000.00', '0', '1', '2017');
INSERT INTO `simpanan_wajib` VALUES ('788', '139', null, '25000.00', '0', '1', '2017');
INSERT INTO `simpanan_wajib` VALUES ('789', '140', null, '25000.00', '0', '1', '2017');
INSERT INTO `simpanan_wajib` VALUES ('790', '141', '2017-02-07', '25000.00', '1', '1', '2017');
INSERT INTO `simpanan_wajib` VALUES ('791', '142', '2017-02-07', '25000.00', '1', '1', '2017');
INSERT INTO `simpanan_wajib` VALUES ('792', '143', '2017-02-07', '25000.00', '1', '1', '2017');
INSERT INTO `simpanan_wajib` VALUES ('793', '144', '2017-02-07', '25000.00', '1', '1', '2017');
INSERT INTO `simpanan_wajib` VALUES ('794', '145', '2017-02-07', '25000.00', '1', '1', '2017');
INSERT INTO `simpanan_wajib` VALUES ('795', '146', '2017-02-07', '25000.00', '1', '1', '2017');
INSERT INTO `simpanan_wajib` VALUES ('796', '147', '2017-02-07', '25000.00', '1', '1', '2017');
INSERT INTO `simpanan_wajib` VALUES ('797', '148', '2017-02-07', '25000.00', '1', '1', '2017');
INSERT INTO `simpanan_wajib` VALUES ('798', '149', '2017-02-07', '25000.00', '1', '1', '2017');
INSERT INTO `simpanan_wajib` VALUES ('799', '150', '2017-02-07', '25000.00', '1', '1', '2017');
INSERT INTO `simpanan_wajib` VALUES ('800', '151', '2017-02-07', '25000.00', '1', '1', '2017');
INSERT INTO `simpanan_wajib` VALUES ('801', '152', '2017-02-07', '25000.00', '1', '1', '2017');
INSERT INTO `simpanan_wajib` VALUES ('802', '153', '2017-02-07', '25000.00', '1', '1', '2017');
INSERT INTO `simpanan_wajib` VALUES ('803', '154', '2017-02-07', '25000.00', '1', '1', '2017');
INSERT INTO `simpanan_wajib` VALUES ('804', '155', '2017-02-07', '25000.00', '1', '1', '2017');
INSERT INTO `simpanan_wajib` VALUES ('805', '156', '2017-02-07', '25000.00', '1', '1', '2017');
INSERT INTO `simpanan_wajib` VALUES ('806', '157', '2017-02-07', '25000.00', '1', '1', '2017');
INSERT INTO `simpanan_wajib` VALUES ('807', '158', '2017-02-07', '25000.00', '1', '1', '2017');
INSERT INTO `simpanan_wajib` VALUES ('808', '159', '2017-02-07', '25000.00', '1', '1', '2017');
INSERT INTO `simpanan_wajib` VALUES ('809', '160', '2017-02-07', '25000.00', '1', '1', '2017');
INSERT INTO `simpanan_wajib` VALUES ('810', '161', '2017-02-07', '25000.00', '1', '1', '2017');
INSERT INTO `simpanan_wajib` VALUES ('811', '162', '2017-02-07', '25000.00', '1', '1', '2017');
INSERT INTO `simpanan_wajib` VALUES ('812', '163', '2017-02-07', '25000.00', '1', '1', '2017');
INSERT INTO `simpanan_wajib` VALUES ('813', '164', '2017-02-07', '25000.00', '1', '1', '2017');
INSERT INTO `simpanan_wajib` VALUES ('814', '165', '2017-02-07', '25000.00', '1', '1', '2017');
INSERT INTO `simpanan_wajib` VALUES ('815', '166', '2017-02-07', '25000.00', '1', '1', '2017');
INSERT INTO `simpanan_wajib` VALUES ('816', '167', '2017-02-07', '25000.00', '1', '1', '2017');
INSERT INTO `simpanan_wajib` VALUES ('817', '168', '2017-02-07', '25000.00', '1', '1', '2017');
INSERT INTO `simpanan_wajib` VALUES ('818', '169', '2017-02-07', '25000.00', '1', '1', '2017');
INSERT INTO `simpanan_wajib` VALUES ('819', '170', '2017-02-07', '25000.00', '1', '1', '2017');
INSERT INTO `simpanan_wajib` VALUES ('820', '171', '2017-02-07', '25000.00', '1', '1', '2017');
INSERT INTO `simpanan_wajib` VALUES ('821', '172', '2017-02-07', '25000.00', '1', '1', '2017');
INSERT INTO `simpanan_wajib` VALUES ('822', '173', '2017-02-07', '25000.00', '1', '1', '2017');
INSERT INTO `simpanan_wajib` VALUES ('823', '174', '2017-02-07', '25000.00', '1', '1', '2017');
INSERT INTO `simpanan_wajib` VALUES ('824', '175', '2017-02-07', '25000.00', '1', '1', '2017');
INSERT INTO `simpanan_wajib` VALUES ('825', '176', '2017-02-07', '25000.00', '1', '1', '2017');
INSERT INTO `simpanan_wajib` VALUES ('826', '177', '2017-02-07', '25000.00', '1', '1', '2017');
INSERT INTO `simpanan_wajib` VALUES ('827', '178', '2017-02-07', '25000.00', '1', '1', '2017');
INSERT INTO `simpanan_wajib` VALUES ('828', '179', '2017-02-07', '25000.00', '1', '1', '2017');
INSERT INTO `simpanan_wajib` VALUES ('829', '180', '2017-02-07', '25000.00', '1', '1', '2017');
INSERT INTO `simpanan_wajib` VALUES ('830', '181', '2017-02-07', '25000.00', '1', '1', '2017');
INSERT INTO `simpanan_wajib` VALUES ('831', '182', '2017-02-07', '25000.00', '1', '1', '2017');
INSERT INTO `simpanan_wajib` VALUES ('832', '183', '2017-02-07', '25000.00', '1', '1', '2017');
INSERT INTO `simpanan_wajib` VALUES ('833', '184', '2017-02-07', '25000.00', '1', '1', '2017');
INSERT INTO `simpanan_wajib` VALUES ('834', '185', '2017-02-07', '25000.00', '1', '1', '2017');
INSERT INTO `simpanan_wajib` VALUES ('835', '186', '2017-02-07', '25000.00', '1', '1', '2017');
INSERT INTO `simpanan_wajib` VALUES ('836', '187', '2017-02-07', '25000.00', '1', '1', '2017');
INSERT INTO `simpanan_wajib` VALUES ('837', '188', '2017-02-07', '25000.00', '1', '1', '2017');
INSERT INTO `simpanan_wajib` VALUES ('838', '189', '2017-02-07', '25000.00', '1', '1', '2017');
INSERT INTO `simpanan_wajib` VALUES ('839', '190', '2017-02-07', '25000.00', '1', '1', '2017');

-- ----------------------------
-- Table structure for transaksi_rutin
-- ----------------------------
DROP TABLE IF EXISTS `transaksi_rutin`;
CREATE TABLE `transaksi_rutin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `transaksi` varchar(100) DEFAULT NULL,
  `id_transaksi` int(11) DEFAULT NULL,
  `tanggal_transaksi` date DEFAULT NULL,
  `masuk` decimal(15,2) DEFAULT '0.00',
  `keluar` decimal(15,2) DEFAULT '0.00',
  `keterangan` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=449 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of transaksi_rutin
-- ----------------------------
INSERT INTO `transaksi_rutin` VALUES ('364', 'Peminjaman Anggota', '15', '2017-02-07', '0.00', '1100000.00', null);
INSERT INTO `transaksi_rutin` VALUES ('365', 'Pembayaran Pinjaman Anggota', '169', '2017-02-07', '100000.00', '0.00', null);
INSERT INTO `transaksi_rutin` VALUES ('366', 'Bunga Pinjaman Anggota', '169', '2017-02-07', '10000.00', '0.00', null);
INSERT INTO `transaksi_rutin` VALUES ('367', 'Pembayaran Pinjaman Anggota', '170', '2017-02-07', '100000.00', '0.00', null);
INSERT INTO `transaksi_rutin` VALUES ('368', 'Bunga Pinjaman Anggota', '170', '2017-02-07', '10000.00', '0.00', null);
INSERT INTO `transaksi_rutin` VALUES ('369', 'Pembayaran Pinjaman Anggota', '171', '2017-02-07', '100000.00', '0.00', null);
INSERT INTO `transaksi_rutin` VALUES ('370', 'Bunga Pinjaman Anggota', '171', '2017-02-07', '10000.00', '0.00', null);
INSERT INTO `transaksi_rutin` VALUES ('371', 'Pembayaran Pinjaman Anggota', '172', '2017-02-07', '100000.00', '0.00', null);
INSERT INTO `transaksi_rutin` VALUES ('372', 'Bunga Pinjaman Anggota', '172', '2017-02-07', '10000.00', '0.00', null);
INSERT INTO `transaksi_rutin` VALUES ('373', 'Pembayaran Pinjaman Anggota', '173', '2017-02-07', '100000.00', '0.00', null);
INSERT INTO `transaksi_rutin` VALUES ('374', 'Bunga Pinjaman Anggota', '173', '2017-02-07', '10000.00', '0.00', null);
INSERT INTO `transaksi_rutin` VALUES ('375', 'Pembayaran Pinjaman Anggota', '174', '2017-02-07', '100000.00', '0.00', null);
INSERT INTO `transaksi_rutin` VALUES ('376', 'Bunga Pinjaman Anggota', '174', '2017-02-07', '10000.00', '0.00', null);
INSERT INTO `transaksi_rutin` VALUES ('377', 'Pembayaran Pinjaman Anggota', '175', '2017-02-07', '100000.00', '0.00', null);
INSERT INTO `transaksi_rutin` VALUES ('378', 'Bunga Pinjaman Anggota', '175', '2017-02-07', '10000.00', '0.00', null);
INSERT INTO `transaksi_rutin` VALUES ('379', 'Pembayaran Pinjaman Anggota', '176', '2017-02-07', '100000.00', '0.00', null);
INSERT INTO `transaksi_rutin` VALUES ('380', 'Bunga Pinjaman Anggota', '176', '2017-02-07', '10000.00', '0.00', null);
INSERT INTO `transaksi_rutin` VALUES ('381', 'Pembayaran Pinjaman Anggota', '177', '2017-02-07', '100000.00', '0.00', null);
INSERT INTO `transaksi_rutin` VALUES ('382', 'Bunga Pinjaman Anggota', '177', '2017-02-07', '10000.00', '0.00', null);
INSERT INTO `transaksi_rutin` VALUES ('383', 'Pembayaran Pinjaman Anggota', '178', '2017-02-07', '100000.00', '0.00', null);
INSERT INTO `transaksi_rutin` VALUES ('384', 'Bunga Pinjaman Anggota', '178', '2017-02-07', '10000.00', '0.00', null);
INSERT INTO `transaksi_rutin` VALUES ('385', 'Peminjaman Anggota', '16', '2017-02-07', '0.00', '1100000.00', null);
INSERT INTO `transaksi_rutin` VALUES ('386', 'Bunga Pinjaman Anggota', '179', '2017-02-07', '10000.00', '0.00', 'Bayar Bunga pinjaman pembayaran ke 1');
INSERT INTO `transaksi_rutin` VALUES ('387', 'Bunga Pinjaman Anggota', '180', '2017-02-07', '10000.00', '0.00', 'Bayar Bunga pinjaman pembayaran ke 2');
INSERT INTO `transaksi_rutin` VALUES ('388', 'Bunga Pinjaman Anggota', '181', '2017-02-07', '10000.00', '0.00', 'Bayar Bunga pinjaman pembayaran ke 3');
INSERT INTO `transaksi_rutin` VALUES ('389', 'Bunga Pinjaman Anggota', '182', '2017-02-07', '10000.00', '0.00', 'Bayar Bunga pinjaman pembayaran ke 4');
INSERT INTO `transaksi_rutin` VALUES ('390', 'Bunga Pinjaman Anggota', '183', '2017-02-07', '10000.00', '0.00', 'Bayar Bunga pinjaman pembayaran ke 5');
INSERT INTO `transaksi_rutin` VALUES ('391', 'Bunga Pinjaman Anggota', '184', '2017-02-07', '10000.00', '0.00', 'Bayar Bunga pinjaman pembayaran ke 6');
INSERT INTO `transaksi_rutin` VALUES ('392', 'Bunga Pinjaman Anggota', '181', '2017-02-07', '10000.00', '0.00', 'Bayar Bunga pinjaman pembayaran ke 3');
INSERT INTO `transaksi_rutin` VALUES ('393', 'Bunga Pinjaman Anggota', '180', '2017-02-07', '10000.00', '0.00', 'Bayar Bunga pinjaman pembayaran ke 2');
INSERT INTO `transaksi_rutin` VALUES ('394', 'Pembayaran Simpanan Wajib', '839', '2017-02-07', '25000.00', '0.00', null);
INSERT INTO `transaksi_rutin` VALUES ('395', 'Pembayaran Simpanan Wajib', '838', '2017-02-07', '25000.00', '0.00', null);
INSERT INTO `transaksi_rutin` VALUES ('396', 'Pembayaran Simpanan Wajib', '837', '2017-02-07', '25000.00', '0.00', null);
INSERT INTO `transaksi_rutin` VALUES ('397', 'Pembayaran Simpanan Wajib', '836', '2017-02-07', '25000.00', '0.00', null);
INSERT INTO `transaksi_rutin` VALUES ('398', 'Pembayaran Simpanan Wajib', '835', '2017-02-07', '25000.00', '0.00', null);
INSERT INTO `transaksi_rutin` VALUES ('399', 'Pembayaran Simpanan Wajib', '834', '2017-02-07', '25000.00', '0.00', null);
INSERT INTO `transaksi_rutin` VALUES ('400', 'Pembayaran Simpanan Wajib', '833', '2017-02-07', '25000.00', '0.00', null);
INSERT INTO `transaksi_rutin` VALUES ('401', 'Pembayaran Simpanan Wajib', '832', '2017-02-07', '25000.00', '0.00', null);
INSERT INTO `transaksi_rutin` VALUES ('402', 'Pembayaran Simpanan Wajib', '831', '2017-02-07', '25000.00', '0.00', null);
INSERT INTO `transaksi_rutin` VALUES ('403', 'Pembayaran Simpanan Wajib', '830', '2017-02-07', '25000.00', '0.00', null);
INSERT INTO `transaksi_rutin` VALUES ('404', 'Pembayaran Simpanan Wajib', '829', '2017-02-07', '25000.00', '0.00', null);
INSERT INTO `transaksi_rutin` VALUES ('405', 'Pembayaran Simpanan Wajib', '828', '2017-02-07', '25000.00', '0.00', null);
INSERT INTO `transaksi_rutin` VALUES ('406', 'Pembayaran Simpanan Wajib', '827', '2017-02-07', '25000.00', '0.00', null);
INSERT INTO `transaksi_rutin` VALUES ('407', 'Pembayaran Simpanan Wajib', '826', '2017-02-07', '25000.00', '0.00', null);
INSERT INTO `transaksi_rutin` VALUES ('408', 'Pembayaran Simpanan Wajib', '825', '2017-02-07', '25000.00', '0.00', null);
INSERT INTO `transaksi_rutin` VALUES ('409', 'Pembayaran Simpanan Wajib', '824', '2017-02-07', '25000.00', '0.00', null);
INSERT INTO `transaksi_rutin` VALUES ('410', 'Pembayaran Simpanan Wajib', '823', '2017-02-07', '25000.00', '0.00', null);
INSERT INTO `transaksi_rutin` VALUES ('411', 'Pembayaran Simpanan Wajib', '822', '2017-02-07', '25000.00', '0.00', null);
INSERT INTO `transaksi_rutin` VALUES ('412', 'Pembayaran Simpanan Wajib', '821', '2017-02-07', '25000.00', '0.00', null);
INSERT INTO `transaksi_rutin` VALUES ('413', 'Pembayaran Simpanan Wajib', '820', '2017-02-07', '25000.00', '0.00', null);
INSERT INTO `transaksi_rutin` VALUES ('414', 'Pembayaran Simpanan Wajib', '819', '2017-02-07', '25000.00', '0.00', null);
INSERT INTO `transaksi_rutin` VALUES ('415', 'Pembayaran Simpanan Wajib', '818', '2017-02-07', '25000.00', '0.00', null);
INSERT INTO `transaksi_rutin` VALUES ('416', 'Pembayaran Simpanan Wajib', '817', '2017-02-07', '25000.00', '0.00', null);
INSERT INTO `transaksi_rutin` VALUES ('418', 'Pembayaran Simpanan Wajib', '816', '2017-02-07', '25000.00', '0.00', null);
INSERT INTO `transaksi_rutin` VALUES ('420', 'Pembayaran Simpanan Wajib', '815', '2017-02-07', '25000.00', '0.00', null);
INSERT INTO `transaksi_rutin` VALUES ('421', 'Pembayaran Simpanan Wajib', '814', '2017-02-07', '25000.00', '0.00', null);
INSERT INTO `transaksi_rutin` VALUES ('423', 'Pembayaran Simpanan Wajib', '813', '2017-02-07', '25000.00', '0.00', null);
INSERT INTO `transaksi_rutin` VALUES ('424', 'Pembayaran Simpanan Wajib', '812', '2017-02-07', '25000.00', '0.00', null);
INSERT INTO `transaksi_rutin` VALUES ('425', 'Pembayaran Simpanan Wajib', '811', '2017-02-07', '25000.00', '0.00', null);
INSERT INTO `transaksi_rutin` VALUES ('426', 'Pembayaran Simpanan Wajib', '810', '2017-02-07', '25000.00', '0.00', null);
INSERT INTO `transaksi_rutin` VALUES ('427', 'Pembayaran Simpanan Wajib', '809', '2017-02-07', '25000.00', '0.00', null);
INSERT INTO `transaksi_rutin` VALUES ('428', 'Pembayaran Simpanan Wajib', '808', '2017-02-07', '25000.00', '0.00', null);
INSERT INTO `transaksi_rutin` VALUES ('429', 'Pembayaran Simpanan Wajib', '807', '2017-02-07', '25000.00', '0.00', null);
INSERT INTO `transaksi_rutin` VALUES ('430', 'Pembayaran Simpanan Wajib', '806', '2017-02-07', '25000.00', '0.00', null);
INSERT INTO `transaksi_rutin` VALUES ('432', 'Pembayaran Simpanan Wajib', '805', '2017-02-07', '25000.00', '0.00', null);
INSERT INTO `transaksi_rutin` VALUES ('433', 'Pembayaran Simpanan Wajib', '804', '2017-02-07', '25000.00', '0.00', null);
INSERT INTO `transaksi_rutin` VALUES ('434', 'Pembayaran Simpanan Wajib', '803', '2017-02-07', '25000.00', '0.00', null);
INSERT INTO `transaksi_rutin` VALUES ('435', 'Pembayaran Simpanan Wajib', '802', '2017-02-07', '25000.00', '0.00', null);
INSERT INTO `transaksi_rutin` VALUES ('436', 'Pembayaran Simpanan Wajib', '801', '2017-02-07', '25000.00', '0.00', null);
INSERT INTO `transaksi_rutin` VALUES ('437', 'Pembayaran Simpanan Wajib', '800', '2017-02-07', '25000.00', '0.00', null);
INSERT INTO `transaksi_rutin` VALUES ('438', 'Pembayaran Simpanan Wajib', '799', '2017-02-07', '25000.00', '0.00', null);
INSERT INTO `transaksi_rutin` VALUES ('439', 'Pembayaran Simpanan Wajib', '798', '2017-02-07', '25000.00', '0.00', null);
INSERT INTO `transaksi_rutin` VALUES ('440', 'Pembayaran Simpanan Wajib', '797', '2017-02-07', '25000.00', '0.00', null);
INSERT INTO `transaksi_rutin` VALUES ('441', 'Pembayaran Simpanan Wajib', '796', '2017-02-07', '25000.00', '0.00', null);
INSERT INTO `transaksi_rutin` VALUES ('442', 'Pembayaran Simpanan Wajib', '795', '2017-02-07', '25000.00', '0.00', null);
INSERT INTO `transaksi_rutin` VALUES ('443', 'Pembayaran Simpanan Wajib', '794', '2017-02-07', '25000.00', '0.00', null);
INSERT INTO `transaksi_rutin` VALUES ('444', 'Pembayaran Simpanan Wajib', '793', '2017-02-07', '25000.00', '0.00', null);
INSERT INTO `transaksi_rutin` VALUES ('445', 'Pembayaran Simpanan Wajib', '792', '2017-02-07', '25000.00', '0.00', null);
INSERT INTO `transaksi_rutin` VALUES ('446', 'Pembayaran Simpanan Wajib', '791', '2017-02-07', '25000.00', '0.00', null);
INSERT INTO `transaksi_rutin` VALUES ('447', 'Pembayaran Simpanan Wajib', '790', '2017-02-07', '25000.00', '0.00', null);
INSERT INTO `transaksi_rutin` VALUES ('448', 'Peminjaman Anggota', '17', '2017-02-07', '0.00', '1200000.00', null);

-- ----------------------------
-- Table structure for tunggakan
-- ----------------------------
DROP TABLE IF EXISTS `tunggakan`;
CREATE TABLE `tunggakan` (
  `id_pinjaman` int(11) DEFAULT NULL,
  `jumlah_tunggakan` decimal(15,2) DEFAULT NULL,
  `tanggal` date DEFAULT NULL,
  `status` int(11) DEFAULT '0',
  `tanggal_bayar` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tunggakan
-- ----------------------------

-- ----------------------------
-- View structure for count_all_tunggakan
-- ----------------------------
DROP VIEW IF EXISTS `count_all_tunggakan`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER  VIEW `count_all_tunggakan` AS SELECT
(SELECT
Count(A.id)
from 
simpanan_wajib as A
INNER JOIN anggota as B on A.id_anggota = B.id
WHERE 
-- A.periode <= MONTH(now()) AND A.tahun <= YEAR(now()) AND 
A.status_pembayaran = 0) as simpanan_wajib
,
(SElect count(A.id) from pembayaran as A
WHERE A.tanggal_batas <= now() and A.`status` = 0) as peminjaman ;

-- ----------------------------
-- View structure for get_count_anggota_tunggakan
-- ----------------------------
DROP VIEW IF EXISTS `get_count_anggota_tunggakan`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER  VIEW `get_count_anggota_tunggakan` AS (SELECT
Count(A.id) simpanan_wajib, 0 as peminjaman, B.id as id_anggota
from 
simpanan_wajib as A
INNER JOIN anggota as B on A.id_anggota = B.id
WHERE 
-- A.periode <= MONTH(now()) AND A.tahun <= YEAR(now()) AND 
A.status_pembayaran = 0 group by B.id)
union
(SElect 0 as simpanan_wajib, count(A.id) as peminjaman, BB.id_anggota from pembayaran as A
INNER JOIN peminjaman as BB on A.id_peminjaman = BB.id
WHERE 
-- A.tanggal_batas <= now() and 
A.`status` = 0 GROUP BY BB.id_anggota) ;

-- ----------------------------
-- View structure for v_daftar_akses_anggota
-- ----------------------------
DROP VIEW IF EXISTS `v_daftar_akses_anggota`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost`  VIEW `v_daftar_akses_anggota` AS SELECT * FROM anggota
INNER JOIN hak_akses_anggota on anggota.id = hak_akses_anggota.id_anggota
GROUP BY anggota.id ;

-- ----------------------------
-- View structure for v_get_access
-- ----------------------------
DROP VIEW IF EXISTS `v_get_access`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER  VIEW `v_get_access` AS SELECT
B.id_anggota,
C.nama,
B.id_hak_akses,
A.hak_akses
FROM
hak_akses AS A
INNER JOIN hak_akses_anggota AS B ON B.id_hak_akses = A.id
INNER JOIN anggota AS C ON B.id_anggota = C.id ;

-- ----------------------------
-- View structure for v_get_daftar_shu_anggota
-- ----------------------------
DROP VIEW IF EXISTS `v_get_daftar_shu_anggota`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost`  VIEW `v_get_daftar_shu_anggota` AS SELECT A.*, B.periode, B.tahun, C.nama
FROM
detail_shu AS A
INNER JOIN shu AS B ON A.id_shu = B.id
INNER JOIN anggota AS C ON A.id_anggota = C.id ;

-- ----------------------------
-- View structure for v_get_detail_shu
-- ----------------------------
DROP VIEW IF EXISTS `v_get_detail_shu`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER  VIEW `v_get_detail_shu` AS SELECT
detail_shu.id_shu,
detail_shu.id,
shu.periode,
shu.tahun,
detail_shu.id_anggota,
anggota.nama,
detail_shu.anggota,
detail_shu.peminjam,
detail_shu.pengurus,
detail_shu.pengawas,
detail_shu.jumlah
FROM
detail_shu
INNER JOIN shu ON detail_shu.id_shu = shu.id
INNER JOIN anggota ON detail_shu.id_anggota = anggota.id ;

-- ----------------------------
-- View structure for v_get_tunggakan
-- ----------------------------
DROP VIEW IF EXISTS `v_get_tunggakan`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER  VIEW `v_get_tunggakan` AS SELECT * from pembayaran WHERE tanggal_batas <= now() and `status` = 0 ;

-- ----------------------------
-- View structure for v_get_tunggakan_pembayaran
-- ----------------------------
DROP VIEW IF EXISTS `v_get_tunggakan_pembayaran`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER  VIEW `v_get_tunggakan_pembayaran` AS SELECT
A.id,
A.id_peminjaman,
B.no_pinjaman,
B.id_anggota,
C.nama as nama_anggota,
A.tanggal_bayar,
A.tanggal_batas,
A.jumlah_bayar,
A.bunga,
A.pembayaran_ke,
A.keterangan,
A.`status`
from 
pembayaran as A
INNER JOIN peminjaman as B on A.id_peminjaman = B.id
INNER JOIN anggota as C on B.id_anggota = C.id
WHERE A.tanggal_batas <= now() and A.`status` = 0 ;

-- ----------------------------
-- View structure for v_grafikpeminjaman
-- ----------------------------
DROP VIEW IF EXISTS `v_grafikpeminjaman`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost`  VIEW `v_grafikpeminjaman` AS SELECT
  Year(tgl_pinjaman) as Tahun,
  Count(case when month(`tgl_pinjaman`)=1 then id end) As Januari,
  Count(case when month(`tgl_pinjaman`)=2 then id end) As Februari,
  Count(case when month(`tgl_pinjaman`)=3 then id end) As Maret,
  Count(case when month(`tgl_pinjaman`)=4 then id end) As April,
  Count(case when month(`tgl_pinjaman`)=5 then id end) As Mei,
  Count(case when month(`tgl_pinjaman`)=6 then id end) As Juni,
  Count(case when month(`tgl_pinjaman`)=7 then id end) As Juli,
  Count(case when month(`tgl_pinjaman`)=8 then id end) As Agustus,
  Count(case when month(`tgl_pinjaman`)=9 then id end) As September,
  Count(case when month(`tgl_pinjaman`)=10 then id end) As Oktober,
  Count(case when month(`tgl_pinjaman`)=11 then id end) As Nopember,
  Count(case when month(`tgl_pinjaman`)=12 then id end) As Desember
FROM peminjaman
GROUP BY Year(`tgl_pinjaman`) ;

-- ----------------------------
-- View structure for v_grafik_tunggakan_bulanan
-- ----------------------------
DROP VIEW IF EXISTS `v_grafik_tunggakan_bulanan`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER  VIEW `v_grafik_tunggakan_bulanan` AS SELECT  
	MONTH(tanggal_batas) as Bulan, 
	YEAR(tanggal_batas) as Tahun,	
	Count(id) as Total,
  Count(case when `status`=1 then id end) As Membayar,
  Count(case when `status`=0 then id end) As Menunggak
FROM pembayaran
GROUP BY YEAR(tanggal_batas), MONTH(tanggal_batas) ;

-- ----------------------------
-- View structure for v_list_total_tunggakan
-- ----------------------------
DROP VIEW IF EXISTS `v_list_total_tunggakan`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost`  VIEW `v_list_total_tunggakan` AS SELECT 
	id_anggota,
	no_pinjaman,
	count(`status`) kali_tunggakan, 
	sum(jumlah_bayar) total_jml_bayar, 
	sum(bunga) total_jml_bunga, 
	(sum(jumlah_bayar)+sum(bunga)) total
	FROM v_get_tunggakan_pembayaran 
	GROUP BY id_anggota ;

-- ----------------------------
-- View structure for v_pinjaman_aktif
-- ----------------------------
DROP VIEW IF EXISTS `v_pinjaman_aktif`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost`  VIEW `v_pinjaman_aktif` AS SELECT * from peminjaman WHERE `status` = 0 ;

-- ----------------------------
-- View structure for v_status_pinjaman
-- ----------------------------
DROP VIEW IF EXISTS `v_status_pinjaman`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost`  VIEW `v_status_pinjaman` AS SELECT
A.id_peminjaman,
B.no_pinjaman,
B.tgl_pinjaman,
B.id_anggota,
C.nama,
B.total_pinjaman,
sum(CASE WHEN A.`status` = 0 THEN 1 ELSE 0 end) as jml_belum_bayar,
sum(CASE WHEN A.`status` = 1 THEN 1 ELSE 0 end) as jml_sudah_bayar,
sum(CASE WHEN A.`status` = 0 THEN A.jumlah_bayar + A.bunga ELSE 0 end) as total_belum_bayar,
sum(CASE WHEN A.`status` = 1 THEN A.jumlah_bayar + A.bunga ELSE 0 end) as total_sudah_bayar,
(CASE WHEN B.`status` = 1 THEN 'Lunas' ELSE 'Belum Lunas' end) as `status`
from pembayaran AS A
INNER JOIN peminjaman AS B on A.id_peminjaman = B.id
INNER JOIN anggota AS C on B.id_anggota = C.id
GROUP BY A.id_peminjaman ;

-- ----------------------------
-- Procedure structure for generate_pembayaran
-- ----------------------------
DROP PROCEDURE IF EXISTS `generate_pembayaran`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `generate_pembayaran`(`id_pinjaman` int,`jumlah_pinjaman` decimal,`bunga` decimal,`kali_bayar` int, tgl_pinjaman date)
BEGIN
	declare xx int;	
	declare split_jml_pinjaman decimal(15,2);
	declare split_bunga decimal(15,2);
	
	set xx = 1;
	set split_jml_pinjaman = jumlah_pinjaman / kali_bayar;
	set split_bunga = bunga;
	
	while xx <=kali_bayar DO
		-- set tgl_batas = SELECT now() + INTERVAL xx MONTH;
		INSERT INTO pembayaran(id_peminjaman,jumlah_bayar,bunga,pembayaran_ke, tanggal_batas)VALUES(id_pinjaman,split_jml_pinjaman,split_bunga,xx,(SELECT tgl_pinjaman + INTERVAL xx MONTH));
		SET xx= xx+1;
	end while;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for p_generateSimpananW
-- ----------------------------
DROP PROCEDURE IF EXISTS `p_generateSimpananW`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `p_generateSimpananW`()
BEGIN
	DECLARE jumlahGen INT;
	declare jumlah_simpanan decimal(15,2);
	
	SELECT count(*) INTO jumlahGen from simpanan_wajib where periode = MONTH(NOW()) and tahun = YEAR(now());
	
	if(jumlahGen < 1) then
		select jumlah into jumlah_simpanan from jenis_simpanan where kode_simpanan = "wajib" limit 1;
		INSERT INTO simpanan_wajib(id_anggota,jumlah_bayar,status_pembayaran,periode,tahun)
		SELECT id,jumlah_simpanan,0,MONTH(NOW()),YEAR(now()) FROM anggota;
	end if;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for p_get_total_tunggakan_per_pelanggan
-- ----------------------------
DROP PROCEDURE IF EXISTS `p_get_total_tunggakan_per_pelanggan`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `p_get_total_tunggakan_per_pelanggan`(`anggota_id` int)
BEGIN
	SELECT 
	count(`status`) kali_tunggakan, 
	sum(jumlah_bayar) total_jml_bayar, 
	sum(bunga) total_jml_bunga, 
	(sum(jumlah_bayar)+sum(bunga)) total
 
	FROM v_get_tunggakan_pembayaran 
	WHERE id_anggota = anggota_id
	GROUP BY id_peminjaman;

END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for p_trans_rutin_del
-- ----------------------------
DROP PROCEDURE IF EXISTS `p_trans_rutin_del`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `p_trans_rutin_del`(`id_trans` int(11),`jns` varchar(255))
BEGIN
	DELETE FROM transaksi_rutin WHERE id = id_trans AND transaksi = jns;

END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for p_trans_rutin_in
-- ----------------------------
DROP PROCEDURE IF EXISTS `p_trans_rutin_in`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `p_trans_rutin_in`(`id_trans` int(11),`jenis_trans` varchar(255),`tgl_trans` date,`v_masuk` decimal(15,2),`v_keluar` decimal(15,2),`ket` varchar(255))
BEGIN
	INSERT INTO transaksi_rutin(transaksi,id_transaksi,tanggal_transaksi,masuk,keluar,keterangan) 
	VALUES (jenis_trans,id_trans,tgl_trans,v_masuk, v_keluar, ket);

END
;;
DELIMITER ;

-- ----------------------------
-- Event structure for generateSimpananWajib
-- ----------------------------
DROP EVENT IF EXISTS `generateSimpananWajib`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` EVENT `generateSimpananWajib` ON SCHEDULE EVERY 1 MONTH STARTS '2017-03-02 01:06:46' ON COMPLETION NOT PRESERVE ENABLE DO call p_generateSimpananW()
;;
DELIMITER ;
DROP TRIGGER IF EXISTS `generate_pembayaran`;
DELIMITER ;;
CREATE TRIGGER `generate_pembayaran` AFTER INSERT ON `peminjaman` FOR EACH ROW call generate_pembayaran(NEW.id,NEW.jumlah_pinjaman, NEW.bunga, NEW.kali_bayar, NEW.tgl_pinjaman)
;;
DELIMITER ;
