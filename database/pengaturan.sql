/*
Navicat MySQL Data Transfer

Source Server         : local
Source Server Version : 50505
Source Host           : localhost:3306
Source Database       : sikopat

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2017-02-06 11:54:56
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for pengaturan
-- ----------------------------
DROP TABLE IF EXISTS `pengaturan`;
CREATE TABLE `pengaturan` (
  `key` varchar(50) NOT NULL,
  `value` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`key`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of pengaturan
-- ----------------------------
INSERT INTO `pengaturan` VALUES ('bunga_pinjaman', '1');
INSERT INTO `pengaturan` VALUES ('iuran_wajib', '25000');
INSERT INTO `pengaturan` VALUES ('kali_bayar', '10');
INSERT INTO `pengaturan` VALUES ('max_kali_bunga', '3');
INSERT INTO `pengaturan` VALUES ('max_kali_tunggakan', '3');
