/*
Navicat MySQL Data Transfer

Source Server         : local
Source Server Version : 50505
Source Host           : localhost:3306
Source Database       : sikopat

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2017-02-02 01:07:30
*/
SET GLOBAL event_scheduler = ON;
SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Event structure for generateSimpananWajib
-- ----------------------------
DROP EVENT IF EXISTS `generateSimpananWajib`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` EVENT `generateSimpananWajib` ON SCHEDULE EVERY 1 MONTH STARTS '2017-03-02 01:06:46' ON COMPLETION NOT PRESERVE ENABLE DO call p_generateSimpananW()
;;
DELIMITER ;
