/*
Navicat MySQL Data Transfer

Source Server         : local
Source Server Version : 50505
Source Host           : localhost:3306
Source Database       : sikopat

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2017-02-10 15:03:23
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for detail_shu
-- ----------------------------
DROP TABLE IF EXISTS `detail_shu`;
CREATE TABLE `detail_shu` (
`id`  int(11) NOT NULL AUTO_INCREMENT ,
`id_shu`  int(11) NULL DEFAULT NULL ,
`id_anggota`  int(11) NULL DEFAULT NULL ,
`anggota`  decimal(15,2) NULL DEFAULT NULL ,
`peminjam`  decimal(15,2) NULL DEFAULT NULL ,
`pengurus`  decimal(15,2) NULL DEFAULT NULL ,
`pengawas`  decimal(15,2) NULL DEFAULT NULL ,
`jumlah`  decimal(15,2) NULL DEFAULT NULL ,
PRIMARY KEY (`id`),
FOREIGN KEY (`id_anggota`) REFERENCES `anggota` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
FOREIGN KEY (`id_shu`) REFERENCES `shu` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
INDEX `fk_detail_shu_shu_1` (`id_shu`) USING BTREE ,
INDEX `fk_detail_shu_anggota_1` (`id_anggota`) USING BTREE 
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=latin1 COLLATE=latin1_swedish_ci
AUTO_INCREMENT=833

;

-- ----------------------------
-- Records of detail_shu
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for shu
-- ----------------------------
DROP TABLE IF EXISTS `shu`;
CREATE TABLE `shu` (
`id`  int(11) NOT NULL AUTO_INCREMENT ,
`periode`  int(4) NULL DEFAULT NULL ,
`tahun`  int(5) NULL DEFAULT NULL ,
`laba_tahunan`  double(15,2) NULL DEFAULT NULL ,
`tanggal_generate`  datetime NULL DEFAULT NULL ,
`generate_oleh`  int(11) NULL DEFAULT NULL ,
PRIMARY KEY (`id`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=latin1 COLLATE=latin1_swedish_ci
AUTO_INCREMENT=15

;

-- ----------------------------
-- Records of shu
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Auto increment value for detail_shu
-- ----------------------------
ALTER TABLE `detail_shu` AUTO_INCREMENT=833;

-- ----------------------------
-- Auto increment value for shu
-- ----------------------------
ALTER TABLE `shu` AUTO_INCREMENT=15;
