<?php 

$string = "
<section class='content'>
  <div class='row'>
    <div class='col-xs-12'>
      <div class='box box-primary'>
        <div class='box-header'>
          <h3 class='box-title'>".  strtoupper($table_name)." LIST";
$string .= "</h3>
        </div>
        <div class='box-body'>
		<div class='row'>
		    <div class='col-md-8'><p>";

$string .= "<?php echo anchor('".$c_url."/create/','Create',array('class'=>'btn btn-danger btn-flat btn-sm'));?>";
if ($export_excel == '1') {
    $string .= "\n\t\t<?php echo anchor(site_url('".$c_url."/excel'), ' <i class=\"fa fa-file-excel-o\"></i>', 'class=\"btn btn-social-icon btn-flat btn-sm\"'); ?>";
}
if ($export_word == '1') {
    $string .= "\n\t\t<?php echo anchor(site_url('".$c_url."/word'), '<i class=\"fa fa-file-word-o\"></i>', 'class=\"btn btn-social-icon btn-flat btn-sm\"'); ?>";
}

if ($export_pdf == '1') {
    $string .= "\n\t\t<?php echo anchor(site_url('".$c_url."/pdf'), '<i class=\"fa fa-file-pdf-o\"></i>', 'class=\"btn btn-social-icon btn-flat btn-sm\"'); ?>";
}

$string .= "</p></div>
            <form action=\"<?php echo site_url('$c_url/index'); ?>\" class=\"form-inline\" method=\"get\">				
				<div class='col-md-4 pull-right'>
					<div class ='form-group form-group-sm'>						
						<label for='pwd'>Nama:</label>
						<div class=\"input-group\">						
							<input type=\"text\" class=\"form-control\" name=\"q\" value=\"<?php echo \$q; ?>\">
							<span class=\"input-group-btn\">
								<?php 
									if (\$q <> '')
									{
										?>
										<a href=\"<?php echo site_url('$c_url'); ?>\" class=\"btn btn-sm btn-flat btn-default\">Reset</a>
										<?php
									}
								?>
							  <button class=\"btn btn-sm btn-flat btn-primary\" type=\"submit\">Search</button>
							</span>
						</div>						
					</div>
				</div>				
            </form>            			
		</div>
		<br>
		<br>
        <div class='table-responsive'>
        <table class=\"table table-bordered\" style=\"margin-bottom: 10px\">
            <tr>
                <th>No</th>";
foreach ($non_pk as $row) {
    $string .= "\n\t\t<th>" . label($row['column_name']) . "</th>";
}
$string .= "\n\t\t<th>Action</th>
            </tr>";
$string .= "<?php
            foreach ($" . $c_url . "_data as \$$c_url)
            {
                ?>
                <tr>";

$string .= "\n\t\t\t<td width=\"80px\"><?php echo ++\$start ?></td>";
foreach ($non_pk as $row) {
    $string .= "\n\t\t\t<td><?php echo $" . $c_url ."->". $row['column_name'] . " ?></td>";
}

$string .= "\n\t\t    <td style=\"text-align:center\" width=\"140px\">"
    . "\n\t\t\t<?php "
    . "\n\t\t\techo anchor(site_url('".$c_url."/read/'.$".$c_url."->".$pk."),'<i class=\"fa fa-eye\"></i>',array('title'=>'detail','class'=>'btn btn-danger btn-xs')); "
    . "\n\t\t\techo '  '; "
    . "\n\t\t\techo anchor(site_url('".$c_url."/update/'.$".$c_url."->".$pk."),'<i class=\"fa fa-pencil-square-o\"></i>',array('title'=>'edit','class'=>'btn btn-danger btn-xs')); "
    . "\n\t\t\techo '  '; "
    . "\n\t\t\techo anchor(site_url('".$c_url."/delete/'.$".$c_url."->".$pk."),'<i class=\"fa fa-trash-o\"></i>','title=\"delete\" class=\"btn btn-danger btn-xs\" onclick=\"javasciprt: return confirm(\\'Are You Sure ?\\')\"'); "
    . "\n\t\t\t?>"
    . "\n\t\t    </td>";

$string .=  "\n\t\t</tr>
                <?php
            }
            ?>
        </table>
		</div>
		<div class=\"row\">
            <div class=\"col-md-6\">
                <a href=\"#\" class=\"btn btn-primary btn-flat\">Total Record : <?php echo \$total_rows ?></a>
			</div>
            <div class=\"col-md-6 text-right\">
                <?php echo \$pagination ?>
            </div>
        </div>
        </div>
      </div>
    </div>
  </div>
</section>    

";


$hasil_view_list = createFile($string, $target."views/$table_name/" . $v_list_file);

?>