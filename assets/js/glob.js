/**
 * Created by balisoket Design on 1/11/2017.
 */
$(function () {
    $("#example1").DataTable();
    $('#example2').DataTable({
        "paging": true,
        "lengthChange": false,
        "searching": false,
        "ordering": true,
        "info": true,
        "autoWidth": false
    });
    $(".form_date").datepicker({format: 'dd-mm-yyyy'});
    $(".to_date").datepicker({format: 'dd-mm-yyyy'});
    $(".select2").select2({
        placeholder: "Pilih data...",
        allowClear: true,
    });
    $('.angka_indo').number(true,2,',','.');
    // if(is_newRecord){
    //     $(".select2").select2("val", "");
    // }
    // $(":input").inputmask();

    $('#tbl_simpanan_wajib').on('click','a',function (e) {
        e.preventDefault();
        console.log(link_batal_simpanan);
        var tr = $(this).parents('tr');
        var btn = $(this);
        var r = confirm("Yakin akan melakuakan proses "+ btn.html() +"!");
        if($(btn).attr('data') ==0){
            if (r == true) {
                var xlink = link_batal_simpanan + tr.attr('data');

                $.get(xlink,function (res) {
                    res = JSON.parse(res);
                    if(res.batal == true){
                        $(btn).html('Bayarkan')
                            .removeClass('btn-danger')
                            .addClass('btn-success')
                            .attr('data',1);
                        $(tr).find('td:nth-child(3)').html('');
                        $(tr).find('td:nth-child(5)').html('Belum Lunas');
                        alert("Transaksi telah diproses.");
                    }else{
                        alert("Transaksi gagal dilakukan.");
                    }
                });
            }else {
                alert("Proses dibatalkan.");
            }
        }else if($(btn).attr('data') ==1){
            if (r == true) {
                xlink = link_bayar_simpanan + tr.attr('data');
                $.get(xlink,function (res) {
                    res = JSON.parse(res);
                    if(res.bayar == true){
                        $(btn).html('Batal Bayar')
                            .removeClass('btn-success')
                            .addClass('btn-danger')
                            .attr('data',0);
                        $(tr).find('td:nth-child(3)').html(res.tgl_bayar);
                        $(tr).find('td:nth-child(5)').html('Lunas');
                        alert("Transaksi telah diproses.");
                    }else{
                        alert("Transaksi gagal dilakukan.");
                    }
                });
            }else {
                alert("Proses dibatalkan.");
            }
        }
    })
});

function printLap(link,name) {
    //var winPrint = window.open(link, name, 'left=0,top=0,width=800,height=600,toolbar=0,scrollbars=0,status=0');
    var winPrint = window.open(link, name);
    // winPrint.document.write('<title>Print  Report</title><br /><br /> Hellow World');
    // winPrint.document.close();
    // winPrint.focus();
    // winPrint.print();
    // winPrint.close();
}

function readonly_select(objs, action) {
    if (action===true)
        objs.prepend('<div class="disabled-select"></div>');
    else
        $(".disabled-select", objs).remove();
}