<?php
/**
 * Created by PhpStorm.
 * User: dodik
 * Date: 27/01/2017
 * Time: 00.09
 */
$total_masuk = (isset($pembayaranSW->masuk)?$pembayaranSW->masuk:0) +
    (isset($pembayaranPinjaman->masuk)? $pembayaranPinjaman->masuk:0) +
    (isset($bungaPinjaman->masuk)? $bungaPinjaman->masuk:0);
$total_keluar = isset($peminjaman->keluar)?$peminjaman->keluar:0;
?>
<table class="table table-bordered" width="100%">
    <thead>
    <tr>
        <th width="15%">Diskripsi</th>
        <th width="45%"></th>
        <th width="15%">Jumlah</th>
        <th width="15%">Total Rp.</th>
    </tr>
    </thead>
    <tbody>
    <!-- Pendapatan Begin -->
    <tr class="even">
        <td colspan="4"><b>&nbsp;PENDAPATAN</b></td>
    </tr>
    <tr class="even">
        <td colspan="3">&nbsp;&nbsp;PENDAPATAN SIMPANAN</td>
        <td ></td>
    </tr>
    <tr class="even">
        <td></td>
        <td style="padding-left: 20px;">Simpanan Wajib</td>
        <td align="right"><?= angka_indo(isset($pembayaranSW->masuk)?$pembayaranSW->masuk:0) ?></td>
        <td align="right"></td>
    </tr>
    <tr class="even">
        <td></td>
        <td style="padding-left: 20px;">Pembayaran Pinjaman</td>
        <td align="right"><?= angka_indo(isset($pembayaranPinjaman->masuk)?$pembayaranPinjaman->masuk:0) ?></td>
        <td align="right"></td>
    </tr>
    <tr class="even">
        <td></td>
        <td style="padding-left: 20px;">Bunga Pinjaman</td>
        <td align="right"><?= angka_indo(isset($bungaPinjaman->masuk)?$bungaPinjaman->masuk:0) ?></td>
        <td align="right"></td>
    </tr>
    <tr class="odd" style="font-weight: bold;">
        <td>&nbsp;TOTAL PENDAPATAN</td>
        <td align="right"></td>
        <td align="right"></td>
        <td align="right"><?= angka_indo($total_masuk) ?></td>
    </tr>
    <!-- Pendapatan End -->

    <!-- Pengeluaran Start -->

    <tr class="even">
        <td><b>&nbsp;PENGELUARAN</b></td>
        <td colspan="4"></td>
    </tr>
    <tr class="even">
        <td></td>
        <td>&nbsp;&nbsp;Peminjaman Anggota</td>
        <td align="right"><?= angka_indo(isset($peminjaman->keluar)?$peminjaman->keluar:0) ?></td>
        <td align="right"></td>
    </tr>
    <tr class="odd" style="font-weight: bold;">
        <td>&nbsp;TOTAL BEBAN</td>
        <td align="right"></td>
        <td align="right"></td>
        <td align="right"><?= angka_indo(isset($peminjaman->keluar)?$peminjaman->keluar:0) ?></td>
    </tr>
    <?php
    $result_nominal = $total_masuk-$total_keluar;
    if ($result_nominal < 0) {
        $hasil = "(".angka_indo(abs($result_nominal)).")";
    } else {
        $hasil = angka_indo($result_nominal);
    }

    //                            $result_percent = (round($percentage_pendapatan_barang+$percentage_jasa_apt+$percentage_pend_lain,2)-($hpps+$ttl_beban_usaha));
    //                            if ($result_percent < 0) {
    //                                $hasil_percent = "(".abs($result_percent).")";
    //                            } else {
    //                                $hasil_percent = $result_percent;
    //                            }
    ?>
    <tr class="odd" style="font-weight: bold;">
        <td>&nbsp;LABA (RUGI)</td>
        <td align="right"></td>
        <td align="right"></td>
        <td align="right"><?= $hasil ?></td>
    </tr>
    </tbody>
</table>
