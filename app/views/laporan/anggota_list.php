
<section class='content'>
    <div class='row'>
        <div class='col-xs-12'>
            <div class='box box-primary'>
                <div class='box-body'>
                    <div class='row row-centered'>
                        <h3 class="text-center">Laporan Daftar Anggota</h3>
                        <hr>						
                        <form id="form_search" action="<?php echo site_url('laporan/anggota/index'); ?>" class="form-inline" method="get">
                            <div class='col-md-8 col-centered'>

                                <div class ='form-group form-group-sm pull-right'>
									<?php echo anchor('laporan/anggota/report/','<i class="fa fa-print"></i>',array('class'=>'btn btn-social-icon btn-flat  btn-sm', 'target' =>'_blank', 'id' =>'print'));?>
                                    <div class="input-group">
                                        <input type="text" class="form-control" name="q" value="<?php echo $q; ?>" placeholder="Search">
                                        <span class="input-group-btn">
								<?php
                                if ($q <> '')
                                {
                                    ?>
                                    <a href="<?php echo site_url('laporan/anggota'); ?>" class="btn btn-sm btn-flat  btn-default">Reset</a>
                                    <?php
                                }
                                ?>
                                            <button class="btn btn-sm btn-flat btn-primary" type="submit">Search</button>
							</span>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>                    
                    <br>
                    <br>
                    <div class='table-responsive'>
                        <table class="table table-bordered" style="margin-bottom: 10px">
                            <tr>
                                <th>No</th>
                                <th>NIK</th>
                                <th>Nama</th>
                                <th>Tgl Lahir</th>
                                <th>Jenis Kelamin</th>
                                <th>Alamat</th>
                                <th>No Telp</th>
                                <th>Jabatan</th>
                                <th>Tgl Masuk</th>
                                <th>Saldo Simpanan</th>
                                <th>Status</th>
                            </tr><?php
                            foreach ($anggota_data as $anggota)
                            {
                                ?>
                                <tr>
                                    <td width="80px"><?php echo ++$start ?></td>
                                    <td><?php echo $anggota->nik ?></td>
                                    <td><?php echo $anggota->nama ?></td>
                                    <td><?php echo ($anggota->tgl_lahir)?ymdToDmy($anggota->tgl_lahir):null ?></td>
                                    <td><?php echo $anggota->jenis_kelamin ?></td>
                                    <td><?php echo $anggota->alamat ?></td>
                                    <td><?php echo $anggota->no_telp ?></td>
                                    <td><?php echo $anggota->nama_jabatan ?></td>
                                    <td><?php echo ($anggota->tgl_masuk)?ymdToDmy($anggota->tgl_masuk):null ?></td>
                                    <td><?php echo angka_indo($anggota->saldo_simpanan) ?></td>
                                    <td><span class="label <?= ($anggota->status)? 'label-success':'label-danger' ?>"><?php echo element((is_null($anggota->status)?0:$anggota->status),config_item('status_aktif'))?></span></td>
                                </tr>
                                <?php
                            }
                            ?>
                        </table>
                    </div>
                    <br>
                    <br>
                    <div class="row">
                        <div class="col-md-6">
                            <a href="#" class="btn btn-primary btn-flat">Total Record : <?php echo $total_rows ?></a>
                        </div>
                        <div class="col-md-6 text-right">
                            <?php echo $pagination ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<script>
    $(document).ready(function () {
        $('#print').on('click',function (e) {
            e.preventDefault();
            printLap($(this).attr('href'),'Laporan Laba Rugi');
//            printLap($(this).attr('href') +'?'+ $('#form_search').serialize(),'Laporan Laba Rugi');
        });

        $('#excel').on('click',function (e) {
            e.preventDefault();
            printLap($(this).attr('href') +'?'+ $('#form_search').serialize(),'Laporan Laba Rugi');
        });
        $('#word').on('click',function (e) {
            e.preventDefault();
            printLap($(this).attr('href') +'?'+ $('#form_search').serialize(),'Laporan Laba Rugi');
        });
        $('#pdf').on('click',function (e) {
            e.preventDefault();
            printLap($(this).attr('href') +'?'+ $('#form_search').serialize(),'Laporan Laba Rugi');
        });
    })
</script>