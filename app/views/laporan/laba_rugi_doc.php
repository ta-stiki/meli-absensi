<!doctype html>
<html>
<head>
    <title>Laporan Daftar Anggota</title>
    <link rel="shortcut icon" type="image/x-icon" href="<?= base_url('assets/img/new icon png.ico')?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>"/>
    <link rel="stylesheet" type="text/css" media="print" href="<?php echo base_url('assets/css/print.css') ?>"/>
    <style>
        .word-table {
            border:1px solid rgba(25, 25, 25, 0.83) !important;
            border-collapse: collapse !important;
            width: 100%;
        }
        .word-table tr th, .word-table tr td{
            border:1px solid black !important;
            padding: 5px 10px;
        }
        div.wrap {
            width: 100%;
            height:50px;
            position: relative;
        }

        .wrap img {
            position: absolute;
            bottom: 0;
        }

        .wrap img:nth-of-type(1) {
            left: 0;
        }

        .wrap img:nth-of-type(2) {
            right: 0;
        }

    </style>
</head>
<body>
<div id="divPrint" style="width: 100%; margin: auto;">
    <div style="width:80%;margin: 0 auto 10px; padding-bottom: 10px; border-bottom: 1px solid black" id="kop">
        <div style="width:100%;text-align:center;font-size:10pt;font-weight:bold;">
            <h2 class="text-center">Laporan Laba Rugi</h2>
        </div>
    </div>
    <?php
    /**
     * Created by PhpStorm.
     * User: dodik
     * Date: 27/01/2017
     * Time: 00.09
     */
    $total_masuk = (isset($pembayaranSW->masuk)?$pembayaranSW->masuk:0) +
        (isset($pembayaranPinjaman->masuk)? $pembayaranPinjaman->masuk:0) +
        (isset($bungaPinjaman->masuk)? $bungaPinjaman->masuk:0);
    $total_keluar = isset($peminjaman->keluar)?$peminjaman->keluar:0;
    ?>
    <table class="table table-bordered" width="100%">
        <thead>
        <tr>
            <th width="15%">Diskripsi</th>
            <th width="45%"></th>
            <th width="15%">Jumlah</th>
            <th width="15%">Total Rp.</th>
        </tr>
        </thead>
        <tbody>
        <!-- Pendapatan Begin -->
        <tr class="even">
            <td colspan="4"><b>&nbsp;PENDAPATAN</b></td>
        </tr>
        <tr class="even">
            <td colspan="3">&nbsp;&nbsp;PENDAPATAN SIMPANAN</td>
            <td ></td>
        </tr>
        <tr class="even">
            <td></td>
            <td style="padding-left: 20px;">Simpanan Wajib</td>
            <td align="right"><?= angka_indo(isset($pembayaranSW->masuk)?$pembayaranSW->masuk:0) ?></td>
            <td align="right"></td>
        </tr>
        <tr class="even">
            <td></td>
            <td style="padding-left: 20px;">Pembayaran Pinjaman</td>
            <td align="right"><?= angka_indo(isset($pembayaranPinjaman->masuk)?$pembayaranPinjaman->masuk:0) ?></td>
            <td align="right"></td>
        </tr>
        <tr class="even">
            <td></td>
            <td style="padding-left: 20px;">Bunga Pinjaman</td>
            <td align="right"><?= angka_indo(isset($bungaPinjaman->masuk)?$bungaPinjaman->masuk:0) ?></td>
            <td align="right"></td>
        </tr>
        <tr class="odd" style="font-weight: bold;">
            <td>&nbsp;TOTAL PENDAPATAN</td>
            <td align="right"></td>
            <td align="right"></td>
            <td align="right"><?= angka_indo($total_masuk) ?></td>
        </tr>
        <!-- Pendapatan End -->

        <!-- Pengeluaran Start -->

        <tr class="even">
            <td><b>&nbsp;PENGELUARAN</b></td>
            <td colspan="4"></td>
        </tr>
        <tr class="even">
            <td></td>
            <td>&nbsp;&nbsp;Peminjaman Anggota</td>
            <td align="right"><?= angka_indo(isset($peminjaman->keluar)?$peminjaman->keluar:0) ?></td>
            <td align="right"></td>
        </tr>
        <tr class="odd" style="font-weight: bold;">
            <td>&nbsp;TOTAL BEBAN</td>
            <td align="right"></td>
            <td align="right"></td>
            <td align="right"><?= angka_indo(isset($peminjaman->keluar)?$peminjaman->keluar:0) ?></td>
        </tr>
        <?php
        $result_nominal = $total_masuk-$total_keluar;
        if ($result_nominal < 0) {
            $hasil = "(".angka_indo(abs($result_nominal)).")";
        } else {
            $hasil = angka_indo($result_nominal);
        }

        //                            $result_percent = (round($percentage_pendapatan_barang+$percentage_jasa_apt+$percentage_pend_lain,2)-($hpps+$ttl_beban_usaha));
        //                            if ($result_percent < 0) {
        //                                $hasil_percent = "(".abs($result_percent).")";
        //                            } else {
        //                                $hasil_percent = $result_percent;
        //                            }
        ?>
        <tr class="odd" style="font-weight: bold;">
            <td>&nbsp;LABA (RUGI)</td>
            <td align="right"></td>
            <td align="right"></td>
            <td align="right"><?= $hasil ?></td>
        </tr>
        </tbody>
    </table>
</div>
</body>
</html>