
<section class='content'>
	<div class='row'>
		<div class='col-xs-12'>
			<div class='box box-primary'>
				<div class='box-body'>
					<div class='row'>
						<h3 class="text-center">Laporan Pembayaran</h3>
						<hr>
						<form id="form_search" action="<?php echo site_url('laporan/pembayaran/index'); ?>" class="form-inline" method="get">
                            <div class='col-md-8'>
                                <div class ='form-group form-group-sm'>
                                    <label for='pwd'>Filter Berdasarkan: </label>
                                    <div class="input-group input-daterange">
                                        <input type="text" class="form-control form_date" name="dari" value="<?= $dari?>">
                                        <div class="input-group-addon">S/d</div>
                                        <input type="text" class="form-control to_date" name="sampai"  value="<?= $sampai?>">
                                    </div>
                                </div>
                                <div class ='form-group form-group-sm'>
                                    <label for='pwd'>Status:</label>
                                    <?php
									echo $status;
                                    $arr = [
                                        'Lunas' => 'Lunas',
                                        'Belum Lunas' => 'Belum Lunas',
                                    ];
                                    echo arr_to_combo('status',$arr,$status,'');?>
                                </div>
								<?php echo anchor('laporan/pembayaran/report/?q='.$q,'<i class="fa fa-print"></i>',array('class'=>'btn btn-social-icon btn-flat  btn-sm', 'id'=>"print"));?>                            
                            </div>
							<div class='col-md-4 pull-right'>
								<div class ='form-group form-group-sm'>
									<label for='pwd'>No Pinjaman:</label>
									<div class="input-group">
										<input type="text" class="form-control" name="q" value="<?php echo $q; ?>">
										<span class="input-group-btn">
								<?php
								if ($q <> '')
								{
									?>
									<a href="<?php echo site_url('laporan/pembayaran'); ?>" class="btn btn-sm btn-flat btn-default">Reset</a>
									<?php
								}
								?>
											<button class="btn btn-sm btn-flat btn-primary" type="submit">Search</button>
							</span>
									</div>
								</div>
							</div>
						</form>
					</div>                    
                    <br>
                    <br>
					<br>
					<div class='table-responsive'>
						<table class="table table-bordered" style="margin-bottom: 10px">
							<tr>
								<th>No</th>
								<th>No Pinjaman</th>
								<th>Tgl Pinjaman</th>
								<th>Nama Anggota</th>
								<th>Total Pinjaman</th>
								<th>Tunggakan</th>
								<th>Total Tunggalan</th>
								<th>Status</th>

							</tr><?php
							foreach ($pembayaran_data as $pembayaran)
							{
								?>
								<tr>
									<td width="80px"><?php echo ++$start ?></td>
									<td><?php echo $pembayaran->no_pinjaman ?></td>
									<td><?php echo ymdToDmy($pembayaran->tgl_pinjaman) ?></td>
									<td><?php echo $pembayaran->nama ?></td>
									<td><?php echo number_format($pembayaran->total_pinjaman,2,',','.') ?></td>
									<td><?php echo $pembayaran->jml_belum_bayar ?> kali</td>
									<td><?php echo number_format($pembayaran->total_belum_bayar,2,',','.') ?></td>
									<td><?php echo $pembayaran->status ?></td>

								</tr>
								<?php
							}
							?>
						</table>
					</div>
					<div class="row">
						<div class="col-md-6">
							<a href="#" class="btn btn-primary btn-flat">Total Record : <?php echo $total_rows ?></a>
						</div>
						<div class="col-md-6 text-right">
							<?php echo $pagination ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<script>
    $(document).ready(function () {
        $('#print').on('click',function (e) {
            e.preventDefault();
            printLap($(this).attr('href') +'?'+ $('#form_search').serialize(),'Laporan Laba Rugi');
        });

        $('#excel').on('click',function (e) {
            e.preventDefault();
            printLap($(this).attr('href') +'?'+ $('#form_search').serialize(),'Laporan Laba Rugi');
        });
        $('#word').on('click',function (e) {
            e.preventDefault();
            printLap($(this).attr('href') +'?'+ $('#form_search').serialize(),'Laporan Laba Rugi');
        });
        $('#pdf').on('click',function (e) {
            e.preventDefault();
            printLap($(this).attr('href') +'?'+ $('#form_search').serialize(),'Laporan Laba Rugi');
        });
    })
</script>
