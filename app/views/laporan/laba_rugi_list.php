<?php ?>
<section class='content'>
	<div class='row'>
		<div class='col-xs-12'>
			<div class='box box-primary'>
				<div class='box-body'>
					<div class='row row-centered'>
						<h3 class="text-center">Laporan Laba Rugi</h3>
						<hr>
						<form id="form_search" action="<?php echo site_url('laporan/laba_rugi/detail'); ?>" class="form-inline" method="get">
							<div class='col-md-8 col-centered'>
								<div class ='form-group form-group-sm'>
									<div class="input-group input-daterange">
										<input type="text" class="form-control form_date" name="dari" value="<?= date('d-m-Y')?>">
										<div class="input-group-addon"><i class="fa fa-calendar"></i></div>
										<div class="input-group-addon">S/d</div>
										<input type="text" class="form-control to_date" name="sampai"  value="<?= date('d-m-Y')?>">
										<div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                                        <span class="input-group-btn">
                                            <button class="btn btn-sm btn-flat btn-primary" type="submit">Search</button>
                                        </span>										
									</div>
									<?php echo anchor('laporan/laba_rugi/report','<i class="fa fa-print"></i>',array('class'=>'btn btn-social-icon btn-flat  btn-sm','id'=>'print'));?>
								</div>
							</div>
						</form>
					</div>					
					<br>
					<br>
					<div class='table-responsive lap_laba_rugi'>
                        <p>Data Belum di filter</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<script>
    function printLap(link,name) {
        var winPrint = window.open(link, name, 'left=0,top=0,width=800,height=600,toolbar=0,scrollbars=0,status=0');
        // winPrint.document.write('<title>Print  Report</title><br /><br /> Hellow World');
        // winPrint.document.close();
//        winPrint.focus();
//        winPrint.print();
//        winPrint.close();
    }

    $(document).ready(function () {
        $('#form_search').on('submit',function (e) {
            e.preventDefault();
            $.post($(this).attr('action'),$(this).serialize(),function (result) {
                if(result){
                    $('.lap_laba_rugi').html(result);
                }
            })
        });

        $('#print').on('click',function (e) {
            e.preventDefault();
            printLap($(this).attr('href') +'?'+ $('#form_search').serialize(),'Laporan Laba Rugi');
        });

        $('#excel').on('click',function (e) {
            e.preventDefault();
            printLap($(this).attr('href') +'?'+ $('#form_search').serialize(),'Laporan Laba Rugi');
        });
        $('#word').on('click',function (e) {
            e.preventDefault();
            printLap($(this).attr('href') +'?'+ $('#form_search').serialize(),'Laporan Laba Rugi');
        });
        $('#pdf').on('click',function (e) {
            e.preventDefault();
            printLap($(this).attr('href') +'?'+ $('#form_search').serialize(),'Laporan Laba Rugi');
        });
    })
</script>