<!doctype html>
<html>
    <head>
        <title>harviacode.com - codeigniter crud generator</title>
        <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>"/>
        <style>
            .word-table {
                border:1px solid black !important; 
                border-collapse: collapse !important;
                width: 100%;
            }
            .word-table tr th, .word-table tr td{
                border:1px solid black !important; 
                padding: 5px 10px;
            }
        </style>
    </head>
    <body>
        <h2>Pembayaran List</h2>
        <table class="table table-bordered" style="margin-bottom: 10px">
            <tr>
                <th>No</th>
                <th>No Pinjaman</th>
                <th>Tgl Pinjaman</th>
                <th>Nama Anggota</th>
                <th>Total Pinjaman</th>
                <th>Tunggakan</th>
                <th>Total Tunggalan</th>
                <th>Status</th>

            </tr><?php
            foreach ($pembayaran_data as $pembayaran)
            {
                ?>
                <tr>
                    <td width="80px"><?php echo ++$start ?></td>
                    <td><?php echo ymdToDmy($pembayaran->tgl_pinjaman) ?></td>
                    <td><?php echo $pembayaran->no_pinjaman ?></td>
                    <td><?php echo $pembayaran->nama ?></td>
                    <td><?php echo number_format($pembayaran->total_pinjaman,2,',','.') ?></td>
                    <td><?php echo $pembayaran->jml_belum_bayar ?> kali</td>
                    <td><?php echo number_format($pembayaran->total_belum_bayar,2,',','.') ?></td>
                    <td><?php echo $pembayaran->status ?></td>

                </tr>
                <?php
            }
            ?>
        </table>
    </body>
</html>