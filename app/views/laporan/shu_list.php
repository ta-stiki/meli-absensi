
<section class='content'>
	<div class='row'>
		<div class='col-xs-12'>
			<div class='box box-primary'>
				<div class='box-body'>
					<div class='row row-centered'>
						<h3 class="text-center">Laporan SHU</h3>
						<hr>
						<form id="form_search" action="<?php echo site_url('laporan/shu/index'); ?>" class="form-inline" method="get">
							<div class='col-md-4 pull-right'>
								<div class ='form-group form-group-sm'>
									<div class="input-group">
										<input type="text" class="form-control" name="q" value="<?php echo $q; ?>" placeholder="Search">
										<span class="input-group-btn">
								<?php
								if ($q <> '')
								{
									?>
									<a href="<?php echo site_url('laporan/shu'); ?>" class="btn btn-default">Reset</a>
									<?php
								}
								?>
											<button class="btn btn-sm btn-flat btn-primary" type="submit">Search</button>
							</span>
									</div>
								</div>
							</div>
						</form>
					</div>
					<br>
					<br>
					<div class='table-responsive'>
						<table class="table table-bordered" style="margin-bottom: 10px">
							<tr>
								<th>No</th>
								<th>Periode</th>
								<th>Laba Tahunan</th>
								<th>Tanggal Generate</th>
								<th>Generate Oleh</th>
								<th>Print</th>
							</tr><?php
							foreach ($shu_data as $shu)
							{
								?>
								<tr>
									<td width="80px"><?php echo ++$start ?></td>
									<td><?php echo $shu->periode ?></td>
									<td><?php echo angka_indo($shu->laba_tahunan) ?></td>
									<td><?php echo ymdToDmy($shu->tanggal_generate) ?></td>
									<td><?php echo $this->Anggota_model->getNamaAnggota($shu->generate_oleh) ?></td>
									<td><?php echo anchor("laporan/shu/report/$shu->id",'<i class="fa fa-print"></i>',array('class'=>'btn btn-social-icon btn-flat  btn-sm','id'=>'print')) ?></td>
								</tr>
								<?php
							}
							?>
						</table>
					</div>
					<div class="row">
						<div class="col-md-6">
							<a href="#" class="btn btn-primary btn-flat">Total Record : <?php echo $total_rows ?></a>
						</div>
						<div class="col-md-6 text-right">
							<?php echo $pagination ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<script>
    $(document).ready(function () {
        $('#print').on('click',function (e) {
            e.preventDefault();
            printLap($(this).attr('href') +'?'+ $('#form_search').serialize(),'Laporan SHU');
        });
    })
</script>