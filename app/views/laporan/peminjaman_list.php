<section class='content'>
	<div class='row'>
		<div class='col-xs-12'>
			<div class='box box-primary'>
				<div class='box-body'>
					<div class='row row-centered'>
						<h3 class="text-center">Laporan Peminjaman</h3>
						<hr>
						<form id="form_search" action="<?php echo site_url('laporan/peminjaman/index'); ?>" class="form-inline" method="get">
							<div class='col-md-8 col-centered'>
								<div class ='form-group form-group-sm'>
									<div class="input-group input-daterange">
										<input type="text" class="form-control form_date" name="dari" value="<?= $dari?>">
										<div class="input-group-addon"><i class="fa fa-calendar"></i></div>
										<div class="input-group-addon">S/d</div>
										<input type="text" class="form-control to_date" name="sampai"  value="<?= $sampai?>">
										<div class="input-group-addon"><i class="fa fa-calendar"></i></div>
									</div>
								</div>
								<div class ='form-group form-group-sm'>
									<div class="input-group">
										<input type="text" class="form-control" name="q" value="<?php echo $q; ?>" placeholder="Search">
										<span class="input-group-btn">
								<?php
								if ($q <> '')
								{
									?>
									<a href="<?php echo site_url('laporan/peminjaman'); ?>" class="btn btn-default">Reset</a>
									<?php
								}
								?>
											<button class="btn btn-sm btn-flat btn-primary" type="submit">Search</button>
							</span>
									</div>
									<?php echo anchor('laporan/peminjaman/report/','<i class="fa fa-print"></i>',array('class'=>'btn btn-social-icon btn-flat  btn-sm btn-print', 'id'=>"print"));?>
								</div>
							</div>
						</form>
					</div>
					<div class="row row-centered">
						<div class="col-md-3 col-centered text-center btn-print">
<!--							--><?php //echo anchor(site_url('laporan/peminjaman/excel/?q='.$q), ' <i class="fa fa-file-excel-o"></i>', 'class="btn btn-social-icon btn-flat btn-sm"'); ?>
<!--							--><?php //echo anchor(site_url('laporan/peminjaman/word/?q='.$q), '<i class="fa fa-file-word-o"></i>', 'class="btn btn-social-icon btn-flat btn-sm"'); ?>
<!--							--><?php //echo anchor(site_url('laporan/peminjaman/pdf/?q='.$q), '<i class="fa fa-file-pdf-o"></i>', 'class="btn btn-social-icon btn-flat btn-sm"'); ?>
						</div>
					</div>
					<br>
					<br>
					<div class='table-responsive'>
						<table class="table table-bordered" style="margin-bottom: 10px">
							<tr>
								<th>No</th>
								<th>Tgl Pinjaman</th>
								<th>No Pinjaman</th>
								<th>Anggota</th>
								<th>Total Pinjaman</th>
<!--								<th>Sisa Tunggakan</th>-->
								<th>Status</th>
							</tr><?php
							foreach ($peminjaman_data as $peminjaman)
							{
								?>
								<tr>
									<td width="80px"><?php echo ++$start ?></td>
									<td><?php echo ymdToDmy($peminjaman->tgl_pinjaman) ?></td>
									<td><?php echo $peminjaman->no_pinjaman ?></td>
									<td><?php echo $this->Anggota_model->getNamaAnggota($peminjaman->id_anggota) ?></td>
									<td><?php echo number_format($peminjaman->total_pinjaman,2,',','.') ?></td>
<!--									<td>--><?php //echo number_format($peminjaman->sisa_pembayaran,2,',','.') ?><!--</td>-->
									<td><span class="label <?= ($peminjaman->status)? 'label-success':'label-danger' ?>"><?php echo element((is_null($peminjaman->status)?0:$peminjaman->status),config_item('status_pembayaran'))?></span></td>
								</tr>
								<?php
							}
							?>
						</table>
					</div>
				</div>
				<div class="box-footer">
					<div class="row">
						<div class="col-md-6">
							<a href="#" class="btn btn-primary btn-flat">Total Record : <?php echo $total_rows ?></a>
						</div>
						<div class="col-md-6 text-right">
							<?php echo $pagination ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<script>
	$(function () {
		$('#print').on('click',function (e) {
			e.preventDefault();
			printLap($(this).attr('href') +'?'+ $('#form_search').serialize(),'Laporan Laba Rugi');
		});
		$('#q').bind("keydown", function (event) {
			if (event.keyCode === $.ui.keyCode.TAB && $(this).data("ui-autocomplete").menu.active) {
				event.preventDefault();
			}
		}).autocomplete({
			source: function (request, response) {
				var last = request.term;
//				var last = extractLast(request.term);
				$.ajax({
					url: "<?= base_url('peminjaman/no_peminjaman')?>",
					data: {'q':last},
					dataType: "json",
					type: "get",
					contentType: "application/json; charset=utf-8",
					success: function (data) {
						response(data);
//							response($.map(data, function (item) {
//								return { value: item.id };
//							}))
					},
					error: function (XMLHttpRequest, textStatus, errorThrown) {
						console.log(errorThrown);
						alert(textStatus);

					}
				});
			},
			minLength: 3,
			focus: function () {
				return false;
			},
			select: function (event, ui) {
				console.log(ui.item.value);
//				var terms = split(this.value);
//				terms.pop();
//				terms.push(ui.item.value + " ");
//				this.value = terms.join("@@");
				return ui.item.value;
			}
		});
	});
</script>

