<!doctype html>
<html>
<head>
    <title>Laporan simpanan Wajib</title>
    <link rel="shortcut icon" type="image/x-icon" href="<?= base_url('assets/img/new icon png.ico')?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>"/>
    <link rel="stylesheet" type="text/css" media="print" href="<?php echo base_url('assets/css/print.css') ?>"/>
    <style>
        .report-header{
            border-bottom: 4px double #000;
        }
        div.report-header h1,div.report-header h2,div.report-header h3 {
            font-family: "Times New Roman", Georgia, Serif;
            margin: 0 !important;
            text-align: center !important;
        }
        .word-table {
            border:1px solid rgba(25, 25, 25, 0.83) !important;
            border-collapse: collapse !important;
            width: 100%;
        }
        .word-table tr th, .word-table tr td{
            border:1px solid black !important;
            padding: 5px 10px;
        }
        div.wrap {
            width: 100%;
            height:150px;
            position: relative;
        }

        .wrap img {
            position: absolute;
            bottom: 0;
        }

        .wrap img:nth-of-type(1) {
            left: 0;
        }

        .wrap img:nth-of-type(2) {
            right: 0;
        }

    </style>
</head>
<body>
<div id="divPrint" style="width: 100%; margin: auto;">


    <div style="width:80%;margin: 0 auto 10px; padding-bottom: 10px; border-bottom: 1px solid black" id="kop">
        <div class="wrap">
            <img src="<?= base_url('assets/img/logo koperasi.png')?>" width="150"/>
            <img src="<?= base_url('assets/img/logo tiki.png')?>" width="150"/>
        </div>
        <div class="report-header">
            <h2 class="text-center">Laporan simpanan Wajib</h2>
            <h3>Koperasi Karyawan Sejahtera</h3>
            <h3>STMIK STIKOM Indonesia</h3>
        </div>
    </div>
    <table class="table table-bordered" style="margin-bottom: 10px">
        <tr>
            <th>No</th>
            <th>Anggota</th>
            <th>Tgl Bayar</th>
            <th>Jumlah Bayar</th>
            <th>Status Pembayaran</th>
            <th>Periode</th>
            <th>Tahun</th>
        </tr><?php
        foreach ($simpanan_wajib_data as $simpanan_wajib)
        {
            ?>
            <tr>
                <td width="80px"><?php echo ++$start ?></td>
                <td><?php echo $this->Anggota_model->getNamaAnggota($simpanan_wajib->id_anggota) ?></td>
                <td><?php echo ($simpanan_wajib->tgl_bayar)?ymdToDmy($simpanan_wajib->tgl_bayar):null ?></td>
                <td><?php echo angka_indo($simpanan_wajib->jumlah_bayar) ?></td>
                <td><?php echo element($simpanan_wajib->status_pembayaran,config_item('status_pembayaran')) ?></td>
                <td><?php echo element($simpanan_wajib->periode,config_item('periode_bulan')) ?></td>
                <td><?php echo $simpanan_wajib->tahun ?></td>
            </tr>
            <?php
        }
        ?>

    </table>
    <div>
        <div class="col-md-4 pull-right">
            <p class="text-center">Ketua Koperasi</p>
            <br>
            <br>
            <br>

            <p class="text-center">(__________________________________)</p>
        </div>
    </div>
</div>
    </body>
</html>