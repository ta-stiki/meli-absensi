<!doctype html>
<html>
    <head>
        <title>harviacode.com - codeigniter crud generator</title>
        <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>"/>
        <style>
            .word-table {
                border:1px solid black !important; 
                border-collapse: collapse !important;
                width: 100%;
            }
            .word-table tr th, .word-table tr td{
                border:1px solid black !important; 
                padding: 5px 10px;
            }
        </style>
    </head>
    <body>
        <h2>Peminjaman List</h2>
        <table class="word-table" style="margin-bottom: 10px">
            <tr>
                <th>No</th>
		<th>Id Anggota</th>
		<th>No Pinjaman</th>
		<th>Tgl Pinjaman</th>
		<th>Jumlah Pinjaman</th>
		<th>Bunga</th>
		<th>Total Pinjaman</th>
		<th>Status</th>
		<th>Input Oleh</th>
		<th>Tgl Input</th>
		<th>Update Oleh</th>
		<th>Tgl Update</th>
		<th>Sisa Pembayaran</th>
		
            </tr><?php
            foreach ($peminjaman_data as $peminjaman)
            {
                ?>
                <tr>
		      <td><?php echo ++$start ?></td>
		      <td><?php echo $peminjaman->id_anggota ?></td>
		      <td><?php echo $peminjaman->no_pinjaman ?></td>
		      <td><?php echo ymdToDmy($peminjaman->tgl_pinjaman) ?></td>
		      <td><?php echo angka_indo($peminjaman->jumlah_pinjaman) ?></td>
		      <td><?php echo angka_indo($peminjaman->jumlah_pinjaman) ?></td>
		      <td><?php echo angka_indo($peminjaman->total_pinjaman) ?></td>
		      <td><?php echo $peminjaman->status ?></td>
		      <td><?php echo $this->Anggota_model->getNamaAnggota($peminjaman->input_oleh) ?></td>
		      <td><?php echo ymdToDmy($peminjaman->tgl_input) ?></td>
		      <td><?php echo $this->Anggota_model->getNamaAnggota($peminjaman->update_oleh)  ?></td>
		      <td><?php echo ymdToDmy($peminjaman->tgl_update) ?></td>
		      <td><?php echo angka_indo($peminjaman->sisa_pembayaran) ?></td>
                </tr>
                <?php
            }
            ?>
        </table>
    </body>
</html>