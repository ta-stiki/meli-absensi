<!doctype html>
<html>
    <head>
        <title>harviacode.com - codeigniter crud generator</title>
        <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>"/>
        <style>
            .word-table {
                border:1px solid black !important; 
                border-collapse: collapse !important;
                width: 100%;
            }
            .word-table tr th, .word-table tr td{
                border:1px solid black !important; 
                padding: 5px 10px;
            }
        </style>
    </head>
    <body>
        <h2>Simpanan_wajib List</h2>
        <table class="word-table" style="margin-bottom: 10px">
            <tr>
                <th>No</th>
		<th>Id Anggota</th>
		<th>Tgl Bayar</th>
		<th>Jumlah Bayar</th>
		<th>Status Pembayaran</th>
		<th>Periode</th>
		<th>Tahun</th>
		
            </tr><?php
            foreach ($simpanan_wajib_data as $simpanan_wajib)
            {
                ?>
                <tr>
		      <td><?php echo ++$start ?></td>
		      <td><?php echo $this->Anggota_model->getNamaAnggota($simpanan_wajib->id_anggota) ?></td>
		      <td><?php echo ymdToDmy($simpanan_wajib->tgl_bayar) ?></td>
		      <td><?php echo angka_indo($simpanan_wajib->jumlah_bayar) ?></td>
		      <td><?php echo $simpanan_wajib->status_pembayaran ?></td>
		      <td><?php echo $simpanan_wajib->periode ?></td>
		      <td><?php echo $simpanan_wajib->tahun ?></td>	
                </tr>
                <?php
            }
            ?>
        </table>
    </body>
</html>