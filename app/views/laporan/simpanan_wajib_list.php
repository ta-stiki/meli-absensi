
<section class='content'>
	<div class='row'>
		<div class='col-xs-12'>
			<div class='box box-primary'>
				<div class='box-body'>
					<div class='row'>
						<h3 class="text-center">Laporan Simpanan Wajib</h3>
						<hr>
						<form id="form_search" action="<?php echo site_url('laporan/simpanan_wajib/index'); ?>" class="form-inline" method="get">
							<div class='col-md-8'>
								<div class ='form-group form-group-sm'>
									<label for='pwd'>Filter Berdasarkan: </label>
									<div class="input-group input-daterange">
										<input type="text" class="form-control form_date" name="dari" value="<?= $dari?>">
										<div class="input-group-addon">S/d</div>
										<input type="text" class="form-control to_date" name="sampai"  value="<?= $sampai?>">
									</div>
								</div>
								<div class ='form-group form-group-sm'>
									<label for='pwd'>Status:</label>
                                    <?= config_to_combo('status_pembayaran', 'status_pembayaran',$status, '')?>
								</div>
								<?php echo anchor('laporan/simpanan_wajib/report/?q='.$q,'<i class="fa fa-print"></i>',array('class'=>'btn btn-social-icon btn-flat  btn-sm', 'id'=>"print"));?>
							</div>
							<div class='col-md-3 pull-right'>
								<div class ='form-group form-group-sm'>
									<label for='pwd'>Nama:</label>
									<div class="input-group">
										<input type="text" class="form-control" name="q" value="<?php echo $q; ?>">
										<span class="input-group-btn">
								<?php
								if ($q <> '')
								{
									?>
									<a href="<?php echo site_url('laporan/simpanan_wajib'); ?>" class="btn btn-default">Reset</a>
									<?php
								}
								?>
											<button class="btn btn-sm btn-flat btn-primary" type="submit">Search</button>
							</span>
									</div>
								</div>
							</div>
						</form>
					</div>                    
					<br>
					<br>
					<div class='table-responsive'>
						<table class="table table-bordered" style="margin-bottom: 10px">
							<tr>
								<th>No</th>
								<th>Anggota</th>
								<th>Tgl Bayar</th>
								<th>Jumlah Bayar</th>
								<th>Status Pembayaran</th>
								<th>Periode</th>
								<th>Tahun</th>
							</tr><?php
							foreach ($simpanan_wajib_data as $simpanan_wajib)
							{
								?>
								<tr>
									<td width="80px"><?php echo ++$start ?></td>
									<td><?php echo $this->Anggota_model->getNamaAnggota($simpanan_wajib->id_anggota) ?></td>
									<td><?php echo ($simpanan_wajib->tgl_bayar)?ymdToDmy($simpanan_wajib->tgl_bayar):null ?></td>
									<td><?php echo angka_indo($simpanan_wajib->jumlah_bayar) ?></td>
									<td><?php echo element($simpanan_wajib->status_pembayaran,config_item('status_pembayaran')) ?></td>
									<td><?php echo element($simpanan_wajib->periode,config_item('periode_bulan')) ?></td>
									<td><?php echo $simpanan_wajib->tahun ?></td>
								</tr>
								<?php
							}
							?>
						</table>
					</div>
					<div class="row">
						<div class="col-md-6">
							<a href="#" class="btn btn-primary btn-flat">Total Record : <?php echo $total_rows ?></a>
						</div>
						<div class="col-md-6 text-right">
							<?php echo $pagination ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<script>
    $(document).ready(function () {
        $('#print').on('click',function (e) {
            e.preventDefault();
            printLap($(this).attr('href') +'?'+ $('#form_search').serialize(),'Laporan Laba Rugi');
        });

        $('#excel').on('click',function (e) {
            e.preventDefault();
            printLap($(this).attr('href') +'?'+ $('#form_search').serialize(),'Laporan Laba Rugi');
        });
        $('#word').on('click',function (e) {
            e.preventDefault();
            printLap($(this).attr('href') +'?'+ $('#form_search').serialize(),'Laporan Laba Rugi');
        });
        $('#pdf').on('click',function (e) {
            e.preventDefault();
            printLap($(this).attr('href') +'?'+ $('#form_search').serialize(),'Laporan Laba Rugi');
        });
    })
</script>

