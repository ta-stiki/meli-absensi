<!doctype html>
<html>
    <head>
        <title>harviacode.com - codeigniter crud generator</title>
        <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>"/>
        <style>
            .word-table {
                border:1px solid black !important; 
                border-collapse: collapse !important;
                width: 100%;
            }
            .word-table tr th, .word-table tr td{
                border:1px solid black !important; 
                padding: 5px 10px;
            }
        </style>
    </head>
    <body>
        <h2>Shu List</h2>
        <table class="word-table" style="margin-bottom: 10px">
            <tr>
                <th>No</th>
		<th>Periode</th>
		<th>Laba Tahunan</th>
		<th>Tanggal Generate</th>
		<th>Generate Oleh</th>
		
            </tr><?php
            foreach ($shu_data as $shu)
            {
                ?>
                <tr>
		      <td><?php echo ++$start ?></td>
		      <td><?php echo $shu->periode ?></td>
		      <td><?php echo angka_indo($shu->laba_tahunan) ?></td>
		      <td><?php echo ymdToDmy($shu->tanggal_generate) ?></td>
		      <td><?php echo $this->Anggota_model->getNamaAnggota($shu->generate_oleh) ?></td>
                </tr>
                <?php
            }
            ?>
        </table>
    </body>
</html>