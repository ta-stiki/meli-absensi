<!doctype html>
<html>
<head>
    <title>Laporan Daftar SHU</title>
    <link rel="shortcut icon" type="image/x-icon" href="<?= base_url('assets/img/new icon png.ico')?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>"/>
    <link rel="stylesheet" type="text/css" media="print" href="<?php echo base_url('assets/css/print.css') ?>"/>
    <style>
        .report-header{
            border-bottom: 4px double #000;
        }
        div.report-header h1,div.report-header h2,div.report-header h3 {
            font-family: "Times New Roman", Georgia, Serif;
            margin: 0 !important;
            text-align: center !important;
        }
        .word-table {
            border:1px solid rgba(25, 25, 25, 0.83) !important;
            border-collapse: collapse !important;
            width: 100%;
        }
        .word-table tr th, .word-table tr td{
            border:1px solid black !important;
            padding: 5px 10px;
        }
        div.wrap {
            width: 100%;
            height:150px;
            position: relative;
        }

        .wrap img {
            position: absolute;
            bottom: 0;
        }

        .wrap img:nth-of-type(1) {
            left: 0;
        }

        .wrap img:nth-of-type(2) {
            right: 0;
        }

    </style>
</head>
<body>
<div id="divPrint" style="width: 100%; margin: auto;">
<!-- Main content -->
    <div style="width:80%;margin: 0 auto 10px; padding-bottom: 10px; id="kop">
        <div class="wrap">
            <img src="<?= base_url('assets/img/logo koperasi.png')?>" width="150"/>
            <img src="<?= base_url('assets/img/logo tiki.png')?>" width="150"/>
        </div>
        <div class="report-header">
            <h2><strong>Laporan SHU</strong></h2>
            <h3>Koperasi Karyawan Sejahtera</h3>
            <h3>STMIK STIKOM Indonesia</h3>
        </div>
    </div>
<section class='content'>
    <div class='row'>
        <div class='col-xs-12'>
            <div class='box'>
                <div class='box-header'>
                    <h3 class='box-title'>Info SHU</h3>
                    <table class="table table-bordered">
                        <tr><td width='200'>Periode</td><td><?php echo $periode; ?></td></tr>
                        <tr><td width='200'>Laba Tahunan</td><td><?php echo angka_indo($laba_tahunan); ?></td></tr>
                        <tr><td width='200'>Tanggal Generate</td><td><?php echo $tanggal_generate; ?></td></tr>
<!--                        <tr><td width='200'>Generate Oleh</td><td>--><?php //echo $generate_oleh; ?><!--</td></tr>-->
                    </table>
                    <br>
                    <h3 class="box-title">Detail SHU</h3>
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th>Anggota</th>
                            <th>SHU Anggota</th>
                            <th>SHU Pengurus</th>
                            <th>SHU Pengawas</th>
                            <th>SHU Peminjaman</th>
                            <th>Sub Total</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $detailShu = $this->Detail_shu_model->get_by_id_shu($id);
                        if($detailShu){
                            $agt = 0;
                            $pgrus = 0;
                            $peminj = 0;
                            $pengawas = 0;
                            foreach($detailShu as $row){
                                ?>
                                <tr>
                                    <td><?php echo $row->nama?></td>
                                    <td class="text-right"><?php echo angka_indo($row->anggota)?></td>
                                    <td class="text-right"><?php echo angka_indo($row->pengurus)?></td>
                                    <td class="text-right"><?php echo angka_indo($row->pengawas)?></td>
                                    <td class="text-right"><?php echo angka_indo($row->peminjam)?></td>
                                    <td class="text-right"><?php echo angka_indo($row->anggota + $row->pengurus + $row->peminjam)?></td>
                                </tr>
                                <?php
                                $agt +=$row->anggota;
                                $pgrus +=$row->pengurus;
                                $pengawas +=$row->pengawas;
                                $peminj +=$row->peminjam;

                            }
                            $total = $agt + $pgrus + $peminj + $pengawas;
                        }
                        $modal = $laba_tahunan - $total;
                        ?>
                        <tr><td colspan="6"><h3>Keterangan</h3></td></tr>
                        <tr>
                            <td colspan="3"></td>
                            <td colspan="2"><strong>Anggota</strong></td>
                            <td class="text-right"><strong><?= angka_indo($agt)?></strong></td>
                        </tr>
                        <tr>
                            <td colspan="3"></td>
                            <td colspan="2"><strong>Pengurus</strong></td>
                            <td class="text-right"><strong><?= angka_indo($pgrus)?></strong></td>
                        </tr>
                        <tr>
                            <td colspan="3"></td>
                            <td colspan="2"><strong>Pengawas</strong></td>
                            <td class="text-right"><strong><?= angka_indo($pengawas)?></strong></td>
                        </tr>
                        <tr>
                            <td colspan="3"></td>
                            <td colspan="2"><strong>Peminjam</strong></td>
                            <td class="text-right"><strong><?= angka_indo($peminj)?></strong></td>
                        </tr>
                        <tr>
                            <td colspan="3"></td>
                            <td colspan="2"><strong>Modal</strong></td>
                            <td class="text-right"><strong><?= angka_indo($modal)?></strong></td>
                        </tr>
                        <tr>
                            <td colspan="3"></td>
                            <td colspan="2"><strong>Total</strong></td>
                            <td class="text-right"><strong><?= angka_indo($total + $modal)?></strong></td>
                        </tr>

                        </tbody>
                    </table>

                    <div>
                        <div class="col-md-4 pull-right">
                            <p class="text-center">Ketua Koperasi</p>
                            <br>
                            <br>
                            <br>

                            <p class="text-center">(__________________________________)</p>
                        </div>
                    </div>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- /.col -->
    </div><!-- /.row -->
</section><!-- /.content -->
</div>
</body>
</html>