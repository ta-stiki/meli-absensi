<?php
/**
 * Created by PhpStorm.
 * User: dodik
 * Date: 09/01/2017
 * Time: 20.54
 */?>
<div id="divPrint" style="width: 100%; margin: auto;">
    <div class="borderL noPrint">
    </div>
    <div class="borderR" id="boredr">
        <div style="width:80%;margin: 0 auto 10px; padding-bottom: 10px; border-bottom: 1px solid black" id="kop">
            <div style="width:100%;text-align:center;font-size:10pt;font-weight:bold;">

                <label>Laporan Data Anggota</label>
                <br>
            </div>
            <br>
            <br>
            <br>
            <table id="tblPrt" class="viewTable" style="width: 1000px; margin: auto;">
				<colgroup>
					<col style="width:10%;" />
					<col style="width:5%;" />
					<col style="width:10%;" />
					<col style="width:33%;" />
					<col style="width:8%;" />
					<col style="width:10%;" />
					<col style="width:5%;" />
					<col style="width:5%;" />
					<col style="width:10%;" />
					<col style="width:5%;" />
				</colgroup>
                <thead>
                    <tr>
                        <td align="left">Nama</td>
                        <td align="left">Tgl Lahir</td>
                        <td align="left">Jenis Kelamin</td>
                        <td align="left">Alamat</td>
                        <td align="left">No Telp</td>
                        <td align="left">Jabatan</td>
                        <td align="left">Tgl Masuk</td>
                        <td align="left">Tgl Berhenti</td>
                        <td align="left">No Identitas</td>
                        <td align="left">Status</td>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    foreach ($anggota_data as $row){
                       ?>
                        <tr>
                            <td><?= $row->nama?></td>
                            <td><?= date('d-M-Y',strtotime($row->tgl_lahir))?></td>
                            <td><?= $row->jenis_kelamin?></td>
                            <td><?= $row->alamat?></td>
                            <td><?= $row->no_telp?></td>
                            <td><?= $row->nama_jabatan?></td>
                            <td><?= date('d-M-Y',strtotime($row->tgl_masuk))?></td>
                            <td><?= ($row->tgl_berhenti)?date('d-M-Y',strtotime($row->tgl_berhenti)):'-'?></td>
                            <td><?= $row->no_identitas?></td>
                            <td><?= element((($row->status)?1:0),$this->config->item('status_aktif'))?></td>
                        </tr>
                        <?php
                    }
                    ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
