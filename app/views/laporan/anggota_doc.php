<!doctype html>
<html>
    <head>
        <title>harviacode.com - codeigniter crud generator</title>
        <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>"/>
        <style>
            .word-table {
                border:1px solid black !important; 
                border-collapse: collapse !important;
                width: 100%;
            }
            .word-table tr th, .word-table tr td{
                border:1px solid black !important; 
                padding: 5px 10px;
            }
        </style>
    </head>
    <body>
        <h2>Anggota List</h2>
        <table class="word-table" style="margin-bottom: 10px">
            <tr>
                <th>No</th>
		<th>Nama</th>
		<th>Tgl Lahir</th>
		<th>Jenis Kelamin</th>
		<th>Alamat</th>
		<th>No Telp</th>
		<th>Id Jabatan</th>
		<th>Tgl Masuk</th>
		<th>Tgl Berhenti</th>
		<th>Tipe Anggota</th>
		<th>No Identitas</th>
		<th>Saldo Simpanan</th>
		<th>Status</th>
		<th>Username</th>
		<th>Password</th>
		
            </tr><?php
            foreach ($anggota_data as $anggota)
            {
                ?>
                <tr>
		      <td><?php echo ++$start ?></td>
		      <td><?php echo $anggota->nama ?></td>
		      <td><?php echo ymdToDmy($anggota->tgl_lahir) ?></td>
		      <td><?php echo $anggota->jenis_kelamin ?></td>
		      <td><?php echo $anggota->alamat ?></td>
		      <td><?php echo $anggota->no_telp ?></td>
		      <td><?php echo $anggota->id_jabatan ?></td>
		      <td><?php echo ymdToDmy($anggota->tgl_masuk) ?></td>
		      <td><?php echo $anggota->tgl_berhenti ?></td>
		      <td><?php echo $anggota->tipe_anggota ?></td>
		      <td><?php echo $anggota->no_identitas ?></td>
		      <td><?php echo angka_indo($anggota->saldo_simpanan) ?></td>
		      <td><?php echo $anggota->status ?></td>
		      <td><?php echo $anggota->username ?></td>
		      <td><?php echo $anggota->password ?></td>	
                </tr>
                <?php
            }
            ?>
        </table>
    </body>
</html>