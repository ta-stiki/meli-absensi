
<!-- Main content -->
<section class='content'>
    <div class='row'>
        <div class='col-xs-12'>
            <div class='box'>
                <div class='box-header'>
                    <h3 class='box-title'>Profile</h3>
                </div>
                <form method="post"><table class='table table-bordered'>
                <div class='box-body'>
                    <div class='row'>
                        <div class='col-xs-12'>
                            <table class="table table-bordered">
                                <tr><td width='200'>Nama User</td><td><input type="text" class="form-control" name="nama_user" id="nama_user" placeholder="Nama User" value="<?php echo $nama_user; ?>" />
                                        <?php echo form_error('nama_user') ?></td></tr>
                                <tr><td width='200'>Username</td><td><?php echo $username; ?></td></tr>
                                <tr><td width='200'>Password</td><td><input type="password" class="form-control" name="password" id="password" placeholder="Password" value="<?php echo $password; ?>" />
                                        <?php echo form_error('password') ?></td></tr>
                                <tr><td width='200'>Email</td><td><input type="text" class="form-control" name="email" id="email" placeholder="Email" value="<?php echo $email; ?>" />
                                        <?php echo form_error('email') ?></td></tr>
                                <tr><td width='200'>Akses</td><td><?php echo $akses; ?></td></tr>
                            </table>
                            <br>
                        </div>
                    </div>
                </div><!-- /.box-body -->
                <div class='box-footer'>
                    <button type="submit" class="btn btn-primary">Update</button>
                    <a href="<?php echo site_url('/') ?>" class="btn btn-default">Cancel</a>
                    <input type="hidden" name="id" value="<?php echo $id; ?>" />
                </div>
                </form>
            </div><!-- /.box -->
        </div><!-- /.col -->
    </div><!-- /.row -->
</section><!-- /.content -->