
<?php
$options = [
    0 => 'Sample1',
    1 => 'Sample2',
];
?>
<!-- Main content -->
<section class='content'>
    <div class='row'>
        <div class='col-xs-12'>
            <div class='box box-primary'>
                <div class='box-header'>
                    <h3 class='box-title'>TBL_USER</h3>
                </div>
                <div class='box-body'>

                    <form action="<?php echo $action; ?>" method="post"><table class='table table-bordered'>
                            <tr><td>Nama User</td>
                                <td>
                                    <input type="text" class="form-control" name="nama_user" id="nama_user" placeholder="Nama User" value="<?php echo $nama_user; ?>" />
                                    <?php echo form_error('nama_user') ?>
                                </td>
                            <tr><td>Email</td>
                                <td>
                                    <input type="text" class="form-control" name="email" id="email" placeholder="Email" value="<?php echo $email; ?>" />
                                    <?php echo form_error('email') ?>
                                </td>
                            <tr><td>Username</td>
                                <td>
                                    <input type="text" class="form-control" name="username" id="username" placeholder="Username" value="<?php echo $username; ?>" />
                                    <?php echo form_error('username') ?>
                                </td>
                            <tr><td>Password</td>
                                <td>
                                    <input type="password" class="form-control" name="password" id="password" placeholder="Password" value="<?php echo $password; ?>" />
                                    <?php echo form_error('password') ?>
                                </td>
                            <tr><td>Akses</td>
                                <td>
                                    <?= config_to_combo('akses','akses_user',$akses,'form-control select2')?>
                                    <?php echo form_error('akses') ?>
                                </td>
                                <input type="hidden" name="id" value="<?php echo $id; ?>" />
                            <tr><td colspan='2'><button type="submit" class="btn btn-primary"><?php echo $button ?></button>
                                    <a href="<?php echo site_url('tbl_user') ?>" class="btn btn-default">Cancel</a></td></tr>

                        </table>
                    </form>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- /.col -->
    </div><!-- /.row -->
</section><!-- /.content -->