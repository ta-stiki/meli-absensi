
<section class='content'>
  <div class='row'>
    <div class='col-xs-12'>
      <div class='box box-primary'>
        <div class='box-header'>
          <h3 class='box-title'>Daftar User</h3>
        </div>
        <div class='box-body'>
		<div class='row'>
		    <div class='col-md-6'>
                <p><?php echo anchor('user/create/','Tambah User',array('class'=>'btn btn-success btn-flat btn-sm'));?></p>
            </div>
            <form action="<?php echo site_url('user/index'); ?>" class="form-inline" method="get">
				<div class='col-md-6'>
					<div class ='form-group form-group-sm pull-right'>
						<div class="input-group">						
							<input type="text" class="form-control" name="q" value="<?php echo $q; ?>">
							<span class="input-group-btn">
								<?php 
									if ($q <> '')
									{
										?>
										<a href="<?php echo site_url('user'); ?>" class="btn btn-sm btn-flat btn-default">Reset</a>
										<?php
									}
								?>
							  <button class="btn btn-sm btn-flat btn-primary" type="submit">Cari</button>
							</span>
						</div>						
					</div>
				</div>				
            </form>            			
		</div>
		<br>
		<br>
        <div class='table-responsive'>
        <table class="table table-bordered" style="margin-bottom: 10px">
            <tr>
                <th>No</th>
		<th>Nama User</th>
		<th>Email</th>
		<th>Akses</th>
		<th>Action</th>
            </tr><?php
            foreach ($user_data as $user)
            {
                ?>
                <tr>
			<td width="80px"><?php echo ++$start ?></td>
			<td><?php echo $user->nama_user ?></td>
			<td><?php echo $user->email ?></td>
			<td><?php echo $user->akses ?></td>
		    <td style="text-align:center" width="140px">
			<?php
			echo anchor(site_url('user/update/'.$user->id),'<i class="fa fa-pencil-square-o"></i>',array('title'=>'Ubah','class'=>'btn btn-info btn-xs'));
			?>
		    </td>
		</tr>
                <?php
            }
            ?>
        </table>
		</div>
		<div class="row">
            <div class="col-md-6">
                <a href="#" class="btn btn-primary btn-flat">Total Record : <?php echo $total_rows ?></a>
			</div>
            <div class="col-md-6 text-right">
                <?php echo $pagination ?>
            </div>
        </div>
        </div>
      </div>
    </div>
  </div>
</section>    

