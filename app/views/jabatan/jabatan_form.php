
<?php
$options = [
    0 => 'Sample1',
    1 => 'Sample2',
];
?>
<!-- Main content -->
<section class='content'>
    <div class='row'>
        <div class='col-xs-12'>
            <div class='box box-primary'>
                <div class='box-header'>
                    <h3 class='box-title'><?= ($id_jabatan)? 'Update': 'Tambah'?> Jabatan</h3>
                </div>
                <div class='box-body'>

                    <form action="<?php echo $action; ?>" method="post"><table class='table table-bordered'>
                            <tr><td>Nama Jabatan</td>
                                <td>
                                    <input type="text" class="form-control" name="nama_jabatan" id="nama_jabatan" placeholder="Nama Jabatan" value="<?php echo $nama_jabatan; ?>" />
                                    <?php echo form_error('nama_jabatan') ?>
                                </td>
                                <input type="hidden" name="id_jabatan" value="<?php echo $id_jabatan; ?>" />
                            <tr><td colspan='2'><button type="submit" class="btn btn-primary"><?php echo $button ?></button>
                                    <a href="<?php echo site_url('jabatan') ?>" class="btn btn-default">Cancel</a></td></tr>

                        </table>
                    </form>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- /.col -->
    </div><!-- /.row -->
</section><!-- /.content -->