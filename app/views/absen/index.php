<?php
/**
 * Created by PhpStorm.
 * User: AcenetDev
 * Date: 12/1/2017
 * Time: 1:46 AM
 */
?>
<style>
    .table > thead > tr > th {
        vertical-align: middle;
        text-align: center;
    }
</style>
<section class='content'>
    <div class='row'>
        <div class='col-xs-12'>
            <div class='box box-primary'>
                <div class='box-header'>
                    <h3 class='box-title'>Absensi</h3>
                </div>
                <div class='box-body'>
                    <div class="row">
                        <div class="col-md-6">
                            <form action="" method="post">
                                <table class='table table-bordered'>
                                    <tr>
                                        <td>Nama Karyawan</td>
                                        <td colspan="2">
                                            <?= cmb_select2('nama_karyawan','karyawan','nama_karyawan','id_karyawan',$nama_karyawan)?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Periode</td>
                                        <td>
                                            <?= config_to_combo('periode_bulan','periode_bulan',$periode_bulan,null,false)?>
                                        </td>
                                        <td>
                                            <?= arr_to_combo('periode_tahun',function (){
                                                $data =[];
                                                for ($i = date('Y') - 3; $i <= date('Y');$i++){
                                                    $data[$i] = $i;
                                                }
                                                return $data;
                                            },$periode_tahun)?>
                                        </td>

                                    </tr>
                                    <tr>
                                        <td colspan='2'>
                                            <button type="submit" class="btn btn-primary">Filter</button>
                                        </td>
                                        <td>
                                            <div class="pull-right">
                                                <a href="<?= site_url("absen/upload")?>" class="btn btn-flat btn-success"><i class="fa fa-upload"></i> Import</a>
                                                <?php
                                                if($rowData){
                                                    ?>
                                                    <a target="_blank" href="<?= site_url("absen/export_excel?periode_bulan=$periode_bulan&periode_tahun=$periode_tahun&nama_karyawan=$nama_karyawan")?>" id="export-button" class="btn btn-flat"><i class="fa fa-print"></i> Export Excel</a>
                                                    <?php
                                                }
                                                ?>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </form>
                        </div>
                        <div class="col-md-6">
                            <dl class="dl-horizontal">
                                <?php
                                $getJenisLibur = $this->db->query("SELECT * FROM status_keterangan");
                                foreach ($getJenisLibur->result() as $row)
                                {
                                    ?>
                                    <dt><?= $row->kode?></dt>
                                    <dd><?= $row->keterangan?></dd>
                                    <?php
                                }

                                ?>
                            </dl>
                        </div>
                        <div class="col-md-6">

                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <p>
                                <?php
                                $getJenisLibur = $this->db->query("SELECT * FROM jenis_libur");
                                foreach ($getJenisLibur->result() as $row)
                                {
                                    ?>
                                    <button type="button" style="background-color:<?=$row->warna?>;color: <?=$row->text_color?>" class="btn btn-sm btn-flat"><?=$row->jenis_libur?></button>
                                    <?php
                                }

                                ?>
                            </p>

                        </div>
                    </div>
                    <div id="report-absen" class='table-responsive'>
                        <table id="data-absesn" class="table table-bordered table-condensed" style="margin-bottom: 10px">
                            <thead>
                            <tr>
                                <th rowspan="2">No</th>
                                <th rowspan="2">NIK</th>
                                <th rowspan="2">Nama Dosen</th>
                                <th rowspan="2">Jabatan</th>
                                <th colspan="<?= $numberDays?>"><?= $bulan . ' ' . $periode_tahun?></th>
                                <th rowspan="2">Sakit</th>
                                <th rowspan="2">Cuti</th>
                                <th rowspan="2">Absen</th>
                                <th rowspan="2">Total Kehadiran</th>
                            </tr>
                            <tr>
                                <?= $theadDate?>
                            </tr>
                            </thead>
                            <tbody>
                            <?= $rowData?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
