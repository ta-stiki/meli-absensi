<?php
/**
 * Created by PhpStorm.
 * User: AcenetDev
 * Date: 12/1/2017
 * Time: 1:46 AM
 */
?>
<section class='content'>
    <div class='row'>
        <div class='col-xs-12'>
            <div class='box box-primary'>
                <div class='box-header'>
                    <h3 class='box-title'>Imort absensi</h3>
                </div>
                <div class='box-body'>
                    <form id="form-upload" method="post" role="form" action="<?php echo base_url('absen/proses_upload/');?>" enctype="multipart/form-data">
                        <div class="box-body">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Mode Import</label>
                                <?= arr_to_combo('mode_import',[
//                                    'Harian' => 'Harian',
                                    'Bulanan' =>'Bulanan',
//                                    'Tahunan' => 'Tahunan'
                                ],'Bulanan')?>
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">Periode</label>
                                <div class="row">
                                    <div class="col-md-6">
                                        <?= config_to_combo('periode_bulan','periode_bulan',date('m'))?>
                                    </div>
                                    <div class="col-md-6">
                                        <?= arr_to_combo('periode_tahun',function (){
                                            $data =[];
                                            for ($i = date('Y') - 3; $i <= date('Y');$i++){
                                                $data[$i] = $i;
                                            }
                                            return $data;
                                        },date('Y'))?>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="exampleInputFile">Upload File</label>
                                <input type="file" name="file" id="exampleInputFile" required>

                                <p class="help-block">Upload absensi dari fingerprint dengan format excel.</p>
                            </div>
                        </div>
                        <!-- /.box-body -->

                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary">Upload</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>

<br>
<div class="progress" style="display:none">
    <div id="progressBar" class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%">
        <span class="sr-only">0%</span>
    </div>
</div>
<div class="msg alert alert-info text-left" style="display:none"></div>

<script>
    $(document).ready(function () {
        $('#form-upload').on('submit',function (event) {
            event.preventDefault();

            var formData = new FormData($('#form-upload')[0]);
            $('.msg').hide();
            $('.progress').show();
            var progressNum = 0
            $.ajax({
                xhr: function () {
                    var xhr = new window.XMLHttpRequest();
                    xhr.upload.addEventListener('progress', function(e){
                        if(e.lengthComputable){
                            console.log('Bytes Loaded : ' + e.loaded);
                            console.log('Total Size : ' + e.total);
                            console.log('Persen : ' + (e.loaded / e.total));

                            var percent = Math.round((e.loaded / e.total) * 100);
                            progressNum = percent;
                            $('#progressBar').attr('aria-valuenow', percent).css('width', percent + '%').text(percent + '%');
                            $('.msg').html('Proses validasi data...');
                            $('.msg').slideDown();

                        }
                    });
                    return xhr;
                },
                type : 'POST',
                url : '<?php echo base_url('absen/proses_upload/');?>',
                data : formData,
                processData : false,
                contentType : false,
                success: function (response) {
                    var resData = JSON.parse(response);
                    $('#form-upload')[0].reset();
                    $('.progress').hide();
                    $('.msg').show();
                    if(resData.status === "Gagal"){
                        alert(resData.message);
                    }else{
                        var msg = 'File berhasil di upload.';
                        $('.msg').html(msg);
                        $('tbody','#hasil-upload').html(resData.message);
//                        $('#hasil-upload').DataTable({
//                            'paging'      : true,
//                            'lengthChange': false,
//                            'searching'   : false,
//                            'ordering'    : true,
//                            'info'        : true,
//                            'autoWidth'   : false
//                        })


                    }
                }
            });
        })
    });
</script>

<section class='content'>
    <div class='row'>
        <div class='col-xs-12'>
            <div class='box box-primary'>
                <div class='box-header'>
                    <h3 class='box-title'>Hasil Import</h3>
                </div>
                <div class='box-body'>
                    <div class='table-responsive'>
                        <table id="hasil-upload" class="table table-bordered" style="margin-bottom: 10px">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>PIN</th>
                                    <th>NIK</th>
                                    <th>Nama Karyawan</th>
                                    <th>Tanggal</th>
                                    <th>Jam</th>
                                    <th>SN Mesin</th>
                                    <th>Nama Mesin</th>
                                    <th>Verifikasi</th>
                                    <th>Mode</th>
                                    <th>Mode Update</th>
                                    <th>Cabang</th>
                                    <th>Jabatan</th>
                                    <th>Upload status</th>
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

