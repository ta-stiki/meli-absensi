<?php
//$user = $this->ion_auth->user()->row();
$user = null;
$login_user = $this->session->userdata('nama','Guest');;
//if($user){
//    $login_user = $user->first_name . ' ' . $user->last_name;
//}
$is_newRecord = isset($is_newRecord)? $is_newRecord : false;
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Rekapitulasi Absensi Karyawan</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="<?php echo base_url() ?>template/ionicons-2.0.1/css/ionicons.min.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>template/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <!--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">-->
    <link rel="stylesheet" href="<?php echo base_url() ?>template/font-awesome-4.4.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>template/font-awesome-4.4.0/css/fa-animation.css">
    <!-- Ionicons -->
    <!--<link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">-->
    <!-- DataTables -->
    <link rel="stylesheet" href="<?php echo base_url() ?>template/plugins/datatables/dataTables.bootstrap.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>template/plugins/datepicker/datepicker3.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>template/plugins/select2/select2.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?php echo base_url() ?>template/dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="<?php echo base_url() ?>template/dist/css/skins/_all-skins.min.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>template/dist/css/custom.css">
    <link rel="stylesheet" href="<?php echo base_url('template/plugins/jQueryUI/custom-theme/jquery-ui-1.10.0.custom.css') ?>">
    <script src="<?php echo base_url() ?>template/plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <script src="<?php echo base_url('template/plugins/jQueryUI/jquery-ui.min.js') ?>"></script>
    <link rel="apple-touch-icon" sizes="57x57" href="<?= base_url('assets/img/pavicon/apple-icon-57x57.png')?>">
    <link rel="apple-touch-icon" sizes="60x60" href="<?= base_url('assets/img/pavicon/apple-icon-60x60.png')?>">
    <link rel="apple-touch-icon" sizes="72x72" href="<?= base_url('assets/img/pavicon/apple-icon-72x72.png')?>">
    <link rel="apple-touch-icon" sizes="76x76" href="<?= base_url('assets/img/pavicon/apple-icon-76x76.png')?>">
    <link rel="apple-touch-icon" sizes="114x114" href="<?= base_url('assets/img/pavicon/apple-icon-114x114.png')?>">
    <link rel="apple-touch-icon" sizes="120x120" href="<?= base_url('assets/img/pavicon/apple-icon-120x120.png')?>">
    <link rel="apple-touch-icon" sizes="144x144" href="<?= base_url('assets/img/pavicon/apple-icon-144x144.png')?>">
    <link rel="apple-touch-icon" sizes="152x152" href="<?= base_url('assets/img/pavicon/apple-icon-152x152.png')?>">
    <link rel="apple-touch-icon" sizes="180x180" href="<?= base_url('assets/img/pavicon/apple-icon-180x180.png')?>">
    <link rel="icon" type="image/png" sizes="192x192"  href="<?= base_url('assets/img/pavicon/android-icon-192x192.png')?>">
    <link rel="icon" type="image/png" sizes="32x32" href="<?= base_url('assets/img/pavicon/favicon-32x32.png')?>">
    <link rel="icon" type="image/png" sizes="96x96" href="<?= base_url('assets/img/pavicon/favicon-96x96.png')?>">
    <link rel="icon" type="image/png" sizes="16x16" href="<?= base_url('assets/img/pavicon/favicon-16x16.png')?>">
    <link rel="manifest" href="<?= base_url('assets/img/pavicon/manifest.json')?>">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="<?= base_url('assets/img/pavicon/ms-icon-144x144.png')?>">
    <meta name="theme-color" content="#ffffff">
    <style>
        .main-header>.navbar {
            margin-left: auto !important;
        }
        .navbar>.header-name {
            padding:15px;
            float: left;
            color: #ffffff;
        }
    </style>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script>
        var is_newRecord = <?= ($is_newRecord)? '1':'0'?>;
        var base_url = '<?= base_url()?>';
        var base_page = '<?php echo (isset($page))?$page:'' ?>';

    </script>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
    <header class="main-header">

        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top" role="navigation">

            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">
                    <li class="dropdown user user-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                            <span class="hidden-xs"><?= $login_user?></span>
                        </a>
                        <ul class="dropdown-menu">
                            <!-- User image -->
                            <li class="user-header">
                                <p>
                                    <?= $login_user?>
                                </p>
                            </li>
                            <!-- Menu Footer-->
                            <li class="user-footer">
                                <div class="pull-left">
                                    <a href="<?= base_url('/user/profile')?>" class="btn btn-default btn-flat">Profile</a>
                                </div>
                                <div class="pull-right">
                                    <?php
                                    echo anchor('auth/logout','Sign out',array('class'=>'btn btn-default btn-flat'));
                                    ?>
                                </div>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>

            <!-- Sidebar toggle button-->
            <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                <span class="sr-only">Toggle navigation</span>
            </a>
            <b class="header-name">Sistem Rekapitulasi Absensi STIKI Indonesia</b>
        </nav>
    </header>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
            <!-- sidebar menu: : style can be found in sidebar.less -->
            <ul class="sidebar-menu">
                <li class="header">Main Menu</li>
                <li>
                    <a href="<?= base_url('/home')?>">
                        <i class="fa fa-home"></i> <span>Beranda</span>
                    </a>
                </li>
                <?php getMenu()?>
            </ul>
        </section>
        <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <!--<h1><?= (isset($c_header))? $c_header: 'Data Tables'?><small><?= (isset($c_sub_header))? $c_sub_header: 'advanced tables'?></small></h1>-->
            <br>
            <?= ci_breadcrumb('Home',base_url(),'fa fa-home')?>
        </section>


        <?php echo $contents;?>



    </div><!-- /.content-wrapper -->
    <footer class="main-footer">
        <div class="pull-right hidden-xs">
            <b>Version</b> 1.0
        </div>
        <strong>Copyright <a href="http://ruslidevata.com">STIKI Indonesia</a> @ <?= date('Y')?> | Meilita Yani</strong> .
    </footer>

    <!-- Add the sidebar's background. This div must be placed
         immediately after the control sidebar -->
    <div class="control-sidebar-bg"></div>
</div><!-- ./wrapper -->

<!-- jQuery 2.1.4 -->

<!-- Bootstrap 3.3.5 -->
<script src="<?php echo base_url() ?>template/bootstrap/js/bootstrap.min.js"></script>
<!-- DataTables -->
<script src="<?php echo base_url() ?>template/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url() ?>template/plugins/datatables/dataTables.bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="<?php echo base_url('template/plugins/slimScroll/jquery.slimscroll.min.js') ?>"></script>
<!-- FastClick -->
<script src="<?php echo base_url() ?>template/plugins/fastclick/fastclick.min.js"></script>
<script src="<?php echo base_url() ?>template/plugins/datepicker/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url() ?>template/plugins/select2/select2.full.min.js"></script>
<!--        <script src="--><?php //echo base_url() ?><!--template/plugins/input-mask/jquery.inputmask.js"></script>-->
<!--        <script src="--><?php //echo base_url() ?><!--template/plugins/input-mask/jquery.inputmask.numeric.extensions.js"></script>-->
<!-- AdminLTE App -->
<script src="<?php echo base_url('template/plugins/jquery-number/jquery.number.min.js') ?>"></script>
<script src="<?php echo base_url() ?>template/dist/js/app.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url() ?>template/dist/js/demo.js"></script>
<script src="<?php echo base_url() ?>assets/js/glob.js"></script>
<!-- page script -->
</body>
</html>
