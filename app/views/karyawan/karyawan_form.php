
<?php
$options = [
    0 => 'Sample1',
    1 => 'Sample2',
];
?>
<!-- Main content -->
<section class='content'>
    <div class='row'>
        <div class='col-xs-12'>
            <div class='box box-primary'>
                <div class='box-header'>
                    <h3 class='box-title'>KARYAWAN</h3>
                </div>
                <div class='box-body'>

                    <form action="<?php echo $action; ?>" method="post">
                        <table class='table table-bordered'>
                            <?php
                            if($id_karyawan){
                                ?>
                                <input type="hidden" name="id_karyawan" value="<?php echo $id_karyawan; ?>" />
                                <?php
                            }else{
                                ?>
                                <tr>
                                    <td>NIK Karyawan</td>
                                    <td>
                                        <input type="text" class="form-control" name="id_karyawan" id="id_karyawan" placeholder="NIK Karyawan" value="<?php echo $id_karyawan; ?>" />
                                        <?php echo form_error('id_karyawan') ?>
                                    </td>
                                </tr>
                                <?php
                            }
                            ?>
                            <tr>
                                <td>Nama Karyawan</td>
                                <td>
                                    <input type="text" class="form-control" name="nama_karyawan" id="nama_karyawan" placeholder="Nama Karyawan" value="<?php echo $nama_karyawan; ?>" />
                                    <?php echo form_error('nama_karyawan') ?>
                                </td>
                            </tr>
                            <tr><td>Jenis Kelamin</td>
                                <td>
                                    <?= config_to_combo('jenis_kelamin','jenis_kelamin',$jenis_kelamin,'form-control select2')?>
                                    <?php echo form_error('jenis_kelamin') ?>
                                </td>
                            <tr><td>Alamat</td>
                                <td>
                                    <textarea class="form-control" rows="3" name="alamat" id="alamat" placeholder="Alamat"><?php echo $alamat; ?></textarea>
                                    <?php echo form_error('alamat') ?>
                                </td></tr>
                            <tr><td>Jabatan</td>
                                <td>
                                    <?php
                                    echo cmb_dinamis('id_jabatan','jabatan','nama_jabatan','id_jabatan',$id_jabatan,'');
                                    ?>
                                    <!--<input type="text" class="form-control form_date" name="id_jabatan" id="id_jabatan" placeholder="Id Jabatan" value="<?php //echo $id_jabatan; ?>" />-->
                                    <?php echo form_error('id_jabatan') ?>
                                </td>
                            <tr><td colspan='2'><button type="submit" class="btn btn-primary"><?php echo $button ?></button>
                                    <a href="<?php echo site_url('karyawan') ?>" class="btn btn-default">Cancel</a></td></tr>

                        </table>
                    </form>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- /.col -->
    </div><!-- /.row -->
</section><!-- /.content -->