
<section class='content'>
    <div class='row'>
        <div class='col-xs-12'>
            <div class='box box-primary'>
                <div class='box-header'>
                    <h3 class='box-title'>Daftar Karyawan</h3>
                </div>
                <div class='box-body'>
                    <div class='row'>
                        <div class='col-md-6'>
                            <p><?php echo anchor('karyawan/create/','Tambah Karyawan',array('class'=>'btn btn-success btn-flat btn-sm'));?></p>
                        </div>
                        <form action="<?php echo site_url('karyawan/index'); ?>" class="form-inline" method="get">
                            <div class='col-md-6'>
                                <div class ='form-group form-group-sm pull-right'>
                                    <div class="input-group">
                                        <input type="text" class="form-control" name="q" value="<?php echo $q; ?>">
                                        <span class="input-group-btn">
								<?php
                                if ($q <> '')
                                {
                                    ?>
                                    <a href="<?php echo site_url('karyawan'); ?>" class="btn btn-sm btn-flat btn-default">Reset</a>
                                    <?php
                                }
                                ?>
                                            <button class="btn btn-sm btn-flat btn-primary" type="submit">Cari</button>
							</span>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <br>
                    <br>
                    <div class='table-responsive'>
                        <table class="table table-bordered table-condensed" style="margin-bottom: 10px">
                            <thead>
                            <tr>
                                <th>No</th>
                                <th>Nama Karyawan</th>
                                <th>Jabatan</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            foreach ($karyawan_data as $karyawan)
                            {
//                                $statusColor = ($karyawan->status)? '': 'danger';
                                ?>
                                <tr>
                                    <td width="80px"><?php echo ++$start ?></td>
                                    <td><?php echo $karyawan->nama_karyawan ?></td>
                                    <td><?php echo getJabatan($karyawan->id_jabatan) ?></td>
                                    <td style="text-align:center" width="140px">
                                        <?php
                                        echo anchor(site_url('karyawan/update/'.$karyawan->id_karyawan),'<i class="fa fa-pencil-square-o"></i>',array('title'=>'edit','class'=>'btn btn-info btn-xs'));
                                        //                                        echo '  ';
                                        //                                        echo anchor(site_url('karyawan/enable/'.$karyawan->id_karyawan),'<i class="fa fa-ban"></i>','title="Nonaktifkan" class="btn btn-danger btn-xs" onclick="javasciprt: return confirm(\'Yakin akan dinonaktifkan ?\')"');
                                        //                                        echo anchor(site_url('karyawan/disable/'.$karyawan->id_karyawan),'<i class="fa fa-check-circle-o"></i>','title="Aktifkan" class="btn btn-danger btn-xs" onclick="javasciprt: return confirm(\'Yakin akan diaktifkan ?\')"');
                                        ?>
                                    </td>
                                </tr>
                                <?php
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <a href="#" class="btn btn-primary btn-flat">Total Record : <?php echo $total_rows ?></a>
                        </div>
                        <div class="col-md-6 text-right">
                            <?php echo $pagination ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

