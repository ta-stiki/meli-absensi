<?php
/**
 * Created by PhpStorm.
 * User: balisoket Design
 * Date: 1/18/2017
 * Time: 3:26 PM
 */

function upload_profile($fileinput,$path){
    $ci = &get_instance();

    $config['upload_path']          = $path;
    $config['allowed_types']        = 'gif|jpg|png';
    $config['max_size']             = 100;
    $config['max_width']            = 1024;
    $config['max_height']           = 768;
    $ci->load->library('upload', $config);

    if(!is_dir($config['upload_path']))
    {
        mkdir($config['upload_path'], 0755, TRUE);
    }

    if ( ! $ci->upload->do_upload($fileinput))
    {
        $error = array('error' => $ci->upload->display_errors());
        $data = [
           'status' =>false,
            'error' =>$error
        ];
    }
    else
    {
        $image_data = array('upload_data' => $ci->upload->data());

        $source_img = $image_data['full_path']; //Defining the Source Image
        $new_img = $image_data['file_path'] . $image_data['raw_name'].'_thumb'.$image_data['file_ext']; //Defining the Destination/New Image
        create_thumb_gallery($image_data, $source_img, $new_img, 250, 250);
        $data = [
            'status' =>true,
            'image_data' =>$image_data,
        ];
        $data['source_image'] = $new_img;

    }

    return $data;
}

function create_thumb_gallery($upload_data, $source_img, $new_img, $width, $height)
{
    $ci = &get_instance();
//Copy Image Configuration
    $config['image_library'] = 'gd2';
    $config['source_image'] = $source_img;
    $config['create_thumb'] = FALSE;
    $config['new_image'] = $new_img;
    $config['quality'] = '100%';

    $ci->load->library('image_lib');
    $ci->image_lib->initialize($config);

    if ( ! $ci->image_lib->resize() )
    {
        echo $ci->image_lib->display_errors();
    }
    else
    {
//Images Copied
//Image Resizing Starts
        $config['image_library'] = 'gd2';
        $config['source_image'] = $source_img;
        $config['create_thumb'] = FALSE;
        $config['maintain_ratio'] = TRUE;
        $config['quality'] = '100%';
        $config['new_image'] = $source_img;
        $config['overwrite'] = TRUE;
        $config['width'] = $width;
        $config['height'] = $height;
        $dim = (intval($upload_data['image_width']) / intval($upload_data['image_height'])) - ($config['width'] / $config['height']);
        $config['master_dim'] = ($dim > 0)? 'height' : 'width';

        $ci->image_lib->clear();
        $ci->image_lib->initialize($config);

        if ( ! $ci->image_lib->resize())
        {
            echo $ci->image_lib->display_errors();
        }
        else
        {
//echo 'Thumnail Created';
            return true;
        }
    }
}