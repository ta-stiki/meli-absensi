<?php
/**
 * Created by PhpStorm.
 * User: dodik
 * Date: 15/01/2017
 * Time: 01.23
 */

function dx_notify(){
    $ci = &get_instance();
    if($ci->akses->in_access(['Bendahara'])){
        $linkTpeminjaman = site_url('admin/peminjaman');
        $linkTSimpanan = site_url('admin/simpanan_wajib');
    }else{
        $linkTpeminjaman = site_url('anggota/profile/riwayat#settings');
        $linkTSimpanan = site_url('anggota/profile/riwayat#timeline');
    }

    ?>
    <li class="dropdown notifications-menu">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            <i class="fa fa-bell-o"></i>
            <span id="dx_count" class="label label-warning">10</span>
        </a>
        <ul class="dropdown-menu">
            <li id="dx_info" class="header">Anda Memiliki 10 Pesan</li>
            <li>
                <!-- inner menu: contains the actual data -->
                <ul class="menu">
                    <li id="dx_t_pinjaman">
                        <a href="#">
                            <i class="fa fa-users text-aqua"></i> 5 new members joined today
                        </a>
                    </li>
                    <li id="dx_t_iuran_w">
                        <a href="#">
                            <i class="fa fa-warning text-yellow"></i> Very long description here that may not fit into the
                            page and may cause design problems
                        </a>
                    </li>
                </ul>
            </li>
            <li class="footer"><a href="#">Daftar Tunggakan</a></li>
        </ul>
    </li>
    <script>

        if (!!window.EventSource) {
            var source = new EventSource(base_url+"/Sse/notify?page="+base_page);
            var dx_count = $('#dx_count');
            var dx_info = $('#dx_info');
            var dx_t_pinjaman = $('#dx_t_pinjaman');
            var dx_t_iuran_w = $('#dx_t_iuran_w');

            source.addEventListener("message", function(e) {
                var msg = $.parseJSON(e.data);
                //{"simpanan_wajib":"106","peminjaman":"22"}
                if (typeof msg.simpanan_wajib !== 'undefined') {
                    var count_tunggakan = parseInt(msg.simpanan_wajib) + parseInt(msg.peminjaman);
                    dx_count.html(count_tunggakan);
                    dx_info.html("Terdapat " + count_tunggakan + " Tunggakan Pembayaran");
                    var peminjaman_txt = "<a href='<?= $linkTpeminjaman?>'><i class='fa fa-warning text-yellow fa-ring animated'></i>"+ parseInt(msg.peminjaman) +"  Tunggakan Peminjaman </a>";

                    dx_t_pinjaman.html(peminjaman_txt);
                    var iuran_txt = "<a href='<?= $linkTSimpanan?>'><i class='fa fa-warning text-yellow'></i> " + parseInt(msg.simpanan_wajib) + " Tunggakan simpanan wajib</a>";
                    dx_t_iuran_w.html(iuran_txt);
                }

            }, false);
            // source.addEventListener("open", function(e) {
            //     console.log("Connection was opened.");
            // }, false);
            // source.addEventListener("error", function(e) {
            //     console.log("Error - connection was lost.");
            // }, false);
        } else {
            alert("Your browser does not support Server-sent events! Please upgrade it!");
        }
    </script>
    <?php
}

function dx_header_title($title){

}