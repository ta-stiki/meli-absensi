<?php
/**
 * Created by PhpStorm.
 * User: dodik
 * Date: 09/01/2017
 * Time: 18.42
 */

if ( !function_exists( 'get_username' ) ){

    function get_username(){
        $ci = &get_instance();
        $ci->session->userdata('user_name');
    }
}



if ( !function_exists( 'get_daftar_akses' ) ){

    function get_daftar_akses($user_id= null,$sparator = null){
        $ci = &get_instance();
        ($user_id)? $user_id:$user_id = $ci->session->userdata('user_id');
        if($user_id){
            $query = $ci->db->query("SELECT
    hak_akses_anggota.id_anggota,
    hak_akses.hak_akses,
    hak_akses_anggota.id_hak_akses
    FROM
    hak_akses_anggota
    INNER JOIN hak_akses ON hak_akses_anggota.id_hak_akses = hak_akses.id
    WHERE hak_akses_anggota.id_anggota = $user_id");
            if($query){
                $count = $query->num_rows();
                if($count >1){
                    $data = [];
                    foreach ($query->result() as $row){
                        $data[] = $row->hak_akses;
                    }
                    ($sparator)?$sparator: $sparator=' | ';
                    $result = implode($sparator, $data);
                }else{
                    $row = $query->row();
                    $result = $row->hak_akses;
                }
            }
        }else{
            $result = 'Guest';
        }
        return $result;
    }

}