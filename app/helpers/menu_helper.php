<?php
/**
 * Created by PhpStorm.
 * User: balisoket Design
 * Date: 1/18/2017
 * Time: 8:09 PM
 */

function getMenu(){

    $ci =&get_instance();
    $akses = $ci->session->userdata('akses','user');
/*
    if($ci->akses->in_access(['Anggota','Ketua','Pengawas','Bendahara','Wakil'])){
        ?>
        <li class="header">Anggota</li>
        <li class=""> <?= anchor(base_url('anggota/profile'),'<i class="fa fa-user-md"></i><span>Profile</span>')?></li>
        <li class=""> <?= anchor(base_url('anggota/profile/riwayat'),'<i class="fa fa-user-md"></i><span>Riwayat</span>')?></li>
        <?php
    }
    if($ci->akses->in_access(['Bendahara'])){
        $menu = $ci->db->get_where('menu', array('is_parent' => 0,'is_active'=>1));
        echo "<li class=\"header\">Transaksi</li>";
        foreach ($menu->result() as $m) {
            // chek ada sub menu
            $submenu = $ci->db->get_where('menu',array('is_parent'=>$m->id,'is_active'=>1));
            if($submenu->num_rows()>0){
                // tampilkan submenu
                echo "<li class='treeview'>
                                    ".anchor('#',  "<i class='$m->icon'></i>".$m->name.' <i class="fa fa-angle-left pull-right"></i>')."
                                        <ul class='treeview-menu'>";
                foreach ($submenu->result() as $s){
                    echo "<li>" . anchor($s->link, "<i class='$s->icon'></i> <span>" . $s->name) . "</span></li>";
                }
                echo"</ul>
                                    </li>";
            }else{
                echo "<li>" . anchor($m->link, "<i class='$m->icon'></i> <span>" . $m->name) . "</span></li>";
            }

        }
    }

    if($ci->akses->in_access(['Ketua','Bendahara','Wakil','Pengawas'])) {
        ?>
        <li class="header">Laporan</li>
        <li class="treeview">
            <a href="#"><i class="fa fa-file-text"></i>Laporan <i class="fa fa-angle-left pull-right"></i></a>
            <ul class="treeview-menu" style="display: none;">
                <li><a href="<?= base_url('laporan/anggota') ?>"><i class="fa fa-file-text"></i> <span>Laporan Data Anggota</span></a>
                </li>
                <li><a href="<?= base_url('laporan/peminjaman') ?>"><i class="fa fa-file-text"></i> <span>Laporan Data Peminjaman</span></a>
                </li>
                <li><a href="<?= base_url('laporan/pembayaran') ?>"><i class="fa fa-file-text"></i> <span>Laporan Data Pembayaran</span></a>
                </li>
                <li><a href="<?= base_url('laporan/simpanan_wajib') ?>"><i class="fa fa-file-text"></i> <span>Laporan Data Simpanan Wajib</span></a>
                </li>
                <li><a href="<?= base_url('laporan/shu') ?>"><i class="fa fa-file-text"></i>
                        <span>Laporan SHU</span></a></li>
                <li><a href="<?= base_url('laporan/laba_rugi') ?>"><i class="fa fa-file-text"></i> <span>Laporan Laba Rugi</span></a>
                </li>
            </ul>
        </li>
        <?php
    }
*/
    //f($ci->akses->in_access(['Administrator','Ketua'])){
    switch ($akses){
        case 'admin':
            ?>
            <li class="header">Manajemen Data</li>
            <li class=""> <?= anchor(base_url('karyawan'),'<i class="fa fa-users"></i><span>Karyawan</span>')?></li>
            <li class=""> <?= anchor(base_url('absen/upload'),'<i class="fa fa-calendar"></i><span>Absensi</span>')?></li>
            <li class="header">Laporan</li>
            <li class=""> <?= anchor(base_url('laporan/absen'),'<i class="fa fa-file-archive-o"></i><span>Laporan Absensi</span>')?></li>
            <li class=""> <?= anchor(base_url('laporan/grafik'),'<i class="fa fa-bar-chart"></i><span>Laporan Grafik Absensi</span>')?></li>
            <li class="header">Konfigurasi</li>
            <li class="treeview">
                <a href="#"><i class="fa fa-cogs"></i>Pengaturan <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu" style="display: none;">
                    <li><a href="<?= base_url('user')?>"><i class="fa fa-user-md"></i> <span>User</span></a></li>
                    <li><a href="<?= base_url('jabatan')?>"><i class="fa fa-tag"></i> <span>Jabatan</span></a></li>
                    <li><a href="<?= base_url('hari_libur')?>"><i class="fa fa-tag"></i> <span>Hari Libur</span></a></li>
                </ul>
            </li>
            <?php
            break;
        case 'user':
            ?>
            <li class="header">Manajemen Data</li>
            <li class=""> <?= anchor(base_url('karyawan'),'<i class="fa fa-users"></i><span>Karyawan</span>')?></li>
            <li class=""> <?= anchor(base_url('absen/upload'),'<i class="fa fa-calendar"></i><span>Absensi</span>')?></li>
            <li class="header">Laporan</li>
            <li class=""> <?= anchor(base_url('laporan/absen'),'<i class="fa fa-file-archive-o"></i><span>Laporan Absensi</span>')?></li>
            <li class=""> <?= anchor(base_url('laporan/grafik'),'<i class="fa fa-bar-chart"></i><span>Laporan Grafik Absensi</span>')?></li>
            <li class="header">Konfigurasi</li>
            <li class="treeview">
                <a href="#"><i class="fa fa-cogs"></i>Pengaturan <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu" style="display: none;">
                    <li><a href="<?= base_url('hari_libur')?>"><i class="fa fa-tag"></i> <span>Hari Libur</span></a></li>
                </ul>
            </li>
            <?php
            break;
    }
    //}
}