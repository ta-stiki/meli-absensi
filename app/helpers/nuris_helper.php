<?php
/**
 * Combobox dinamis dari database
 * @param string $name Nama element
 * @param string $table Tabel yang akan dikunakan untuk combobox
 * @param string $field Field yang akan digunakan sebagai label pada options
 * @param string $pk Primary key yang akan digunakan sebagai value dari setiap options
 * @param string $selected Set key option yang akan terselect
 * @param null $class Class tambahan pada css
 * @param bool $is_readonly Apakah elemen statusnya readonly
 * @return mixed string Hasil rendering element
 */
function cmb_dinamis($name,$table,$field,$pk,$selected,$class = null, $is_readonly = false){
    $ci = get_instance();
    ($is_readonly)? $is_readonly = 'disabled':null;
    $cmb = "<select $is_readonly name='$name' id='$name' class='form-control $class'><option value=''>Pilih data ...</option>";
    $data = $ci->db->get($table)->result();
    foreach ($data as $d){
        $cmb .="<option value='".$d->$pk."'";
        $cmb .= $selected==$d->$pk?" selected='selected'":'';
        $cmb .=">".  ucwords($d->$field)."</option>";
    }
    $cmb .="</select>";
    return $cmb;
}

/**
 * Fungsi untuk generate combobox yang datanya bersumber dari config
 * @param $name
 * @param $config
 * @param $selected
 * @param $class
 * @param bool $is_readonly
 * @return string
 */
function config_to_combo($name, $config, $selected,$class = null, $is_readonly = false, $required = true){
    $ci = get_instance();
    $array = $ci->config->item($config);
    $required = ($required == true)? 'required': null;
    $cmb = "<select $required name='$name' class='form-control $class '><option value=''>Pilih data ...</option>";
    foreach ($array as $key => $value){
        $cmb .="<option value='".$key."'";
        if($selected !='') $cmb .= $selected == $key?" selected='selected'":'';
        $cmb .=">".  ucwords($value)."</option>";
    }
    $cmb .="</select>";
    return $cmb;
}

/**
 * Fungsi untuk merender combobox dengan sumber data dari array
 * @param $name
 * @param $array
 * @param $selected
 * @param $class
 * @param bool $is_readonly
 * @return string
 */
function arr_to_combo($name, $array, $selected,$class = null,$is_readonly = false, $required = true){
    if(is_object($array) == true ){
        $array = call_user_func($array);
    }
    $ci = get_instance();
    $required = ($required == true)? 'required': null;
    $cmb = "<select $required name='$name' id='$name' class='form-control $class'><option value=''>Pilih data ...</option>";
    foreach ($array as $key => $value){
        $cmb .="<option value='".$key."'";
        if($selected !='')
            $cmb .= ($selected == $key)?" selected='selected'":'';

        $cmb .=">".  ucwords($value)."</option>";
    }
    $cmb .="</select>";
    return $cmb;
}

/**
 * Fungsi untuk menggenerate combobox select 2 dari database
 * @param $name
 * @param $table
 * @param $field
 * @param $pk
 * @param $selected
 * @param bool $is_readonly
 * @return string
 */
function cmb_select2($name,$table,$field,$pk,$selected, $is_readonly = false,$required = true){
    $ci = get_instance();
    ($is_readonly)? $is_readonly = 'disabled':null;
    $required = ($required == true)? 'required': null;
    $cmb = "<select $is_readonly name='$name' class='form-control select2 $required'><option value=''>Pilih data ...</option>";
    $data = $ci->db->get($table)->result();
    $cmb .="<option value=''></option>";
    foreach ($data as $d){
        $cmb .="<option value='".$d->$pk."'";
        $cmb .= $selected==$d->$pk?" selected='selected'":'';
        $cmb .=">".  ucwords($d->$field)."</option>";
    }
    $cmb .="</select>";
    return $cmb;
}

function CheckBoxArrayAkses($name,$table,$field,$pk,$selected, $is_readonly = false){
    $ci = get_instance();
    ($is_readonly)? $is_readonly = 'readonly':null;
    $data = $ci->db->get($table)->result();
    $chk = "";
    foreach ($data as $d) :
        $enabled = $ci->akses->in_access($d->$field,$selected);
        $chk .="<div class=\"checkbox\"><label>";
        $chk .= form_checkbox($name.'[]', $d->$pk,$enabled,"");
        $chk .= $d->$field;
        $chk .= "</label></div>";
    endforeach;
    return $chk;
}

/**
 * Fungsi untuk membuat format auto kode
 * @param $tabel
 * @param $kolom
 * @param int $lebar
 * @param $awalan
 * @return string
 */
function autoNumber($tabel, $kolom, $lebar=0, $awalan) {
    $ci = get_instance();
    $deretnol = str_repeat('0',$lebar);
    $ci->db->order_by($kolom, "desc");
    $ci->db->limit(1);
    $ci->db->select($kolom);
    $query = $ci->db->get($tabel);
    $rows = $query->row();
    $jum = $query->num_rows();

    if ($jum == 0) {
        $nomor = 1;
    } else {
        $nomor = intval(substr($rows->$kolom,strlen($awalan))) + 1;
    }

    if($lebar > 0) {

        $angka = $awalan.str_pad($nomor,$lebar - strlen($awalan),"0",STR_PAD_LEFT);

    } else {

        $angka = $awalan.$nomor;
    }

    return $angka;
}


/**
 * Fungsi untuk memformat angka dengan format standar indonesia
 * @param $angka
 * @return string
 */
function angka_indo($angka){
    return number_format($angka,2,',','.');
}


/**
 * Fungsi untuk menampilkan tanggal dengan format indonesia
 * @param $date
 * @param bool $short
 * @return string
 */
function TanggalIndo($date,$short = true){
    if($short){
        $BulanIndo = array("Jan", "Feb", "Mar", "Apr", "Mei", "Jun", "Jul", "Agst", "Sept", "Okt", "Nov", "Des");
    }else{
        $BulanIndo = array("Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember");
    }

    $tahun = substr($date, 0, 4);
    $bulan = substr($date, 5, 2);
    $tgl   = substr($date, 8, 2);

    $result = $tgl . " " . $BulanIndo[(int)$bulan-1] . " ". $tahun;
    return($result);
}

function ymdToDmy($date){
    list($year, $month, $day) = explode("-", $date);
    $ymd = "$day-$month-$year";
    return $ymd;
}

function dmyToYmd($date,$delimiter = "-"){
    list($day, $month, $year) = explode($delimiter, $date);
    $ymd = "$year-$month-$day";
    return $ymd;
}

function stringToNumber($nilai){
    $nilai = str_replace(',','.',str_replace('.','',$nilai));
    return $nilai;
}

function getdbConfig($config){
    $ci = get_instance();
    $ci->db->where("pengaturan.key = '$config'");
    $data = $ci->db->get('pengaturan')->row();
    if($data){
        return $data->value;
    }else{
        return false;
    }
}

/**
 *
 * @param $name
 * @return mixed
 */
function getJabatanIdByName($name){
    $ci = get_instance();
    $ci->load->model('Jabatan_model');
    $res = $ci->Jabatan_model->get_jabatan_nama($name);
    return $res;
}

/**
 * Fungsi untuk mendapatkan nama jabatan berdasarkan ID Jabatan
 * @param $idJabatan
 * @return string
 */
function getJabatan($idJabatan){
    $ci = get_instance();
    $ci->load->model('Jabatan_model');
    $res = $ci->Jabatan_model->get_by_id($idJabatan);
    if($res){
        return $res->nama_jabatan;
    }
    return '(belum diset)';
}

function getLibur($tanggal){
    $ci = get_instance();
    $query = $ci->db->query("SELECT
a.id,
a.tanggal,
a.keterangan,
b.id AS id_jenis_libur,
b.jenis_libur,
b.warna,
b.text_color,
b.kode
FROM
hari_libur AS a
INNER JOIN jenis_libur AS b ON a.jenis_libur = b.id where date(a.tanggal) = '$tanggal'");
    $row = $query->row();
    if (isset($row)){
        return $row;
    }
    return false;
}
function getIzin($id_karyawan, $dari_tanggal,$sampai_tanggal){
    $ci = get_instance();
    $ci->load->model('Izin_model');
    $res = $ci->Izin_model->get_izin($id_karyawan, $dari_tanggal,$sampai_tanggal);

    if ($res){
        return $res;
    }
    return false;
}
function getIzinPeriode($dari_tanggal,$sampai_tanggal){
    $ci = get_instance();
    $ci->load->model('Izin_model');
    $res = $ci->Izin_model->get_izinPeriode($dari_tanggal,$sampai_tanggal);

    if ($res){
        return $res;
    }
    return false;
}


function cekKehadiran($id, $date){
    $ci = get_instance();
    $ci->load->model('Absensi_model');
    $res = $ci->Absensi_model->is_available($id, $date);
    if($res){
        return true;
    }
    return false;
}





function cekKesediaanJabatan($id){
    $ci = get_instance();
    $ci->load->model('Jabatan_model');
    $jabatan = $ci->Jabatan_model->get_by_id($id);

    $user_id = $ci->session->userdata('user_id');
    if($id){
        switch ($jabatan->nama_jabatan){
            case 'Anggota':
                $config = 99;
                break;
            case 'Ketua':
                $config = $ci->config->item('jml_ketua');
                $ci->db->where('anggota.id !='.$id);
                break;
            case 'Bendahara':
                $config = $ci->config->item('jml_bendahara');
                break;
            case 'Pengawas':
                $config = $ci->config->item('jml_pengawas');
                break;
            case 'Wakil':
                $config = $ci->config->item('jml_wakil');
                break;
        }

        $ci->db->where([
            'anggota.id_jabatan'=>$id,
        ]);
        $ci->db->from('anggota');
        $jumlah = $ci->db->count_all_results();
        $ci->db->reset_query();
        $data = [
            'check' => ($jumlah < $config)? true:false,
            'jabatan' => $jabatan->nama_jabatan,
            'count' => $jumlah,
            'max' => $config
        ];
        return $data;
    }
}

/**
 * Fungsi untuk membaca tabel pada suatu worksheet excel
 * @param $sheet PHPExcel_Worksheet
 * @return array
 */
function readastable($sheet, $headerColumn = 'A1', $bodyStart = 'A2') {

    $ci = get_instance();
    $ci->load->library('PHPExcel');

    $highestrow = $sheet->getHighestRow();
    $highestcolumn = $sheet->getHighestColumn();
    $columncount = PHPExcel_Cell::columnIndexFromString($highestcolumn);
    $titles = $sheet->rangeToArray("{$headerColumn}:" . $highestcolumn . "1");
    $boddy = $sheet->rangeToArray("{$bodyStart}:" . $highestcolumn . $highestrow);

    $table = array();
    for ($row = 0; $row <= $highestrow - 2; $row++) {
        $a = array();
        for ($column = 0; $column <= $columncount - 1; $column++) {
            $a[$titles[0][$column]] = $boddy[$row][$column];
        }
        $table[$row] = $a;
    }
    return $table;
}

function build_calendar($month,$year,$dateArray = []) {

    // Create array containing abbreviations of days of week.
    $daysOfWeek = array('S','M','T','W','T','F','S');

    // What is the first day of the month in question?
    $firstDayOfMonth = mktime(0,0,0,$month,1,$year);

    // How many days does this month contain?
    $numberDays = date('t',$firstDayOfMonth);

    // Retrieve some information about the first day of the
    // month in question.
    $dateComponents = getdate($firstDayOfMonth);

    // What is the name of the month in question?
    $monthName = $dateComponents['month'];

    // What is the index value (0-6) of the first day of the
    // month in question.
    $dayOfWeek = $dateComponents['wday'];

    // Create the table tag opener and day headers

    $calendar = "<table class='calendar'>";
    $calendar .= "<caption>$monthName $year</caption>";
    $calendar .= "<tr>";

    // Create the calendar headers

    foreach($daysOfWeek as $day) {
        $calendar .= "<th class='header'>$day</th>";
    }

    // Create the rest of the calendar

    // Initiate the day counter, starting with the 1st.

    $currentDay = 1;

    $calendar .= "</tr><tr>";

    // The variable $dayOfWeek is used to
    // ensure that the calendar
    // display consists of exactly 7 columns.

    if ($dayOfWeek > 0) {
        $calendar .= "<td colspan='$dayOfWeek'>&nbsp;</td>";
    }

    $month = str_pad($month, 2, "0", STR_PAD_LEFT);

    while ($currentDay <= $numberDays) {

        // Seventh column (Saturday) reached. Start a new row.

        if ($dayOfWeek == 7) {

            $dayOfWeek = 0;
            $calendar .= "</tr><tr>";

        }

        $currentDayRel = str_pad($currentDay, 2, "0", STR_PAD_LEFT);

        $date = "$year-$month-$currentDayRel";

        $calendar .= "<td class='day' rel='$date'>$currentDay</td>";

        // Increment counters

        $currentDay++;
        $dayOfWeek++;

    }



    // Complete the row of the last week in month, if necessary

    if ($dayOfWeek != 7) {

        $remainingDays = 7 - $dayOfWeek;
        $calendar .= "<td colspan='$remainingDays'>&nbsp;</td>";

    }

    $calendar .= "</tr>";

    $calendar .= "</table>";

    return $calendar;

}