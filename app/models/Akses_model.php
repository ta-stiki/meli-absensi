<?php

/**
 * Created by PhpStorm.
 * User: dodik
 * Date: 09/01/2017
 * Time: 18.05
 */
class Akses_model extends CI_Model
{
    public $table = 'tbl_user';
    public $id = 'id';

    public function __construct()
    {
        parent::__construct();
    }

    public function login($username, $password)
    {
        $this->db->group_start()
            ->where('username', $username)
            ->or_where('email', $username)
            ->group_end();
        $this->db->where('password', $password);
        return $this->db->get($this->table)->row();
    }
}