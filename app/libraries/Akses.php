<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Created by PhpStorm.
 * User: dodik
 * Date: 09/01/2017
 * Time: 17.53
 */
class Akses
{

    public function __construct()
    {
        $this->load->helper(array('cookie', 'language','url'));
        $this->load->library('session');
        $this->load->model('Akses_model');
    }

    public function __get($var)
    {
        return get_instance()->$var;
    }

    public function login($username,$password)
    {
        $result = $this->Akses_model->login($username,$password);
        if($result){
            $sessionData = [
                'user_id'=> $result->id,
                'nama'=> $result->nama_user,
                'akses'=> $result->akses,
                'is_login'=> true,

            ];
            $this->session->set_userdata($sessionData);
            return true;
        }
        return false;
    }

    public function is_login()
    {
        if($this->session->userdata('is_login')){
            return true;
        } else{
            return false;
        }
    }

    public function get_user_id()
    {
        $user_id = $this->session->userdata('user_id');
        if (!empty($user_id))
        {
            return $user_id;
        }
        return null;
    }



}