<?php
/**
 * Created by PhpStorm.
 * User: AcenetDev
 * Date: 11/30/2017
 * Time: 9:10 AM
 */

class Home extends CI_Controller
{

    public function index()
    {
        $this->template->load('template','home/index');
    }

}