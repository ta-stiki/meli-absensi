<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->library(['form_validation']);
		$this->load->helper(['url','language']);
//        $this->load->model('Anggota_model');
	}

	// redirect if needed, otherwise display the user list
	function index()
	{
        if($this->akses->is_login()){
            redirect('/home', 'refresh');
        }else{
            $data=[];
            $data['title'] = "Login";

            $data['message'] = 'Halaman Login';

            $data['identity'] = array('name' => 'identity',
                'id'    => 'identity',
                'class' => 'form-control',
                'placeholder'=>'Email / Username',
                'type'  => 'text',
                'value' => $this->form_validation->set_value('identity'),
            );
            $data['password'] = array('name' => 'password',
                'id'   => 'password',
                'type' => 'password',
                'class' => 'form-control',
                'placeholder'=>'Password',
            );

            $this->load->view('auth/login',$data);
        }

	}

	// log the user in
	function login()
	{
		$this->data['title'] = "Login";

		//validate form input
		$this->form_validation->set_rules('identity', 'Identity', 'required',
            array('required' => '%s Tidak Boleh Kosong.'));
		$this->form_validation->set_rules('password', 'Password', 'required',
            array('required' => '%s  Tidak Boleh Kosong.'));

		if ($this->form_validation->run() == true)
		{
			// check to see if the user is logging in
			// check for "remember me"
			$remember = (bool) $this->input->post('remember');

			if ($this->akses->login($this->input->post('identity'), $this->input->post('password'), $remember))
			{
				//if the login is successful
				//redirect them back to the home page
				redirect('/', 'refresh');
			}
			else
			{
				// if the login was un-successful
				// redirect them back to the login page
				$this->session->set_flashdata('message', "Username dan Password tidak sesuai");
				redirect('auth/login', 'refresh'); // use redirects instead of loading views for compatibility with MY_Controller libraries
			}
		}
		else
		{
			// the user is not logging in so display the login page
			// set the flash data error message if there is one
//			$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
			$this->data['message'] = 'Login Error';

			$this->data['identity'] = array('name' => 'identity',
				'id'    => 'identity',
                                'class' => 'form-control',
                                'placeholder'=>'Email / Username',
				'type'  => 'text',
				'value' => $this->form_validation->set_value('identity'),
			);
			$this->data['password'] = array('name' => 'password',
				'id'   => 'password',
				'type' => 'password',
                                'class' => 'form-control',
                                'placeholder'=>'Password',
			);

            $this->load->view('auth/login', $this->data);
		}
	}

	// log the user out
	function logout()
	{
        $this->session->sess_destroy();
		// redirect them to the login page
		$this->session->set_flashdata('message', 'Logout Berhasil');
		redirect('auth/login', 'refresh');
	}



}
