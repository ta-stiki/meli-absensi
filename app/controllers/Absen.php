<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Absen extends CI_Controller {

	function __construct()
	{
		parent::__construct();
        $this->load->library('PHPExcel');
        $this->load->model('Import_absen_model');
        $this->load->model('Absensi_model');
        $this->load->model('Karyawan_model');
	}

	function index(){
        $periode_bulan ='';
        $periode_tahun ='';
        $nama_karyawan ='';
        $theadDate ='';
        $rowData = '';
        $numberDays =0;
        $bulan = '';
	    if($this->input->method() == 'post'){
            $periode_bulan = $this->input->post('periode_bulan');
            $periode_tahun = $this->input->post('periode_tahun');
            $nama_karyawan = $this->input->post('nama_karyawan');
            $firstDayOfMonth = mktime(0,0,0,intval($periode_bulan),1,$periode_tahun);
            $bulan = date('F',$firstDayOfMonth);
            $numberDays = date('t',$firstDayOfMonth);
            $dateComponents = getdate($firstDayOfMonth);
            $dayOfWeek = $dateComponents['wday'];
            $theadDate = '';
            for ($dayNum = 1; $dayNum <= $numberDays; $dayNum++){
                $date    = new DateTime("$periode_tahun-$periode_bulan-$dayNum");
                $sundayColor = ($date->format('D') == 'Sun')? 'bg-red-active color-palette':null;
                $libur = getLibur("$periode_tahun-$periode_bulan-$dayNum");

                $rowHeader = "<th>$dayNum</th>";

                if($libur){
                    $rowHeader = "<th style='background-color: $libur->warna;color: $libur->text_color'>$dayNum</th>";
                }
                if($sundayColor){
                    $rowHeader = "<th class='$sundayColor'>$dayNum</th>";
                }


                $theadDate .= $rowHeader;
            }
            if($nama_karyawan){
                $karyawan = $this->Karyawan_model->get_by_id2($nama_karyawan);
            }else{
                $karyawan = $this->Karyawan_model->get_all();
            }
            $rowData = '';
            $i = 0;
            foreach ($karyawan as $item){
                $csakit = 0;
                $ccuti = 0;
                $chadir = 0;
                $calpa = 0;
                $i++;
                $rowData .= "<tr>";
                $rowData .= "<td>$i</td>";
                $rowData .= "<td>$item->id_karyawan</td>";
                $rowData .= "<td>$item->nama_karyawan</td>";
                $rowData .= "<td>". getJabatan($item->id_jabatan) ."</td>";
                $izin = getIzin($item->id_karyawan, "$periode_tahun-$periode_bulan-1","$periode_tahun-$periode_bulan-$numberDays");
                for ($dayNum = 1; $dayNum <= $numberDays; $dayNum++){
                    $date    = new DateTime("$periode_tahun-$periode_bulan-$dayNum");
                    $tanggal = date('Y-m-d',strtotime("$periode_tahun-$periode_bulan-$dayNum"));
                    $sundayColor = ($date->format('D') == 'Sun')? 'bg-red-active color-palette':null;
                    $isHadir = cekKehadiran($item->id_karyawan, "$periode_tahun-$periode_bulan-$dayNum");
                    $chadir += ($isHadir && !$sundayColor)?1:0;
                    $hadir = ($isHadir)? '&#x2714;': '&#x2716;';
                    $hadir = (!$sundayColor)?$hadir:'M';
                    $dataRow = "<td class='$sundayColor'>$hadir</td>";

                    if($izin){

                        foreach ($izin as $rowIzin){

                            $dari = date('Y-m-d',strtotime($rowIzin->dari_tanggal));
                            $sampai = date('Y-m-d',strtotime($rowIzin->sampai_tanggal));

                            if (($tanggal >= $dari) && ($tanggal <= $sampai))
                            {
                                $dataRow = "<td class='$sundayColor'>$rowIzin->jenis</td>";
                                switch ($rowIzin->jenis){
                                    case 'A':
                                        $calpa += 1;
                                        break;
                                    case 'S':
                                        $csakit += 1;
                                        break;

                                }
                                continue;
                            }
                        }

                    }

                    $libur = getLibur("$periode_tahun-$periode_bulan-$dayNum");
                    if($libur){
                        switch ($libur->kode){
                            case 'C':
                                $ccuti += 1;
                                break;
                        }
                        $chadir += ($isHadir && !$sundayColor)?-1:0;
                        $dataRow = "<td style='background-color: $libur->warna;color: $libur->text_color'>L</td>";
                    }

                    $rowData .= $dataRow;

                }
                $rowData .= "<td>$csakit</td>";
                $rowData .= "<td>$ccuti</td>";
                $rowData .= "<td>$calpa</td>";
                $rowData .= "<td>$chadir</td>";
                $rowData .= "</tr>";
            }
        }else{

        }
        $this->template->load('template','absen/index',[
            'theadDate' =>$theadDate,
            'rowData' =>$rowData,
            'numberDays' =>$numberDays,
            'bulan' =>$bulan,
            'nama_karyawan' => $nama_karyawan,
            'periode_bulan' => $periode_bulan,
            'periode_tahun' => $periode_tahun,
        ]);

    }

    function getAbsen($params){

    }

    function export_excel(){
        $periode_bulan ='';
        $periode_tahun ='';
        $theadDate ='';
        $rowData = '';
        $numberDays =0;
        $bulan = '';
        if($this->input->method() == 'get'){
            $periode_bulan = $this->input->get('periode_bulan');
            $periode_tahun = $this->input->get('periode_tahun');
            $nama_karyawan = $this->input->get('nama_karyawan');
            $objPHPExcel = new PHPExcel();
            $firstDayOfMonth = mktime(0,0,0,intval($periode_bulan),1,$periode_tahun);
            $bulan = date('F',$firstDayOfMonth);
            $numberDays = date('t',$firstDayOfMonth);
            $dateComponents = getdate($firstDayOfMonth);
            $dayOfWeek = $dateComponents['wday'];
            $theadDate = '';
            $objPHPExcel->getActiveSheet()->setTitle($bulan.'-'.$periode_tahun);

            $rowNumberH = 1;
            $colH = 'A';
            /*
             * Generate Header
             * */

            $objPHPExcel->getActiveSheet()->setCellValue('A1','No');
            $objPHPExcel->getActiveSheet()->setCellValue('B1','NIK');
            $objPHPExcel->getActiveSheet()->setCellValue('C1','Nama Dosen');
            $objPHPExcel->getActiveSheet()->setCellValue('D1','Jabatan');
            $objPHPExcel->getActiveSheet()->setCellValue('E1',$bulan . ' ' . $periode_tahun);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4 + $numberDays,1,'Sakit');
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5 + $numberDays,1,'Cuti');
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(6 + $numberDays,1,'Ket A');
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(7 + $numberDays,1,'Total Kehadiran');
            $objPHPExcel->getActiveSheet()->mergeCells('A1:A2');
            $objPHPExcel->getActiveSheet()->mergeCells('B1:B2');
            $objPHPExcel->getActiveSheet()->mergeCells('C1:C2');
            $objPHPExcel->getActiveSheet()->mergeCells('D1:D2');
            $objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(4,1,3 + $numberDays,1);
            $objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(4 + $numberDays,1,4 + $numberDays,2);
            $objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(5 + $numberDays,1,5 + $numberDays,2);
            $objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(6 + $numberDays,1,6 + $numberDays,2);
            $objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(7 + $numberDays,1,7 + $numberDays,2);

            for ($dayNum = 1; $dayNum <= $numberDays; $dayNum++){
                $date    = new DateTime("$periode_tahun-$periode_bulan-$dayNum");
                $sundayColor = ($date->format('D') == 'Sun')? 'D33724':null;
                $libur = getLibur("$periode_tahun-$periode_bulan-$dayNum");
                $stylecolor = [
                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                    'font' => [
                        'bold'  => true,
                        'size'  => 14,
                    ]
                ];
                if($libur){
                    $stylecolor = [
                        'type' => PHPExcel_Style_Fill::FILL_SOLID,
                        'startcolor' => [
                            'rgb' => str_replace('#','',$libur->warna)
                        ],
                        'font' => [
                            'bold'  => true,
                            'color' => array('rgb' => $libur->text_color),
                            'size'  => 14,
                        ]
                    ];
                }
                $objPHPExcel->getActiveSheet()
                    ->getStyleByColumnAndRow(3 + $dayNum,2,$dayNum)
                    ->getFill()->applyFromArray($stylecolor);
                $objPHPExcel->getActiveSheet()
                    ->setCellValueByColumnAndRow(3 + $dayNum,2,$dayNum);

                if($sundayColor){
                    $stylecolor = [
                        'type' => PHPExcel_Style_Fill::FILL_SOLID,
                        'startcolor' => [
                            'rgb' => $sundayColor
                        ],
                        'font' => [
                            'bold'  => true,
                            'color' => array('rgb' => 'fffff'),
                            'size'  => 14,
                        ]
                    ];
                    $objPHPExcel->getActiveSheet()
                        ->getStyleByColumnAndRow(3 + $dayNum,2,$dayNum)
                        ->getFill()->applyFromArray($stylecolor);
                }
//
//
//                $theadDate .= $rowHeader;
            }
            if($nama_karyawan){
                $karyawan = $this->Karyawan_model->get_by_id2($nama_karyawan);
            }else{
                $karyawan = $this->Karyawan_model->get_all();
            }
            $rowData = '';
            $i = 0;
            foreach ($karyawan as $item){
                $csakit = 0;
                $ccuti = 0;
                $chadir = 0;
                $calpa = 0;
                $i++;

                $objPHPExcel->getActiveSheet()->setCellValue('A'.($rowNumberH+ 1+$i),$i);
                $objPHPExcel->getActiveSheet()->setCellValue('B'.($rowNumberH+ 1+$i),$item->id_karyawan);
                $objPHPExcel->getActiveSheet()->setCellValue('C'.($rowNumberH+ 1+$i),$item->nama_karyawan);
                $objPHPExcel->getActiveSheet()->setCellValue('D'.($rowNumberH+ 1+$i),getJabatan($item->id_jabatan));

                $izin = getIzin($item->id_karyawan, "$periode_tahun-$periode_bulan-1","$periode_tahun-$periode_bulan-$numberDays");
                for ($dayNum = 1; $dayNum <= $numberDays; $dayNum++){
                    $date    = new DateTime("$periode_tahun-$periode_bulan-$dayNum");
                    $tanggal = date('Y-m-d',strtotime("$periode_tahun-$periode_bulan-$dayNum"));
                    $sundayColor = ($date->format('D') == 'Sun')? 'D33724':null;
                    $isHadir = cekKehadiran($item->id_karyawan, "$periode_tahun-$periode_bulan-$dayNum");
                    $chadir += ($isHadir && !$sundayColor)?1:0;
                    $hadir = ($isHadir)? '&#x2714;': '&#x2716;';
                    $hadir = (!$sundayColor)?$hadir:'M';
                    if($isHadir){
                        $objPHPExcel->getActiveSheet()
                            ->setCellValueByColumnAndRow(3 + $dayNum,$rowNumberH + 1 +$i,'✓');
                    }else{
                        $objPHPExcel->getActiveSheet()
                            ->setCellValueByColumnAndRow(3 + $dayNum,$rowNumberH + 1 +$i,'x');
                    }

                    if($sundayColor){
                        $stylecolor = [
                            'type' => PHPExcel_Style_Fill::FILL_SOLID,
                            'startcolor' => [
                                'rgb' => $sundayColor
                            ],
                            'font' => [
                                'bold'  => true,
                                'color' => array('rgb' => 'fffff'),
                                'size'  => 14,
                            ]
                        ];

                        $objPHPExcel->getActiveSheet()
                            ->setCellValueByColumnAndRow(3 + $dayNum,$rowNumberH + 1 +$i,'M');
                        $objPHPExcel->getActiveSheet()
                            ->getStyleByColumnAndRow(3 + $dayNum,$rowNumberH + 1 +$i,$dayNum)
                            ->getFill()->applyFromArray($stylecolor);
                    }

                    if($izin){

                        foreach ($izin as $rowIzin){

                            $dari = date('Y-m-d',strtotime($rowIzin->dari_tanggal));
                            $sampai = date('Y-m-d',strtotime($rowIzin->sampai_tanggal));

                            if (($tanggal >= $dari) && ($tanggal <= $sampai))
                            {

                                $objPHPExcel->getActiveSheet()
                                    ->setCellValueByColumnAndRow(3 + $dayNum,$rowNumberH + 1 +$i,$rowIzin->jenis);
                                switch ($rowIzin->jenis){
                                    case 'A':
                                        $calpa += 1;
                                        break;
                                    case 'S':
                                        $csakit += 1;
                                        break;

                                }
                                continue;
                            }
                        }

                    }

                    $libur = getLibur("$periode_tahun-$periode_bulan-$dayNum");
                    if($libur){
                        switch ($libur->kode){
                            case 'C':
                                $ccuti += 1;
                                break;
                        }
                        $chadir += ($isHadir && !$sundayColor)?-1:0;
                        $objPHPExcel->getActiveSheet()
                            ->setCellValueByColumnAndRow(3 + $dayNum,$rowNumberH + 1 +$i,'L');

                        $stylecolor = [
                            'type' => PHPExcel_Style_Fill::FILL_SOLID,
                            'startcolor' => [
                                'rgb' => str_replace('#','',$libur->warna)
                            ],
                            'font' => [
                                'bold'  => true,
                                'color' => array('rgb' => 'fffff'),
                                'size'  => 14,
                            ]
                        ];
                        $objPHPExcel->getActiveSheet()
                            ->getStyleByColumnAndRow(3 + $dayNum,$rowNumberH + 1 +$i,$dayNum)
                            ->getFill()->applyFromArray($stylecolor);
                    }



                }

                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4 + $numberDays,$rowNumberH + 1 +$i,$csakit);
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5 + $numberDays,$rowNumberH + 1+ $i,$ccuti);
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(6 + $numberDays,$rowNumberH + 1+ $i,$calpa);
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(7 + $numberDays,$rowNumberH + 1+ $i,$chadir);

            }

            $getJenisLibur = $this->db->query("SELECT * FROM status_keterangan");
            $z=0;
            $objPHPExcel->getActiveSheet()->setCellValue('B'.($rowNumberH+ 3+$i),'Keterangan Lain :');
            foreach ($getJenisLibur->result() as $row)
            {
                $z +=1;

                $objPHPExcel->getActiveSheet()->setCellValue('B'.($rowNumberH+ 4+$i+$z),$row->kode);
                $objPHPExcel->getActiveSheet()->setCellValue('C'.($rowNumberH+ 4+$i+$z),$row->keterangan);
            }

            $getJenisLibur = $this->db->query("SELECT * FROM jenis_libur");
            $h= 0;
            foreach ($getJenisLibur->result() as $row)
            {
                $h++;
                $objPHPExcel->getActiveSheet()->setCellValue('B'.($rowNumberH+ 6+$i+$z+$h),$row->jenis_libur);

                $stylecolor = [
                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                    'startcolor' => [
                        'rgb' => str_replace('#','',$row->warna)
                    ],
                    'font' => [
                        'bold'  => true,
                        'color' => array('rgb' => 'fffff'),
                        'size'  => 14,
                    ]
                ];
                $objPHPExcel->getActiveSheet()
                    ->getStyleByColumnAndRow(1,($rowNumberH+ 6+$i+$z+$h),1,($rowNumberH+ 6+$i+$z+$h))
                    ->getFill()->applyFromArray($stylecolor);

                $objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(1,($rowNumberH+ 6+$i+$z+$h),2,($rowNumberH+ 6+$i+$z+$h));
            }

            $stylecolor = [
                'borders' => [
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                    'color' => array('rgb' => '00000')
                ]
            ];
            $objPHPExcel->getActiveSheet()
                ->getStyleByColumnAndRow(0,1,7 + $numberDays,($rowNumberH+ $i+1))
                ->getBorders()->applyFromArray($stylecolor);

            $objPHPExcel->getActiveSheet()->setCellValue('A'.($rowNumberH+ 8+$i+$z+$h),'Keterangan Karyawan Yang Alfa DiKarenakan :');
            $objPHPExcel->getActiveSheet()->setCellValue('C'.($rowNumberH+ 8+$i+$z+$h),'Keterangan');
            /**
             * @var $izin mysqli_result
            **/
            $izin = getIzinPeriode("$periode_tahun-$periode_bulan-1","$periode_tahun-$periode_bulan-$numberDays");
            if($izin){
                $zz =0;
                foreach ($izin as $rowIzin){
                    $zz++;
                    if($rowIzin->jenis =='A'){
                        $objPHPExcel->getActiveSheet()->setCellValue('A'.($rowNumberH+ 9+$i+$z+$h+ $zz),$rowIzin->nama_karyawan);
                        $objPHPExcel->getActiveSheet()->setCellValue('C'.($rowNumberH+ 9+$i+$z+$h+ $zz),$rowIzin->keterangan . " ({$rowIzin->dari_tanggal} - {$rowIzin->sampai_tanggal})");
                    }
                }
            }

            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel5');
            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment;filename="Laporan Absensi Bulan '.$bulan. ' '.$periode_tahun.'.xls"');
            header('Cache-Control: max-age=0');

            $objWriter->save('php://output');
            exit();
        }

    }

	// redirect if needed, otherwise display the user list
	function upload()
	{
        $this->template->load('template','absen/upload',[]);
	}

    function proses_upload(){
	    if($this->input->method() == 'post'){

	        $mode = $this->input->post('mode_import');
	        $periode_bulan = $this->input->post('periode_bulan');
	        $periode_tahun = $this->input->post('periode_tahun');
            $dataProses = [
                'tanggal' => date('Y-m-d h:i:s'),
                'mode_import' => $mode,
                'id_user' => '',
                'periode' => $periode_tahun  .'-'. $periode_bulan,
            ];
            $idProses = $this->Import_absen_model->insertProses($dataProses);

            if($idProses){
                $firstDayOfMonth = mktime(0,0,0,intval($periode_bulan),1,$periode_tahun);
                $numberDays = date('t',$firstDayOfMonth);
                $dateComponents = getdate($firstDayOfMonth);

                $dayOfWeek = $dateComponents['wday'];

                $fileName = 'abs_' . time() . $_FILES['file']['name'];
                $config['upload_path'] = './assets/upload/'; //buat folder dengan nama assets di root folder
                $config['file_name'] = $fileName;
                $config['allowed_types'] = 'xls|xlsx|csv';
                $config['max_size'] = 10000;

                $this->load->library('upload');
                $this->upload->initialize($config);

                if(! $this->upload->do_upload('file') )
                    $this->upload->display_errors();

                $media = $this->upload->data('file');
                $inputFileName = './assets/upload/'.$fileName;

//        $Reader = PHPExcel_IOFactory::load($inputFileName);
//        foreach ($Reader->getWorksheetIterator() as $worksheet){
//            $worksheetTitle     = $worksheet->getTitle();
//            $highestRow         = $worksheet->getHighestRow(); // e.g. 10
//            $highestColumn      = $worksheet->getHighestColumn(); // e.g 'F'
//            $highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);
//            $nrColumns = ord($highestColumn) - 64;
//            echo "<br>The worksheet ".$worksheetTitle." has ";
//            echo $nrColumns . ' columns (A-' . $highestColumn . ') ';
//            echo ' and ' . $highestRow . ' row.';
//            echo '<br>Data: <table border="1"><tr>';
//            for ($row = 1; $row <= $highestRow; ++ $row) {
//                echo '<tr>';
//                for ($col = 0; $col < $highestColumnIndex; ++ $col) {
//                    $cell = $worksheet->getCellByColumnAndRow($col, $row);
//                    $val = $cell->getValue();
//                    $dataType = PHPExcel_Cell_DataType::dataTypeForValue($val);
//                    echo '<td>' . $val . '<br>(Typ ' . $dataType . ')</td>';
//                }
//                echo '</tr>';
//            }
//            echo '</table>';
//
//        }



                try {
                    $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
                    $objReader = PHPExcel_IOFactory::createReader($inputFileType);
                    $objPHPExcel = $objReader->load($inputFileName);
                } catch(Exception $e) {
                    die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
                }

                $sheet = $objPHPExcel->getSheet(0);

                $data = readastable($sheet);
                $i=0;
                $response = [];
                $resData = '';
                foreach ($data as $item) {
                    $inserStatus = 'Sudah pernah diupload';
                    $i++;
                    $PIN = $item['PIN'];
                    $NIK = $item['NIK'];
                    $NamaKaryawan = $item['Nama Karyawan'];
                    $Tanggal = $item['Tanggal'];
                    $Jam = $item['Jam'];
                    $SNMesin = $item['SN Mesin'];
                    $NamaMesin = $item['Nama Mesin'];
                    $Verifikasi = $item['Verifikasi'];
                    $Mode = $item['Mode'];
                    $ModeUpdate = $item['Mode Update'];
                    $Cabang = $item['Cabang'];
                    $Jabatan = $item['Jabatan'];

                    $data = [
                        'PIN' => $PIN,
                        'NIK' => $NIK,
                        'Nama_Karyawan' => $NamaKaryawan,
                        'Tanggal' => dmyToYmd($Tanggal,'/'),
                        'Jam' => $Jam,
                        'SN_Mesin' => $SNMesin,
                        'Nama_Mesin' => $NamaMesin,
                        'Verifikasi' => $Verifikasi,
                        'Mode' => $Mode,
                        'Mode_Update' => $ModeUpdate,
                        'Cabang' => $Cabang,
                        'Jabatan' => $Jabatan,
                        'import_id' => '',
                    ];
                    $status = $this->Import_absen_model->insert($data);
                    $inserStatus = ($status)? 'Sukses':'Gagal';
                    $resData .= "<tr>
                        <td>{$i}</td>
                        <td>{$PIN}</td>
                        <td>{$NIK}</td>
                        <td>{$NamaKaryawan}</td>
                        <td>{$Tanggal}</td>
                        <td>{$Jam}</td>
                        <td>{$SNMesin}</td>
                        <td>{$NamaMesin}</td>
                        <td>{$Verifikasi}</td>
                        <td>{$Mode}</td>
                        <td>{$ModeUpdate}</td>
                        <td>{$Cabang}</td>
                        <td>{$Jabatan}</td>
                        <td>{$inserStatus}</td>
                        </tr>";

                    if($item['Mode'] == 'Scan Masuk'){
                        $absen = $this->Absensi_model->insert([
                            'id_karyawan' => $NIK,
                            'tanggal' => dmyToYmd($Tanggal,'/') . ' ' . $Jam . ':00',
                            'keterangan' => 'V',
                        ]);
                    }
                }

                if($resData){
                    $response = [
                        'status' => 'Sukses',
                        'message' => $resData
                    ];
                }else{
                    $response = [
                        'status' => 'Gagal',
                        'message' => 'tidak ada data yang cocok'
                    ];
                }

                echo json_encode($response);

//        $arrSheet = $objPHPExcel->getSheetNames();
//        $arrMonth = $this->config->item('periode_bulan');

//        echo '<pre>';
////        print_r(readastable($sheet));
//        print_r(readastable($sheet));
//        echo '</pre>';
//        return false;
//
////        echo '<pre>';
//        foreach ($arrSheet as $listItem){
//
//            if(array_key_exists($listItem,$arrMonth)){
//                $sheet = $objPHPExcel->getSheetByName($listItem);
//                $highestRow = $sheet->getHighestRow();
//                $highestColumn = $sheet->getHighestColumn();
//                for ($row = 2; $row <= $highestRow; $row++){                  //  Read a row of data into an array
//                    $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row,NULL,TRUE,FALSE);
//                    //Sesuaikan sama nama kolom tabel di database
////                    $data = array(
////                        "idimport"=> $rowData[0][0],
////                        "nama"=> $rowData[0][1],
////                        "alamat"=> $rowData[0][2],
////                        "kontak"=> $rowData[0][3]
////                    );
//            echo '<pre>';
//            print_r($data);
//            echo '</pre>';
//                    //sesuaikan nama dengan nama tabel
////            $insert = $this->db->insert("eimport",$data);
////            delete_files($media['file_path']);
//
//                }
//            }else{
//                echo 'Not available sheet data';
//            }
//
//
//        }
////        print_r($objPHPExcel->getSheetNames());
////        echo '</pre>';
//
//
//
////        echo '<pre>';
////        print_r($highestRow);
////        print_r($highestColumn);
////        echo '</pre>';

                unset($objPHPExcel);
//        redirect('excel/');
            }else{
                echo json_encode([
                    'status' => 'Gagal',
                    'message' => 'Sudah Pernah diimport'
                ]);
            }

        }

    }

}
