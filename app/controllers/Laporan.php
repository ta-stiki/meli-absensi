<?php
/**
 * Created by PhpStorm.
 * User: AcenetDev
 * Date: 12/28/2017
 * Time: 5:03 PM
 */

class Laporan extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->library('PHPExcel');
        $this->load->model('Import_absen_model');
        $this->load->model('Absensi_model');
        $this->load->model('Karyawan_model');
    }

    function absen(){
        $periode_bulan ='';
        $periode_tahun ='';
        $nama_karyawan ='';
        $theadDate ='';
        $rowData = '';
        $numberDays =0;
        $bulan = '';
        if($this->input->method() == 'post'){
            $periode_bulan = $this->input->post('periode_bulan');
            $periode_tahun = $this->input->post('periode_tahun');
            $nama_karyawan = $this->input->post('nama_karyawan');
            $firstDayOfMonth = mktime(0,0,0,intval($periode_bulan),1,$periode_tahun);
            $bulan = date('F',$firstDayOfMonth);
            $numberDays = date('t',$firstDayOfMonth);
            $dateComponents = getdate($firstDayOfMonth);
            $dayOfWeek = $dateComponents['wday'];
            $theadDate = '';
            for ($dayNum = 1; $dayNum <= $numberDays; $dayNum++){
                $date    = new DateTime("$periode_tahun-$periode_bulan-$dayNum");
                $sundayColor = ($date->format('D') == 'Sun')? 'bg-red-active color-palette':null;
                $libur = getLibur("$periode_tahun-$periode_bulan-$dayNum");

                $rowHeader = "<th>$dayNum</th>";

                if($libur){
                    $rowHeader = "<th style='background-color: $libur->warna;color: $libur->text_color'>$dayNum</th>";
                }
                if($sundayColor){
                    $rowHeader = "<th class='$sundayColor'>$dayNum</th>";
                }


                $theadDate .= $rowHeader;
            }
            if($nama_karyawan){
                $karyawan = $this->Karyawan_model->get_by_id2($nama_karyawan);
            }else{
                $karyawan = $this->Karyawan_model->get_all();
            }
            $rowData = '';
            $i = 0;
            foreach ($karyawan as $item){
                $csakit = 0;
                $ccuti = 0;
                $chadir = 0;
                $calpa = 0;
                $i++;
                $rowData .= "<tr>";
                $rowData .= "<td>$i</td>";
                $rowData .= "<td>$item->id_karyawan</td>";
                $rowData .= "<td>$item->nama_karyawan</td>";
                $rowData .= "<td>". getJabatan($item->id_jabatan) ."</td>";
                $izin = getIzin($item->id_karyawan, "$periode_tahun-$periode_bulan-1","$periode_tahun-$periode_bulan-$numberDays");
                for ($dayNum = 1; $dayNum <= $numberDays; $dayNum++){
                    $date    = new DateTime("$periode_tahun-$periode_bulan-$dayNum");
                    $tanggal = date('Y-m-d',strtotime("$periode_tahun-$periode_bulan-$dayNum"));
                    $sundayColor = ($date->format('D') == 'Sun')? 'bg-red-active color-palette':null;
                    $isHadir = cekKehadiran($item->id_karyawan, "$periode_tahun-$periode_bulan-$dayNum");
                    $chadir += ($isHadir && !$sundayColor)?1:0;
                    $hadir = ($isHadir)? '&#x2714;': '&#x2716;';
                    $hadir = (!$sundayColor)?$hadir:'M';
                    $dataRow = "<td class='$sundayColor'>$hadir</td>";

                    if($izin){

                        foreach ($izin as $rowIzin){

                            $dari = date('Y-m-d',strtotime($rowIzin->dari_tanggal));
                            $sampai = date('Y-m-d',strtotime($rowIzin->sampai_tanggal));

                            if (($tanggal >= $dari) && ($tanggal <= $sampai))
                            {
                                $dataRow = "<td class='$sundayColor'>$rowIzin->jenis</td>";
                                switch ($rowIzin->jenis){
                                    case 'A':
                                        $calpa += 1;
                                        break;
                                    case 'S':
                                        $csakit += 1;
                                        break;

                                }
                                continue;
                            }
                        }

                    }

                    $libur = getLibur("$periode_tahun-$periode_bulan-$dayNum");
                    if($libur){
                        switch ($libur->kode){
                            case 'C':
                                $ccuti += 1;
                                break;
                        }
                        $chadir += ($isHadir && !$sundayColor)?-1:0;
                        $dataRow = "<td style='background-color: $libur->warna;color: $libur->text_color'>L</td>";
                    }

                    $rowData .= $dataRow;

                }
                $rowData .= "<td>$csakit</td>";
                $rowData .= "<td>$ccuti</td>";
                $rowData .= "<td>$calpa</td>";
                $rowData .= "<td>$chadir</td>";
                $rowData .= "</tr>";
            }
        }else{

        }
        $this->template->load('template','absen/index',[
            'theadDate' =>$theadDate,
            'rowData' =>$rowData,
            'numberDays' =>$numberDays,
            'bulan' =>$bulan,
            'nama_karyawan' => $nama_karyawan,
            'periode_bulan' => $periode_bulan,
            'periode_tahun' => $periode_tahun,
        ]);

    }

}