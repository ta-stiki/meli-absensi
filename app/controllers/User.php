<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class User extends CI_Controller
{

    public $akses;
    public $userId;

    function __construct()
    {
        parent::__construct();
        $this->load->model('User_model');
        $this->load->library('form_validation');
        $this->akses = $this->session->userdata('akses','user');
        $this->userId = $this->session->userdata('user_id');
    }

    public function index()
    {
        if($this->akses != 'admin'){
            show_error("Maaf anda tidak memiliki akses pada halaman ini.",500,"Akses ditolak");
        }
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));
        
        if ($q <> '') {
            $config['base_url'] = base_url() . 'user/?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'user/?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'user/';
            $config['first_url'] = base_url() . 'user/';
        }

        $config['per_page'] = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->User_model->total_rows($q);
        $user = $this->User_model->get_limit_data($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'user_data' => $user,
            'c_header' => 'user',
            'c_sub_header' => 'Daftar user',
            'q' => $q,
            'pagination' => $this->pagination->create_links(),
            'total_rows' => $config['total_rows'],
            'start' => $start,
        );
        $this->template->load('template','user/user_list', $data);
    }

    public function read($id) 
    {
        if($this->akses != 'admin'){
            show_error("Maaf anda tidak memiliki akses pada halaman ini.",500,"Akses ditolak");
        }
        $row = $this->User_model->get_by_id($id);
        if ($row) {
            $data = array(
		'id' => $row->id,
		'username' => $row->username,
		'password' => $row->password,
		'nama_user' => $row->nama_user,
		'email' => $row->email,
		'akses' => $row->akses,
	    );
            $this->template->load('template','user/user_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('user'));
        }
    }

    public function create() 
    {
        if($this->akses != 'admin'){
            show_error("Maaf anda tidak memiliki akses pada halaman ini.",500,"Akses ditolak");
        }
        $data = array(
            'button' => 'Create',
            'action' => site_url('user/create_action'),
            'is_newRecord' => true,
	    'id' => set_value('id'),
	    'username' => set_value('username'),
	    'password' => set_value('password'),
	    'nama_user' => set_value('nama_user'),
	    'email' => set_value('email'),
	    'akses' => set_value('akses'),
	);
        $this->template->load('template','user/user_form', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'username' => $this->input->post('username',TRUE),
		'password' => $this->input->post('password',TRUE),
		'nama_user' => $this->input->post('nama_user',TRUE),
		'email' => $this->input->post('email',TRUE),
		'akses' => $this->input->post('akses',TRUE),
	    );

            $this->User_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('user'));
        }
    }
    
    public function update($id) 
    {
        if($this->akses != 'admin'){
            show_error("Maaf anda tidak memiliki akses pada halaman ini.",500,"Akses ditolak");
        }
        $row = $this->User_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('user/update_action'),
                'is_newRecord' => false,
		'id' => set_value('id', $row->id),
		'username' => set_value('username', $row->username),
		'password' => set_value('password', $row->password),
		'nama_user' => set_value('nama_user', $row->nama_user),
		'email' => set_value('email', $row->email),
		'akses' => set_value('akses', $row->akses),
	    );
            $this->template->load('template','user/user_form', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('user'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id', TRUE));
        } else {
            $data = array(
		'username' => $this->input->post('username',TRUE),
		'password' => $this->input->post('password',TRUE),
		'nama_user' => $this->input->post('nama_user',TRUE),
		'email' => $this->input->post('email',TRUE),
		'akses' => $this->input->post('akses',TRUE),
	    );

            $this->User_model->update($this->input->post('id', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('user'));
        }
    }
    
    public function delete($id) 
    {
        if($this->akses != 'admin'){
            show_error("Maaf anda tidak memiliki akses pada halaman ini.",500,"Akses ditolak");
        }
        $row = $this->User_model->get_by_id($id);

        if ($row) {
            $this->User_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('user'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('user'));
        }
    }

    public function profile()
    {

        if($this->input->method() == 'post'){
            $data = array(
//                'username' => $this->input->post('username',TRUE),
                'password' => $this->input->post('password',TRUE),
                'nama_user' => $this->input->post('nama_user',TRUE),
                'email' => $this->input->post('email',TRUE),
            );

            $this->User_model->update($this->input->post('id', TRUE), $data);
            redirect(site_url('auth/logout'), 'refresh');
        }else{
            if($this->userId) {
                $row = $this->User_model->get_by_id($this->userId);
                if ($row) {
                    $data = array(
                        'id' => $row->id,
                        'username' => $row->username,
                        'password' => $row->password,
                        'nama_user' => $row->nama_user,
                        'email' => $row->email,
                        'akses' => $row->akses,
                    );
                    $this->template->load('template','user/profile', $data);
                } else {
                    $this->session->set_flashdata('message', 'Record Not Found');
                    redirect('/', 'refresh');
                }
            }else{
                show_error("Maaf anda tidak memiliki akses pada halaman ini.", 500, "Akses ditolak");
            }
        }


    }

    public function _rules() 
    {
        $this->form_validation->set_rules('username', 'username', 'trim|required');
        $this->form_validation->set_rules('password', 'password', 'trim|required');
        $this->form_validation->set_rules('nama_user', 'nama user', 'trim|required');
        $this->form_validation->set_rules('email', 'email', 'trim|required|valid_email');
        $this->form_validation->set_rules('akses', 'akses', 'trim|required');

        $this->form_validation->set_rules('id', 'id', 'trim');
        $this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    function pdf()
    {
        $data = array(
            'user_data' => $this->User_model->get_all(),
            'start' => 0
        );
        
        ini_set('memory_limit', '32M');
        $html = $this->load->view('user/user_pdf', $data, true);
        $this->load->library('pdf');
        $pdf = $this->pdf->load();
        $pdf->WriteHTML($html);
        $pdf->Output('user.pdf', 'D');
    }

}

/* End of file user.php */
/* Location: ./application/controllers/user.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2017-12-01 01:19:21 */
/* Modification By Rusli */
/* http://harviacode.com */